#ifndef __K60N_OS_TASKS_H_
#define __K60N_OS_TASKS_H_
#include "FreeRTOS.h"
#include "timers.h"
 #include "header.h"
 #include "k60n_uart.h"
 #include "command_decode.h"
 #include "command_handler.h"
 #include "k60n_os_tasks.h"
 #include "task.h"
 #include "semphr.h"
 #include "queue.h"
 #include "BT43.h"
 #include "mdcr_func.h"
 #include "k60n_gpio.h"
 #include "k60n_spi.h"
#include "AT25DFspi_memory.h"
 #include "iic.h"
 #include "LTC2941_i2c.h"

/*-------------------Имена задач------------------------*/
#define vLED1TaskName									"LED1_GR"
#define vLED2TaskName									"LED2"
#define vLEDRedTaskName								"LED_Red"
#define vSerialPortRecvTaskName							"SerialPortRecv"
#define vSerialPortSendTaskName							"SerialPortSend"
#define vCommandHandlerTaskName							"CommandHandler"
#define vBTHandlerSendTaskName							"BTSend"
#define vBTHandlerRecvTaskName							"BTRecv"
#define vI2CHandlerTaskName								"I2C020Port"
#define vWriteToFlashTaskNAME							"WriteToFlash"

#define vMdcrTestTaskName								"MDCR_TEST"


/*-----------------Приоритеты задач------------------------*/
#define vLED1TaskPrio	  						(configMAX_PRIORITIES - 1)
#define vLED2TaskPrio	  						(configMAX_PRIORITIES - 1)
#define vLEDRedTaskPrio	 						(tskIDLE_PRIORITY + 1)
#define vBTRecvTaskPrio							(tskIDLE_PRIORITY + 6)
#define vBTSendTaskPrio							(tskIDLE_PRIORITY + 4)
#define vSerialPortRecvTaskPrio					(tskIDLE_PRIORITY + 3)
#define vCommandHandlerTaskPrio					(tskIDLE_PRIORITY + 7)
#define vSerialPortSendTaskPrio					(tskIDLE_PRIORITY + 5)
#define vI2C0TaskPrio							(tskIDLE_PRIORITY + 3)

#define vMdcrTestTaskPrio						(tskIDLE_PRIORITY + 4)
#define vWriteToFlashTaskPrio					192//(tskIDLE_PRIORITY + 5)

#define vMdcrTestTaskPrio_IRS 					configMAX_PRIORITIES - 1

/*Tasks Stack Size (integer type (1 = 4 bytes)) -------------*/
/* Объём стека из кучи (configTOTAL_HEAP_SIZE), необходимый для задач, можно оценить в stm32f217.htm */
#define vLED1TaskStackSize							75
#define vLED2TaskStackSize							75
#define vLEDRedTaskStackSize						75
#define vSerialPortRecvTaskStackSize				100
#define vSerialPortSendTaskStackSize				100
#define vCommandHandlerTaskStackSize				500
#define vBTRecvTaskStackSize						256
#define vBTSendTaskStackSize						1000
#define vMdcrTestTaskStackSize						5*1024//1024 было
#define vWriteToFlashTaskStackSize					2*1024//1024 было



#define SendBufferQ_Max_Size 100


// Стандартный размер очереди (количество элементов)
#define xQueueStandartSize								32
//	Стандартный размер очередии для тестов
#define xQueueMdcrTestSize								8

typedef struct _tsTimersDataElement
{
	unsigned short TIMER_VAR	: 1;				//varrible changed in programm
	unsigned short TIMER_1		: 1;
	unsigned short TIMER_2		: 1;
	unsigned short TIMER_60sec	: 1;
	unsigned short TIMER_4		: 1;
	unsigned short TIMER_5		: 1;
	unsigned short COUNTER		: 8;
	unsigned short dummy 			: 2;
}tsTimersDataElement;

// Meta Data Element
typedef union _uTimersDataElement
{
	tsTimersDataElement 	BIT;
	unsigned short 		ALL;
}uTimersDataElement;



void vQueueCreation(void);

void vLed1Task(void * pvParameters);
void vLed2Task(void * pvParameters);
void vLedRedTask(void * pvParameters);

void vSerialPortRecvTask(void * pvParameters);
void vSerialPortSendTask(void * pvParameters);
void vCommandHandlerTask(void * pvParameters);
void vBTSendHandlerTask(void * pvParameters);
void vBTReceiveHandlerTask(void * pvParameters);
void vAutoReloadTimerFunction(xTimerHandle xTimer);
void vOneShotTimersFunction(xTimerHandle xTimer);
void vI2CHandlerTask( void *pvParameters );
void vWriteToFlashTask(void * pvParameters);

void KeyScan(void);
	
void vMdcrTestTask(void* pvParameters);

void govnoSleep(unsigned int iGovnoDelay);
void unprotectAllFlashSectors(void);
void sendWriteEnableCommand(void);
void sendEraze_4KBBlockCommand(unsigned long long Address);
bool waitFlashCardErazeCompletedFlag(void);
void waitFlashCardNoBusyStatus(void);
bool eraze4kBlockInFlash(unsigned long long AddressForEraze);
bool writePageInFlash(unsigned long long FlashAddress, unsigned char *buffer, unsigned int Length);
bool writeBlockInFlash(unsigned long long FlashAddress, unsigned char *buffer, unsigned int Length);
bool flashPrepareToReWrite4KBlock(unsigned long long AddressForPrepare);
void readFlashData(unsigned long long FlashAddress, unsigned short* ReadedDataBlock, unsigned int Length);
void verifyRecordedBlock(unsigned long long FlashAddress, unsigned char* RecordedDataBlock, unsigned int Length);
#endif
