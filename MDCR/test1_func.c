/*************************************************************
 *	  File:		mdcr_func.c
 *	Author:		Kirillov A.V.
 *************************************************************/
#define __C_MDCR_
#include "mdcr_func.h"
#include "AT25DFspi_memory.h"
#include "header.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "k60n_gpio.h"
#include <stdint.h>

#if ENABLE_TestSignalADC > 0 
extern unsigned short TestCounter;
#endif
extern unsigned short FIRdataADC[4]; //buffer processed filtered data ADC
extern uMetaSwElement SwDataFlag;
extern uTimersDataElement TimersDataFlag;
extern tsMDCRCheckSignal DataValidSignal;
uint32_t timeOfAPP_LESS_TEST2;
uint32_t uintMax = 4294967295;
uint32_t tim = 0;
uint32_t ticks_ms = 0;
void Test1_setNextParamIter(tsTestParam* param)
{

#if ENABLE_TestSignalADC > 0 
    TestCounter = 0;
#endif
    switch (param->CurrentIteration)
    {
        case 0:
            param->CurrentIteration++;
            param->numChankSamples = param->TF[0] * mdcr_get_frequency();
            param->numCurReqSamples = param->numChankSamples; //инкремент чанков самплов
            param->curMetaDE.str.numTest = param->numTest & 0xf;
            param->curMetaDE.str.mode = 0;
            param->curMetaDE.str.Iterations = param->CurrentIteration;
            param->numMode = 1;
            break;
        case 1:
            param->CurrentIteration++;
            param->numChankSamples = param->TF[1] * mdcr_get_frequency();
            param->numCurReqSamples += param->numChankSamples; //инкремент чанков самплов
            param->curMetaDE.str.Iterations = param->CurrentIteration;
            param->curMetaDE.str.numTest = param->numTest & 0xf;
            param->numMode = 3;
            break;
        case 2:
            param->CurrentIteration++;
            param->numChankSamples = param->TF[2] * mdcr_get_frequency();
            param->numCurReqSamples += param->numChankSamples; //инкремент чанков самплов
            param->curMetaDE.str.numTest = param->numTest & 0xf;
            param->curMetaDE.str.Iterations = param->CurrentIteration - 2;
            param->curMetaDE.str.mode = 1; //будем передавать V1-V6 (pereodical test)
            param->numMode = 2; //start blinks Green
            break;
        default:
            param->CurrentIteration++;
            param->numChankSamples = param->TF[2] * mdcr_get_frequency();
            param->numCurReqSamples += param->numChankSamples; //инкремент чанков самплов V1-V6
            param->curMetaDE.str.Iterations = param->CurrentIteration - 2; //start blinks Green
            break;
    }

}

static int CheckBtnTest1InStart(tsTestParam* pTestData)
{
#if (DISABLE_SW_CONFIRMATION_Test1 == 0)
    startTimer2();
    while (TimersDataFlag.BIT.TIMER_2 == 0) // ждем таймер 20 сек
    {
        vTaskDelay(2);
        KeyScan();
        if (SwDataFlag.BIT.SW2_flag || SwDataFlag.BIT.SW2_GUI_flag)
        {
            setLEDState(YellowAndRed2Solid);
            SwDataFlag.BIT.SW2_GUI_flag = 0;
        }
        if (SwDataFlag.BIT.SW1_flag || SwDataFlag.BIT.SW1_GUI_flag)
        {
            setLEDState(YellowSolid);
            SwDataFlag.BIT.SW1_GUI_flag = 0;
            SwDataFlag.BIT.SW2_GUI_flag = 0;
            SendButtonPressedDevice(pTestData);
            return 1;
        }
    }
    return 0;
#else
    return 1;
#endif
}

void setLEDStateInTest1Measurements(tsTestParam* param)
{
    switch (param->CurrentIteration)
    {
        case 1:
            setLEDState(YellowBlink);
            break;
        case 2:
					if (param->appLessMode == 2)
						tim = ticks_ms;
						setLEDState(YellowSolid);
            break;
        default:
            setLEDState(GreenBlink);
            break;
    }
}

static int CheckBtnTest1AfterTF2(tsTestParam* param)
{
#if (DISABLE_SW_CONFIRMATION_Test1 == 0 && SKIP_SW_GREEN_CONFIRMATION_TEST1 == 0)
    startTimer2();
    while (TimersDataFlag.BIT.TIMER_2 == 0) // ждем таймер 20 сек
    {
        vTaskDelay(2);
        KeyScan();
        if (SwDataFlag.BIT.SW2_flag || SwDataFlag.BIT.SW2_GUI_flag)
        {
            SwDataFlag.BIT.SW1_GUI_flag = 0;
            SwDataFlag.BIT.SW2_GUI_flag = 0;
            SendButtonPressedDevice(param);
            return 1;
        }
    }
    return 0;
#else
    return 1;
#endif
}

// --------------работает в тесте 1 МЕЖДУ итерациями TF3-----------------------------------------

int Test1_TF3TestSignal(tsTestParam* param, unsigned short *channel)
{
#if (SKIP_INVALID_WAITING_IN_TEST1 == 0)
    unsigned int OneSecInvalidCounter = 0;
    bool DisableFilteringInMeasLocal = false;
    if (param->acqMode != STREAM)
        DisableFilteringInMeasLocal = true;
#endif
    if (param->CurrentIteration == 3)
    {
        setLEDState(YellowSolid);
        vTaskDelay(1000);
        setLEDState(AllLEDsOff);
        setLEDState(GreenSolid);
        SwDataFlag.ALL = 0;
        if (CheckBtnTest1AfterTF2(param) == 0)
            return 1;
    }
#if (SKIP_INVALID_WAITING_IN_TEST1 == 0)
    startTimer2();
    do
    {
        getMeasure(channel, FIRdataADC, param->numChannels, param->numFilter, DisableFilteringInMeasLocal);
        if (OneSecInvalidCounter >= mdcr_get_frequency() && DataValidSignal.flagMain == 0)
            return 0;
        if (DataValidSignal.flagMain == 1)
            OneSecInvalidCounter++;
        else
            OneSecInvalidCounter = 0;
    } while (TimersDataFlag.BIT.TIMER_2 == 0); // ждем, пока сигнал НЕверен "at least 1 second"->10страница спеки
    return 1;
#else
    vTaskDelay(1000);
    return 0;
#endif
}

void interruptActionsTest1(tsTestParam* param)
{
    if (param->CurrentIteration != 2)
        setLEDState(Red1AndRed2Solid);
    SendMessage_MeasInterrupt(param);
    param->CurrentSampleInIteration = 0;
    startTimer2();
    param->status = testCheckValidSignal;
}

void finishIterationActionsTest1(tsTestParam* param)
{
		if (param->appLessMode == 2 && tim != 0)
		{
				if (ticks_ms <= tim)
						tim = uintMax - tim + ticks_ms;
				else
						tim = ticks_ms - tim;
				timeOfAPP_LESS_TEST2 = tim/1000;
		}
    param->CurrentSampleInIteration = 0;
    SendMessage_MeasFinish(param);
    startTimer2();
    param->status = testWaitAnswer;
}

int Test1Func(tsTestParam* param, int recvCmd)
{
    unsigned short channel[4] = {0};
    static bool TestWasStarted = false;
    static bool DisableFilteringInMeas = false;
    int result = 1;
    int i = 0;
    static enum FlashAddressShift RecordingShift = ContinueRecording;
    static bool Add1SecMore = ADD1SEC;
    static bool waitAck = false;
    switch (param->status)
    {
        case testStart:
            SwDataFlag.BIT.SW1_GUI_flag = 0;
            SwDataFlag.BIT.SW2_GUI_flag = 0;
            Add1SecMore = ADD1SEC;
            DisableFilteringInMeas = false;
            TestWasStarted = false;
            setLEDState(AllLEDsOff);
            vTaskDelay(500);
            setLEDState(YellowSolid);
            if (CheckBtnTest1InStart(param) == 0)
            {
                param->status = testStop;
                break;
            }
            waitAck = false;
            param->CurrentIteration = 0;
            Test1_setNextParamIter(param);
            perform_the_analog_switch(param);
            param->CurrentSampleInIteration = 0;
            param->numCurReqSamples = 0;
            param->addrMeasurement = ADDRESS_MEASUREMENTS;
            param->TestMode = 0;
            ADC_Start(1000);
            initChecksValidSignal();
            InitDigFilter();
            for (i = 0; i < mdcr_get_frequency(); i++)
            {
                getMeasure(channel, FIRdataADC, param->numChannels, param->numFilter, DisableFilteringInMeas);
                GercelaPutBuf(channel, param->numChannels); // need 500 sampl for work
            }
            if (param->numFilter == 0x03)
                param->numFilter = GercelaAlgorithm(channel, param->numChannels); // calc FFT for 50/60Hz from buf GercelaPutBuf()
            else if (param->numFilter == 0x04)
                param->numFilter = AVGAlgorithm(channel, param->numChannels);
            InitDigFilter();
            vTaskDelay(10);
            //			for (i=0; i < mdcr_get_frequency(); i++)	// 500 выборок на инициализ детектора 150Гц
            //				getMeasure (channel,FIRdataADC,param->numChannels, param->numFilter, DisableFilteringInMeas);
            //			InitDigFilter();
            if (param->acqMode != STREAM)
            {
                DisableFilteringInMeas = true;
                param->RealQuantMeasInTF2 = 0xFFFFFFFF;
                flashWriteTaskInit();
                if (param->appLessMode == 0)
                    recordFlashHeader(param);
                param->mode = CHUNK_MODE;
                RecordingShift = ContinueRecording;
                param->subStatus = testFlashRecording;
            }else
            {
                param->mode = SINGLE_READ_MODE;
                param->subStatus = testStreamRunning;
            }
            startTimer60sec();
            param->status = testCheckValidSignal;
            break;
        case testFlashRecording:
            if (param->CurrentIteration == 2)
                Add1SecMore = DONTADD1SEC;
            else
                Add1SecMore = ADD1SEC;
            if (recordMeasurementsToFlashCard(RecordingShift, param->numChankSamples, param->numChannels, Add1SecMore, false) == COMPLETED)
            {
                if (param->CurrentIteration == 2)
                    param->RealQuantMeasInTF2 = getQuantityOfLastRecordedMeasurements();
                RecordingShift = ContinueRecording;
                if (param->acqMode == STREAM_AND_WRITE)
                    readLastIterationAndSendToGUI(param);
                finishIterationActionsTest1(param);
            }else
            {
                if (param->CurrentIteration != 2)
                {
                    interruptActionsTest1(param);
                    RecordingShift = ReWriteLastIteration;
                    prepareToRepeatLastIteration();
                    startTimer2();
                }else
                {
                    RecordingShift = ContinueRecording;
                    param->RealQuantMeasInTF2 = getQuantityOfLastRecordedMeasurements();
                    if (param->acqMode == STREAM_AND_WRITE)
                        readLastIterationAndSendToGUI(param);
                    finishIterationActionsTest1(param);
                }
            }
            break;
        case testStreamRunning:
            if (param->CurrentSampleInIteration == param->numChankSamples) //прошел чанк
            {
                finishIterationActionsTest1(param);
                break;
            }
            getMeasure(channel, FIRdataADC, param->numChannels, param->numFilter, DisableFilteringInMeas);
            param->validity = DataValidSignal.flagMain;
            SendResultsStreamMode(param, FIRdataADC);
            param->CurrentSampleInIteration++;
            if (DataValidSignal.flagMain)
            {
                if (param->CurrentIteration != 2)
                    interruptActionsTest1(param);
                else
                    finishIterationActionsTest1(param);
            }
            break;
        case testCheckValidSignal:
            getMeasure(channel, FIRdataADC, param->numChannels, param->numFilter, DisableFilteringInMeas);
            if (DataValidSignal.flagMain == 0)
            {
                startTimer2();
                setLEDStateInTest1Measurements(param);
                if (param->acqMode == STREAM)
                    for (i = 0; i < mdcr_get_frequency(); i++)
                        getMeasure(channel, FIRdataADC, param->numChannels, param->numFilter, DisableFilteringInMeas);
                if (TestWasStarted)
                {
                    SendMessage_MeasContinue(param);
                }else
                {
                    stopTimer60sec();
                    SendMessage_MeasStart(param);
                    TestWasStarted = true;
                }
                param->status = param->subStatus;
            }else if ((TimersDataFlag.BIT.TIMER_2 && TestWasStarted) || (TimersDataFlag.BIT.TIMER_60sec && TestWasStarted == false))
            {
                SendMessage_NoValidSignal(param);
                param->status = testStop;
            }
            break;
        case testRepeat:
            param->status = testStart;
            break;
        case testSendingAllTest:
        {
            if (!waitAck)
            {
                if (param->CurrentIteration <= param->Iterations)
                {
                    readLastIterationAndSendToGUI(param);
                    //					SendMessage_MeasFinish(param);
                    Test1_setNextParamIter(param);
                    setLEDStateInTest1Measurements(param);
                    startTimer2();
                }else
                {
                    setLEDState(YellowAndGreenSolid);
                    startTimer2();
                    waitAck = true;
                    SendMessage_MeasFinish(param);
                }
            }else
            {
                if (recvCmd == MEAS_OK || TimersDataFlag.BIT.TIMER_2 != 0)
                {
                    waitAck = false;
                    param->status = testStop;
                }
            }
            break;
        }
        case testWaitAnswer:
            if (TimersDataFlag.BIT.TIMER_2 == 0)
            {
                if (recvCmd == MEAS_OK || param->acqMode == WRITE_TO_FLASH || param->acqMode == WRITE_TO_FLASH_AND_COMMUNICATE)
                {
                    Test1_setNextParamIter(param);
                    if (param->CurrentIteration <= param->Iterations)
                    {
                        if (param->CurrentIteration > 2)
                        {
                            setLEDState(GreenSolid);
                            if (Test1_TF3TestSignal(param, channel) == 1)
                            {
                                SendMessage_NoValidSignal(param);
                                param->status = testStop;
                                break;
                            }
                            startTimer2();
                        }
                        SendMessage_MeasStart(param);
                        setLEDStateInTest1Measurements(param);
                        param->status = param->subStatus;
                    }else
                    {
                        if (param->acqMode == WRITE_TO_FLASH || param->acqMode == WRITE_TO_FLASH_AND_COMMUNICATE)
                        {
                            if (param->appLessMode == 0)
                            {
                                param->CurrentIteration = 0;
                                Test1_setNextParamIter(param);
                                setLEDStateInTest1Measurements(param);
                                param->status = testSendingAllTest;
                            }else
                                param->status = testStop;
                        }else
                        {
                            param->status = testStop;
                        }
                    }
                }
            }else
                param->status = testStop;
            break;
        case testStop:
            SwDataFlag.BIT.SW1_GUI_flag = 0;
            SwDataFlag.BIT.SW2_GUI_flag = 0;
            if (param->acqMode != STREAM)
                recordFlashHeader(param);
            ADC_Stop();
            stopTimer2();
            stopTimer60sec();
            SendMessage_GoingIntoHib(param);
            setLEDState(YellowAndGreenSolid);
            result = 0;
            break;
        default:
            break;
    }
    return result;
}
