/*******************************************************************************
 *	File: k60n_spi.c
 *******************************************************************************/
#define __C_K60N_SPI_

#include "header.h"
#include "k60n_spi.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
//#include "misc.h"

#define CTAS_8BIT           1
#define CTAS_16BIT          0

#define SPI_SR_EOQF_BIT                         (1 << SPI_SR_EOQF_SHIFT)
#ifdef mdcr_interface

#define SPI_SR_TFUF_BIT                         (1 << SPI_SR_TFUF_SHIFT)
#define SPI_MCR_HALT_BIT                        (1 << SPI_MCR_HALT_SHIFT)
#define SPI_SR_RFDF_BIT                         (1 << SPI_SR_RFDF_SHIFT)
#define SPI_SR_TCF_BIT                          (1 << SPI_SR_TCF_SHIFT)
#define SPI_RSER_TCF_RE_BIT                     (1 << SPI_RSER_TCF_RE_SHIFT)
#define SPI_MCR_CLR_RXF_BIT                     (1 << SPI_MCR_CLR_TXF_SHIFT)
#define SPI_MCR_CLR_TXF_BIT                     (1 << SPI_MCR_CLR_RXF_SHIFT)
#define SPI_SR_TXRXS_BIT                        (1 << SPI_SR_TXRXS_SHIFT)

#define SPI_TRANSFER_CURR_FRAME_COMPLETE_FLAG_RESET()   SPI0->SR |= SPI_SR_TCF_BIT

#define SPI_ENABLE_TCF_INTERRUPT()              SPI0->RSER |= SPI_RSER_TCF_RE_BIT
#define SPI_ENABLE_RFDF_INTERRUPT()             SPI0->RSER |= SPI_RSER_RFDF_RE_MASK
#define SPI_ENABLE_TFUF_INTERRUPT()             SPI0->RSER |= SPI_RSER_TFUF_RE_MASK
#define SPI_DISABLE_TCF_INTERRUPT()             SPI0->RSER &= ~SPI_RSER_TCF_RE_BIT

#define SPI_HALT_RESET()                        SPI0->MCR &= ~SPI_MCR_HALT_BIT
#define SPI_HALT_SET()                          SPI0->MCR |= SPI_MCR_HALT_BIT

#define SPI_CLR_RX_FIFO()                       SPI0->MCR |= SPI_MCR_CLR_RXF_BIT
#define SPI_CLR_TX_FIFO()                       SPI0->MCR |= SPI_MCR_CLR_TXF_BIT

#define SPI_CLR_RFDF()                          SPI0->SR |= SPI_SR_RFDF_BIT
#define SPI_CLR_EOQF()                          SPI0->SR |= SPI_SR_EOQF_BIT
#define SPI_CLR_TFUF()                          SPI0->SR |= SPI_SR_TFUF_BIT
#define SPI_CLR_TCF()                           SPI0->SR |= SPI_SR_TCF_BIT

#define SPI_PUSH_FRAME()                        SPI0->PUSHR = *((uint32_t *) &spi_tx_buff[increment_tx_out()])

#define SPI_IS_TXFIFO_NOT_FULL()                SPI0->SR & SPI_SR_TFFF_MASK
#define SPI_IS_TRANSFER_IN_PROGRESS()           (!SPI0->SR & SPI_SR_TXRXS_MASK)
#define SPI_IS_DATA_RECEIVED()                  SPI0->SR & SPI_SR_RFDF_MASK

#define SPI_CLEAR_RX_FIFO_NOT_EMPTY_FLAG()      SPI0->SR|= SPI_SR_RFDF_BIT
#define SPI_TXCTR_VAL                           ((SPI0->SR & SPI_SR_TXCTR_MASK) >> SPI_SR_TXCTR_SHIFT)
#define SPI_TX_FIFO_SIZE                        4
#define SPI_TX_FIFO_IS_NOT_FULL()               ((SPI0->SR & SPI_SR_TXCTR_MASK) <  (SPI_TX_FIFO_SIZE << SPI_SR_TXCTR_SHIFT))
#define SPI_RX_FIFO_IS_NOT_EMPTY()              (SPI0->SR & SPI_SR_RXCTR_MASK)
#define SPI_TX_FIFO_IS_NOT_EMPTY()              (SPI0->SR & SPI_SR_TXCTR_MASK)

#define SPI_WAIT_TRANSFER()                     while ((SPI0->SR & SPI_SR_TXRXS_MASK)){}
#define SPI_WAIT_EOQ()                          while (!(SPI0->SR & SPI_SR_EOQF_MASK)){}
#define SPI_WAIT_BUFF                           while (((SPI0->SR >> SPI_SR_TXCTR_SHIFT) & 0xF) > 3) {}
#define SPI_WAIT_TX_FIFO                        while(!(SPI0->SR  & SPI_SR_TFFF_MASK))
#endif

#define SEMA_TX_TAKE()                          \
                    while(xSemaphoreTake( m_tx_buff, portMAX_DELAY ) != pdTRUE) \
                        vTaskDelay(PTT_DELAY_MS(1));                            \
                    {

#define SEMA_TX_GIVE()                                  \
                        xSemaphoreGive( m_tx_buff );    \
                     }

#define SEMA_RX_TAKE()                          \
                    while(xSemaphoreTake( m_rx_buff, portMAX_DELAY ) != pdTRUE) \
                        vTaskDelay(PTT_DELAY_MS(1));                            \
                    {

#define SEMA_RX_GIVE()                                  \
                        xSemaphoreGive( m_rx_buff );    \
                     }

typedef struct {
    uint16_t txdata;
    union {
        uint16_t ctrl_field;
        struct {
            uint16_t pcs : 6;
            uint16_t empty : 4;
            uint16_t ctcnt : 1;
            uint16_t eoq : 1;
            uint16_t ctas : 3;
            uint16_t cont : 1;
        } ctrl;        
    }un;
} pushrc_reg_t;


uint16_t spi_rx_buff[RXBUFF_SIZE];
uint16_t spi_rx_need = 0;
uint16_t spi_rx_in = 0;


pushrc_reg_t spi_tx_buff[TXBUFF_SIZE];
uint32_t spi_tx_need = 0;
uint32_t spi_tx_out = 0;
//static unsigned int gSPI_BeforeTransfDelay, gSPI_AfterTransfDelay, gSPI_InterTransfDelay;
static xSemaphoreHandle m_tx_buff;
static xSemaphoreHandle m_rx_buff;


void SPI0_DeInit(void)
{
#ifdef mdcr_interface
    PORTA->PCR[14] &= ~PORT_PCR_MUX_MASK;
    PORTA->PCR[14] = PORT_PCR_MUX(0); //|PORT_PCR_DSE_MASK;	// PCS0 -> GPIO
    PORTA->PCR[15] &= ~PORT_PCR_MUX_MASK;
    PORTA->PCR[15] = PORT_PCR_MUX(0); //|PORT_PCR_DSE_MASK;	// SCK-> GPIO
    PORTA->PCR[16] &= ~PORT_PCR_MUX_MASK;
    PORTA->PCR[16] = PORT_PCR_MUX(0); //|PORT_PCR_DSE_MASK;	// SOUT-> GPIO
    PORTA->PCR[17] &= ~PORT_PCR_MUX_MASK;
    PORTA->PCR[17] = PORT_PCR_MUX(0); //|PORT_PCR_DSE_MASK;	// SIN	-> GPIO

    PTA->PDOR |= (1 << 14);
    PTA->PDOR |= (1 << 15);
    PTA->PDOR |= (1 << 16);
    PTA->PDOR |= (1 << 17);

    PTA->PDDR |= (1 << 14);
    PTA->PDDR |= (1 << 15);
    PTA->PDDR |= (1 << 16);
    PTA->PDDR |= (1 << 17);

    PTA->PSOR |= (1 << 14);
    PTA->PSOR |= (1 << 15);
    PTA->PSOR |= (1 << 16);
    PTA->PSOR |= (1 << 17);
#endif
}

void SPI0_Init(void)
{
#ifdef mdcr_interface
    NVIC_InitTypeDef NVIC_InitStructure;

    SIM->SCGC6 |= SIM_SCGC6_DSPI0_MASK; //enable DSPI0 clock gate control
    SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK;
    PORTA->PCR[14] &= ~PORT_PCR_MUX_MASK;
    PORTA->PCR[14] = PORT_PCR_MUX(2); //|PORT_PCR_DSE_MASK;	// PCS0
    PORTA->PCR[15] &= ~PORT_PCR_MUX_MASK;
    PORTA->PCR[15] = PORT_PCR_MUX(2); //|PORT_PCR_DSE_MASK;	// SCK
    PORTA->PCR[16] &= ~PORT_PCR_MUX_MASK;
    PORTA->PCR[16] = PORT_PCR_MUX(2); //|PORT_PCR_DSE_MASK;	// SOUT
    PORTA->PCR[17] &= ~PORT_PCR_MUX_MASK;
    PORTA->PCR[17] = PORT_PCR_MUX(2); //|PORT_PCR_DSE_MASK;	// SIN
    SPI0->MCR = (1 << SPI_MCR_MSTR_SHIFT) |         // DSPI is in master mode
                (0 << SPI_MCR_CONT_SCKE_SHIFT) |    // Continuous SCK disabled
                (SPI_MCR_DCONF(0)) |                // DSPI Configuration = SPI
                (0 << SPI_MCR_FRZ_SHIFT) |          // Do not halt serial transfers in debug mode.
                (0 << SPI_MCR_MTFE_SHIFT) |         // Modified SPI transfer format disabled.
                (1 << SPI_MCR_PCSSE_SHIFT) |        // PCS[5]/PCSS is used as an active-low PCS Strobe signal.
                (0 << SPI_MCR_ROOE_SHIFT) |         // Incoming Overflow data is ignored
                (SPI_MCR_PCSIS(1)) |                // The inactive state of PCSx is high.
                (0 << SPI_MCR_DOZE_SHIFT) |         // Doze mode has no effect on DSPI
                (0 << SPI_MCR_MDIS_SHIFT) |         // Enable DSPI clocks
                (0 << SPI_MCR_DIS_TXF_SHIFT) |      // Tx FIFO is enabled.
                (0 << SPI_MCR_DIS_RXF_SHIFT) |      // Rx FIFO is enabled.
                (1 << SPI_MCR_CLR_TXF_SHIFT) |      // Clear the Tx FIFO counter.
                (1 << SPI_MCR_CLR_RXF_SHIFT) |      // Clear the Rx FIFO counter.
                (SPI_MCR_SMPL_PT(1));               // 1 system clock between SCK edge and SIN sample
    SPI0->TCR = SPI_TCR_SPI_TCNT(0x00);         // SPI Transfer Counter
    SPI0->CTAR[0] = SPI_CTAR_FMSZ(15) |     //  Frame Size = 16bit
            SPI_CTAR_PCSSCK(0x02) |         //PCS to SCK Delay Prescaler
            SPI_CTAR_PASC(0x01) |           //0x02					//After SCK Delay Prescaler
            SPI_CTAR_PDT(0x01) |            //0x02						//Delay after Transfer
            SPI_CTAR_PBR(0x00) |
            SPI_CTAR_CSSCK(0x00) |          //0x02!
            SPI_CTAR_ASC(0x02) |            //0x02
            SPI_CTAR_DT(0x05) |
            SPI_CTAR_BR(0x05);              //0x05		//Baud Rate Scaler - SCK baud rate = (fSYS/PBR) x [(1+DBR)/BR]
    SPI0->CTAR[1] = SPI_CTAR_FMSZ(0x07) |   // Frame Size = 8bit
            SPI_CTAR_PCSSCK(0x02) |         //PCS to SCK Delay Prescaler 0x2 = *5
            SPI_CTAR_PASC(0x01) |           //After SCK Delay Prescaler
            SPI_CTAR_PDT(0x01) |            //0x02			//Delay after Transfer			0x2=	*5
            SPI_CTAR_PBR(0x00) |
            SPI_CTAR_CSSCK(0x00) |
            SPI_CTAR_ASC(0x02) |
            SPI_CTAR_DT(0x0D) |             //Delay After Transfer Scaler // 3ms = PDT*DT*(1/150MHz) Р С—Р В°РЎС“Р В·Р В° Р С�Р ВµР В¶Р Т‘РЎС“ Р С—Р С•РЎРѓРЎвЂ№Р В»Р С”Р В°Р С�Р С‘
            SPI_CTAR_BR(0x05);              //Baud Rate Scaler - SCK baud rate = (fSYS/PBR) x [(1+DBR)/BR]
    SPI0->SR = (1 << SPI_SR_TCF_SHIFT) |
            (1 << SPI_SR_TXRXS_SHIFT) |
            (1 << SPI_SR_EOQF_SHIFT) |
            (1 << SPI_SR_TFUF_SHIFT) |
            (1 << SPI_SR_TFFF_SHIFT) |
            (1 << SPI_SR_RFOF_SHIFT) |
            (1 << SPI_SR_RFDF_SHIFT);
    SPI0->RSER = 0;
    SPI_ENABLE_RFDF_INTERRUPT();
    SPI_ENABLE_TFUF_INTERRUPT();
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4); // For FreeRTOS using only this Priority Group
    NVIC_InitStructure.NVIC_IRQChannel = SPI0_IRQn;

    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0; // A lower priority value indicates a higher priority
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = (uint8_t) (15 - 1);
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    
    while(SPI_RX_FIFO_IS_NOT_EMPTY())
    {
        volatile uint32_t tmp;


        tmp = SPI0->POPR;

    }
    SPI_HALT_SET();
    
    m_tx_buff = xSemaphoreCreateMutex();
    m_rx_buff = xSemaphoreCreateMutex();
#endif    
}


static void rx_buff_copy(uint8_t * buff, uint16_t len)
{
    int i = 0;
    for (i = 0; i < len; i++)
        buff[i] = (uint8_t)spi_rx_buff[(spi_rx_in - (len - i)) & SPI_RXBUFF_MASK];
}

static void rx_buff_copy_16bit(uint16_t * buff, uint16_t len)
{
    int i = 0;
    for (i = 0; i < len; i++)
        buff[i] = spi_rx_buff[(spi_rx_in - (len - i)) & SPI_RXBUFF_MASK];
}

static void reset_rx_count(void)
{
    spi_rx_in = 0;
}

static uint32_t increment_tx_out(void)
{
    uint32_t old = spi_tx_out;
    spi_tx_out++;
    spi_tx_out%= TXBUFF_SIZE;
    return old;
}

static void increment_tx_need(void)
{
    spi_tx_need++;
    spi_tx_need%= TXBUFF_SIZE;
}

void SPI_add_tx_data_8bit(uint8_t data)
{
    spi_tx_buff[spi_tx_need].un.ctrl_field = 0;
    spi_tx_buff[spi_tx_need].un.ctrl.ctas = CTAS_8BIT;
    spi_tx_buff[spi_tx_need].un.ctrl.cont = 1;
    spi_tx_buff[spi_tx_need].un.ctrl.pcs = 1;
    spi_tx_buff[spi_tx_need].txdata = data;
    increment_tx_need();
}

void SPI_add_tx_data_8bit_eoq(unsigned short data)
{
    spi_tx_buff[spi_tx_need].un.ctrl_field = 0;
    spi_tx_buff[spi_tx_need].un.ctrl.ctas = CTAS_8BIT;
    spi_tx_buff[spi_tx_need].un.ctrl.eoq = 1;
    spi_tx_buff[spi_tx_need].un.ctrl.pcs = 1;
    spi_tx_buff[spi_tx_need].txdata = data;
    increment_tx_need();
}

void SPI_add_tx_data_16bit(uint16_t data)
{
    spi_tx_buff[spi_tx_need].un.ctrl_field = 0;
    spi_tx_buff[spi_tx_need].un.ctrl.ctas = CTAS_16BIT;
    spi_tx_buff[spi_tx_need].un.ctrl.cont = 1;
    spi_tx_buff[spi_tx_need].un.ctrl.pcs = 1;
    spi_tx_buff[spi_tx_need].txdata = data;
    increment_tx_need();
}

void SPI_add_tx_data_16bit_eoq(unsigned short data)
{
    spi_tx_buff[spi_tx_need].un.ctrl_field = 0;
    spi_tx_buff[spi_tx_need].un.ctrl.ctas = CTAS_16BIT;
    spi_tx_buff[spi_tx_need].un.ctrl.eoq = 1;
    spi_tx_buff[spi_tx_need].un.ctrl.pcs = 1;
    spi_tx_buff[spi_tx_need].txdata = data;
    increment_tx_need();
}

static void SPI_Start_Transfer(void)
{
    taskENTER_CRITICAL(); 
    {
    #ifdef mdcr_interface
        if (SPI_TX_FIFO_IS_NOT_FULL() && (spi_tx_out != spi_tx_need))
        {
            SPI_CLR_TFUF();
            SPI_CLR_RFDF();
            SPI_PUSH_FRAME();
            SPI_HALT_RESET();
        }
      #endif
    }
    taskEXIT_CRITICAL(); 
}

void SPI_Read_Data_16bit(uint16_t *buff, int size)
{
    SEMA_TX_TAKE();
    SEMA_RX_TAKE();
    reset_rx_count();
    spi_rx_need = size;
    SPI_Send_Push_Data_16bit(buff, size - 1);
    SPI_add_tx_data_16bit_eoq(buff[size - 1]);
    SPI_Start_Transfer();
    SPI_WAIT_TRANSFER();
    rx_buff_copy_16bit(buff, size);
    SEMA_RX_GIVE();
    SEMA_TX_GIVE();
}

void SPI_Read_Data_8bit(uint8_t *buff, int size)
{
    SEMA_TX_TAKE();
    SEMA_RX_TAKE();
    reset_rx_count();
    spi_rx_need = size;
    SPI_Send_Push_Data_8bit(buff, size - 1);
    SPI_add_tx_data_8bit_eoq(buff[size - 1]);
    SPI_Start_Transfer();
    SPI_WAIT_TRANSFER();
    rx_buff_copy(buff, size);
    SEMA_RX_GIVE();
    SEMA_TX_GIVE();
}

void SPI_Send_Push_Data_8bit(const uint8_t *buff, int size)
{
    unsigned int i = 0;
    for (; i < size; i++)
    {
        SPI_add_tx_data_8bit(buff[i]);
    }
}

void SPI_Send_Finalize_Data_8bit(const uint8_t *buff, int size)
{
    SPI_Send_Push_Data_8bit(buff, size - 1);
    SPI_add_tx_data_8bit_eoq(buff[size - 1]);
    SPI_Start_Transfer();
}

void SPI_Send_Push_Data_16bit(const uint16_t *buff, int size)
{
    unsigned int i = 0;
    for (; i < size; i++)
    {
        SPI_add_tx_data_16bit(buff[i]);
    }
}

void SPI_Send_Finalize_Data_16bit(const uint16_t *buff, int size)
{
    SPI_Send_Push_Data_16bit(buff, size - 1);
    SPI_add_tx_data_16bit_eoq(buff[size - 1]);
    SPI_Start_Transfer();
}

void SPI_Send_Data_8bit(const uint8_t *buff, int size)
{
    SPI_Send_Push_Data_8bit(buff, size - 1);
    SPI_Send_Finalize_Data_8bit(&buff[size-1], 1);
}

void SPI_Send_Data_16bit(const uint16_t *buff, int size)
{
    SPI_Send_Push_Data_16bit(buff, size - 1);
    SPI_Send_Finalize_Data_16bit(&buff[size-1], 1);
}

void SPI_Transfer_Data_8bit(const uint8_t *buff, int size)
{
    SEMA_TX_TAKE();
    SPI_Send_Push_Data_8bit(buff, size - 1);
    SPI_Send_Finalize_Data_8bit(&buff[size-1], 1);
    #ifdef mdcr_interface
    SPI_WAIT_TRANSFER();
    #endif
    SEMA_TX_GIVE();
}

void SPI_Transfer_Data_16bit(const uint16_t *buff, int size)
{
    SEMA_TX_TAKE();
    SPI_Send_Push_Data_16bit(buff, size - 1);
    SPI_Send_Finalize_Data_16bit(&buff[size-1], 1);
    SPI_WAIT_TRANSFER();
    SEMA_TX_GIVE();
}

uint32_t SPI_Request_8bit(const uint8_t *buff_out, int out_length, uint8_t *buff_in, int in_length)
{
    int i = 0;
    if (buff_in == NULL || !in_length)
    {
        SEMA_TX_TAKE();
        SPI_Send_Finalize_Data_8bit(buff_out, out_length);
        SEMA_TX_GIVE();
        #ifdef mdcr_interface
        SPI_WAIT_TRANSFER();
        #endif
        return 0;
    }
    SEMA_TX_TAKE();
    SEMA_RX_TAKE();
    reset_rx_count();
    SPI_Send_Push_Data_8bit(buff_out, out_length);
    for (i = 0; i < in_length - 1; i++)
    {
        SPI_add_tx_data_8bit(0);
    }
    SPI_add_tx_data_8bit_eoq(0);
    SPI_Start_Transfer();
    #ifdef mdcr_interface
    SPI_WAIT_TRANSFER();
    #endif
    rx_buff_copy(buff_in, in_length);
    SEMA_RX_GIVE();
    SEMA_TX_GIVE();
    return i;
}

uint32_t SPI_Request_16bit(const uint8_t *buff_out, int out_length, uint16_t *buff_in, int in_length)
{
    int i = 0;
    if (buff_in == NULL || !in_length)
    {
        SEMA_TX_TAKE();
        SPI_Send_Finalize_Data_8bit(buff_out, out_length);
        SEMA_TX_GIVE();
        #ifdef mdcr_interface
        SPI_WAIT_TRANSFER();
        #endif
        return 0;
    }
    SEMA_TX_TAKE();
    SEMA_RX_TAKE();
    reset_rx_count();
    SPI_Send_Push_Data_8bit(buff_out, out_length);
    for (i = 0; i < in_length - 1; i++)
    {
        SPI_add_tx_data_16bit(0);
    }
    SPI_add_tx_data_16bit_eoq(0);
    SPI_Start_Transfer();
    #ifdef mdcr_interface
    SPI_WAIT_TRANSFER();
    #endif
    rx_buff_copy_16bit(buff_in, in_length);
    SEMA_RX_GIVE();
    SEMA_TX_GIVE();
    return i;
}

void SPI0_IRQHandler(void)
{
    static int i = 0;
    SPI_CLR_RFDF();
    SPI_CLR_TFUF();
    while(SPI_RX_FIFO_IS_NOT_EMPTY())
    {
#ifdef mdcr_interface
        spi_rx_buff[spi_rx_in] = (uint16_t)SPI0->POPR;
#endif
        spi_rx_in = (++spi_rx_in & SPI_RXBUFF_MASK);
    }
    if ((spi_tx_out == spi_tx_need) && !SPI_TX_FIFO_IS_NOT_EMPTY())
        SPI_HALT_SET();
    while (SPI_TX_FIFO_IS_NOT_FULL() && (spi_tx_out != spi_tx_need))
    {
        if (spi_tx_buff[spi_tx_out].un.ctrl.eoq)
        {
            i++;
        }
        SPI_PUSH_FRAME();
    }
}
