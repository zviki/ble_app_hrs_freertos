/*************************************************************
 *	  File:		mdcr_func.c
 *	Author:		Kirillov A.V.
 *************************************************************/
#define __C_MDCR_

#include "mdcr_func.h"
#include "AT25DFspi_memory.h"
#include "header.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "k60n_gpio.h"
#include "k60n_adc.h"

#if ENABLE_TestSignalADC > 0 
extern unsigned short TestCounter;
#endif
extern unsigned short FIRdataADC[4];	//buffer processed filtered data ADC
extern uMetaSwElement SwDataFlag;
extern uTimersDataElement TimersDataFlag;
extern tsMDCRCheckSignal DataValidSignal;

static int CheckBtnTest3(tsTestParam* pTestData)
{
	#if (DISABLE_SW_CONFIRMATION_Test3 == 0)
		startTimer2();
		while (TimersDataFlag.BIT.TIMER_2 == 0)
		{	
			vTaskDelay(2);
			KeyScan();
			if (SwDataFlag.BIT.SW1_flag || SwDataFlag.BIT.SW1_GUI_flag)
			{
				SwDataFlag.BIT.SW1_GUI_flag = 0;
				SwDataFlag.BIT.SW2_GUI_flag = 0;
				SendButtonPressedDevice(pTestData);
				return 1;
			}
		}
		return 0;
	#else
		return 1;
	#endif
}

void setProgressToZeroInTest3(tsTestParam* param)
{
	param->WouldNoValid = 0;
	if (param->acqMode == STREAM)
		param->CurrentSampleInIteration = 0;
	param->Iterations = 1;
	param->CurrentIteration = 1;
}

void interruptActionsTest3(tsTestParam* param)
{
	param->WouldNoValid = true;
	setLEDState(Red1Solid);
	SendMessage_MeasInterrupt(param);
	startTimer2();
	param->status = testCheckValidSignal;
	setProgressToZeroInTest3(param);
}

int Test3Func(tsTestParam* param, int recvCmd)
{
	unsigned short channel[4];
	int i = 0;
	int result = 1;
	static bool TestWasStarted = false;
	static enum FlashAddressShift RecordingShift = ContinueRecording;
	static bool DisableFilteringInMeas = false;
	static unsigned long NeededQuantityOfMeas = 0;
	switch (param->status)
	{
		case testStart:
			SwDataFlag.BIT.SW1_GUI_flag = 0;
			SwDataFlag.BIT.SW2_GUI_flag = 0;			
			#if ENABLE_TestSignalADC > 0 
				TestCounter = 0;
			#endif
			DisableFilteringInMeas = false;
			TestWasStarted = false;
			setLEDState(AllLEDsOff);
			vTaskDelay(500);
			setLEDState(YellowSolid);
			if (CheckBtnTest3(param) == 0)
			{
				SendMessage_NoValidSignal(param);
				param->status=testStop; 
				break;
			}
			if (CheckAnalogSNS(param) == 0)
			{
				SendMessage_ExtPlugMisconnected(param);
				param->status=testStop; 
				break;
			}
			param->MeasNumOfLastValid = 0;
			param->WouldNoValid = false;
			param->CurrentIteration = 1;
			perform_the_analog_switch(param);
			param->subStatus = testStart;
			setProgressToZeroInTest3(param);
			param->addrMeasurement = ADDRESS_MEASUREMENTS;
			param->numChankSamples = param->numReqSamples;
			param->TestMode = 0;
			param->numChannels = 3;
			param->numMode = 0;
			initChecksValidSignal();
			InitDigFilter();
			param->curMetaDE.str.numTest = param->numTest&0x7;
			param->curMetaDE.str.mode = 0;
			param->curMetaDE.str.Iterations = param->Iterations;
			ADC_Start(1000);
			for (i=0; i < mdcr_get_frequency(); i++)								// выкинуть 500 выборок на инициализ фильтра
			{
				getMeasure (channel,FIRdataADC,param->numChannels, param->numFilter, DisableFilteringInMeas);
				GercelaPutBuf(channel,param->numChannels);			// need 500 sampl for work
			}
			if (param->numFilter == 0x03)	
				param->numFilter = GercelaAlgorithm(channel,param->numChannels);	// calc FFT for 50/60Hz from buf GercelaPutBuf()
			else if (param->numFilter == 0x04)	
				param->numFilter = AVGAlgorithm(channel,param->numChannels);
			InitDigFilter();
			for (i=0; i < mdcr_get_frequency(); i++)	// 500 выборок на инициализ детектора 150Гц
				getMeasure (channel,FIRdataADC,param->numChannels, param->numFilter, DisableFilteringInMeas);
			InitDigFilter();
			if (param->acqMode != STREAM)
			{
				DisableFilteringInMeas = true;
				flashWriteTaskInit();
				recordFlashHeader(param);
				param->mode = CHUNK_MODE;
				RecordingShift = ContinueRecording;
				param->subStatus = testFlashRecording;
			}else
			{
				vTaskDelay(10);
				param->mode = SINGLE_READ_MODE;
				param->subStatus = testStreamRunning;
			}
			startTimer60sec();
			param->status = testCheckValidSignal;
			break;
		case testFlashRecording:
			NeededQuantityOfMeas = param->numReqSamples - param->MeasNumOfLastValid;
			if (recordMeasurementsToFlashCard(RecordingShift, NeededQuantityOfMeas, param->numChannels, ADD1SEC, false) == COMPLETED)
			{
				param->MeasNumOfLastValid = param->numReqSamples;
				readLastIterationAndSendToGUI(param);
				SendMessage_MeasFinish(param);
				startTimer2();
				param->status = testWaitAnswer;
			}else
			{
				param->MeasNumOfLastValid+= getQuantityOfLastRecordedMeasurements();
				interruptActionsTest3(param);
				startTimer2();
			}
			break;
		case testStreamRunning:
			getMeasure (channel,FIRdataADC,param->numChannels, param->numFilter, DisableFilteringInMeas);
			param->validity = DataValidSignal.flagMain;
			SendResultsStreamMode(param, FIRdataADC);
			param->CurrentSampleInIteration++;
			if (DataValidSignal.flagMain)
			{
				interruptActionsTest3(param);
			}else if(param->CurrentSampleInIteration == param->numReqSamples)
			{
				setLEDState(YellowAndGreenSolid);
				param->CurrentIteration++;
				SendMessage_MeasFinish(param);
				startTimer2();
				param->status = testWaitAnswer;
			}
			break;
		case testDataInterrupt:
			break;
		case testCheckValidSignal:
			getMeasure (channel,FIRdataADC,param->numChannels, param->numFilter, DisableFilteringInMeas);
			if (DataValidSignal.flagMain == 0)		// Сигнал вернулся
			{
				startTimer2();
				setLEDState(YellowBlink);
				if (param->acqMode == STREAM)
					for (i = 0; i < mdcr_get_frequency(); i++)
						getMeasure (channel,FIRdataADC,param->numChannels, param->numFilter, DisableFilteringInMeas);
				if (TestWasStarted == true)
					SendMessage_MeasContinue(param);
				else
					stopTimer60sec();
				TestWasStarted = true;
				SendMessage_MeasStart(param);
				param->status = param->subStatus;
			}else if ((TimersDataFlag.BIT.TIMER_2 && TestWasStarted) || (TimersDataFlag.BIT.TIMER_60sec && TestWasStarted == false))
			{
				SendMessage_NoValidSignal(param);
				param->status = testStop;
			}
			break;
		case testDummyRead:
			break;
		case testStop:
			SwDataFlag.BIT.SW1_GUI_flag = 0;
			SwDataFlag.BIT.SW2_GUI_flag = 0;
			ADC_Stop();
			stopTimer2();
			stopTimer60sec();
			setLEDState(YellowAndGreenSolid);
			SendMessage_GoingIntoHib(param);
			result = 0;
			break;
		case testRepeat:
			param->status=testStart;
			break;
		case testWaitAnswer:
			if (TimersDataFlag.BIT.TIMER_2 || recvCmd == MEAS_OK || param->acqMode == WRITE_TO_FLASH || param->acqMode == WRITE_TO_FLASH_AND_COMMUNICATE)	
				param->status=testStop;
			else if (recvCmd == MEAS_REPEAT)
				param->status = testRepeat;
		break;
	}
	return result;
}
