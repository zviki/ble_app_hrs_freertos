/****************************************************************
 *	File:	mdcr_func.h
 ****************************************************************/
#ifndef __H_MDCR_
#define __H_MDCR_

#include "header.h"
#include "mdcr_protocol.h"

#include "k60n_uart.h"
#include "k60n_os_tasks.h"
#include "k60n_adc.h"

#define ADDRESS_DEVICE_CONFIGURATION 0x00000000
#define ADDRESS_MEASUREMENT_INFO 0x00001000
#define ADDRESS_MEASUREMENTS 0x00002000

#define Filter50Hz FILTER_50HZ
#define Filter60Hz FILTER_60HZ
#define FilterAutoGercel 3
#define FilterAutoAVG		 4

#define MDCR_FLASH_HEADER	256				// SIZE BYTE


#define ANALOG_CTRL1			(1<<12)
#define ANALOG_CTRL2			(1<<11)
#define ANALOG_CTRL3			(1<<10)
#ifdef MDCR_INTERFACE
#define ANALOG_CTRL_PORT	PTE

#define	TGL_ANALOG_CTRL1 	ANALOG_CTRL_PORT->PTOR ^= ANALOG_CTRL1;	
#define	TGL_ANALOG_CTRL2 	ANALOG_CTRL_PORT->PTOR ^= ANALOG_CTRL2;	
#define	TGL_ANALOG_CTRL3 	ANALOG_CTRL_PORT->PTOR ^= ANALOG_CTRL3;	

#define	SET_ANALOG_CTRL1 	ANALOG_CTRL_PORT->PSOR |= ANALOG_CTRL1;	
#define	SET_ANALOG_CTRL2 	ANALOG_CTRL_PORT->PSOR |= ANALOG_CTRL2;	
#define	SET_ANALOG_CTRL3 	ANALOG_CTRL_PORT->PSOR |= ANALOG_CTRL3;	

#define	CLR_ANALOG_CTRL1 	ANALOG_CTRL_PORT->PCOR |= ANALOG_CTRL1;	
#define	CLR_ANALOG_CTRL2 	ANALOG_CTRL_PORT->PCOR |= ANALOG_CTRL2;	
#define	CLR_ANALOG_CTRL3 	ANALOG_CTRL_PORT->PCOR |= ANALOG_CTRL3;
#else
#define	TGL_ANALOG_CTRL1 		
#define	TGL_ANALOG_CTRL2 		
#define	TGL_ANALOG_CTRL3 		

#define	SET_ANALOG_CTRL1 		
#define	SET_ANALOG_CTRL2 		
#define	SET_ANALOG_CTRL3 		

#define	CLR_ANALOG_CTRL1 		
#define	CLR_ANALOG_CTRL2 		
#define	CLR_ANALOG_CTRL3 	
#endif


// The Address of Frame. Frame it's one iteration or TF.
typedef struct
{
	short metaData;					//	meta data
	unsigned long long samples;			//	The number of samples in frame.
	unsigned long long address;			//	The Address of the block. The Address must be a multiple of 4096 bytes.
}tsAddressFrame;
/*
// Test Info Block
typedef struct
{
	unsigned short  signature;		//	Signature of infoblock. Should be 0xa5a5	
	unsigned short  numTest;		//	Number of the test.
	unsigned short  numIterations;	//	Number of the Iterations
	unsigned char   Date[22];		//	Date
	unsigned char   UserName[40];	//	Information about user
	unsigned short  TF[5];			//	Array of time intervals
	unsigned long   numMeasures;	//	Number of samples for all Iterations.
	tsAddressFrame  Frames[8];		//	
	unsigned short	numUseFrame;	// 
	int numChannels;				//	Number of channels
}tsTestInfoBlock;
*/
// The struct for work with meta data element
typedef struct _tsMetaDataElement
{
	unsigned short numTest 		: 2;
	unsigned short mode 		: 1;
	unsigned short Iterations 	: 3;
	unsigned short dummy 		: 10;
}tsMetaDataElement;

// Meta Data Element
typedef union _uMetaDataElement
{
	tsMetaDataElement 	str;
	unsigned short 		byte;
}uMetaDataElement;

#define SPIBUF_len 				4096

enum StatesOfLEDsInTest3{
	NormalWaiting = 0,		// LED1 SOLID YELLOW		LED2 OFF
	NormalMeasuring,		// LED1 BLINK YELLOW		LED2 OFF
	Abnormal,				// LED1 SOLID RED			LED2 OFF
	OkMeasWaiting,			// LED1 SOLID YELLOW		LED2 SOLID GREEN
	LEDsOff
};
enum StatesOfLEDsInTests{
	YellowSolid = 1,
	YellowBlink,
	GreenSolid,
	GreenBlink,
	YellowAndGreenSolid,
	YellowAndGreenBlink,
	YellowAndRed2Solid,
	Red1AndRed2Solid,
	Red1Solid,
	AllLEDsOff,
	NotDefined
};
enum StatesOfTest{
	LeadUp = 1,
	NormalWaitingAtStart,
	NotSpecifiedButtonPressed,
	ExternalPlugNotConnected,
	ValidSignalWaiting,
	TFsMeasuring,
	AfterMeasurementsBeforeOK,
	AfterMeasurementsAfterOK,
	InvalidSignalDuringTFs,
};

enum CurrentTF{
	MeasTF1 = 1,
	MeasTF2,
	MeasTF3,
	MeasTF4,
	MeasTF5
};

enum{
	testDummy=0,					//	0 -
	testStart=1,					//	1 -
	testInit,						//	2 -
	testAcknowladge,				//	3 -
	testStreamRunning,				//	4 -
	testStop,						//	5 -
	testWaitAnswer,					//	6 -
	testOk,							//	7 -
	testRepeat,						//	8 -
	testDataInterrupt,				//	9 -
	testDummyRead,					//	10 - Dummy reading in during 0.5 sec
	testCheckValidSignal,			//	11 -
	testSignalBetweenIteration,		//	12 -
	testWaitPlugInsert,				//	13 -
	testDelayTest,					//	14 -
	testFinish,						//	15 -
	testFlashRecording,				//	16 -
	testSendingAllTest				//	17 -
};

enum{
	testWriteToFlash=1,					//	1 -
	testReadFlash,						//	2 -
	testStream,
	testNumberChank,
};


//-------------- gercel algorithm---------------------------
#define	FFTBufSize		250											//
typedef struct
{
	double temp[FFTBufSize+4];
	unsigned short pbuff[4];
	double wr,wi,alp;	
	unsigned short frequency;									// from FFT algorithm
	unsigned short frequency_avg;							//from AVG algorithm
}tsFFTDataSignal;

typedef struct
{
	unsigned short INbuff[4][FFTBufSize+4];						//for 4 ADC
	unsigned int AVG[4];
	unsigned int SUM[4];		
	double SR[4];																				//real for FFT	(SR + j*SI)
////	double SI_50[4]; 																			// irreal for FFT (SR + j*SI)	
	double SR_max;
}tsFFTDataBuf;
//------------------------------------------------------------


#define BandPasFILTER_TAP_NUM 500
typedef struct {
  int history[BandPasFILTER_TAP_NUM];
  unsigned int last_index;
} BandPasFilter;


//------------ for check N2 --------------------------------
#define	CheckSignalBufSize				128											// 128
#define	CheckSignalMAX					56000		//55000 Pochti ok					// 60000
#define	CheckSignalMIN					18000		//20000 Pochti ok					// 10000

//------------ for check N3 --------------------------------
#define	CheckSignal3BufSize				128											// 128
#define	CheckSignal3dif					39000		//40000 Pochti ok				// 20000
#define	CheckSignal3delta				20			//20 Pochti ok					// 10

typedef struct
{
	unsigned short buff[4][CheckSignalBufSize+4];						//for 4 ADC
	unsigned short counter[4];															// if 		10000	<data> 60000	counter++
	unsigned short pbuff[4];
	unsigned short flag[4];																		// if 		10000	<data> 60000	flag =1
	unsigned short flagMain;																		// if 		10000	<data> 60000	flag =1
}tsMDCRCheckSignal;


//----------------- check N3 ---------------------------------
#define	CheckSignal2BufSize			128											// 128
#define	CheckSignal2Count				64											//	64
typedef struct
{
	unsigned short buff[4][CheckSignal2BufSize+4];						//for 4 ADC
	unsigned short counter[4];															// if 		10000	<data> 60000	counter++
	unsigned short pbuff[4];
	unsigned short flag[4];																		// if 		10000	<data> 60000	flag =1
	unsigned short flagMain;																		// if 		10000	<data> 60000	flag =1
}tsMDCRCheckSignal2;

typedef struct
{
	unsigned short buff[4][CheckSignal3BufSize+4];						//for 4 ADC
	unsigned short pbuff[4];
	unsigned short flag[4];																		// if AVG(data)	of 200sampl  > 20000	flag =1
	unsigned short flagMain;																		
	unsigned short counter[4];																		// to wait 200 sampl if signal return 
	unsigned int SUM[4];	 																		// sum of buf [4]
	unsigned int AVG[4];																		//Average of 	200 sampl
}tsMDCRCheckSignal3;


typedef struct {
	unsigned short ADCxBuff;		// 2bytes
//	portTickType TickTime;			// 4bytes
}ADCQueueStruct;					

#define LENGTH_DATATOWRITE_INTO_FLASH	8
#define MAX_LENGTH_DATATOWRITE_INTO_FLASH	16
#define LENGTH_OF_FLASH_BLOCK		256UL
#define CHECKPOINT_LEFT_OFFSET_TO_START		(127)
typedef struct {
	bool EndOfDataFlag;
	bool CheckPointFlag;
	unsigned char CheckPointLeftOffset;
	char DataToWrite[MAX_LENGTH_DATATOWRITE_INTO_FLASH];		// 8bytes
	unsigned int Length;
}WriteToFlashStruct;					// 8bytes

//----------------------------------------------------------------------------
typedef struct _tsTestParam
{
	unsigned char numTest;				//	Number of test
	unsigned char numMode;				//	Number of mode
	unsigned char Iterations;			//	Number of Iterations
	unsigned char CurrentIteration;		//	The index current Iterations
	unsigned char sizeBreach;			// 
	unsigned char validity;				// 
	short mode;							// 
	short recvCmd;						// 
	short beginTest;					//
	unsigned short status;				// 
	unsigned short subStatus;			//
	unsigned short DummyReadTime;		// 
	unsigned short ExternalPlugConnect;	// 
	int CurrentTF;			// 
	int CurrentStateOfTest;	// 
	unsigned short WouldNoValid;		// bylo preryvanie validnosti
	unsigned short TestMode;			// 
	unsigned long  numCompressedData;	// size of compressed data	
	unsigned long  CurrentSampleInIteration;			// current samples
	unsigned long  numReqSamples;		// all samples
	unsigned long  numCurReqSamples;	// 	for calc end chank`s
	unsigned long  numChankSamples;		// number sampl in 1 chank
	unsigned short numChannels;			// number of channels
	unsigned long  addrMeasurement;		// ??
	unsigned long  RealQuantMeasInTF2;	// 
	unsigned long  MeasNumOfLastValid;	// nomer poslednego validnogo izmereniya
	unsigned short TF[5];				//
	unsigned char  Date[22];			// It was 22
	unsigned char  UserName[40]; 		// It was 40
	unsigned short curFrame;			// 
	uMetaDataElement curMetaDE;			// 
	tsAddressFrame Frames[8];			// 
	unsigned int numFilter;			//
	short acqMode;	
	short appLessMode;
	short testWasDownloaded;
}tsTestParam;

#pragma anon_unions

typedef struct
{
	short source;
	short type;
	short numFilter;	
	short numTest;
	short cmdID;
	short acqMode;
	short iteration;
	short TF1;
	short TF2;
	short TF3;
	short TF4;
	short TF5;
    union {
        char Date[20];
        char data[20];
    };
	char UserName[40];
	
}tsMDCRCommandType;

typedef struct
{
	unsigned short channel1 : 1;
	unsigned short channel2 : 1;
	unsigned short channel3 : 1;
	unsigned short channel4 : 1;
	unsigned short reserved : 12;
}tsADCMask;

typedef union _uADCMask
{
	tsADCMask mask;
	unsigned short body;
}uADCMask;

typedef struct SendBuffer_t
{
	char data[100];
	unsigned int data_size;
} SendBuffer;


void recordFlashHeader(tsTestParam *param);

void setLEDState(enum StatesOfLEDsInTests LEDState);
void mdcr_decode_recv_command(serial_port com_port);
void vMdcrTestTask(void* pvParameters);
void perform_the_analog_switch(tsTestParam* param);
//-------------------------------------------------------------------------------
int fWriteResults(char* cmdBuff, tsTestParam* pDataStruct, unsigned short* pData);
int fWriteResultsHiLo(char* cmdBuff, tsTestParam* pDataStruct, unsigned short* pData);
int fWriteRawData(char* cmdBuff, tsTestParam* pDataStruct, unsigned short* pData);
int fReadResults(unsigned char* cmdBuff, tsTestParam* pDataStruct);
int fSendCmdMeasStart(char* buff, tsTestParam* pTestData);
int fSendFinishCommand(char* buff, tsTestParam* pTestData);
int GoingIntoHibernation(char* buff);
int MeasInterrupt(char* buff);
int MeasContinue(char* buff);
int NoValidSignal(char* buff);
int ExtPlugMisconnect(char* buff);
int fStartFlashHeader(unsigned char* cmdBuff, tsTestParam* pTestData);
int fSendGetTestInformation(char* buff, tsTestParam* pTestData);
void InitCheckValidSignal(void);
void InitCheckValidSignal2(void);
void InitCheckValidSignal3(void);
int CheckValidSignal2(unsigned short* pFIRBuffer, int numChannels);
int CheckValidSignal3(unsigned short* pFIRBuffer, int numChannels);
int CheckValidSignal(unsigned short* pFIRBuffer, int numChannels);
void InitDigFilter(void);
int CheckAnalogSNS(tsTestParam* param);
void SetFlashHeader(tsTestParam* param);
//int Write_toFlash(tsTestParam* param, 	char*  buff, int lenght, char BUF_ENABLE );
int readLastIterationAndSendToGUI(tsTestParam* param);
int getMeasure (unsigned short* pBuffer, unsigned short* pFIRBuffer, int numChannels, unsigned int FIRHz, bool DisableFiltering);
int AVGAlgorithm (unsigned short* pBuffer, int numChannels);
void  GercelaPutBuf(unsigned short* pBuffer, int numChannels);
int GercelaAlgorithm (unsigned short* pBuffer, int numChannels);

int sendDataUSART_Async(char * data, int data_len);

void Test1_setNextParamIter(tsTestParam* param);
void setLEDStateInTest1Measurements(tsTestParam* param);

int Test1Func(tsTestParam* param, int recvCmd);
int Test2Func(tsTestParam* param, int recvCmd);
int Test3Func(tsTestParam* param, int recvCmd);
void startADC(void);
void stopADC(void);

void SendMessage_ExtPlugMisconnected(tsTestParam* param);
void SendMessage_MeasStart(tsTestParam* param);
void SendResultsStreamMode(tsTestParam* param, unsigned short* FIRdataADC);
void SendMessage_MeasInterrupt(tsTestParam* param);
void SendMessage_MeasContinue(tsTestParam* param);
void SendMessage_MeasFinish(tsTestParam* param);
void SendMessage_NoValidSignal(tsTestParam* param);
void SendMessage_GoingIntoHib(tsTestParam* param);
//SendBuffer * getSendBufferQ();
int writeRawDataToFlash(tsTestParam* param, unsigned short* FIRdataADCBuff, char WriteFlag);
void clearWriteToFlashElement(WriteToFlashStruct *WriteStruct);
void startTimerLED30SecAfterTests(void);
void stopTimerLED30SecAfterTests(void);
void startTimer2(void);
void stopTimer2(void);
void startTimer60sec(void);
void stopTimer60sec(void);
void recordInFlashFilter (unsigned short* ChannelsBuff, int NumOfChannels, unsigned int FIRHz);
void initChecksValidSignal(void);

int GercelDetect150Hz(float inSample);

void SendButtonPressedDevice(tsTestParam* pTestData);
#endif	//	__H_MDCR_
