/***********************************************************
 *	File: main.c
 *	Description:
 ***********************************************************/
 #define __C_MAIN_
///**********************************************************
#if 1
#include <stdint.h>
 #include "header.h"
 #include "k60n_gpio.h"
 #include "k60n_uart.h"
 #include "k60n_os_tasks.h"
 #include "k60n_adc.h"
 #include "k60n_spi.h"
 #include "FreeRTOS.h"
 #include "task.h"
 #include "semphr.h"
 #include "queue.h"
 #include "timers.h"
 #include "iic.h"
 //#include "smc.h"
 #ifdef mdcr_interface
 #include <cstddef>
 #endif
 #include "mdcr_func.h"

 #include "AT25DFspi_memory.h"
 #include "nrf_drv_saadc.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_timer.h"

 #endif

 ////-----------------------------------------------
 #define IO_NMI_DEF 1  // 1 - sets the Port interrupt, 0 - sets the NMI interrupt
	
unsigned portBASE_TYPE uxMaxCountUART0Semaph = SendBufferQ_Max_Size;
unsigned portBASE_TYPE uxInitialCountUARTSemaph = 0;

const char date_of_compile[30] = "Compile Date Dev = " __DATE__;  //size = 30		//May  8 2015//May 15 2015//
const char time_of_compile[] = "Compile Time Dev = " __TIME__;  //size = 27		//11:57:56//
const char SN_device[9] = {1,1,3,4,5,6,7,8,9};								// size = 11	//  S/N:123-456-789
const char Bee_APP_Software[2]={3,1};												//0x0205
const char Bee_Device_version[2]={1,5};

//	code const char *months[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
///**********************************************************
const uint32_t led_mask[] = {1UL << 26, 1UL << 27, 1UL << 28, 1UL << 29};
const uint32_t analog_sw[] = { 1UL<<12, 1UL<<11, 1UL<<10};


//	xSemaphoreHandle CountSemaphoreUART0_Handle;
//	xSemaphoreHandle SemaphoreUART0_Handle;


extern	xTaskHandle xHandleSerialPortRecv;
extern	xTaskHandle xHandleSerialPortSend;
extern	xTaskHandle xHandleBTRecv;
extern	xTaskHandle xHandleBTSend;
extern	xTaskHandle xHandleDeviceCommand;
extern	xTaskHandle xHandleMdcrTest;
extern	xTaskHandle xHandleLed1Gr;
extern	xTaskHandle xHandleLed2Yw;
extern	xTaskHandle xHandleLedRed;
extern	xTaskHandle xHandleI2C;	
extern	xTaskHandle xHandleWriteToFlashTask;
extern xQueueHandle Queue_BuffToWriteIntoFlash;
//	extern  xSemaphoreHandle CountSemaphoreUART0_Handle;
 //------------------------------------------------------------
extern xTimerHandle	xAutoReloadTimer;
extern xTimerHandle	xOneShotTimers[NUMBER_OF_TIMERS];
extern unsigned int uiAutoReloadTimerPeriod;
extern const  unsigned portBASE_TYPE uxOneShotTimersIDs [NUMBER_OF_TIMERS];
extern unsigned int uiOneShotTimersDelay[NUMBER_OF_TIMERS];

extern uMetaSwElement SwDataFlag;
extern uTimersDataElement TimersDataFlag;

static void LED_Config(void);

/***********************************************************
 * Description	:
 * Return		:
 ***********************************************************/
static void LED_Config(void)
{
#ifdef MDCR_INTERFACE
	SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK;		// Enable Clock to Port A
	PORTA->PCR[26] = (1UL << 8);			// Pin is GPIO
	PORTA->PCR[27] = (1UL << 8);			// Pin is GPIO
	PORTA->PCR[28] = (1UL << 8);			// Pin is GPIO
	PORTA->PCR[29] = (1UL << 8);			// Pin is GPIO
	PTA->PDOR = (led_mask[0] | led_mask[1] | led_mask[2] | led_mask[3] );
	PTA->PDDR = (led_mask[0] | led_mask[1] | led_mask[2] | led_mask[3] );
	PTA->PCOR = (led_mask[0] | led_mask[1] | led_mask[2] | led_mask[3] );

	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;		// Enable Clock to Port B
	PORTB->PCR[22] = (1UL << 8);			// Pin is GPIO	Blue
	PORTB->PCR[23] = (1UL << 8);			// Pin is GPIO	Red
	PORTB->PCR[22] |= PORT_PCR_DSE_MASK;			// High drive strength is configured
	PORTB->PCR[23] |= PORT_PCR_DSE_MASK;;			// High drive strength is configured

	PTB->PDOR |= LED3_BLUE;							// Blue
	PTB->PDOR |= LED3_RED;							// Red

	PTB->PDDR |= LED3_BLUE;							// Blue
	PTB->PDDR |= LED3_RED;							// Red

	PTB->PSOR |= LED3_BLUE;
	PTB->PSOR |= LED3_RED;
#endif

}

/***********************************************************
 *	Description	:
 *	Return		:
 ***********************************************************/
void AnalogSwitchInit(void)
{
#ifdef MDCR_INTERFACE
	SIM->SCGC5 |= SIM_SCGC5_PORTE_MASK;		// Enable Clock to Port A
	PORTE->PCR[9] = (1UL << 8);			// Pin is GPIO
	PORTE->PCR[10] = (1UL << 8);			// Pin is GPIO
	PORTE->PCR[11] = (1UL << 8);			// Pin is GPIO
	PORTE->PCR[12] = (1UL << 8);			// Pin is GPIO

	PORTE->PCR[ANALOG_SNS] &=~ PORT_PCR_PFE_MASK;					// Internal pull-down resistor is enabled on	=0
	PORTE->PCR[ANALOG_SNS] |= PORT_PCR_PE_MASK;					// Internal pull-up or pull-down resistor is enabled
	PORTE->PCR[ANALOG_SNS] |= PORT_PCR_PFE_MASK;					// Passive Filter Enable for ANALOG input
																	// Now pull down enabled as default
//	PORTE->PCR[ANALOG_SNS] |= PORT_PCR_PS_MASK;					// PullUpEnable
//	PORTE->PCR[ANALOG_SNS] &= ~PORT_PCR_PS_MASK;				// PullDownEnable
	PORTE->PCR[10] |= PORT_PCR_PFE_MASK;				// Passive Filter Enable for ANALOG input
	PORTE->PCR[11] |= PORT_PCR_PFE_MASK;				// Passive Filter Enable for ANALOG input
	PORTE->PCR[12] |= PORT_PCR_PFE_MASK;				// Passive Filter Enable for ANALOG input

	PTE->PDOR = (analog_sw[0] | analog_sw[1] | analog_sw[2] );				//	Logic level 1 is driven on pin
	PTE->PDDR = (analog_sw[0] | analog_sw[1] | analog_sw[2] );
	PTE->PCOR = (analog_sw[0] | analog_sw[1] | analog_sw[2] );				// 	set 0

	PTE->PDOR = (1UL<<ANALOG_SNS);
	PTE->PDDR &=~ (1UL<<ANALOG_SNS);																//	Intput set 0
	PTE->PCOR =  (1UL<<ANALOG_SNS);
#endif

}

//------------------------------------------------------------------
void ChargerInit(void)
{
#ifdef MDCR_INTERFACE
	SIM->SCGC5 |= SIM_SCGC5_PORTC_MASK;		// Enable Clock to Port A
	PORTC->PCR[6] = (1UL << 8);						// Pin is GPIO	//suspend
	PORTC->PCR[7] = (1UL << 8);						// Pin is GPIO	//HPWR

	PTC->PDOR |= (1<<6);									//SUSPEND
	PTC->PDOR |= (1<<7);									//HPWR

	PTC->PDDR |= (1<<6);									//SUSPEND
	PTC->PDDR |= (1<<7);									//HPWR

	PTC->PCOR |= (1<<6);									//SUSPEND
	PTC->PSOR |= (1<<7);									//HPWR

	SIM->SCGC5 |= SIM_SCGC5_PORTD_MASK;			// Enable Clock to Port
	PORTD->PCR[15] = (1UL << 8);						// Pin is GPIO	//HPWR
	PORTD->PCR[15] |= PORT_PCR_PFE_MASK;		// Passive Filter Enable for ANALOG input
	PORTD->PCR[15] &=~ PORT_PCR_PFE_MASK;					// Internal pull-down resistor is enabled on	=0
	PORTD->PCR[15] |= PORT_PCR_PE_MASK;					// Internal pull-up or pull-down resistor is enabled

	PTD->PDOR |= (1<<15);									//Charged
	PTD->PDDR &=~ (1<<15);
	PTD->PCOR |= (1<<15);
#endif
}

//-----------------------------------------------------------------
static void Button_Config(void)
{
#ifdef MDCR_INTERFACE
	SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK;		// Enable Clock to Port A
	PORTA->PCR[SW2] |= (1UL << 8);			// Pin is GPIO
	PORTA->PCR[SW1] |= (1UL << 8);			// Pin is GPIO


	PORTA->PCR[SW2] |= PORT_PCR_PFE_MASK;				// Passive Filter Enable for ANALOG input
	PORTA->PCR[SW1] |= PORT_PCR_PFE_MASK;;			// A low pass filter (10 MHz to 30 MHz bandwidth)

	PORTA->PCR[SW2] |= PORT_PCR_DSE_MASK;			// High drive strength is configured
	PORTA->PCR[SW1] |= PORT_PCR_DSE_MASK;;			// High drive strength is configured
#endif
}

/***********************************************************
 *	Description	:
 *	Return		:
 ***********************************************************/
static void BT_InitLowLevel(void)
{
#ifdef MDCR_INTERFACE
//	long i=0;

	SIM->SCGC5 |= SIM_SCGC5_PORTD_MASK;        // Enable Clock to Port D
/*
#if BTModule == BTM411_chip	
	PORTD->PCR[8] = (1UL << 8);
	PORTD->PCR[9] = (1UL << 8);
	PORTD->PCR[10] = (1UL << 8);
	PORTD->PCR[11] = (1UL << 8);
	
	PTD->PDDR |= (1<<8);						//	Output
	PTD->PDDR |= (1<<9);						//	Output
	PTD->PDDR |= (1<<10);						//	Output
	PTD->PDDR |= (1<<11);						//	Output

	PTD->PDOR |= (1<<8);
	PTD->PDOR |= (1<<9);
	PTD->PDOR |= (1<<10);
	PTD->PDOR |= (1<<11);

	PTD->PCOR |= (1<<8);
	PTD->PCOR |= (1<<9);
	PTD->PCOR |= (1<<10);
	PTD->PCOR |= (1<<11);

	PORTD->PCR[14] = (1UL << 8);
	PTD->PDDR |= (1<<14);						//	Output
	PTD->PDOR |= (1<<14);
	
	PORTD->PCR[13] = (1UL << 8);
	PTD->PDDR |= (1<<13);						//	Output
	PTD->PDOR |= (1<<13);
	PTD->PCOR |= (1<<13);						// reset BT
	
#else
*/
	PORTD->PCR[8] = (1UL << 8);
	PORTD->PCR[9] = (1UL << 8);
	
	PORTD->PCR[8] |= PORT_PCR_PE_MASK;					// Internal pull-down resistor is enabled on	=1
	PORTD->PCR[8] &=~ PORT_PCR_PS_MASK;					// Internal pull-down resistor is enabled	
	PORTD->PCR[9] |= PORT_PCR_PE_MASK;					// Internal pull-down resistor is enabled on	=1
	PORTD->PCR[9] &=~ PORT_PCR_PS_MASK;					// Internal pull-down resistor is enabled

	PTD->PDDR &=~  (1<<8);						//	Input
	PTD->PDDR &=~  (1<<9);						//	Input

	PTD->PDOR |= (1<<8);
	PTD->PDOR |= (1<<9);

	PTD->PCOR |= (1<<8);
	PTD->PCOR |= (1<<9);


	PORTD->PCR[14] = (1UL << 8);
	PTD->PDDR |= (1<<14);						//	Output
	PTD->PDOR |= (1<<14);

//#endif


	PTD->PCOR |= (1<<14);						// power to BT

//	for(i=0;i<1000000; i++)		{	}
	
#if BTModule == BTM411_chip	
	PTD->PSOR |=(1<<13);
#endif

#endif
	
}
/***********************************************************
 * Description:
 ***********************************************************/
static void DebugInitLowLevel(void)
{
#ifdef MDCR_INTERFACE
	SIM->SCGC5 |= SIM_SCGC5_PORTE_MASK;	// Enable Clock to Port E

	PORTE->PCR[4] = (1UL << 8);			//	Init to GPIO
	PORTE->PCR[5] = (1UL << 8);			//	Init to GPIO

	PTE->PDDR |= (1<<4);				//	Set as "Output"
	PTE->PDDR |= (1<<5);				//	Set as "Output"

	PTE->PDOR |= (1<<4);				//
	PTE->PDOR |= (1<<5);				//

	PTE->PCOR |= (1<<4);				//	Set "0" level
	PTE->PSOR |= (1<<5);				//	Set "1" level
#endif
}


//-----------------------------------------------------------------------------------------
void clockMonitor(unsigned char state)
{
#ifdef MDCR_INTERFACE
    if(state)
      MCG->C6 |= MCG_C6_CME0_MASK;
    else
      MCG->C6 &= ~MCG_C6_CME0_MASK;
#endif
}

//-----------------------------------------------------------------------------------------
void GoToWait(void)
{
//	LED_Clear();

//	AT25D_GoDeepSleep();
	SPI0_DeInit();

#ifdef MDCR_INTERFACE
	PTE->PSOR = (analog_sw[0] | analog_sw[1] | analog_sw[2] );				// 	set 0
	PORTE->PCR[ANALOG_SNS] &=~ PORT_PCR_PE_MASK;					// Internal pull-up or pull-down resistor is disabled
	PTE->PSOR =  (1UL<<ANALOG_SNS);
#endif

//	clockMonitor(OFF);
	enter_wait();

}

void ExitWait(void)
{
//	clockMonitor(ON);

#ifdef MDCR_INTERFACE

	PTE->PCOR = (analog_sw[0] | analog_sw[1] | analog_sw[2] );				// 	set 0
	PORTE->PCR[ANALOG_SNS] |= PORT_PCR_PE_MASK;					// Internal pull-up or pull-down resistor is disabled
	PTE->PCOR =  (1UL<<ANALOG_SNS);
#endif

	SPI0_Init();
//	AT25D_ExitDeepSleep();

	LED_Clear();
	LED1_YW_SET;
	LED2_GR_SET;

}


//-----------------------------------------------------------------------------------------
void GoToHiberBAT (void)
{
#ifdef MDCR_INTERFACE
	PTD->PTOR |= (1<<14);																				// power to BT
	AT25D_GoDeepSleep();
	SPI0_DeInit();


	PTE->PSOR = (analog_sw[0] | analog_sw[1] | analog_sw[2] );	// 	set 0
	PORTE->PCR[ANALOG_SNS] &=~ PORT_PCR_PE_MASK;								// Internal pull-up or pull-down resistor is disabled
	PTE->PSOR =  (1UL<<ANALOG_SNS);


	PTD->PTOR ^= (1<<8);																				// BT module
	PTD->PTOR ^= (1<<9);
	PTD->PTOR ^= (1<<10);
	PTD->PTOR ^= (1<<11);

	UARTx_DeInit(UART0);
	UARTx_DeInit(UART1);																				//com port
	PTE->PSOR |= (1<<4);				//	Set "0" level - OFF com port
	PTE->PCOR |= (1<<5);				//	Set "0" level - OFF DD7 (com port)
	
	LED3_BL_CLR;
	LED3_RED_CLR;
	LED_Clear();
	Dinit_portX();

	clockMonitor(OFF);
	enter_wait();
#endif
}
//-----------------------------------------------------------------------------------------
void GoToHiberBAT43BT (void)
{
#ifdef MDCR_INTERFACE
	PTD->PTOR |= (1<<14);																				// power to BT
	AT25D_GoDeepSleep();
	SPI0_DeInit();


	PTE->PSOR = (analog_sw[0] | analog_sw[1] | analog_sw[2] );	// 	set 0
	PORTE->PCR[ANALOG_SNS] &=~ PORT_PCR_PE_MASK;								// Internal pull-up or pull-down resistor is disabled
	PTE->PSOR =  (1UL<<ANALOG_SNS);


	UARTx_DeInit(UART0);
	UARTx_DeInit(UART1);																				//com port

	LED3_BL_CLR;
	LED3_RED_CLR;
	LED_Clear();
	Dinit_portX();

	clockMonitor(OFF);
//	enter_wait();
	enter_vlls0(0);
#endif                

}

//***********************************************************
int main_mdcr(void)
{
	size_t free_heap_size0;
//	size_t free_heap_size1;
//	size_t free_heap_sizeDiff;
	size_t free_heap_size;
	int i = 0;
	SystemCoreClockUpdate();	// Get Core Clock Frequency
#ifdef MDCR_INTERFACE
	SIM->SCGC7 |= SIM_SCGC7_FLEXBUS_MASK; // FlexBus controller clock gate control
#endif
	Uart0Protocol = BT43protocol;	//	Setup Protocol
	Uart1Protocol = MDCRprotocol;	//

	ChargerInit();
	LED_Config();	//	Configuration of LEDs
	LED1_RED_SET;
	LED2_RED_SET;
#ifdef MDCR_INTERFACE	
	UARTx_Init(UART1, SystemCoreClock/1000, 115200);
#endif
//	UART_DEBUG("UART1 init success...\r\n");
	
	AnalogSwitchInit();
	Button_Config();
	BT_InitLowLevel();
	DebugInitLowLevel();
	SPI0_Init();
	Init_I2C();

//	free_heap_size = xPortGetFreeHeapSize();
	// UART_DEBUG("Connect FreeRTOS, heap size = %d.\r\n", free_heap_size);
	
#if (CONFIG_STRESS_TEST == 0)
	vQueueCreation();
#endif

	free_heap_size0 = xPortGetFreeHeapSize();
#ifdef MDCR_INTERFACE
	
	UARTx_Init(UART0, SystemCoreClock/1000, 115200);  //115200
	// UART_DEBUG("UART0 init success...\r\n");

	ADCConfigure(ADC0); 	// UART_DEBUG("ADC0 init success...\r\n");
	ADCConfigure(ADC1);	// UART_DEBUG("ADC1 init success...\r\n");
	ADCConfigure(ADC2);	// UART_DEBUG("ADC2 init success...\r\n");
	ADCConfigure(ADC3);	// UART_DEBUG("ADC3 init success...\r\n");
#else
        saadc_sampling_event_init();
        saadc_init();
        saadc_sampling_event_enable();
#endif
	
	#if ENABLE_FreeRunADC > 0
		PDBConfigure();
	#endif

#if (CONFIG_STRESS_TEST == 0)

	free_heap_size0 = xPortGetFreeHeapSize();


	// Create TASKs:
	 if (pdPASS !=xTaskCreate(vSerialPortRecvTask, vSerialPortRecvTaskName, vSerialPortRecvTaskStackSize,NULL, vSerialPortRecvTaskPrio, &xHandleSerialPortRecv))
	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vSerialPortRecvTaskName);
		while(1){}
	}

	free_heap_size0 = xPortGetFreeHeapSize();

	// UART_DEBUG("Init \"%s\" task...\r\n", vSerialPortRecvTaskName);
	if (xTaskCreate(vSerialPortSendTask, vSerialPortSendTaskName, vSerialPortSendTaskStackSize,
					NULL, vSerialPortSendTaskPrio, &xHandleSerialPortSend) == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vSerialPortSendTaskName);
		while(1){}
	}
	// UART_DEBUG("Init \"%s\" task...\r\n", vSerialPortSendTaskName);
      


	free_heap_size0 = xPortGetFreeHeapSize();
	
	if (xTaskCreate(vCommandHandlerTask, vCommandHandlerTaskName, vCommandHandlerTaskStackSize,
					NULL, vCommandHandlerTaskPrio, &xHandleDeviceCommand) == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vCommandHandlerTaskName);
		while(1){}
	}
	// UART_DEBUG("Init \"%s\" task...\r\n", vCommandHandlerTaskName);

	free_heap_size0 = xPortGetFreeHeapSize();
	
	if (xTaskCreate(vBTSendHandlerTask, vBTHandlerSendTaskName, vBTSendTaskStackSize,
					NULL, vBTSendTaskPrio, &xHandleBTSend) == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vBTHandlerSendTaskName);
		while(1){}
	}

	free_heap_size0 = xPortGetFreeHeapSize();
	// UART_DEBUG("Init \"%s\" task...\r\n", vBTHandlerSendTaskName);
	if (xTaskCreate(vBTReceiveHandlerTask, vBTHandlerRecvTaskName, vBTRecvTaskStackSize,
					NULL, vBTRecvTaskPrio, &xHandleBTRecv) == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vBTHandlerRecvTaskName);
		while(1){}
	}
	// UART_DEBUG("Init \"%s\" task...\r\n", vBTHandlerRecvTaskName);

	free_heap_size0 = xPortGetFreeHeapSize();

	// SetUp test`s
	if (xTaskCreate(vMdcrTestTask, vMdcrTestTaskName, vMdcrTestTaskStackSize,
					NULL, vMdcrTestTaskPrio, &xHandleMdcrTest) == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vMdcrTestTaskName);
		while(1){}
	}
	else
	{	// UART_DEBUG("Init \"%s\" task...\r\n", vMdcrTestTaskName);
	}
	
		// UART_DEBUG("Init \"%s\" task...\r\n", vLED1TaskName);

	free_heap_size0 = xPortGetFreeHeapSize();
	// Create a periodic timer - Software vochdog (1min). The identifier is not used the timer (0)
	xAutoReloadTimer = xTimerCreate("AutoReloadTimer", 60000 / portTICK_PERIOD_MS, pdTRUE, 0, vAutoReloadTimerFunction);
	if( xAutoReloadTimer == NULL ) 
	{
		// UART_DEBUG("Error: The timer was not created %s.\r\n", "xAutoReloadTimer");
		while(1){};
	}else if( xTimerStart( xAutoReloadTimer, 0 ) != pdPASS )										// Start the timer.
    {
		// The timer could not be set into the Active state.
	}

	free_heap_size0 = xPortGetFreeHeapSize();
	// Create an instance of 3 interval timers.
         uiOneShotTimersDelay[0] = TIMER_VAR_DELAY;
         uiOneShotTimersDelay[1] = TIMER_1_DELAY;
         uiOneShotTimersDelay[2] = TIMER_2_DELAY;
         uiOneShotTimersDelay[3] = TIMER_60sec_DELAY;
  // Each of them pass their identifier. * Function for them all alone. — vOneShotTimersFunction().
	for( i = 0; i < NUMBER_OF_TIMERS; i++ ) 
	{
		xOneShotTimers[i] =	xTimerCreate("OneShotTimer_n", uiOneShotTimersDelay[i], \
											pdFALSE,(void*) uxOneShotTimersIDs[i], vOneShotTimersFunction);
		if( xOneShotTimers[ i ] == NULL )  
		{
			while(1){}; 
		}else if( xTimerStart( xOneShotTimers[i], 0 ) != pdPASS )// Start the timer.
		{  // The timer could not be set into the Active state.
		}
    }
	
	free_heap_size0 = xPortGetFreeHeapSize();
	if (xTaskCreate(vI2CHandlerTask, vI2CHandlerTaskName, configMINIMAL_STACK_SIZE, NULL,
			vI2C0TaskPrio, &xHandleI2C )	== errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vSerialPortRecvTaskName);
		while(1){}
	}
	
//	if (xTaskCreate(vWriteToFlashTask, vWriteToFlashTaskNAME, vWriteToFlashTaskStackSize, NULL,
//			vWriteToFlashTaskPrio, &xHandleWriteToFlashTask )	== errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
//	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vSerialPortRecvTaskName);
//		while(1){}
//	}

	free_heap_size0 = xPortGetFreeHeapSize();
	if (xTaskCreate(vLed1Task, vLED1TaskName, vLED1TaskStackSize,
					NULL, vLED1TaskPrio, &xHandleLed1Gr) == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vLED1TaskName);
		while(1){}
	}
	
	free_heap_size0 = xPortGetFreeHeapSize();
	if (xTaskCreate(vLed2Task, vLED2TaskName, vLED2TaskStackSize,
					NULL, vLED2TaskPrio, &xHandleLed2Yw) == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vLED2TaskName);
		while(1){}
	}
	// UART_DEBUG("Init \"%s\" task...\r\n", vLED2TaskName);
	vTaskSuspend( xHandleLed2Yw );																						//Freeze

	free_heap_size0 = xPortGetFreeHeapSize();
	if (xTaskCreate(vLedRedTask, vLEDRedTaskName, vLEDRedTaskStackSize,
					NULL, vLEDRedTaskPrio, &xHandleLedRed) == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vLEDRedTaskName);
		while(1){}
	}
	vTaskSuspend( xHandleLed1Gr );																						//Freeze

	// UART_DEBUG("Init \"%s\" task...\r\n", vLEDRedTaskName);
	vTaskSuspend( xHandleLedRed );
#else
	xTaskCreate(vTask1_debug, vLED1TaskName, 1024*12/*configMINIMAL_STACK_SIZE*/, NULL, 9, NULL );

	if (xTaskCreate(vLed1Task, vLED1TaskName, vLED1TaskStackSize,
					NULL, vLED1TaskPrio, &xHandleLed1Gr) == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vLED1TaskName);
		while(1){}
	}
	
	if (xTaskCreate(vLed2Task, vLED2TaskName, vLED2TaskStackSize,
					NULL, vLED2TaskPrio, &xHandleLed2Yw) == errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY)
	{	// UART_DEBUG("Error: can't allocate memory for task %s.\r\n", vLED2TaskName);
		while(1){}
	}
#endif
	//Freeze
	
	//CountSemaphoreUART0_Handle = xSemaphoreCreateCounting(uxMaxCountUART0Semaph, uxInitialCountUARTSemaph);
	//SemaphoreUART0_Handle = xSemaphoreCreateCounting(uxMaxCountUART0Semaph, SendBufferQ_Max_Size);
	
	free_heap_size = xPortGetFreeHeapSize();
	free_heap_size =free_heap_size;

//	free_heap_size = xPortGetFreeHeapSize();
	// UART_DEBUG("Size of the heap after tasks creation = %d bytes\r\n", free_heap_size);
	
	
//	vTaskStartScheduler();			//	Start OS scheduler
#ifdef MDCR_INTERFACE
	UARTx_DeInit(UART1);				//	DeInit com port
	PTE->PSOR |= (1<<4);				//	Set "0" level - OFF com port
	PTE->PCOR |= (1<<5);				//	Set "0" level - OFF DD7 (com port)
#endif

	free_heap_size0 = free_heap_size0;
//	vTaskDelay(5000);
//	UARTx_DeInit(UART1);				//com port	disable
//	GoToWait();

//	while(1){
//	}
}
