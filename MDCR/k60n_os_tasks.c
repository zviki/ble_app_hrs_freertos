/*****************************************************************************
 *	File:	k60n_os_tasks.c
 *	Description:
 *****************************************************************************/
 #include "header.h"
 #include "k60n_uart.h"
 #include "command_decode.h"
 #include "command_handler.h"
 #include "k60n_os_tasks.h"
 #include "FreeRTOS.h"
 #include "task.h"
 #include "semphr.h"
 #include "queue.h"
 #include "BT43.h"
 #include "mdcr_func.h"
 #include "timers.h"
 #include "k60n_gpio.h"
 #include "k60n_spi.h"
#include "AT25DFspi_memory.h"
 #include "iic.h"
 #include "LTC2941_i2c.h"

xTaskHandle xHandleSerialPortRecv;
xTaskHandle xHandleSerialPortSend;
xTaskHandle xHandleBTRecv;
xTaskHandle xHandleBTSend;
xTaskHandle xHandleDeviceCommand;
xTaskHandle xHandleMdcrTest;
xTaskHandle xHandleLed1Gr;
xTaskHandle xHandleLed2Yw;
xTaskHandle xHandleLedRed;
xTaskHandle xHandleI2C;

xTaskHandle xHandleWriteToFlashTask;

//------------------------------------
xTimerHandle	xAutoReloadTimer;										 	// Descriptor periodic timer
xTimerHandle	xOneShotTimers[NUMBER_OF_TIMERS];			// Descriptor array interval timers
// An array of IDs interval timers
// Timer 0 - for different tasks (variable in the program period)
const  unsigned portBASE_TYPE uxOneShotTimersIDs  [NUMBER_OF_TIMERS] = { ID_TIMER_VAR, ID_TIMER_1,ID_TIMER_2, ID_TIMER_60sec};
unsigned int uiOneShotTimersDelay[NUMBER_OF_TIMERS];// = {1000/ portTICK_PERIOD_MS, TIMER_1_DELAY,TIMER_2_DELAY, TIMER_60sec_DELAY };
// Work period periodic timer = 60 seconds. Awaiting response from the GUI
//unsigned int uiAutoReloadTimerPeriod = 60000 / portTICK_RATE_MS;
extern tsIIC_LTC2941Data LTC2941_Reg;

extern unsigned short ADCbuffer[4][2];
//extern struct ADCQueueStruct;
///**********************************************************
extern xSemaphoreHandle xSemaphoreADC0End;
extern xSemaphoreHandle xSemaphoreADC1End;
extern xSemaphoreHandle xSemaphoreADC2End;
extern xSemaphoreHandle xSemaphoreADC3End;
//extern xSemaphoreHandle CountSemaphoreUART0_Handle;
//extern xSemaphoreHandle SemaphoreUART0_Handle;

xSemaphoreHandle xSemaphoreKeyEnd;

xQueueHandle xQueueSerialPortRecv;
xQueueHandle xQueueSerialPortSend;
xQueueHandle xQueueBTRecv;
xQueueHandle xQueueBTSend;
xQueueHandle xQueueDeviceCommand;
xQueueHandle xQueueMdcrTest;

xQueueHandle Queue_ADC0Buff;
xQueueHandle Queue_ADC1Buff;
xQueueHandle Queue_ADC2Buff;
xQueueHandle Queue_ADC3Buff;

xQueueHandle Queue_BuffToWriteIntoFlash;

char response_buffer[RX_BUFFER_SIZE];
uTimersDataElement TimersDataFlag;
uMetaSwElement SwDataFlag;
uMetaChrgElement ChargeFlag;


#define MAX_NUM_OF_CHECK_POINT			10
#define GOVNODELAY_VALUE				1000
#define AT25_SR_WEL_BIT					0x200
#define AT25_SR_BUSY_STATUS_BITS		0x11
#define AT25_SR_WRITE_PROTECT_BITS		0x8000
#define AT25_SR_LOCKDOWN_BIT			0x8
#define WLASH_WELWAIT_SENDINIT1			25
#define WLASH_WELWAIT_SENDINIT2			50
#define FLASH_WELWAIT_EXIT				75

///**********************************************************
extern ErrorStatus CommandDecode(serial_port serial);
extern int BT53_Decode(serial_port serial);
///**********************************************************
#if (configCHECK_FOR_STACK_OVERFLOW != 0)

//------------------------------------------------------------------------------
// Description: Processor tasks stack overflow
// Input variables: task handler, the task name.
void vApplicationStackOverflowHook(xTaskHandle *pxTask, signed portCHAR *pcTaskName)
{
	// UART_DEBUG("Error: stack overflow for task %s.\r\n", pcTaskName);
	while(1){}
}
#endif


// Description: The queuing
//------------------------------------------------------------------------------
void vQueueCreation(void)
{
	xQueueSerialPortRecv = xQueueCreate(4/*xQueueStandartSize*/, sizeof(serial_port));
	if (xQueueSerialPortRecv == NULL)
	{
		// UART_DEBUG("Error: xQueueComPortReceive = NULL...\r\n");
	}
	xQueueSerialPortSend = xQueueCreate(4/*xQueueStandartSize*/, sizeof(serial_port));
	if (xQueueSerialPortSend == NULL)
	{
		// UART_DEBUG("Error: xQueueComPortTransmit = NULL...\r\n");
	}
	xQueueBTRecv = xQueueCreate(4/*xQueueStandartSize*2*/, sizeof(serial_port));
	if (xQueueBTRecv == NULL)
	{
		// UART_DEBUG("Error: xQueueBTRecv = NULL...\r\n");
	}
	xQueueDeviceCommand = xQueueCreate(xQueueStandartSize, sizeof(device_command));
	if (xQueueDeviceCommand == NULL)
	{
		// UART_DEBUG("Error: xQueueDeviceCommand = NULL...\r\n");
	}

	xQueueBTSend = xQueueCreate(xQueueStandartSize, /*sizeof(serial_port)*/ sizeof(SendBuffer));
	if (xQueueBTSend == NULL)
	{
		// UART_DEBUG("Error: xQueueBTSend = NULL...\r\n");
	}
	
	
	xQueueMdcrTest = xQueueCreate(xQueueMdcrTestSize, sizeof(tsMDCRCommandType));
	if (xQueueMdcrTest == NULL)
	{
		// UART_DEBUG("Error: xQueueMdcrTest = NULL...\r\n");
	}

	
	// Create semaphore for ADC0
	vSemaphoreCreateBinary(xSemaphoreADC0End);
	if (!xSemaphoreADC0End)
	{
		// UART_DEBUG("Error: xSemaphoreADC0End = NULL...\r\n");
		while(1){}
	}
	else
	{	
		//if (xSemaphoreTake(xSemaphoreADC0End, portMAX_DELAY) != pdPASS)
		//{
			// UART_DEBUG("Error: xSemaphoreTake(xSemaphoreADC0End)...\r\n");
		//	while(1){
		//	}
		//}
	}
	
	// Create semaphore for ADC1
	vSemaphoreCreateBinary(xSemaphoreADC1End);
	if (!xSemaphoreADC1End)
	{
		// UART_DEBUG("Error: xSemaphoreADC1End = NULL...\r\n");
		while(1){
		}
	}
	else
	{	
		//if (xSemaphoreTake(xSemaphoreADC1End, portMAX_DELAY) != pdPASS)
		//{
			// UART_DEBUG("Error: xSemaphoreTake(xSemaphoreADC1End)...\r\n");
		//	while(1){
		//	}
		//}
	}
	// Create semaphore for ADC2
	vSemaphoreCreateBinary(xSemaphoreADC2End);
	
	if (!xSemaphoreADC2End)
	{
		// UART_DEBUG("Error: xSemaphoreADC2End = NULL...\r\n");
		while(1){}
	}
	else
	{	
		//if (xSemaphoreTake(xSemaphoreADC2End, portMAX_DELAY) != pdPASS)
		//{
			// UART_DEBUG("Error: xSemaphoreTake(xSemaphoreADC2End)...\r\n");
		//	while(1){}
		//}
	}
	
	// Create semaphore for ADC3
	vSemaphoreCreateBinary(xSemaphoreADC3End);
	if (!xSemaphoreADC3End)
	{
		// UART_DEBUG("Error: xSemaphoreADC3End = NULL...\r\n");
		while(1){}
	}
	else
	{	
		//if (xSemaphoreTake(xSemaphoreADC3End, portMAX_DELAY) != pdPASS)
		//{
			// UART_DEBUG("Error: xSemaphoreTake(xSemaphoreADC3End)...\r\n");
		//	while(1){}
		//}
	}

	// Create semaphore for Buttons
	vSemaphoreCreateBinary(xSemaphoreKeyEnd);
	if (!xSemaphoreKeyEnd)
	{
		// UART_DEBUG("Error: xSemaphoreADC0End = NULL...\r\n");
		while(1){}
	}
	else
	{	
		//if (xSemaphoreTake(xSemaphoreKeyEnd, portMAX_DELAY) != pdPASS)
		//{
			// UART_DEBUG("Error: xSemaphoreTake(xSemaphoreADC0End)...\r\n");
		//	while(1){
		//	}
		//}
	}
	Queue_ADC0Buff = xQueueCreate(SIZE_ADC_QUEUE, sizeof(ADCQueueStruct));		//150
	Queue_ADC1Buff = xQueueCreate(SIZE_ADC_QUEUE, sizeof(ADCQueueStruct));		//150
	Queue_ADC2Buff = xQueueCreate(SIZE_ADC_QUEUE, sizeof(ADCQueueStruct));		//150
	Queue_ADC3Buff = xQueueCreate(SIZE_ADC_QUEUE, sizeof(ADCQueueStruct));		//150
	
	Queue_BuffToWriteIntoFlash = 										//*2
		xQueueCreate((LENGTH_OF_FLASH_BLOCK/LENGTH_DATATOWRITE_INTO_FLASH)*3, sizeof(WriteToFlashStruct));

}

/*******************************************************************************
 * Задача 					: vLed1Task
 * Описание    				: Обработчик задачи светодиода LED1
 * Входные переменные 		: pvParameters - интервал (ms) мигания светодиодом LED1.
 * Возвращаемые значения	: -
 *******************************************************************************/
void vLed1Task(void * pvParameters)
{
	unsigned portSHORT task_period = 250;
	while(1)
	{	
#ifdef MDCR_INTERFACE
		LED_PORT->PTOR ^= LED1_GR;
#endif
		vTaskDelay(task_period/portTICK_RATE_MS);
	}
}
/*******************************************************************************
 * Задача 					: vLed2Task
 * Описание    				: Обработчик задачи светодиода LED2
 * Входные переменные  		: интервал (ms) мигания светодиодом LED2.
 * Возвращаемые значения	: -
 *******************************************************************************/
void vLed2Task(void * pvParameters)
{
	unsigned portSHORT task_period = 250;
	while(1)
	{
#ifdef MDCR_INTERFACE
		LED_PORT->PTOR ^= LED2_YW;
#endif
		vTaskDelay(task_period/portTICK_RATE_MS);
	}
}

void vLedRedTask(void * pvParameters)
{
	unsigned portSHORT task_period = 500;
	while(1)
	{
#ifdef MDCR_INTERFACE
		LED3_PORT->PTOR ^= LED3_RED;
#endif
		vTaskDelay(task_period/portTICK_RATE_MS);
	}
}

void vAutoReloadTimerFunction(xTimerHandle xTimer)
{

}
extern bool waitAppLessTest;
// The function of interval timers.
// Multiple instances of interval timers corresponds to one single function.
// This function is called automatically at the expiration time of any of the associated timers.
// In order to find out what time the timer expires, the timer identifier is used
void vOneShotTimersFunction(xTimerHandle xTimer)
{
	portBASE_TYPE timerID = 0;												// A pointer to the timer identifier

	timerID = (long)pvTimerGetTimerID(xTimer);				// Get a timer that calls this function timer

	switch (timerID)
	{
	 case ID_TIMER_VAR:																// Load interval timer 1
		TimersDataFlag.BIT.TIMER_VAR = 1;
	 break;

	 case ID_TIMER_1:																	// Load interval timer 2
		
		waitAppLessTest = false;
		TimersDataFlag.BIT.TIMER_1 = 1;
		setLEDState(AllLEDsOff);
		stopTimerLED30SecAfterTests();
	 break;

	 case ID_TIMER_2:
		TimersDataFlag.BIT.TIMER_2 = 1;
	 break;
	 
	 case ID_TIMER_60sec:
		TimersDataFlag.BIT.TIMER_60sec = 1;
		 
	 break;
	}
}

void KeyScan(void)
{
	unsigned char switchCounter[2]={0};
	unsigned int timetic;
	SwDataFlag.ALL=0;
	switchCounter[0] = 0;
	switchCounter[1] = 0;
	timetic=0;
	while(timetic < 500)			//waiting clicking 0,5sek
	{
#ifdef MDCR_INTERFACE
		if (~(PTA->PDIR>>SW2)& 0x01) 					//if pressed 0
		{
			switchCounter[0]++;
			if(switchCounter[0]>=20)
			{
				switchCounter[0]=0;
				SwDataFlag.BIT.SW2_flag = 1;			//set SW2
			}
		}
		if (~(PTA->PDIR>>SW1)& 0x01UL )
		{
			switchCounter[1]++;
			if(switchCounter[1]>=20)
			{
				switchCounter[1]=0;
				SwDataFlag.BIT.SW1_flag = 1;			//set SW1
			}
		}
#endif
		timetic++;
		vTaskDelay(1);
	}
}

void vSerialPortRecvTask(void * pvParameters)
{
	serial_port com_port;
	(void)pvParameters;
	for(;;)
	{	
		if (xQueueReceive(xQueueSerialPortRecv, &com_port, portMAX_DELAY) != pdPASS){
			// UART_DEBUG("Error: vSerialPortRecv()->xQueueReceive() != pdPASS.\r\n");
		}else
		{
			if (CommandDecode(com_port)!=SUCCESS)
			{
				// UART_DEBUG("Error: CommandDecode()\r\n");
			}
		}
	}
//	vQueueDelete(xQueueSerialPortRecv);
//	UART_DEBUG("Queue \"xQueueSerialPortRecv\" has been deleted.\r\n");
//	vTaskDelete(NULL);
	// UART_DEBUG("Task %s has been deleted.\r\n", vSerialPortRecvTaskName);
}
//-------------------------------------------------------------------------------
void vSerialPortSendTask(void * pvParameters)
{
	serial_port com_port;
	(void)pvParameters;
	for(;;)
	{	if (xQueueReceive(xQueueSerialPortSend, &com_port, portMAX_DELAY) != pdPASS)
		{
			// UART_DEBUG("Error: vSerialPortRecv()->xQueueReceive() != pdPASS.\r\n");
		}
		else
		{
			serial_send_data(com_port);
		}
	}
//	vQueueDelete(xQueueSerialPortSend);
	// UART_DEBUG("Queue \"xQueueSerialPortSend\" has been deleted.\r\n");
//	vTaskDelete(NULL);
	// UART_DEBUG("Task %s has been deleted.\r\n", vSerialPortSendTaskName);
}


//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
void vCommandHandlerTask(void * pvParameters)
{
	device_command command;
	// (void)pvParameters;
	for(;;)
	{	if (xQueueReceive(xQueueDeviceCommand, &command, portMAX_DELAY) != pdPASS)
		{
			// UART_DEBUG("Error: vSerialPortRecv()->xQueueReceive() != pdPASS.\r\n");
		}
		else
		{	if (CommandHandler(command)!=SUCCESS)
			{
				// UART_DEBUG("Error: CommandDecode()\r\n");
			}
		}
	}
//	vQueueDelete(xQueueDeviceCommand);
	// UART_DEBUG("Queue \"xQueueDeviceCommand\" has been deleted.\r\n");
//	vTaskDelete(NULL);
	// UART_DEBUG("Task %s has been deleted.\r\n", vCommandHandlerTaskName);
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
#ifndef MDCR_INTERFACE
typedef int * UART_Type;
#endif
static void UARTSendByte(UART_Type *UART, char byte)
{
//	short i;
//	UART->C2 &= ~ UART_C2_RIE_MASK;									//disable interupt
//	UART->C2 &= ~ UART_C2_RE_MASK;
//	i=0;
#ifdef MDCR_INTERFACE
	while(!(UART->S1 & UART_S1_TDRE_MASK));
	UART->D = (byte & (uint16_t)0x01FF);
	while(!(UART->S1 & UART_S1_TC_MASK));	
#endif
//	while(!(UART->S1 & UART_S1_TC_MASK) || (i<2500)){i++;};
//	UART->C2 |= UART_C2_RIE_MASK;										//enable interupt
//	UART->C2 |= UART_C2_RE_MASK;
}



//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
void vBTReceiveHandlerTask(void* pvParameters)
{
	serial_port com_port;

	for(;;)
	{	
		if (xQueueReceive(xQueueBTRecv, &com_port, portMAX_DELAY) != pdPASS)
		{
			// UART_DEBUG("Error: vBTSendHandlerTask()->xQueueReceive() != pdPASS.\r\n");
		}
		else
		{	
			if (BT53_Decode(com_port)!=SUCCESS)
			{
				// UART_DEBUG("Error: BT53_Decode()\r\n");
			}
		}
	}
}

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
void vI2CHandlerTask( void *pvParameters )
{
//	short i,j;
	short Charge_old=0;
//	short Charge2_old=0;							// за минуту изменение
	short ChargeCounter=0;
	short status=0;
//	short status_led;
//	short BlinkCounter=0;
//	unsigned short temp;
//	short charged =0;

	ChargeFlag.ALL=0;
	I2C0Data.TxBuff[0]=0xFC;											//Threshold Value = 3.0V. for Allarm
	LTC2941_write(&I2C0Data.TxBuff[0],1,LTC2941_ADR_Control);

//	vTaskDelay(100);	//читаем с зарядника
//	LTC2941_Reg.Charge = 0xAA;										// уровень остаточного заряда 0 - вхлам разряжена
//	LTC2941_write((unsigned char*)&LTC2941_Reg.Charge,2,LTC2941_ADR_Charge);

	vTaskDelay(100);	//читаем с зарядника

	LTC2941_ask();		//читаем все		зарядник обновляет свою инфу раз в 1сек

{		
	for( ;; )
	{
	Charge_old	= LTC2941_Reg.Charge;
	vTaskDelay(1000);	//читаем с зарядника
	LTC2941_ask();		//читаем все		зарядник обновляет свою инфу раз в 1сек
		
//----------- обновление уровня потолка заряда раз в минуту если изменяется в + --------
	if (ChargeCounter>60)
	{
		if 	((LTC2941_Reg.Charge > Charge_old) && (LTC2941_Reg.Charge !=0))
		{
			LTC2941_Reg.ChThHI = LTC2941_Reg.Charge;
			I2C0Data.TxBuff[0]=(LTC2941_Reg.ChThHI >> 8) & 0xFF;
			I2C0Data.TxBuff[1]=LTC2941_Reg.ChThHI & 0xFF;
			LTC2941_write(&I2C0Data.TxBuff[0],2,LTC2941_ADR_ChThHI);
		}
		ChargeCounter=0;
//		Charge2_old = LTC2941_Reg.Charge ;
	}
	else 	ChargeCounter++;

//---------- USB подключено ?-------
/*
	if (ChargeCounter>5)
	{
		ChargeCounter=0;
		if 	((LTC2941_Reg.Charge > Charge_old) && (LTC2941_Reg.Charge !=0))
		{
			ChargeFlag.BIT.USB_CON = 1;			// USB подключено 5 сек - заряжается аккум
		}
		else
		{
			ChargeFlag.BIT.USB_CON = 0;
		}
	}
	else 	ChargeCounter++;
*/



//----------- Charge / DisCharge	----
#ifdef MDCR_INTERFACE
	if ((PTD->PDIR>>15)& 0x01 )																	// Charge  ?
		{
			vTaskDelay(1);
			if ((PTD->PDIR>>15)& 0x01 )
				{ChargeFlag.BIT.CHRG=1; LED3_BL_CLR; LED3_RED_CLR;}			//Charge - yes. Enable LED
		}
		else
		{

		//--------- остаточный заряд-------
			if ((LTC2941_Reg.ChThHI / 5) > LTC2941_Reg.Charge)		//уровень 20% остатка заряда осталось меньше
			{ LED3_RED_SET; LED3_BL_CLR;}
			else if (!ChargeFlag.BIT.CHRG)  {LED3_BL_SET; LED3_RED_CLR;}
		//---------------------------------
//			if(ChargeFlag.BIT.CHRG && ChargeFlag.BIT.USB_CON) 					//если до этого заряжали -> зарядили!
			if(ChargeFlag.BIT.CHRG) 					//если до этого заряжали -> зарядили!
			 {
				LED3_BL_SET;
				LED3_RED_CLR;

				Charge_old	= LTC2941_Reg.Charge;				// определяем подключение USB по утечке тока
				vTaskDelay(10000);												//ждем 5 сек для определения разряда
				LTC2941_ask();													//читаем все
				if 	((LTC2941_Reg.Charge > Charge_old))	//USB подключено 5 сек - заряжается аккум
					{
//						ChargeFlag.BIT.USB_CON = 1;			//
						LTC2941_Reg.ChThHI = LTC2941_Reg.Charge;
						I2C0Data.TxBuff[0]=(LTC2941_Reg.ChThHI >> 8) & 0xFF;
						I2C0Data.TxBuff[1]=LTC2941_Reg.ChThHI & 0xFF;
						LTC2941_write(&I2C0Data.TxBuff[0],2,LTC2941_ADR_ChThHI);
						ChargeFlag.BIT.CYCLE =0;
					}
			 }
			ChargeFlag.BIT.CHRG=0;
		}
#endif


//	if(ChargeFlag.BIT.CYCLE && ChargeFlag.BIT.CHRG && ChargeFlag.BIT.VALLERT)
//		LTC2941_Reg.Charge =0;																			//разряжаем РАЗРЯЖЕННый аккум

//	if(ChargeFlag.BIT.CYCLE && ChargeFlag.BIT.CHRG)							//заряд РАЗРЯЖЕННОГО аккума пошел
//	{}

//---------------------------------------------
	if(LTC2941_Reg.Status & LTC2941_VBATAlertMASK)		//Value = 3.0V - Allarm!
	{
		ChargeFlag.BIT.VALLERT =1;
		ChargeFlag.BIT.CYCLE =1;
		if (!status)
		{
			vTaskResume(xHandleLedRed);									// Blink red
			LTC2941_Reg.Charge = 0x0;										// Setup Botom level of Charge = 0
//			LTC2941_write((unsigned char*)&LTC2941_Reg.Charge,2,LTC2941_ADR_Charge);
			I2C0Data.TxBuff[0]=0x0;
			I2C0Data.TxBuff[1]=0x0;
			LTC2941_write(&I2C0Data.TxBuff[0],2,LTC2941_ADR_Charge);

		}

		if (status > 119)																// 2 min Blinked RED -> Off dev
		{

			vTaskSuspend(xHandleSerialPortRecv);
			vTaskSuspend(xHandleSerialPortSend);
			vTaskSuspend(xHandleBTRecv);
			vTaskSuspend(xHandleBTSend);
			vTaskSuspend(xHandleDeviceCommand);
			vTaskSuspend(xHandleMdcrTest);
	//		if (eTaskGetState(xHandleLed1Gr)==eSuspended	)
			vTaskSuspend(xHandleLed1Gr );									//stop blinks Green
			vTaskSuspend(xHandleLed2Yw );									//stop blinks Yellow
			vTaskSuspend(xHandleLedRed);

#ifdef MDCR_INTERFACE
			PTD->PTOR |= (1<<14);													// power to BT
#endif
			Dinit_portX();
			vTaskSuspend(xHandleI2C);
		}
		
		
		else	status++;

	}
	else if (status)																				//Charging. come back. Power ON
		{
			vTaskSuspend(xHandleLedRed);
			ChargeFlag.BIT.VALLERT =0;
			vTaskResume(xHandleSerialPortRecv);
			vTaskResume(xHandleSerialPortSend);
			vTaskResume(xHandleBTRecv);
			vTaskResume(xHandleBTSend);
			vTaskResume(xHandleDeviceCommand);
			vTaskResume(xHandleMdcrTest);
			status=0;
		}
	}		

}
//vTaskDelete( NULL );// Уничтожить задачу, если произошел выход из бесконечного цикла
}

void vBTSendHandlerTask(void* pvParameters)
{
//	unsigned int i = 0;
	SendBuffer send_buffer;

	for(;;)
	{	
		//xSemaphoreTake(CountSemaphoreUART0_Handle, portMAX_DELAY);
		if (xQueueReceive(xQueueBTSend, &send_buffer, portMAX_DELAY) == pdPASS)
		{
			int i = 0;
			for (i = 0; i < send_buffer.data_size; i++) 
			{
#ifdef MDCR_INTERFACE
				UARTSendByte(UART0, send_buffer.data[i]);
#endif
			}
		}
//		xSemaphoreGive(SemaphoreUART0_Handle);		
	}
//	vQueueDelete(xQueueBTSend);
//	vTaskDelete(NULL);
}
