/*******************************************************************************
 *	File: k60n_uart.c
 *******************************************************************************/
#include "header.h"
#include "k60n_uart.h"
#include "command_handler.h"
#include "command_decode.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
//#include "misc.h"

#include "mdcr_func.h"
#include "BT43.h"
#include "mdcr_protocol.h"

#define USART1_USE_TEXT_COMMAND

extern xQueueHandle xQueueSerialPortRecv;
extern xQueueHandle xQueueSerialPortSend;

extern xQueueHandle xQueueBTRecv;
extern xQueueHandle xQueueBTSend;
extern int modeProtocol;

extern tsTestParam param;
extern uMetaBT43Element BT43DataFlag;

static buf_st usart1_rbuf = {0, 0,};
static buf_st responce_buf = {0, 0,};

static char uart_printf_buf[RX_BUFFER_SIZE];
//static void UARTSendByte(UART_Type *UART, char byte);

/*******************************************************************************
 * Функция  				:	bool compare_string(char *strA, char *strB, unsigned int str_size)
 * Описание    				:	Функция сравнения строк
 * Входные переменные  		:	-
 * Возвращаемые значения	:	-
 *******************************************************************************/
bool compare_string(char *strA, char *strB, unsigned int str_size)
{
    unsigned int i = 0;
    for (i = 0; i < str_size; i++)
    {
        if (tolower(strA[i]) != tolower(strB[i])) return false; // Сравнение с приведением к строчному виду
    }
    return true;
}

//--------------------------------------------------------------------------------
#ifndef MDCR_INTERFACE
typedef int * UART_Type;
#endif

void UARTx_DeInit(UART_Type *UART)
{
#ifdef MDCR_INTERFACE

    if (UART == UART0)
    {
        // connect to Bluetooth module
        PORTD->PCR[6] = PORT_PCR_MUX(0); // RX ->GPIO
        PORTD->PCR[7] = PORT_PCR_MUX(0); // TX->GPIO
        PORTD->PCR[4] = PORT_PCR_MUX(0); // RTS->GPIO
        PORTD->PCR[5] = PORT_PCR_MUX(0); // CTS->GPIO

        PTD->PDDR |= (1 << 4); //	Output
        PTD->PDDR |= (1 << 5); //	Output
        PTD->PDDR |= (1 << 6); //	Output
        PTD->PDDR |= (1 << 7); //	Output

        PTD->PDOR |= (1 << 4);
        PTD->PDOR |= (1 << 5);
        PTD->PDOR |= (1 << 6);
        PTD->PDOR |= (1 << 7);

        PTD->PSOR |= (1 << 4);
        PTD->PSOR |= (1 << 5);
        PTD->PSOR |= (1 << 6);
        PTD->PSOR |= (1 << 7);
    }else if (UART == UART1)
    {
        PORTE->PCR[0] = PORT_PCR_MUX(0); // RX->GPIO
        PORTE->PCR[1] = PORT_PCR_MUX(0); // TX->GPIO
        PORTE->PCR[2] = PORT_PCR_MUX(0); // RTS->GPIO
        PORTE->PCR[3] = PORT_PCR_MUX(0); // CTS		->GPIO

        PTE->PDDR |= (1 << 0); //	Output
        PTE->PDDR |= (1 << 1); //	Output
        PTE->PDDR |= (1 << 2); //	Output
        PTE->PDDR |= (1 << 3); //	Output

        PTE->PDOR |= (1 << 0);
        PTE->PDOR |= (1 << 1);
        PTE->PDOR |= (1 << 2);
        PTE->PDOR |= (1 << 3);

        PTE->PSOR |= (1 << 0);
        PTE->PSOR |= (1 << 1);
        PTE->PSOR |= (1 << 2);
        PTE->PSOR |= (1 << 3);

        PTE->PSOR |= (1 << 4); //	Set "0" level
        PTE->PCOR |= (1 << 5); //	Set "0" level - OFF DD7 (com port)
    }else
    {
        // UART_DEBUG("Unknown UART type...\r\n");
        return;
    }
#endif
}


//----------------------------------------------------------------------

void UARTx_Init(UART_Type *UART, int sysclk, int baud)
{
#ifdef MDCR_INTERFACE

    NVIC_InitTypeDef NVIC_InitStructure;
    register uint16_t sbr, brfa;
    uint8_t temp;
    // Enable the clock to the selected UART
    if (UART == UART0)
    {
        // connect to Bluetooth module
        SIM->SCGC5 |= SIM_SCGC5_PORTD_MASK;
        PORTD->PCR[6] = PORT_PCR_MUX(3); // RX
        PORTD->PCR[7] = PORT_PCR_MUX(3); // TX
        PORTD->PCR[4] = PORT_PCR_MUX(3); // RTS
        PORTD->PCR[5] = PORT_PCR_MUX(3); // CTS
        SIM->SCGC4 |= SIM_SCGC4_UART0_MASK;
    }else if (UART == UART1)
    {
        SIM->SCGC5 |= SIM_SCGC5_PORTE_MASK;
        PORTE->PCR[0] = PORT_PCR_MUX(3); // RX
        PORTE->PCR[1] = PORT_PCR_MUX(3); // TX
        PORTE->PCR[2] = PORT_PCR_MUX(3); // RTS
        PORTE->PCR[3] = PORT_PCR_MUX(3); // CTS
        SIM->SCGC4 |= SIM_SCGC4_UART1_MASK;
    }else if (UART == UART2)
    {
        SIM->SCGC4 |= SIM_SCGC4_UART2_MASK;
        // UART_DEBUG("Not configure PORT for UART2...\r\n");
        return;
    }else if (UART == UART3)
    {
        SIM->SCGC4 |= SIM_SCGC4_UART3_MASK;
        // UART_DEBUG("Not configure PORT for UART3...\r\n");
        return;
    }else if (UART == UART4)
    {
        SIM->SCGC1 |= SIM_SCGC1_UART4_MASK;
        // UART_DEBUG("Not configure PORT for UART4...\r\n");
        return;
    }else if (UART == UART5)
    {

    }else
    {
        // UART_DEBUG("Unknown UART type...\r\n");
        return;
    }

    // Make sure that the transmitter and receiver are disabled while we change settings.
    //UART_C2_REG(uartch) &= ~(UART_C2_TE_MASK|UART_C2_RE_MASK );
    UART->C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK);

    // Configure the UART for 8-bit mode, no parity
    UART->C1 = 0;
    //	UART->C1 |=UART_C1_ILT_MASK;

    // Calculate baud settings
    sbr = (uint16_t) ((sysclk * 1000) / (baud * 16));

    // Save off the current value of the UARTx_BDH except for the SBR field
    temp = UART->BDH & ~(UART_BDH_SBR(0x1F));

    UART->BDH = temp | UART_BDH_SBR(((sbr & 0x1F00) >> 8));
    UART->BDL = (uint8_t) (sbr & UART_BDL_SBR_MASK);

    // Determine if a fractional divider is needed to get closer to the baud rate
    brfa = (((sysclk * 32000) / (baud * 16)) - (sbr * 32));

    // Save off the current value of the UARTx_C4 register except for the BRFA field
    temp = UART->C4 & ~(UART_C4_BRFA(0x1F));

    //UART_C4_REG(UART) = temp |  UART_C4_BRFA(brfa);
    UART->C4 = temp | UART_C4_BRFA(brfa);

    // Enable receiver and transmitter
    UART->C2 |= (UART_C2_RIE_MASK | UART_C2_TE_MASK | UART_C2_RE_MASK);

    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4); // For FreeRTOS using only this Priority Group
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0; // A lower priority value indicates a higher priority
    if (UART == UART0)
    {
        //		UART->MODEM |= (UART_MODEM_TXCTSE_MASK|UART_MODEM_TXRTSE_MASK);				//enable CTS / RTS pin
        //		UART->MODEM |= (UART_MODEM_TXRTSE_MASK);				//enable CTS / RTS pin		
        UART->MODEM |= (UART_MODEM_TXCTSE_MASK | UART_MODEM_RXRTSE_MASK); //enable CTS / RTS pin

        NVIC_InitStructure.NVIC_IRQChannel = UART0_RX_TX_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = (uint8_t) (15);
    }else if (UART == UART1)
    {
        // UART->MODEM |= (UART_MODEM_TXCTSE_MASK|UART_MODEM_RXRTSE_MASK);

        NVIC_InitStructure.NVIC_IRQChannel = UART1_RX_TX_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = (uint8_t) (15 - 1);
    }else if (UART == UART2)
    {
    }else if (UART == UART3)
    {
    }else if (UART == UART4)
    {
    }else
    {
        NVIC_InitStructure.NVIC_IRQChannel = UART5_RX_TX_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = (uint8_t) (15 - 2);
    }
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#endif
}


//----------------------------------------------------------------------------

void UART0SendData(char* buff, int size)
{
    int i;
    for (i = 0; i < size; i++)
    {
#ifdef MDCR_INTERFACE

        UARTSendByte(UART0, buff[i]);
#endif
    }
}
void UART0SendByte_u(uint8_t byte)
{
#ifdef MDCR_INTERFACE

    while (!(UART0->S1 & UART_S1_TDRE_MASK));
    while (!(UART0->SFIFO & UART_SFIFO_TXEMPT_MASK));
    UART0->D = byte ;
    while (!(UART0->S1 & UART_S1_TC_MASK));
#endif
}


//----------------------------------------------------------------------------

void UART0_printf(char *arg_list, ...)
{
    unsigned int i, str_size;
    va_list arg_buffer;
    va_start(arg_buffer, arg_list);
    memset(uart_printf_buf, 0, sizeof (uart_printf_buf));
    str_size = vsprintf(uart_printf_buf, arg_list, arg_buffer);
    va_end(arg_buffer);
    for (i = 0; i < str_size; i++)
    {
#ifdef MDCR_INTERFACE

        UARTSendByte(UART0, uart_printf_buf[i]);
#endif
    }
}


//----------------------------------------------------------------------------

void UART1SendData(char* buff, int size)
{
    int i;
    for (i = 0; i < size; i++)
    {
#ifdef MDCR_INTERFACE

        UARTSendByte(UART1, buff[i]);
#endif
    }
}


//----------------------------------------------------------------------------

void UART1_printf(char *arg_list, ...)
{
    unsigned int i, str_size;
    va_list arg_buffer;
    va_start(arg_buffer, arg_list);
    memset(uart_printf_buf, 0, sizeof (uart_printf_buf));
    str_size = vsprintf(uart_printf_buf, arg_list, arg_buffer);
    va_end(arg_buffer);
    for (i = 0; i < str_size; i++)
    {
#ifdef MDCR_INTERFACE

        UARTSendByte(UART1, uart_printf_buf[i]);
#endif
    }
}



//----------------------------------------------------------------------------

void UART5_printf(char *arg_list, ...)
{
    unsigned int i, str_size;
    va_list arg_buffer;
    va_start(arg_buffer, arg_list);
    memset(uart_printf_buf, 0, sizeof (uart_printf_buf));
    str_size = vsprintf(uart_printf_buf, arg_list, arg_buffer);
    va_end(arg_buffer);
    for (i = 0; i < str_size; i++)
    {
#ifdef MDCR_INTERFACE

        UARTSendByte(UART5, uart_printf_buf[i]);
#endif
    }
}


//----------------------------------------------------------------------------

static void UARTSendByte(UART_Type *UART, char byte)
{
    //	short i;
    //	UART->C2 &= ~ UART_C2_RIE_MASK;									//disable interupt
    //	UART->C2 &= ~ UART_C2_RE_MASK;
    //	i=0;
#ifdef MDCR_INTERFACE

    while (!(UART->S1 & UART_S1_TDRE_MASK));
    UART->D = (byte & (uint16_t) 0x01FF);
    while (!(UART->S1 & UART_S1_TC_MASK));
#endif
    //	while(!(UART->S1 & UART_S1_TC_MASK) || (i<2500)){i++;};
    //	UART->C2 |= UART_C2_RIE_MASK;										//enable interupt
    //	UART->C2 |= UART_C2_RE_MASK;
}



extern uMetaSwElement SwDataFlag;
//----------------------------------------------------------------------------

static portBASE_TYPE mdcrDecodeReceiveData(buf_st* p, serial_port* com_port, int deviceID)
{
    short strlen_CONNECT = strlen("CONNECT");
    short strlen_CARRIER = strlen("CARRIER");
    int i = 0;

    portBASE_TYPE queue_status = pdPASS;
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

    com_port->data_source = deviceID; // data source.
    com_port->support_protocol = MDCRprotocol; //

    //		if(compare_string(p->buf[p->out], "OK", strlen("OK")))			


    switch (p->buf[p->out])
    {
        case TEST1:
            if ((p->in - p->out) >= TEST1_LENGTH)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->in = 0;
                p->out = p->in;
            }
            break;
        case TEST2:
            if ((p->in - p->out) >= TEST2_LENGTH)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->in = 0;
                p->out = p->in;
            }
            break;
        case TEST3:
            if ((p->in - p->out) >= TEST3_LENGTH)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->in = 0;
                p->out = p->in;
            }
            break;
            /*
                            case MEAS_REPEAT: //{
                                    if ((p->in - p->out)>=2)
                                    {
                                            p->out+=2;
                                    }
                                    break;	//}
             */
        case MEAS_OK:
            if ((p->in - p->out) >= OK_MEAS_LENGTH)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->in = 0;
                p->out = p->in;
            }
            break;
        case BUTTON_PRESSED:
            if ((p->in - p->out) >= 3)
            {
                switch (p->buf[p->out + 2])
                {
                    case 0x10: // 0x10: yellow button was pressed on the PC side.
                        SwDataFlag.BIT.SW1_GUI_flag = 1;
                        break;
                    case 0x11: // 0x11: green button was pressed on the PC side.
                        SwDataFlag.BIT.SW2_GUI_flag = 1;
                        break;
                }
                p->in = 0;
                p->out = p->in;
            }
            break;
        case STOP_TEST:
            if ((p->in - p->out) >= 2)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->in = 0;
                p->out = p->in;
            }
            break;
        case GET_LAST_MEASUREMENT:
            if ((p->in - p->out) >= 2)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->in = 0;
                p->out = p->in;
            }
            break;
        case SET_FILTER:
            if ((p->in - p->out) >= 3)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // размер данных.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->in = 0;
                p->out = p->in;
            }
            break;
        case SET_BT_CONFIGURATION:
            if ((p->in - p->out) >= 24)
            {
                p->out += 24;
            }
            break;
            //----------------------------------------------------------------------
        case GET_BEE_SN:
            if ((p->in - p->out) >= 2)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->in = 0;
                p->out = p->in;
            }
            break;
        case GET_BEE_APP_VERSION:
            if ((p->in - p->out) >= 2)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->in = 0;
                p->out = p->in;
            }
            break;
        case GET_BEE_DEVICE_VER:
            if ((p->in - p->out) >= 2)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->in = 0;
                p->out = p->in;
            }
            break;
        case GET_DATE_DEVICE_FIRMWARE: //{
            if ((p->in - p->out) >= 2)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->in = 0;
                p->out = p->in;
            }
            break; //}				

        case GET_CHARGED_LEVEL: //{
            if ((p->in - p->out) >= 2)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->out += 2;
                p->in = 0;
                p->out = p->in;
            }
            break; //}		
        case SEND_INFO_ABOUT_LAST_SAVED_TEST:
            if ((p->in - p->out) >= 2)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->out += 2;
                p->in = 0;
                p->out = p->in;
            }
            break;
        case SET_MEASURING_FREQUENCY:
            if ((p->in - p->out) >= 4)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->out += 2;
                p->in = 0;
                p->out = p->in;
            }
            break;
        case GET_MEASURING_FREQUENCY:
            if ((p->in - p->out) >= 2)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->out += 2;
                p->in = 0;
                p->out = p->in;
            }
            break;
        case SET_INVALID_SIGNAL_DETECT:
            if ((p->in - p->out) >= 4)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->out += 2;
                p->in = 0;
                p->out = p->in;
            }
            break;
        case GET_INVALID_SIGNAL_DETECT:
            if ((p->in - p->out) >= 2)
            {
                for (i = 0; i < (p->in - p->out); i++)
                    com_port->data[i] = p->buf[p->out + i];
                com_port->data_size = (p->in - p->out); // data size.
                queue_status = xQueueSendToBackFromISR(xQueueBTRecv, com_port, &xHigherPriorityTaskWoken);
                p->out += 2;
                p->in = 0;
                p->out = p->in;
            }
            break;

            //-----------------------------------------------------------------------			
        default:
            //			if (p->in > (RX_BUFFER_SIZE-MAX_COMMAND_SIZE))	{	p->in = 0;}
            p->out = p->in;
            if (p->in >= RX_BUFFER_SIZE) //(p->in > (RX_BUFFER_SIZE - MAX_COMMAND_SIZE))
            {
                p->in = 0;
                p->out = p->in;
            }
            break;
    }
    return queue_status;
}


//----------------------------------------------------------------------------

void UART0_RX_TX_IRQHandler(void)
{
    buf_st *p;
    portBASE_TYPE queue_status = pdPASS;
    serial_port com_port;
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    static char rx_data = 0;
    int i = 0;

#ifdef MDCR_INTERFACE

    if (UART0->S1 & UART_S1_RDRF_MASK)
    {
        rx_data = UART0->D;
        p = &usart1_rbuf;
        p->buf[p->in] = rx_data;
        p->in++;
        if (Uart0Protocol == USERprotocol || Uart0Protocol == BT43protocol)
        {
            if (compare_string((char*) &p->buf[p->in - 2], "\r\n", 2))
            {
                if ((p->in - p->out) > strlen("\r\n")) // If accepted that command is not empty
                {
                    if (xQueueSerialPortRecv != NULL)
                    {
                        for (i = 0; i < (p->in - p->out); i++)
                            com_port.data[i] = p->buf[p->out + i];
                        com_port.data_size = (p->in - p->out); // data size.
                        com_port.data_source = eUSART0; // data source.
                        queue_status = xQueueSendToBackFromISR(xQueueBTRecv, &com_port, &xHigherPriorityTaskWoken);
                        if (queue_status != pdPASS)
                        {
                            if (queue_status == errQUEUE_FULL)
                            {
                                // UART_DEBUG("Error: xQueueSerialPortRecv(UART5) is FULL\r\n");
                                return;
                            }
                        }
                        p->in = 0;
                        p->out = p->in;
                        if (xHigherPriorityTaskWoken != pdFALSE)
                        {
                            portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
                        }
                    }else
                    {
                        // UART_DEBUG("Error: UART1_RX_TX_IRQHandler()->xQueueSerialPortRecv = NULL\r\n");
                        p->out = p->in;
                        return;
                    }
                }else
                {
                    p->out = p->in;
                }
            }
        }else if (Uart0Protocol == MDCRprotocol)
        {
            queue_status = mdcrDecodeReceiveData(p, &com_port, eUSART0);
            if (queue_status != pdPASS)
            {
                if (queue_status == errQUEUE_FULL)
                {
                    // UART_DEBUG("Error: xQueueSerialPortRecv(UART5) is FULL\r\n");
                    return;
                }
            }
        }
    }else if (UART0->S1 & UART_S1_OR_MASK)
    {
        rx_data = UART0->D;
    }
#endif
}

//------------------------------------------------------------------------------

void UART1_RX_TX_IRQHandler(void)
{
    buf_st *p;
    portBASE_TYPE queue_status = pdPASS;
    serial_port com_port;
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    static char rx_data = 0;
#ifdef MDCR_INTERFACE

    if (UART1->S1 & UART_S1_RDRF_MASK)
    {
        rx_data = UART1->D;
        p = &usart1_rbuf;
        p->buf[p->in] = rx_data;
        p->in++;

        if (Uart1Protocol == USERprotocol || Uart1Protocol == BT43protocol)
        {
            if (compare_string((char*) &p->buf[p->in - 2], "\r\n", 2))
            {
                if ((p->in - p->out) > strlen("\r\n")) // If accepted that command is not empty
                {
                    if (xQueueSerialPortRecv != NULL)
                    {
                        int i = 0;
                        for (i = 0; i < (p->in - p->out); i++)
                            com_port.data[i] = p->buf[p->out + i];
                        com_port.data_size = (p->in - p->out); // data size.
                        com_port.data_source = eUSART1; // data source.
                        queue_status = xQueueSendToBackFromISR(xQueueBTRecv, &com_port, &xHigherPriorityTaskWoken);

                        if (queue_status != pdPASS)
                        {
                            if (queue_status == errQUEUE_FULL)
                            {
                                // UART_DEBUG("Error: xQueueSerialPortRecv(UART5) is FULL\r\n");
                                return;
                            }
                        }
                        if (p->in > (RX_BUFFER_SIZE - MAX_COMMAND_SIZE))
                        {
                            p->in = 0;
                        }
                        p->out = p->in;
                        if (xHigherPriorityTaskWoken != pdFALSE)
                        {
                            portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
                        }
                    }else
                    {
                        // UART_DEBUG("Error: UART1_RX_TX_IRQHandler()->xQueueSerialPortRecv = NULL\r\n");
                        p->out = p->in;
                        return;
                    }
                }else
                {
                    p->out = p->in;
                }
            }
        }else if (Uart1Protocol == MDCRprotocol)
        {
            queue_status = mdcrDecodeReceiveData(p, &com_port, eUSART1);
            if (queue_status != pdPASS)
            {
                if (queue_status == errQUEUE_FULL)
                {
                    // UART_DEBUG("Error: xQueueSerialPortRecv(UART5) is FULL\r\n");
                    return;
                }
            }
        }
    }else if (UART1->S1 & UART_S1_OR_MASK)
    {
        rx_data = UART1->D;
    }
#endif
}


//---------------------------------------------------------------------------

ErrorStatus SendDevResponce(int data_source, char *arg_list, ...)
{
    buf_st *p = &responce_buf;
    unsigned int str_size = 0;
    serial_port serial;
    va_list arg_buffer;
    va_start(arg_buffer, arg_list);
    str_size = vsprintf((char*) &p->buf[p->in], arg_list, arg_buffer);
    // Дополняем TermChar:
    memcpy(&p->buf[p->in + str_size], "\r\n", strlen("\r\n"));
    str_size = str_size + strlen("\r\n");
    p->in = (p->in + str_size);
    if (xQueueSerialPortSend != NULL)
    {
        int i = 0;
        for (i = 0; i < (p->in - p->out); i++)
            serial.data[i] = p->buf[p->out + i];
        serial.data_size = (p->in - p->out);
        serial.data_source = data_source;
        // Send responce packet to usb:
        if (xQueueSend(xQueueSerialPortSend, &serial, 1000 / portTICK_RATE_MS) != pdPASS)
        {
            // UART_DEBUG("Error: cTMCBulkInMsg()->xQueueSend(xQueueSerialDataOut) != pdPASS\r\n");
            va_end(arg_buffer);
            return ERROR;
        }
        if (p->in > (RX_BUFFER_SIZE - MAX_COMMAND_SIZE))
        {
            p->in = 0;
        }
        p->out = p->in;
    }else
    {
        // UART_DEBUG("Error: cTMCBulkInMsg()->xQueueSerialDataOut = NULL\r\n");
        va_end(arg_buffer);
        return ERROR;
    }
    va_end(arg_buffer);
    return SUCCESS;
}

//---------------------------------------------------------------------------

void serial_send_data(serial_port com_port)
{
    unsigned int i = 0;
    if ((strncmp((char*) com_port.data_source, DEVICE0, strlen(DEVICE0))) == 0)
    {
        for (i = 0; i < com_port.data_size; i++)
        {
#ifdef MDCR_INTERFACE

            UARTSendByte(UART1, com_port.data[i]);
#endif
        }
    }else if ((strncmp((char*) com_port.data_source, DEVICE1, strlen(DEVICE1))) == 0)
    {
        for (i = 0; i < com_port.data_size; i++)
        {
#ifdef MDCR_INTERFACE

            UARTSendByte(UART0, com_port.data[i]);
#endif
        }
    }else
    {
        // UART_DEBUG("Error: serial_send_data()->unknown data source: %s...\r\n", com_port.data_source);
    }
}
