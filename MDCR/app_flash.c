#include "app_flash.h"
#include "mdcr_protocol.h"
#include "AT25DFspi_memory.h"
#include "sst26vf064b_driver.h"
#include "header.h"

static volatile app_flash_type_t flash_type = FLASH_AT25D;

static void stub(void)
{
    ;
}

app_flash_type_t app_flash_get_type(void)
{
    return flash_type;
}

uint32_t app_flash_get_flash_len(void)
{
    if (flash_type == FLASH_SST26)
        return 0x7FFFFF;
    else
        return (0x400*0x400);
}

void app_flash_init(void)
{
    if (AT25D_Check_Chip_SPRL())
        flash_type = FLASH_AT25D;
    else
        flash_type = FLASH_SST26;
    if (flash_type == FLASH_SST26)
    {
        sst26_init();
    } else
    {
        AT25D_Init();
    }
}

void app_flash_sleep(void)
{
    if (flash_type == FLASH_SST26)
    {
        ;
    } else
    {
        AT25D_GoDeepSleep();
    }
}

void app_flash_wakeup(void)
{
    if (flash_type == FLASH_SST26)
    {
        ;
    } else
    {
        AT25D_ExitDeepSleep();
    }
}

void app_flash_erase_block_4k(uint32_t address, bool wait)
{
    if (flash_type == FLASH_SST26)
        sst26_erase_sector(address, wait);
    else
    {
        if (wait)
            AT25D_Erase_Block_4k_Wait(address);
        else
            AT25D_Erase_Block_4k(address);
    }
}

void app_flash_read_data_16bit(uint32_t address, uint16_t *buff, uint32_t length)
{
    if (flash_type == FLASH_SST26)
    {
        sst26_read_16bit(address, buff, length);
    } else
    {
        AT25D_Read_Data_16bit(address, buff, length);
    }
}

void app_flash_read_data_8bit(uint32_t address, uint8_t *buff, uint32_t length)
{
    if (flash_type == FLASH_SST26)
        sst26_read(address, buff, length);
    else
    {
        AT25D_Read_Data_8bit(address, buff, length);
    }
}

void app_flash_write_data_16bit(uint32_t address, uint16_t *buff, uint32_t length, bool wait)
{
    if (flash_type == FLASH_SST26)
    {
        sst26_write_16bit(address, buff, length, wait);
    } else
    {
        if (wait)
            AT25D_Write_Page_16bit_Wait(address, buff, length);
        else
            AT25D_Write_Page_16bit(address, buff, length);
    }
}

void app_flash_write_data_8bit(uint32_t address, uint8_t *buff, uint32_t length, bool wait)
{
    if (flash_type == FLASH_SST26)
    {
        sst26_write(address, buff, length, wait);
    } else
    {
        if (wait)
            AT25D_Write_Page_8bit_Wait(address, buff, length);
        else
            AT25D_Write_Page_8bit(address, buff, length);
    }
}

void app_flash_write_data_8bit_verify(uint32_t address, uint8_t *buff, uint32_t length)
{
#if (defined TESTING_CONFIG && TESTING_CONFIG != 0)
    uint8_t verify_buff[0x80] = {0};
    uint16_t i;
    uint16_t verify_len = (length / 8 < 0x80 ? length / 8 : 0x80);
    app_flash_write_data_8bit(address, buff, length, true);
    app_flash_read_data_8bit(address, verify_buff, verify_len);
    for (i = 0; i < verify_len; i++)
    {
        if (buff[i] != verify_buff[i])
        {
            stub();
            return;
        }
    }
#else
    app_flash_write_data_8bit(address, buff, length, true);
#endif
}


void app_flash_rewrite_data_8bit_verify(uint32_t address, uint8_t *buff, uint32_t length)
{
#if (defined TESTING_CONFIG && TESTING_CONFIG != 0)
    uint8_t verify_buff[0x80] = {0};
    uint16_t i;
    uint16_t verify_len = (length / 8 < 0x80 ? length / 8 : 0x80);
    app_flash_write_data_8bit(address, buff, length, true);
    app_flash_read_data_8bit(address, verify_buff, verify_len);
    for (i = 0; i < verify_len; i++)
    {
        if (buff[i] != verify_buff[i])
        {
            stub();
            return;
        }
    }
#else
    app_flash_write_data_8bit(address, buff, length, true);
#endif
}
