/**********************************************************************************
 *	File:	k60n_adc.h
 **********************************************************************************/
#ifndef __K60N_ADC_H_
#define __K60N_ADC_H_

#include <stdint.h>
#ifdef MDCR_interface
#include "MK60F15.h"


/// ADC_defines:
#define ADC_8bit_MODE			0x00
#define ADC_12bit_MODE			0x01
#define ADC_10bit_MODE			0x02
#define ADC_16bit_MODE			0x03

#define ADC_Bus_Clock			0x00
#define ADC_Bus_Clock_Dev_2		0x01
#define ADC_ALTCLK_USE			0x02
#define ADC_ADACK_USE			0x03

void ADCConfigure(ADC_Type* ADC);
void PDBConfigure(void);
void ADC_Start(unsigned int DelayAfter);
void mdcr_set_frequency(uint16_t freq);
uint16_t mdcr_get_frequency(void);
void ADC_Stop(void);
void emptiedADCQueues(void);
#endif

void saadc_sampling_event_init();
void saadc_init();
void saadc_sampling_event_enable();

#endif
