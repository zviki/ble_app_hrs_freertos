/***********************************************************
 *	File: 			BT43.c
 *	Description:	
 ***********************************************************/

#define __C_BT43_

#include "header.h"
#include "k60n_uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "k60n_gpio.h"
#include "BT43.h"
#include "mdcr_func.h"
#include "mdcr_protocol.h"

static int bt43InitState = 0;
static int recvCmdCode = eUndefined;
static int stageInit = 0;

#define AnswerBT_sizebuf 100
char AnswerBT[AnswerBT_sizebuf]; //buff for resived command
unsigned short AnswerBT_len;

uMetaBT43Element BT43DataFlag;
extern uMetaSwElement SwDataFlag;
extern uTimersDataElement TimersDataFlag;
extern xTimerHandle xOneShotTimers[NUMBER_OF_TIMERS];
char PIN_BT[4];
char MAC_BT[12];

//---------------------------------------------------------------
//---------------------------------------------------------------

unsigned short printf_to_mas(char * buff)
{
    unsigned short i;

    memset(AnswerBT, 0, AnswerBT_sizebuf); //clear

    AnswerBT_len = strlen(buff); //calk size and save
    for (i = 0; i < AnswerBT_len; i++)
    {
        AnswerBT[i] = buff[i]; //set mas 
    }

    return AnswerBT_len;
}


//---------------------------------------------------------------
//---------------------------------------------------------------

static int BT53_Configuration(int cmdCommand)
{
    int status = 0;
    if (cmdCommand == eOK)
    {
        switch (stageInit)
        {
            case 0:
//                UART0_printf("AT+AB config StreamingSerial = false\r\n"); //RST/CTS flow control is enabled.
//                printf_to_mas("AT-AB ConfigOk\r\n");
                break;

            case 1:
                UART0_printf("AT+AB reset\r\n");
                printf_to_mas("AT-AB ResetPending\r\n");
                break;

            case 2:
                UART0_printf("AT+AB config DeviceName = MDCRv3\r\n");
                printf_to_mas("AT-AB ConfigOk\r\n");
                break;

            case 3:
                UART0_printf("AT+AB reset\r\n");
                printf_to_mas("AT-AB ResetPending\r\n");
                break;


            case 4:
                UART0_printf("AT+AB config MITMEvent = true\r\n"); //Enables sniff mode. Must be False when no 32.768KHz
                printf_to_mas("AT-AB ConfigOk\r\n");
                break;

            case 5:
                UART0_printf("AT+AB reset\r\n");
                printf_to_mas("AT-AB ResetPending\r\n");
                break;

                //-------			
            case 6:
                UART0_printf("AT+AB config COD = 502918\r\n");
                printf_to_mas("AT-AB ConfigOk\r\n");
                break;

            case 7:
                UART0_printf("AT+AB reset\r\n");
                printf_to_mas("AT-AB ResetPending\r\n");
                break;

            case 8:
                UART0_printf("AT+AB config UartTimeout = 8\r\n");
                printf_to_mas("AT-AB ConfigOk\r\n");
                break;
            case 9:
                UART0_printf("AT+AB config StreamingSerial = false\r\n"); //RST/CTS flow control is enabled.
                printf_to_mas("AT-AB ConfigOk\r\n");
                break;
            case 10:
                //				UARTx_Init(UART0, SystemCoreClock/1000, 57600);				
                status = 2;
                stageInit = -1;
                LED1_RED_CLR;
                LED2_RED_CLR;
                LED2_GR_SET;
                LED1_YW_SET;
                break;
        }
        stageInit += 1;
    }
    return status;
}

                /*														
                //-------			

                                        case 4:
                                        UART0_printf("AT+AB config CreditMax = 7\r\n");				
                                        printf_to_mas("AT-AB ConfigOk\r\n");						
                                        break;
			
                                                                        case 5:
                                                                                UART0_printf("AT+AB reset\r\n");
                                                                        printf_to_mas("AT-AB ResetPending\r\n");									
                                                                        break;		
                //-------			
                                        case 6:
                                        UART0_printf("AT+AB config AccName = MDCRv3\r\n");				
                                        printf_to_mas("AT-AB ConfigOk\r\n");						
                                        break;

                                                                        case 7:
                                                                                UART0_printf("AT+AB reset\r\n");
                                                                        printf_to_mas("AT-AB ResetPending\r\n");									
                                                                        break;		
                //-------							
                                        case 8:
                                        UART0_printf("AT+AB config AccManufacturer = MDCRv3\r\n");				
                                        printf_to_mas("AT-AB ConfigOk\r\n");						
                                        break;
			
                                                                        case 9:
                                                                                UART0_printf("AT+AB reset\r\n");
                                                                        printf_to_mas("AT-AB ResetPending\r\n");									
                                                                        break;		
                //-------
                                        case 10:
                                        UART0_printf("AT+AB config AccModelNumber = 2.1\r\n");				
                                        printf_to_mas("AT-AB ConfigOk\r\n");						
                                        break;

                                                                        case 11:
                                                                                UART0_printf("AT+AB reset\r\n");
                                                                        printf_to_mas("AT-AB ResetPending\r\n");									
                                                                        break;		
                //-------
                                        case 12:
                                        UART0_printf("AT+AB config EnableIAP = true\r\n");				
                                        printf_to_mas("AT-AB ConfigOk\r\n");						
                                        break;
                                                                        case 13:
                                                                                UART0_printf("AT+AB reset\r\n");
                                                                        printf_to_mas("AT-AB ResetPending\r\n");									
                                                                        break;									
                //-------							
                                        case 14:
                                        UART0_printf("AT+AB config iAPAppIDStr = com.mdcr.Demo\r\n");				
                                        printf_to_mas("AT-AB ConfigOk\r\n");						
                                        break;

                                                                        case 15:
                                                                                UART0_printf("AT+AB reset\r\n");
                                                                        printf_to_mas("AT-AB ResetPending\r\n");									
                                                                        break;									
                //-------
                                        case 16:
                                        UART0_printf("AT+AB config iAPProtocolStrMain = com.mdcr.Demo\r\n");				
                                        printf_to_mas("AT-AB ConfigOk\r\n");						
                                        break;

                                                                        case 17:
                                                                                UART0_printf("AT+AB reset\r\n");
                                                                        printf_to_mas("AT-AB ResetPending\r\n");									
                                                                        break;									
                //-------							
                                        case 18:
                                        UART0_printf("AT+AB config iAPProtocolStrAlt = com.mdcr.ProtocolAl\r\n");				
                                        printf_to_mas("AT-AB ConfigOk\r\n");						
                                        break;

                                                                        case 19:
                                                                                UART0_printf("AT+AB reset\r\n");
                                                                        printf_to_mas("AT-AB ResetPending\r\n");									
                                                                        break;		
                //-------														
                                        case 20:
                                        UART0_printf("AT+AB config AccSerialNumber = 1.0\r\n");				
                                        printf_to_mas("AT-AB ConfigOk\r\n");						
                                        break;

                                                                        case 21:
                                                                                UART0_printf("AT+AB reset\r\n");
                                                                        printf_to_mas("AT-AB ResetPending\r\n");									
                                                                        break;		
                //-------
                                        case 22:
                                        UART0_printf("AT+AB config CpuMHz = 12\r\n");				//8MHz default
                                        printf_to_mas("AT-AB ConfigOk\r\n");				
                                        break;

                                                                        case 23:
                                                                                UART0_printf("AT+AB reset\r\n");
                                                                        printf_to_mas("AT-AB ResetPending");									
                                                                        break;		
                //-------
                                        case 24:
                                        UART0_printf("AT+AB config RmtEscapeSequence = false\r\n");				//Remote escape sequence enabled: @#@$@%
                                        printf_to_mas("AT-AB ConfigOk\r\n");				
                                        break;
			
                                                                        case 25:
                                                                                UART0_printf("AT+AB reset\r\n");
                                                                        printf_to_mas("AT-AB ResetPending\r\n");									
                                                                        break;		
							
                //-------
                                        case 26:
                                        UART0_printf("AT+AB config AllowSniff = true\r\n");				//Enables sniff mode. Must be False when no 32.768KHz
                                        printf_to_mas("AT-AB ConfigOk\r\n");				
                                        break;
			
                                                                        case 27:
                                                                                UART0_printf("AT+AB reset\r\n");
                                                                        printf_to_mas("AT-AB ResetPending\r\n");									
                                                                        break;		

                                        case 28:
                                        UART0_printf("AT+AB config StreamingSerial = false\r\n");				//RST/CTS flow control is enabled.
                                        printf_to_mas("AT-AB ConfigOk\r\n");				
                                        break;
									
                                        case 29:
                                                        UART0_printf("AT+AB reset\r\n");
                                                        printf_to_mas("AT-AB ResetPending\r\n");									
				
                //				PTD->PSOR |= (1<<14);						// power off to BT
                //				vTaskDelay(100);		
                //				PTD->PCOR |= (1<<14);						// power on to BT
                //				printf_to_mas("AT-AB BDAddress\r\n");						
                                        break;
			
                                        case 30:
                                                UART0_printf("AT+AB LocalName MDCRv3\r\n");
                                                printf_to_mas("AT-AB LocalNameOk\r\n");						
                                        break;

                                        case 31:
                                                UART0_printf("AT+AB config BD_ADDR\r\n");	//Read local MAC BT Address
                                                printf_to_mas("var02 BD_ADDR");									
                                        break;

                                        case 32:
                                                UART0_printf("AT+AB config PIN\r\n");	//Read PIN Code BT 
                                                printf_to_mas("var05 PIN");									
                                        break;
																	
                 */
extern uMetaSwElement SwDataFlag;

//--------------------------------------------------------------------

static int CheckBtnMITMEvent(void)
{
    int result = 0;
    //	int	len;
    int i;

    TimersDataFlag.BIT.TIMER_2 = 0;
    if (xTimerReset(xOneShotTimers[ID_TIMER_2], 0) != pdPASS)
    {
        // The timer could not be set into the Active state.
    }
    for (i = 0; i < 0xFFFF; i++)
    {
    }; // for start timer
    while (!TimersDataFlag.BIT.TIMER_2)
    {
        vTaskDelay(2);
        KeyScan();
        if (SwDataFlag.BIT.SW2_flag || SwDataFlag.BIT.SW1_flag)
        {
            xTimerStop(xOneShotTimers[ID_TIMER_2], 0);
            result = 1;
            return result;
        }
    };
    /*
            //-------------  ---------------------------
            if (!SwDataFlag.BIT.SW2_flag || SwDataFlag.BIT.SW3_flag)
            {
                    LED1_RED_TGL;
                    LED2_RED_TGL;
                    result = 0;
            }
     */
    return result;
}

//---------------------------------------------------------------
//	Description:	The function for decoding received data.
//---------------------------------------------------------------

int BT53_Decode(serial_port serial)
{
    unsigned int offset = 0;
    int status = 0;
    short i = 0;
    //	buf_st *p = NULL;


    if (serial.support_protocol == MDCRprotocol)
    {
        mdcr_decode_recv_command(serial);
    }else
        //	if(serial.support_protocol==BT43protocol)
    {

        while (offset < 5)
        {
            if (compare_string((char*) &serial.data[offset], "AT-AB", strlen("AT-AB"))) break; //if anser have AT-AB
            else if (compare_string((char*) &serial.data[offset], "var", strlen("var"))) break; //if anser have "var"
            offset++;
        }
        if (offset > 4) offset = 0;

        if ((compare_string((char*) &serial.data[offset], "AT-AB ConnectionUp", strlen("AT-AB ConnectionUp"))) || (compare_string((char*) &serial.data[offset], "AT-AB -BLE-ConnectionUp", strlen("AT-AB -BLE-ConnectionUp"))))
        {
            //				p->in = 0;
            //				p->out = p->in;
            //				memset(serial.data,0,RX_BUFFER_SIZE);
            serial.support_protocol = MDCRprotocol;
            Uart0Protocol = MDCRprotocol;
        }
        else
            if (compare_string((char*) &serial.data[offset], "var05 PIN", strlen("var05 PIN")))
        {
            for (i = 0; i < 4; i++)
                PIN_BT[i] = serial.data[serial.data_size - 6 + i]; //+2byte  "\r\n" at the end of data
        }
        else
            if (compare_string((char*) &serial.data[offset], "var02 BD_ADDR", strlen("var02 BD_ADDR")))
        {
            for (i = 0; i < 12; i++)
                MAC_BT[i] = serial.data[serial.data_size - 14 + i]; //+2byte  "\r\n" at the end of data
        }else
            if (compare_string((char*) &serial.data[offset], "BDAddress", strlen("BDAddress")))
        {
            for (i = 0; i < 12; i++)
                MAC_BT[i] = serial.data[serial.data_size - 14 + i]; //+2byte  "\r\n" at the end of data
        }



        //		else

        /*		if(compare_string((char*)&serial.data[offset], "AT-AB BondPending", strlen("AT-AB BondPending")))					
                        {
                                if(CheckBtnMITMEvent())  																					//if pressed
                                                UART0_printf("AT+AB PassKeyAccept y\r\n");										//Ok PIN Code BT 
				
                        }	
         */

        if (compare_string((char*) &serial.data[offset], "AT-AB PassKeyConfirmReq", strlen("AT-AB PassKeyConfirmReq")))
        {
            if (CheckBtnMITMEvent()) //if pressed
                UART0_printf("AT+AB PassKeyAccept y\r\n"); //Ok PIN Code BT 

        }

        else

            //		if(compare_string((char*)&serial.data[offset], AnswerBT, AnswerBT_len))
            if (compare_string((char*) &serial.data[offset], AnswerBT, AnswerBT_len))
            //		if(compare_string((char*)&serial.data[offset], "OK", strlen("OK")))			
        {
            recvCmdCode = eOK;
            //			 ("BT43: eOK\r\n");
        }



        /*		else
                        if(compare_string((char*)&serial.data[offset], "AT-AB SPPConnectionClosed", strlen("AT-AB SPPConnectionClosed")))					
                        {
                                        serial.support_protocol=BT43protocol;
                                        Uart0Protocol = BT43protocol;				
                        }		
         */

        if (bt43InitState == 0) //not yet init
        {
            status = BT53_Configuration(recvCmdCode);
            if (status == 2) // init BT ?	
            {
                bt43InitState = 1; //YES - finish init BT module
                serial.support_protocol = MDCRprotocol;
                //				Uart0Protocol = MDCRprotocol;		
                //				BT43DataFlag.ALL=0;								
                vTaskDelay(500);
                //				UARTx_Init(UART0, SystemCoreClock/1000, 115200);
                //			UART_DEBUG("BT43: Init SUCCESS - 115200\r\n");
            }
        }
    }
    return SUCCESS;
}
