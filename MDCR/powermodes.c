void clockMonitor(unsigned char state)
{
#ifdef MDCR_INTERFACE

    if(state)
      MCG_C6 |= MCG_C6_CME0_MASK;
    else
      MCG_C6 &= ~MCG_C6_CME0_MASK;
#endif
}