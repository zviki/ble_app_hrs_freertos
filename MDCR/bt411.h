#ifndef __H_BT411_APPFUNC_
#define __H_BT411_APPFUNC_

#ifdef __C_BT411_
	#define BT411_VAR volatile
#else
	#define BT411_VAR extern
#endif

//enum
//{
//	eUndefined=0,
//	eOK,
//	eATcmd,
//};

typedef struct _tsMetaBT411Element
{
	unsigned short Connected:	 1;							// PC - Dev connected = 1
	unsigned short dummy: 		15;
}tsMetaBT411Element;

// Meta Data Element
typedef union _uMetaBTElement411
{
	tsMetaBT411Element 	BIT;
	unsigned short 		ALL;
}uMetaBT411Element;

#endif	//	__H_BT411_APPFUNC_
