#ifndef APP_FLASH_H
#define APP_FLASH_H

#include <stdint.h>
#include <stdbool.h>
#include "mdcr_protocol.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef enum {
    FLASH_AT25D,
    FLASH_SST26
} app_flash_type_t;

void app_flash_init(void);

app_flash_type_t app_flash_get_type(void);

uint32_t app_flash_get_flash_len(void);
void app_flash_sleep(void);
void app_flash_wakeup(void);
void app_flash_erase_block_4k(uint32_t address, bool wait);

void app_flash_read_data_16bit(uint32_t address, uint16_t *buff, uint32_t length);
void app_flash_read_data_8bit(uint32_t address, uint8_t *buff, uint32_t length);

void app_flash_write_data_16bit(uint32_t address, uint16_t *buff, uint32_t length, bool wait);
void app_flash_write_data_8bit(uint32_t address, uint8_t *buff, uint32_t length, bool wait);

void app_flash_write_data_8bit_verify(uint32_t address, uint8_t *buff, uint32_t length);
    


#if 0

int8_t app_flash_header_write(const mdcr_flash_header_t *header);

int8_t app_flash_header_read(mdcr_flash_header_t *header);

int8_t app_flash_header_erase(void);

int8_t app_flash_acqdata_prepare_iteration_write(uint8_t iteration_number);
int8_t app_flash_acqdata_write_sample(mdcr_sample_t *sample);
int8_t app_flash_acqdata_write_samples(mdcr_sample_t *samples_buff, uint32_t samples_num);
int8_t app_flash_acqdata_complete_iteration_write(void);
int8_t app_flash_acqdata_repeat_current_iteration(void);

int8_t app_flash_acqdata_prepare_iteration_read(mdcr_test_num_t test_num, uint8_t iteration_number);
int8_t app_flash_acqdata_read_sample(mdcr_sample_t *sample);
int8_t app_flash_acqdata_read_samples(mdcr_sample_t *samples_buff, uint32_t samples_num, uint32_t samples_readed);
int8_t app_flash_acqdata_prepare_iteration_read_next(void);

#endif

#ifdef __cplusplus
}
#endif

#endif /* APP_FLASH_H */

