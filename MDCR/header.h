#ifndef __HEADER_H_
#define __HEADER_H_

//#include <MK60F15.h>
//#include <nrf52.h>
#include <FreeRTOSConfig.h>
#include <portmacro.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>

#include "write_to_flash_main_funcs.h"

#define OFF     (0)
#define ON      (1)
#define TESTING_CONFIG                  0

#if (TESTING_CONFIG == 0)
#define ENABLE_Check150HzNoise                              0
#define ENABLE_CheckAnalogSNS_IsraelDebug                   1
#define ENABLE_SW3_PIN_EQUAL_INVALID_SIGNAL_IsraelDebug     0
#define SKIP_SW_CONFIRMATION_ALL_TESTS_IsraelDebug          0
#define SKIP_SW_GREEN_CONFIRMATION_TEST1                    0
#define SKIP_INVALID_WAITING_IN_TEST1_IsraelDebug           0
#define DISABLE_FILTER                                      0
#define ENABLE_TestSignalADC                                0  //1 = enable 
#define NUM_OF_ITER_IN_APP_LESS_TEST                        8
#define NUM_OF_ITER_IN_APP_LESS_TEST2                       2
#else
#define ENABLE_Check150HzNoise                              0
#define ENABLE_CheckAnalogSNS_IsraelDebug                   0
#define ENABLE_SW3_PIN_EQUAL_INVALID_SIGNAL_IsraelDebug     1
#define SKIP_SW_CONFIRMATION_ALL_TESTS_IsraelDebug          1
#define SKIP_SW_GREEN_CONFIRMATION_TEST1                    0
#define SKIP_INVALID_WAITING_IN_TEST1_IsraelDebug           0
#define DISABLE_FILTER                                      0
#define ENABLE_TestSignalADC                                0
#define NUM_OF_ITER_IN_APP_LESS_TEST                        8
#define NUM_OF_ITER_IN_APP_LESS_TEST2                       2
#endif

#define ISRAEL_HEADER_DEFINE_SETTINGS                       1

#define ENABLE_TESTING_SIGNALS_Prev                         0
#define ENABLE_SW3_PIN_EQUAL_INVALID_SIGNAL_Prev            1
#define SKIP_INVALID_WAITING_IN_TEST1_Prev                  0
#define SKIP_SW_CONFIRMATION_ALL_TESTS_Prev                 0
#define ENABLE_CheckAnalogSNS_Prev                          1
#define CONFIG_STRESS_TEST_Prev                             0

#if (ISRAEL_HEADER_DEFINE_SETTINGS == 0)
#define SKIP_INVALID_WAITING_IN_TEST1   SKIP_INVALID_WAITING_IN_TEST1_Prev
#define DISABLE_SW_CONFIRMATION_Test1   SKIP_SW_CONFIRMATION_ALL_TESTS_Prev
#define DISABLE_SW_CONFIRMATION_Test2   SKIP_SW_CONFIRMATION_ALL_TESTS_Prev
#define DISABLE_SW_CONFIRMATION_Test3   SKIP_SW_CONFIRMATION_ALL_TESTS_Prev
#define ENABLE_SW3_PIN_EQUAL_INVALID_SIGNAL  ENABLE_SW3_PIN_EQUAL_INVALID_SIGNAL_Prev
#define CONFIG_TESTING_SIGNALS     ENABLE_TESTING_SIGNALS_Prev
#define CONFIG_STRESS_TEST      CONFIG_STRESS_TEST_Prev
#define CompressedFirmWare      0  //1 = enable - Compressed ENABLE
#define ENABLE_chargerLogic      0  //1 = enable 	
#define ENABLE_CheckAnalogSNS           ENABLE_CheckAnalogSNS_Prev
#define ENABLE_CheckDataValidSignal     0 //1 = enable 
#define ENABLE_CheckValidSignal2    0  //1 = enable 	
#define ENABLE_CheckValidSignal2_3    0 //1 = enable 
#else
#define SKIP_INVALID_WAITING_IN_TEST1               SKIP_INVALID_WAITING_IN_TEST1_IsraelDebug
#define DISABLE_SW_CONFIRMATION_Test1               SKIP_SW_CONFIRMATION_ALL_TESTS_IsraelDebug
#define DISABLE_SW_CONFIRMATION_Test2               SKIP_SW_CONFIRMATION_ALL_TESTS_IsraelDebug
#define DISABLE_SW_CONFIRMATION_Test3               SKIP_SW_CONFIRMATION_ALL_TESTS_IsraelDebug
#define ENABLE_SW3_PIN_EQUAL_INVALID_SIGNAL         ENABLE_SW3_PIN_EQUAL_INVALID_SIGNAL_IsraelDebug
#define CONFIG_TESTING_SIGNALS                      0
#define CONFIG_STRESS_TEST                          0
#define CompressedFirmWare                          0  //1 = enable - Compressed ENABLE
#define ENABLE_chargerLogic                         1  //1 = enable 	
#define ENABLE_CheckAnalogSNS                       ENABLE_CheckAnalogSNS_IsraelDebug
#define ENABLE_CheckDataValidSignal                 1 //1 = enable 
#define ENABLE_CheckValidSignal2                    0  //1 = enable 	
#define ENABLE_CheckValidSignal2_3                  1 //1 = enable
#endif

#define SIZE_ADC_QUEUE                  260
#define ENABLE_FreeRunADC               1
#define ENABLE_ExternalADC_Vcc          1  //1 = enable 	
#define QUANTITY_SAMPLES_PER_SEC        500
#define SampPerSecDev_GercelAVG         QUANTITY_SAMPLES_PER_SEC //500 samp/sec - ADC



#define DEFAULT_QUANTITY_SAMPLES_PER_SEC            500
#define PTT_DELAY_MS(ms)                ((ms) / portTICK_RATE_MS)

enum {
    BTM411_chip = 0,
    BT43_chip,
};
#define BTModule         BT43_chip

typedef enum {
    DISABLE = 0, ENABLE = !DISABLE
} FunctionalState;
#define IS_FUNCTIONAL_STATE(STATE) (((STATE) == DISABLE) || ((STATE) == ENABLE))

typedef enum {
    ERROR = 0, SUCCESS = !ERROR
} ErrorStatus;


#define NOT_COMPLETED  false
#define COMPLETED   true

#define ADD1SEC    true
#define DONTADD1SEC   false

#define UART_DEBUG  UART1_printf

#define NUMBER_OF_TIMERS    4
#define ID_TIMER_VAR        0UL
#define ID_TIMER_1          1UL
#define ID_TIMER_2          2UL
#define ID_TIMER_60sec      3UL

#define TIMER_VAR_DELAY     1000/ portTICK_PERIOD_MS
#define TIMER_1_DELAY       30000 / portTICK_PERIOD_MS
#define TIMER_2_DELAY       20000 / portTICK_PERIOD_MS
#define TIMER_60sec_DELAY   60000 / portTICK_PERIOD_MS


#ifdef  USE_FULL_ASSERT

/**
 * @brief  The assert_param macro is used for function's parameters check.
 * @param  expr: If expr is false, it calls assert_failed function
 *   which reports the name of the source file and the source
 *   line number of the call that failed. 
 *   If expr is true, it returns no value.
 * @retval None
 */
#define assert_param(expr) ((expr) ? (void)0 : assert_failed((uint8_t *)__FILE__, __LINE__))
/* Exported functions ------------------------------------------------------- */
void assert_failed(uint8_t* file, uint32_t line);
#else
#define assert_param(expr) ((void)0)
#endif /* USE_FULL_ASSERT */

//#define TEST_LEDS_BLINKING

#ifdef __C_MAIN_
#define MAIN_VAR
#else
#define MAIN_VAR extern
#endif

enum {
    BT43protocol = 0,
    USERprotocol,
    MDCRprotocol,
};

enum {
    eUSART0 = 0,
    eUSART1,
};

MAIN_VAR int Uart0Protocol;
MAIN_VAR int Uart1Protocol;

void GoToWait(void);
void ExitWait(void);

extern uint32_t ticks_ms;

#endif
