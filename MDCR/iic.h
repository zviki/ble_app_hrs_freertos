/*
 * File:		iic.h
 * Purpose:     
 *
 * Notes:
 */

#ifndef __IIC_H__
#define __IIC_H__

//#include "common.h"
//#include "LTC2941_i2c.h"



/********************************************************************/

#define i2c_DisableAck()       I2C0->C1 |= I2C0_C1_TXAK_MASK

#define i2c_SendAck()          I2C0->SMB |= I2C0_SMB_FACK_MASK; \
	                           I2C0->C1  &= ~I2C0_C1_TXAK_MASK     

#define i2c_RepeatedStart()    I2C0->C1 |= I2C0_C1_RSTA_MASK;

#define i2c_Start()            I2C0->C1 |= I2C0_C1_TX_MASK;\
                               I2C0->C1 |= I2C0_C1_MST_MASK;\
                               while((!(I2C0->S & I2C0_S_BUSY_MASK))       // wait until busy

#define i2c_Stop()             I2C0->C1 &= ~I2C0_C1_MST_MASK;\
                               I2C0->C1 &= ~I2C0_C1_TX_MASK;\
                               while(I2C0->S & I2C0_S_BUSY_MASK)        // wait until not busy

#define i2c_EnterRxMode()      I2C0->C1 &= ~I2C0_C1_TX_MASK;\
                               I2C0->C1 &= ~I2C0_C1_TXAK_MASK

#define i2c_Wait()             while((I2C0->S & I2C0_S_IICIF_MASK)==0); \
                               I2C0->S |= I2C0_S_IICIF_MASK

#define i2c_write_byte(data)   I2C0->D = data



typedef struct
{
	unsigned char RxBuff[10];
	unsigned char TxBuff[10];																													 
	unsigned int FrameLength;															 
}tsIICData;
extern tsIICData I2C0Data;
															 
															 
/*															 
unsigned int m_uiIIC_TxIndex;
unsigned int m_uiIIC_RxIndex;															 
*/
															 
extern void Init_I2C(void);
extern unsigned char I2C_Start(void);
extern unsigned char I2C_Stop(void);
extern unsigned char I2C_RepeatStart(void);
extern void I2C_Delay(void);
extern unsigned char I2C_CycleWrite(unsigned char bout);
extern unsigned char I2C_CycleRead(unsigned char ack);
															 
extern unsigned char  IIC_SendData(unsigned char *pData,unsigned int uiLength);
extern unsigned char  IIC_ReadData(unsigned char *pData,unsigned int uiLength);
															 
/********************************************************************/

#endif /* __IIC_H__ */
