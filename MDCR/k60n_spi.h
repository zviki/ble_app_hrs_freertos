/*******************************************************
 *	File:	k60n_spi.h
 *******************************************************/
#ifndef __K60N_SPI_H_
#define __K60N_SPI_H_

#include "header.h"

#define TX_FIFO_EMPTY       (0x2000000U)
#define RX_FIFO_EMPTY       (0x20000U)
#define SPI_TXBUFF_MASK     (0xFFF)
#define SPI_RXBUFF_MASK     (0xFFF)
#define TXBUFF_SIZE         (SPI_TXBUFF_MASK + 1)      // 4100- data + 400 headers
#define RXBUFF_SIZE         (SPI_RXBUFF_MASK + 1)

void SPI0_Init(void);
void SPI0_DeInit(void);

uint32_t SPI_Request_8bit(const uint8_t *buff_out, int buff_out_size, uint8_t *buff_in, int buff_in_size);

uint32_t SPI_Request_16bit(const uint8_t *buff_out, int out_length, uint16_t *buff_in, int in_length);

void SPI_Read_Data_8bit(uint8_t *buff, int size);

void SPI_Read_Data_16bit(uint16_t *buff, int size);

void SPI_Send_Push_Data_8bit(const uint8_t *buff, int size);
void SPI_Send_Finalize_Data_8bit(const uint8_t *buff, int size);

void SPI_Send_Push_Data_16bit(const uint16_t *buff, int size);
void SPI_Send_Finalize_Data_16bit(const uint16_t *buff, int size);

void SPI_Send_Data_8bit(const uint8_t *buff, int size);
void SPI_Send_Data_16bit(const uint16_t *buff, int size);

void SPI_Transfer_Data_8bit(const uint8_t *buff, int size);

void SPI_Transfer_Data_16bit(const uint16_t *buff, int size);

#endif //	__K60N_SPI_H__
