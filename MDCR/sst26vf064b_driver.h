#ifndef SST26VF064B_DRIVER_H
#define SST26VF064B_DRIVER_H


#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/** @defgroup SST26VF064B_ADDRESSES Address constants
  * @{
  */

#define SST26_ADDR_START            0x000000

#define SST26_ADDR_8K_LOW_START     0x000000
#define SST26_ADDR_8K_LOW_END       0x007FFF

#define SST26_ADDR_32K_LOW_START    0x008000
#define SST26_ADDR_32K_LOW_END      0x00FFFF

#define SST26_ADDR_64K_START        0x010000
#define SST26_ADDR_64K_END          0x7EFFFF

#define SST26_ADDR_32K_HIGHT_START  0x7F0000
#define SST26_ADDR_32K_HIGHT_END    0x7F7FFF

#define SST26_ADDR_8K_HIGHT_START   0x7F8000
#define SST26_ADDR_8K_HIGHT_END     0x7FFFFF

#define SST26_ADDR_END              0x7FFFFF

/** @} */ /* End of group SST26VF064B_ADDRESSES */

/** @defgroup SST26VF064B_Exported_Functions SST26VF064B Exported Functions
  * @{
  */

void sst26_init(void);
void sst26_erase_chip(bool wait);


void sst26_write_enable_check(void);

void sst26_erase_sector(uint32_t address, bool wait);
int8_t sst26_erase_block(uint32_t address, bool wait);
int8_t sst26_erase_block_8kb(uint32_t address, bool wait);
int8_t sst26_erase_block_32kb(uint32_t address, bool wait);
int8_t sst26_erase_block_64kb(uint32_t address, bool wait);


void sst26_write(uint32_t start_address, uint8_t *buff, int32_t len, bool wait);
void sst26_read(uint32_t start_address, uint8_t *buff, int32_t len);

void sst26_write_16bit(uint32_t start_address, uint16_t *buff, int32_t len, bool wait);
void sst26_read_16bit(uint32_t start_address, uint16_t *buff, int32_t len);
/**
  * @brief  Prepare sequential write. Should called once before writing a
  *         sequential data.
  * @param  start_address: address of the start to write.
  * @param  len: the length of the data to be written.
  * @retval None.
  */
void sst26_sequential_write_prepare(uint32_t start_address);

/**
  * @brief  Perform write piece of data.
  * @param  buff: data to be written.
  * @param  len: the length of the data to be written.
  * @retval None.
  */
int32_t sst26_sequential_write(uint8_t *buff, int32_t len, bool wait);
int32_t sst26_sequential_write_16bit(uint16_t *buff, int32_t len, bool wait);

/**
  * @brief  Prepare sequential read. Should called once before writing a
  *         sequential data.
  * @param  start_address: address of the start to read.
  * @retval None.
  */
void sst26_sequential_read_prepare(uint32_t start_address);

/**
  * @brief  Perform write piece of data.
  * @param  buff: buffer to put the data.
  * @param  len: the length of the data to be read.
  * @retval None.
  */
void sst26_sequential_read(uint8_t *buff, uint32_t len);

void sst26_sequential_read_16bit(uint16_t *buff, uint32_t len);

/** @} */ /* End of group SST26VF064B_Exported_Functions */

#ifdef __cplusplus
}
#endif

#endif /* SST26VF064B_DRIVER_H */

