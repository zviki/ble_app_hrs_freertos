//	File:	spi_memory.c
//
#include "header.h"
#include "k60n_spi.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
//#include "misc.h"
#include "AT25DFspi_memory.h"
//#include "k60n_uart.h"

/////////////////////////////////////////////////
/// Commands:

#define AT25_WRITE_STATUS_REG_BYTE1                     0x01
#define AT25_WRITE_STATUS_REG_BYTE2                     0x31
#define AT25_PROGRAM_PAGE                               0x02
#define AT25_WRITE_ENABLE                               0x06
#define AT25_WRITE_DISABLE                              0x04
#define AT25_PROTECT_SECTOR                             0x36
#define AT25_UPPROTECT_SECTOR                           0x39
#define AT25_READ_STATUS_REG                            0x05
#define AT25_READ_ARRAY_50MHZ                           0x03
#define AT25_READ_ARRAY_100MHZ                          0x1B
#define AT25_ERASE_BLOCK_4KB                            0x20
#define AT25_ERASE_BLOCK_32KB                           0x52
#define AT25_ERASE_BLOCK_64KB                           0xD8
#define AT25_PROGRAM_OTP_SECURITY_REG                   0x9B
#define AT25_READ_OTP_SECURITY_REG                      0x77
#define AT25_READ_STATUS_REG                            0x05
#define AT25_READ_SECTOR_LOCKDOWN_REG                   0x35
#define AT25_READ_SECTOR_PROTECT_REG                    0x3C
#define AT25_ERASE_CHIP                                 0x60
#define AT25_RESUME_FROM_DEEP_POWER_DOWN                0xAB
#define AT25_DEEP_POWER_DOWN                            0xB9
#define AT25_RESET                                      0xF0
#define AT25_RESET_CONF_BYTE                            0xD0

#define AT25_STATUS_REG_BYTE1_RDY_MASK                  (1 << 0)
#define AT25_STATUS_REG_BYTE1_WEL_MASK                  (1 << 1)
#define AT25_STATUS_REG_BYTE1_SWP_MASK                  ((1 << 2) | (1 << 3))
#define AT25_STATUS_REG_BYTE1_WPP_MASK                  (1 << 4)
#define AT25_STATUS_REG_BYTE1_EPE_MASK                  (1 << 5)
#define AT25_STATUS_REG_BYTE1_SPRL_MASK                 (1 << 7)

#define AT25_STATUS_REG_BYTE2_RDY_MASK                  (1 << 0)
#define AT25_STATUS_REG_BYTE2_SLE_MASK                  (1 << 3)
#define AT25_STATUS_REG_BYTE2_RSTE_MASK                 (1 << 4)


#define AT25_STATUS_REG_RDY1_MASK                 (1 << 0)
#define AT25_STATUS_REG_SLE_MASK                  (1 << 3)
#define AT25_STATUS_REG_RSTE_MASK                 (1 << 4)
#define AT25_STATUS_REG_RDY2_MASK                 (1 << 8)
#define AT25_STATUS_REG_WEL_MASK                  (1 << 9)
#define AT25_STATUS_REG_SWP_MASK                  ((1 << 10) | (1 << 11))
#define AT25_STATUS_REG_WPP_MASK                  (1 << 12)
#define AT25_STATUS_REG_EPE_MASK                  (1 << 13)
#define AT25_STATUS_REG_SPRL_MASK                 (1 << 15)


#define DELAY_ERASE_CHIP                30000   //30SEC!!
#define DELAY_ERASE_4KB                 200
#define DELAY_ERASE_32KB                600
#define DELAY_ERASE_64KB                950
#define DELAY_COMMAND                   1
#define DELAY_WRITE_PAGE                3

#define AT25_WritePageMAX               256     //size of page byte (8bit)

#define FLASH_MASK                      0xFFFFF       // for max address flash
#define FLASH_MASK_page                 0xFFF00       // for max address flash
#define FLASH_MASK_page4k               0xFF000       // for address flash
/////////////////////////////////////////////////

#define MASS_LEN(mass)          (sizeof(mass)/sizeof(mass[0]))


static void AT25D_Send_Cmd_Addr(uint8_t cmd, uint32_t address)
{
    uint8_t temp[4];
    temp[0] = cmd;
    temp[1] = ((address >> 16) & 0xFF);
    temp[2] = ((address >> 8)  & 0xFF);
    temp[3] = ((address >> 0)  & 0xFF);
    SPI_Send_Data_8bit(temp, 4);	
}

void AT25D_Send_Cmd(uint8_t cmd)
{
    uint8_t temp[4];
    temp[0] = cmd;
    SPI_Send_Data_8bit(temp, 4);	
}

at25d_status_reg_t AT25D_Read_Status_Reg(void)
{
    at25d_status_reg_t reg;
    uint8_t cmd[1] = {AT25_READ_STATUS_REG};
    uint8_t answer[2];
    SPI_Request_8bit(cmd, 1, answer, 2);
    reg.byte1 = *((at25d_status_reg_byte1_t*)&answer[0]);
    reg.byte2 = *((at25d_status_reg_byte2_t*)&answer[1]);
    return reg;
}

void AT25D_Wait_Wel(void)
{
    while (!AT25D_Read_Status_Reg().byte1.bitfield.wel);
}

static void AT25D_Wait_Rdy(void)
{
    while (AT25D_Read_Status_Reg().byte1.bitfield.busy);
}

void AT25D_GoDeepSleep(void)
{
    uint8_t temp[1];
    temp[0] = AT25_DEEP_POWER_DOWN;
    SPI_Send_Data_8bit(temp, 1);
}

void AT25_ExitDeepSleep(void)
{
    uint8_t temp[1];
    temp[0] = AT25_RESUME_FROM_DEEP_POWER_DOWN;
    SPI_Send_Data_8bit(temp, 1);
}

void AT25D_Write_Enable(void)
{
    uint8_t temp[1];
    temp[0] = AT25_WRITE_ENABLE;
    SPI_Send_Data_8bit(temp, 1);
    while (!AT25D_Read_Status_Reg().byte1.bitfield.wel)
    {
        SPI_Send_Data_8bit(temp, 1);
    }
    
}

void AT25D_Write_Status_Reg_B1(at25d_status_reg_byte1_t val)
{
    uint8_t temp[2];
    AT25D_Write_Enable();
    temp[0] = AT25_WRITE_STATUS_REG_BYTE1;
    temp[1] = *((uint8_t*)&val);
    SPI_Request_8bit(temp, 2, NULL, 0);
}

bool AT25D_Check_Chip_SPRL(void)
{
    bool res;
    at25d_status_reg_byte1_t val;
    val.byte=0;
    val.bitfield.sprl = 1;
    
    AT25D_Write_Status_Reg_B1(val);
    
    vTaskDelay(1 / portTICK_RATE_MS);
    
    if (AT25D_Read_Status_Reg().byte1.bitfield.sprl)
    {
        res = true;
        val.byte = 0;
        AT25D_Write_Status_Reg_B1(val);
    }else
    {
        res = false;
    }
    return res;
}

void AT25D_Write_Status_Reg_B2(at25d_status_reg_byte2_t val)
{
    uint8_t temp[2];
    AT25D_Write_Enable();
    temp[0] = AT25_WRITE_STATUS_REG_BYTE2;
    temp[1] = *((uint8_t*)&val);
    SPI_Request_8bit(temp, 2, NULL, 0);
}

void AT25D_Write_Status_Reg(at25d_status_reg_t reg)
{
    AT25D_Write_Status_Reg_B1(reg.byte1);
    AT25D_Write_Status_Reg_B2(reg.byte2);
}

void AT25D_Reset(void)
{
    uint8_t rst_cmd[2] = {AT25_RESET, AT25_RESET_CONF_BYTE};
    at25d_status_reg_byte2_t byte2 = {0};
    byte2.bitfield.rste = 1;
    AT25D_Write_Status_Reg_B2(byte2);
    AT25D_Write_Enable();
    AT25D_Wait_Rdy();
    do{
        SPI_Request_8bit(rst_cmd, 2, NULL, 0);
    }while (AT25D_Read_Status_Reg().byte1.bitfield.wel == 0x01);    // The WEL, PS, and ES bits, will be reset back to their default states
}

void AT25D_Init(void)
{
    at25d_status_reg_byte1_t byte1;
    byte1.byte = 0;
    AT25D_Write_Enable();
    while (!AT25D_Read_Status_Reg().byte1.bitfield.wel);
    AT25D_Write_Status_Reg_B1(byte1);
}

void AT25D_Erase_Block_4k(uint32_t address)
{
    AT25D_Write_Enable();
    AT25D_Send_Cmd_Addr(AT25_ERASE_BLOCK_4KB, address);
}

void AT25D_Erase_Block_32k(uint32_t address)
{
    AT25D_Write_Enable();
    AT25D_Send_Cmd_Addr(AT25_ERASE_BLOCK_32KB, address);
}

void AT25D_Erase_Block_64k(uint32_t address)
{
    AT25D_Write_Enable();
    AT25D_Send_Cmd_Addr(AT25_ERASE_BLOCK_64KB, address);
}

void AT25D_Read_Data_8bit(uint32_t address, uint8_t *data, uint32_t length)
{
    uint8_t cmd[6] = {0};
    cmd[0] = AT25_READ_ARRAY_100MHZ;
    cmd[1] = ((address >> 16) & 0xFF);
    cmd[2] = ((address >> 8)  & 0xFF);
    cmd[3] = ((address >> 0)  & 0xFF);
    cmd[4] = 0;
    cmd[5] = 0;
    SPI_Request_8bit(cmd, MASS_LEN(cmd), data, length);
}

void AT25D_Read_Data_16bit(uint32_t address, uint16_t *data, uint32_t length)
{
    uint8_t cmd[6] = {0};
    cmd[0] = AT25_READ_ARRAY_100MHZ;
    cmd[1] = ((address >> 16) & 0xFF);
    cmd[2] = ((address >> 8)  & 0xFF);
    cmd[3] = ((address >> 0)  & 0xFF);
    cmd[4] = 0;
    cmd[5] = 0;
    SPI_Request_16bit(cmd, MASS_LEN(cmd), data, length);
}

void AT25D_Write_Page_16bit(uint32_t address, uint16_t *data, uint16_t length)
{
    uint8_t cmd[4] = {0};
    AT25D_Write_Enable();
    if (length > 128)
        length = 128;
    cmd[0] = AT25_PROGRAM_PAGE;
    cmd[1] = ((address >> 16) & 0xFF);
    cmd[2] = ((address >> 8)  & 0xFF);
    cmd[3] = ((address >> 0)  & 0xFF);
    SPI_Send_Push_Data_8bit(cmd, MASS_LEN(cmd));
    SPI_Transfer_Data_16bit(data, length);
}

void AT25D_Write_Page_16bit_Wait(uint32_t address, uint16_t *data, uint16_t length)
{
    AT25D_Write_Page_16bit(address, data, length);
    AT25D_Wait_Ready_Flag(0);
}

void AT25D_Write_Page_8bit(uint32_t address, uint8_t* data, uint16_t length)
{
    uint8_t cmd[4] = {0};
    AT25D_Write_Enable();
    if (length > 256)
        length = 256;
    cmd[0] = AT25_PROGRAM_PAGE;
    cmd[1] = ((address >> 16) & 0xFF);
    cmd[2] = ((address >> 8)  & 0xFF);
    cmd[3] = ((address >> 0)  & 0xFF);
    SPI_Send_Push_Data_8bit(cmd, MASS_LEN(cmd));
    SPI_Transfer_Data_8bit(data, length);
}

void AT25D_ExitDeepSleep(void)
{
    AT25D_Init();
}

void AT25D_Write_Page_8bit_Wait(uint32_t address, uint8_t* data, uint16_t length)
{
    AT25D_Write_Page_8bit(address, data, length);
    AT25D_Wait_Ready_Flag(0);
}

void AT25D_Wait_Ready_Flag(uint32_t delay_ms)
{
    at25d_status_reg_t Status;
    do
    {
        Status = AT25D_Read_Status_Reg();
        if (!Status.byte2.bitfield.busy)
            return;
    } while (1);
}

void AT25D_Erase_Block_4k_Wait(uint32_t address)
{
    AT25D_Erase_Block_4k(address);
    AT25D_Wait_Ready_Flag(1);
}

void AT25D_Erase_Block_32k_Wait(uint32_t address)
{
    AT25D_Erase_Block_32k(address);
    AT25D_Wait_Ready_Flag(1);
}

void AT25D_Erase_Block_64k_Wait(uint32_t address)
{
    AT25D_Erase_Block_64k(address);
    AT25D_Wait_Ready_Flag(1);
}
