#ifndef _AT25DFSPI_MEMORY_H__
#define _AT25DFSPI_MEMORY_H__

#include <stdio.h>

#define AT25D_ADDR_END              (0x400*0x400)

typedef union {
    uint8_t byte;
    struct {
        uint8_t busy : 1;
        uint8_t wel  : 1;
        uint8_t swp  : 2;
        uint8_t wpp  : 1;
        uint8_t epe  : 1;
        uint8_t res  : 1;
        uint8_t sprl : 1;
    }bitfield;    
}at25d_status_reg_byte1_t;

typedef union {
    uint8_t byte;
    struct {
        uint8_t busy : 1;
        uint8_t res1 : 2;
        uint8_t sle  : 1;
        uint8_t rste : 1;
        uint8_t res2 : 3;
    }bitfield;
}at25d_status_reg_byte2_t;


typedef struct {
    at25d_status_reg_byte1_t byte1;
    at25d_status_reg_byte2_t byte2;
}at25d_status_reg_t;


void AT25D_Init (void);
void AT25D_GoDeepSleep(void);
void AT25D_ExitDeepSleep(void);

bool AT25D_Check_Chip_SPRL(void);

at25d_status_reg_t AT25D_Read_Status_Reg(void);

void AT25D_Read_Data_16bit(uint32_t address, uint16_t *data, uint32_t length);
void AT25D_Read_Data_8bit(uint32_t address, uint8_t *data, uint32_t length);
void AT25D_write_data(uint32_t address, uint16_t *data, uint32_t length);

void AT25D_Erase_Block_4k(uint32_t address);
void AT25D_Erase_Block_32k(uint32_t address);
void AT25D_Erase_Block_64k(uint32_t address);

void AT25D_Erase_Block_4k_Wait(uint32_t address);
void AT25D_Erase_Block_32k_Wait(uint32_t address);
void AT25D_Erase_Block_64k_Wait(uint32_t address);

void AT25D_Write_Enable(void);

void AT25D_Write_Page_8bit(uint32_t address, uint8_t* data, uint16_t length);
void AT25D_Write_Page_8bit_Wait(uint32_t address, uint8_t* data, uint16_t length);

void AT25D_Write_Page_16bit(uint32_t address, uint16_t *data, uint16_t length);
void AT25D_Write_Page_16bit_Wait(uint32_t address, uint16_t *data, uint16_t length);

void AT25D_Wait_Ready_Flag(uint32_t delay_ms);

#endif // _AT25DFSPI_MEMORY_H__
