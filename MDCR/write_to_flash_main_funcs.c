#include "header.h"
#include "k60n_adc.h"
#include "k60n_uart.h"
//#include "misc.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "mdcr_func.h"
#include "app_flash.h"

#if ENABLE_TestSignalADC > 0 
extern unsigned short TestCounter;
#endif

extern xQueueHandle Queue_ADC0Buff;
extern xQueueHandle Queue_ADC1Buff;
extern xQueueHandle Queue_ADC2Buff;
extern xQueueHandle Queue_ADC3Buff;
#define FLASH_ADDRESS_START  (0x100) // If change - uncomment conditions in lines 72 and 77

#define ERASED     false
#define NOT_ERASED    true
#define VALID     true
#define INVALID     false
#define LEN_OF_OUTPUT_BLOCK  (0x400*3)
//bool listOfNotErazed4kBlocks[0x100] = {NOT_ERASED};
bool ReWritingAddressWasPrepared = false;
bool Last256bBlockIn4KBlockStartWasRewriting = false;
bool NeedToReWriteStart256bInLast4KBlock = true;
bool NeedToSaveBlockInLastIteration = true;
uint32_t NumError = 0;
uint32_t NumDoubleError = 0;
uint32_t PreviousAddress = 0;
uint32_t CurrentFlashAddress = FLASH_ADDRESS_START;
uint32_t StartOfLast4KBlock = 0;
uint32_t LastQuantityOfRecordedMeasurements = 0;

char *listOfNotErazed4kBlocks;
uint8_t *uchar_OutputDataBlock;
uint8_t *uchar_HeaderDataBlock;
uint8_t *uchar_DataBlockInStartOfLast4KBlock;
uint16_t *ushort_InputDataBlock;
uint16_t *ushort_InputDataBlockLocal;

void flashWriteMemoryAllocate(void)
{
    static bool MemoryWasAllocated = false;
    if (MemoryWasAllocated == false)
    {
        uchar_OutputDataBlock = (uint8_t *) pvPortMalloc((sizeof (uint8_t)*LEN_OF_OUTPUT_BLOCK));

        uchar_HeaderDataBlock = (uint8_t *) pvPortMalloc((sizeof (uint8_t)*0x102));

        uchar_DataBlockInStartOfLast4KBlock = (uint8_t *) pvPortMalloc((sizeof (uint8_t)*0x100));
        ushort_InputDataBlock = (uint16_t *) pvPortMalloc((sizeof (uint16_t)*0x102));
        ushort_InputDataBlockLocal = (uint16_t *) pvPortMalloc((sizeof (uint16_t)*0x102));

        listOfNotErazed4kBlocks = (char *) pvPortMalloc((sizeof (char)*0x100));
        MemoryWasAllocated = true;
    }
}

void prepareToRecordFlashHeader(void);

void initListOfNotErased4kBlocks(void)
{
    uint32_t i = 0;
    for (i = 0; i < 0x100; i++)
        listOfNotErazed4kBlocks[i] = NOT_ERASED;
}

void flashWriteTaskInit(void)
{
    NeedToReWriteStart256bInLast4KBlock = true;
    CurrentFlashAddress = 0;
    PreviousAddress = 0;
    flashWriteMemoryAllocate();
    initListOfNotErased4kBlocks();
    prepareToRecordFlashHeader();
    Last256bBlockIn4KBlockStartWasRewriting = false;
    memset(uchar_OutputDataBlock, 0, sizeof (uint8_t)*LEN_OF_OUTPUT_BLOCK);

    memset(ushort_InputDataBlock, 0, sizeof (uint16_t)*0x100);

    memset(uchar_HeaderDataBlock, 0xFF, sizeof (uint8_t)*0x100);
    memset(uchar_DataBlockInStartOfLast4KBlock, 0xFF, sizeof (uint8_t)*0x100);
}

void flashWriteTaskInitInStartup(void)
{
    flashWriteMemoryAllocate();
}

uint32_t getNumOfCurrent4KBBlock(uint32_t FlashAddressLocal)
{
    return (uint32_t) (FlashAddressLocal / 0x1000);
}

bool eraseStatusOfFlashAddress(uint32_t FlashAddressLocal)
{
    return (bool) listOfNotErazed4kBlocks[getNumOfCurrent4KBBlock(FlashAddressLocal)];
}

void setEraseStatus_ERASED(uint32_t FlashAddressLocal)
{
    if (/*FlashAddress >= FLASH_ADDRESS_START &&*/ FlashAddressLocal <= app_flash_get_flash_len())
        listOfNotErazed4kBlocks[getNumOfCurrent4KBBlock(FlashAddressLocal)] = ERASED;
}

void setEraseStatus_NOT_ERASED(uint32_t FlashAddressLocal)
{
    if (/*FlashAddress >= FLASH_ADDRESS_START &&*/ FlashAddressLocal <= app_flash_get_flash_len())
        listOfNotErazed4kBlocks[getNumOfCurrent4KBBlock(FlashAddressLocal)] = NOT_ERASED;
}

uint32_t getNextNotErasedAddress(uint32_t FlashAddressLocal)
{
    for (FlashAddressLocal = FlashAddressLocal - FlashAddressLocal % 0x1000; FlashAddressLocal < app_flash_get_flash_len(); FlashAddressLocal += 0x1000)
        if (eraseStatusOfFlashAddress(FlashAddressLocal) == NOT_ERASED)
        {
            return FlashAddressLocal;
        }
    return FlashAddressLocal;
}

bool checkValidSignal(uint16_t* pFIRBuffer, int numChannels)
{
    bool ValidityFlag = VALID;

#if ENABLE_CheckValidSignal2
    if (CheckValidSignal2(pFIRBuffer, numChannels))
        ValidityFlag = INVALID;
#endif 
#if ENABLE_CheckValidSignal2_3
    if (CheckValidSignal2(pFIRBuffer, numChannels) || CheckValidSignal3(pFIRBuffer, numChannels))
        ValidityFlag = INVALID;
#endif
#if ENABLE_CheckDataValidSignal == 0
    ValidityFlag = VALID; //????????? ???????? ???????
#endif

#if (ENABLE_SW3_PIN_EQUAL_INVALID_SIGNAL == 1)
    ValidityFlag = (bool)!((~(PTA->PDIR >> SW1)& 0x01UL));
#endif

    return ValidityFlag;
}

void eraseBlock4KBWithVerify(uint32_t FlashAddressForErase)
{
    app_flash_erase_block_4k(FlashAddressForErase, true);
}

void setLastWritingAddressAsERASED(void)
{
    setEraseStatus_ERASED(CurrentFlashAddress);
}

uint32_t getQuantityOfLastRecordedMeasurements(void)
{
    return LastQuantityOfRecordedMeasurements;
}

void saveStart256bInLast4kBlock(void)
{
    StartOfLast4KBlock = CurrentFlashAddress - CurrentFlashAddress % 0x1000;
    app_flash_read_data_8bit(StartOfLast4KBlock, uchar_DataBlockInStartOfLast4KBlock, 0x100);
}

void reWriteStart256bInLast4kBlock(void)
{
    if (app_flash_get_type() == FLASH_AT25D)
    {
        if (StartOfLast4KBlock == 0x000)
            app_flash_write_data_8bit_verify(StartOfLast4KBlock, uchar_HeaderDataBlock, 0x100);
        else
            app_flash_write_data_8bit_verify(StartOfLast4KBlock, (uint8_t *) uchar_DataBlockInStartOfLast4KBlock, 0x100);
    }
}

bool flashWriteMainFunc(enum FlashAddressShift AddressShifting, uint32_t NumOfMeasurements, uint32_t NumOfChannels, bool DisableInvalidCheckIn1SecAtStart)
{

    uint32_t NumOfCurMeasurement = 0;
    uint32_t CurrentPosInDataBlock = 0;
    //	static uint32_t CurrentPosInDataBlockMax = 0;
    ADCQueueStruct ADC_All_Buffer[4] = {0};
    bool DataTaked_Channel0 = false;
    bool DataTaked_Channel1 = false;
    bool DataTaked_Channel2 = false;
    bool DataTaked_Channel3 = false;
    bool AllDataAccepted = false;
    bool WasInvalid = false;
    if (AddressShifting == GoToStartOfFlashCard)
    {
        CurrentFlashAddress = FLASH_ADDRESS_START;
    } else if (AddressShifting == ReWriteLastIteration)
    {
        if (ReWritingAddressWasPrepared == true)
            CurrentFlashAddress = PreviousAddress;
        else
            while (true)
                ;
    }

    if (NumOfMeasurements == 0)
    {
        LastQuantityOfRecordedMeasurements = 0;
        return COMPLETED;
    }
    if (NeedToSaveBlockInLastIteration)
        saveStart256bInLast4kBlock();
    NumOfCurMeasurement = 0;
    CurrentPosInDataBlock = 0;
    ReWritingAddressWasPrepared = false;
    PreviousAddress = CurrentFlashAddress;
    LastQuantityOfRecordedMeasurements = 0;
    for (;;)
    {
        if (xQueueReceive(Queue_ADC0Buff, &ADC_All_Buffer[0], 0) == pdPASS)
            DataTaked_Channel0 = true;
        if (xQueueReceive(Queue_ADC1Buff, &ADC_All_Buffer[1], 0) == pdPASS)
            DataTaked_Channel1 = true;
        if (xQueueReceive(Queue_ADC2Buff, &ADC_All_Buffer[2], 0) == pdPASS)
            DataTaked_Channel2 = true;
        if (xQueueReceive(Queue_ADC3Buff, &ADC_All_Buffer[3], 0) == pdPASS)
            DataTaked_Channel3 = true;

        if (AllDataAccepted == false)
        {
            if (
                    ((NumOfChannels == 3) && DataTaked_Channel0 && DataTaked_Channel1 && DataTaked_Channel2)
                    ||
                    ((NumOfChannels == 4) && DataTaked_Channel0 && DataTaked_Channel1 && DataTaked_Channel2 && DataTaked_Channel3)
                    )
            {
                int BuffCounter = 0;
                uint16_t ChannelsBuff[4];
                for (BuffCounter = 0; BuffCounter < NumOfChannels; BuffCounter++)
                    ChannelsBuff[BuffCounter] = ADC_All_Buffer[BuffCounter].ADCxBuff;
                if (checkValidSignal(ChannelsBuff, NumOfChannels) == INVALID
#if (ENABLE_Check150HzNoise == 1)
                        || (GercelDetect150Hz((float) (ADC_All_Buffer[0].ADCxBuff >> 8)))
#endif
                        )
                {
                    WasInvalid = true;
                    AllDataAccepted = true;
                }
                if (NumOfCurMeasurement % 2000 == 0)
                {
                    char buff[2] = {DATA_CON, 0};
                    UART0SendData(buff, 2); //MeasContinue	
                }
                DataTaked_Channel0 = false;
                DataTaked_Channel1 = false;
                DataTaked_Channel2 = false;
                DataTaked_Channel3 = false;
#if (ENABLE_TestSignalADC == 0)
                uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) (((ADC_All_Buffer[0].ADCxBuff)&0xFF00) >> 8);
                uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) (((ADC_All_Buffer[0].ADCxBuff)&0x00FF) >> 0);
                uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) (((ADC_All_Buffer[1].ADCxBuff)&0xFF00) >> 8);
                uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) (((ADC_All_Buffer[1].ADCxBuff)&0x00FF) >> 0);
                uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) (((ADC_All_Buffer[2].ADCxBuff)&0xFF00) >> 8);
                uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) (((ADC_All_Buffer[2].ADCxBuff)&0x00FF) >> 0);
                if (NumOfChannels == 4)
                {
                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) (((ADC_All_Buffer[3].ADCxBuff)&0xFF00) >> 8);
                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) (((ADC_All_Buffer[3].ADCxBuff)&0x00FF) >> 0);
                }
#else
                {
                    static uint16_t test[4] = {0};

                    if (TestCounter % (2 * mdcr_get_frequency()) == 0)
                    {
                        if (test[0] == 0)
                            test[0] = 0xFFFF;
                        else
                            test[0] = 0;
                    }
                    TestCounter += 2;
                    
                    test[0] = test[0];
                    test[1] = TestCounter;
                    test[2] = TestCounter * 2;
                    test[3] = TestCounter * 4;
                    
//                    test[0] = 0xAAAA;
//                    test[1] = 0xFFFF;
//                    test[2] = 0xBBBB;
//                    test[3] = 0xFFFF;
                    
                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) ((test[0]&0xFF00) >> 8);
                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) ((test[0]&0x00FF) >> 0);
                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) ((test[1]&0xFF00) >> 8);
                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) ((test[1]&0x00FF) >> 0);
                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) ((test[2]&0xFF00) >> 8);
                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) ((test[2]&0x00FF) >> 0);
                    if (NumOfChannels == 4)
                    {
                        uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) ((test[3]&0xFF00) >> 8);
                        uchar_OutputDataBlock[CurrentPosInDataBlock++] = (uint8_t) ((test[3]&0x00FF) >> 0);
                    }
                    
//                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = 0x01;
//                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = 0x02;
//                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = 0x03;
//                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = 0x04;
//                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = 0x05;
//                    uchar_OutputDataBlock[CurrentPosInDataBlock++] = 0x06;
//                    if (NumOfChannels == 4)
//                    {
//                        uchar_OutputDataBlock[CurrentPosInDataBlock++] = 0x07;
//                        uchar_OutputDataBlock[CurrentPosInDataBlock++] = 0x08;
//                    }

                }

#endif
                //				if (CurrentPosInDataBlock > CurrentPosInDataBlockMax)
                //					CurrentPosInDataBlockMax = CurrentPosInDataBlock;
                if (CurrentPosInDataBlock > LEN_OF_OUTPUT_BLOCK - 8)
                    CurrentPosInDataBlock = 0;
                NumOfCurMeasurement++;
                if (NumOfCurMeasurement == NumOfMeasurements)
                    AllDataAccepted = true;
            }
        }
        if (((CurrentPosInDataBlock > 0x400) || AllDataAccepted || (((getNextNotErasedAddress(CurrentFlashAddress) - CurrentFlashAddress) >= 6 * 0x1000) && (CurrentPosInDataBlock >= 0x100)))
                &&
                (eraseStatusOfFlashAddress(CurrentFlashAddress) == ERASED)
                )
        {
            app_flash_write_data_8bit_verify(CurrentFlashAddress, uchar_OutputDataBlock, CurrentPosInDataBlock);
            CurrentFlashAddress += CurrentPosInDataBlock;
            CurrentPosInDataBlock = 0;
            if (CurrentFlashAddress % 0x1000 <= 0x200)
                setEraseStatus_NOT_ERASED(CurrentFlashAddress - 0x202);
            if (AllDataAccepted && (CurrentPosInDataBlock == 0))
            {
                NeedToSaveBlockInLastIteration = true;
                reWriteStart256bInLast4kBlock();
                LastQuantityOfRecordedMeasurements = NumOfCurMeasurement;
                if (WasInvalid == false)
                    return COMPLETED;
                else
                    return NOT_COMPLETED;
            }
        } else if ((getNextNotErasedAddress(CurrentFlashAddress) - CurrentFlashAddress) < 6 * 0x1000)
        {
            uint32_t FlashAddressForErase = getNextNotErasedAddress(CurrentFlashAddress);
            eraseBlock4KBWithVerify(FlashAddressForErase);
            setEraseStatus_ERASED(FlashAddressForErase);
        }
        if (CurrentFlashAddress >= app_flash_get_flash_len())
        {
            CurrentFlashAddress = FLASH_ADDRESS_START;
        }
    }
}

bool recordMeasurementsToFlashCard(enum FlashAddressShift AddressShifting, uint32_t NumOfMeasurements, uint32_t NumOfChannels, bool Add1SecToFilterInit, bool DisableInvalidCheckIn1SecAtStart)
{
    bool Result = NOT_COMPLETED;
    if (Add1SecToFilterInit == ADD1SEC)
        NumOfMeasurements += mdcr_get_frequency();
    Result = flashWriteMainFunc(AddressShifting, NumOfMeasurements, NumOfChannels, DisableInvalidCheckIn1SecAtStart);
    return Result;
}

void prepareBlockToReWriting(uint32_t FlashAddressForPrepare)
{
    int CurrentShiftAddress = 0;
    uint32_t AddressStartBlock4KBForPrepare = 0x1000 * getNumOfCurrent4KBBlock(FlashAddressForPrepare);
    uint32_t AddressStartBlock4KBForTempSaving = 0x1000 * (getNumOfCurrent4KBBlock(FlashAddressForPrepare) + 1);
    uint32_t LenOfBlockToReWrite = 0;
    uint32_t CurrentReWriteBlockAddress = 0;
    uint32_t CurrentTempSaveBlockAddress = 0;
    int i = 0;
    uint16_t Temp = 0;

    if (FlashAddressForPrepare % 0x1000 == 0)
    {
        initListOfNotErased4kBlocks();
        eraseBlock4KBWithVerify(FlashAddressForPrepare);
        setEraseStatus_ERASED(FlashAddressForPrepare);
        return;
    }

    eraseBlock4KBWithVerify(AddressStartBlock4KBForTempSaving);

    for (CurrentShiftAddress = 0; CurrentShiftAddress < (FlashAddressForPrepare % 0x1000);)
    {
        if (FlashAddressForPrepare % 0x1000 - CurrentShiftAddress >= 0x100)
            LenOfBlockToReWrite = 0x100;
        else
            LenOfBlockToReWrite = FlashAddressForPrepare % 0x1000 - CurrentShiftAddress;

        CurrentReWriteBlockAddress = AddressStartBlock4KBForPrepare + CurrentShiftAddress;
        CurrentTempSaveBlockAddress = AddressStartBlock4KBForTempSaving + CurrentShiftAddress;

        app_flash_read_data_16bit(CurrentReWriteBlockAddress, ushort_InputDataBlock, LenOfBlockToReWrite / 2 + 2);
        for (i = 0; i < LenOfBlockToReWrite / 2; i++)
        {
            Temp = ushort_InputDataBlock[i];
            ushort_InputDataBlock[i] = (Temp & 0x00FF) << 8;
            ushort_InputDataBlock[i] |= (Temp & 0xFF00) >> 8;
        }
        app_flash_write_data_8bit_verify(CurrentTempSaveBlockAddress, (uint8_t *) ushort_InputDataBlock, LenOfBlockToReWrite);
        CurrentShiftAddress += LenOfBlockToReWrite;
    }

    eraseBlock4KBWithVerify(AddressStartBlock4KBForPrepare);

    for (CurrentShiftAddress = 0; CurrentShiftAddress < (FlashAddressForPrepare % 0x1000);)
    {
        if (FlashAddressForPrepare % 0x1000 - CurrentShiftAddress >= 0x100)
            LenOfBlockToReWrite = 0x100;
        else
            LenOfBlockToReWrite = FlashAddressForPrepare % 0x1000 - CurrentShiftAddress;

        CurrentReWriteBlockAddress = AddressStartBlock4KBForPrepare + CurrentShiftAddress;
        CurrentTempSaveBlockAddress = AddressStartBlock4KBForTempSaving + CurrentShiftAddress;

        app_flash_read_data_16bit(CurrentTempSaveBlockAddress, ushort_InputDataBlock, LenOfBlockToReWrite / 2 + 2);
        for (i = 0; i < LenOfBlockToReWrite / 2; i++)
        {
            Temp = ushort_InputDataBlock[i];
            ushort_InputDataBlock[i] = (Temp & 0x00FF) << 8;
            ushort_InputDataBlock[i] |= (Temp & 0xFF00) >> 8;
        }
        app_flash_write_data_8bit_verify(CurrentReWriteBlockAddress, (uint8_t *) ushort_InputDataBlock, LenOfBlockToReWrite);
        CurrentShiftAddress += LenOfBlockToReWrite;
    }
    eraseBlock4KBWithVerify(AddressStartBlock4KBForTempSaving);
    initListOfNotErased4kBlocks();
    setEraseStatus_ERASED(AddressStartBlock4KBForTempSaving);
    setEraseStatus_ERASED(FlashAddressForPrepare);
}

void prepareToRepeatLastIteration(void)
{
    NeedToSaveBlockInLastIteration = false;
    reWriteStart256bInLast4kBlock();
    prepareBlockToReWriting(PreviousAddress);
    ReWritingAddressWasPrepared = true;
    NeedToReWriteStart256bInLast4KBlock = false;
}

void prepareToRecordFlashHeader(void)
{
    memset(uchar_HeaderDataBlock, 0xFF, 0x100);
    CurrentFlashAddress = 0x000;
    StartOfLast4KBlock = 0x000;
    eraseBlock4KBWithVerify(CurrentFlashAddress);
    setEraseStatus_ERASED(0x000);
    CurrentFlashAddress += 0x100;
    PreviousAddress = CurrentFlashAddress;
}

void recordFlashHeader(tsTestParam *param)
{
    fStartFlashHeader(uchar_HeaderDataBlock, param);
    app_flash_write_data_8bit_verify(0x000, uchar_HeaderDataBlock, MDCR_FLASH_HEADER);
}

void recordThatTestWasDownloaded(void)
{
    uint16_t LenToRecord = MDCR_FLASH_HEADER;
    uint8_t dummyBlock[MDCR_FLASH_HEADER];
    int i = 0;
    for (i = 0; i < MDCR_FLASH_HEADER; i++)
    {
        dummyBlock[i] = 0xFF;
    }
    dummyBlock[90] = 0;
    dummyBlock[91] = 0;
    vTaskDelay(100);
    app_flash_write_data_8bit(0, dummyBlock, LenToRecord, true);
    vTaskDelay(100);
}
