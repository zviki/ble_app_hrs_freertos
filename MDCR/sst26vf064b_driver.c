/*!
	\brief SST26VF064B Flash Chip driver for K60P144M150SF3RM SPI Driver
	\author Ildar Sadykov
	\version 1.0
	\date February 2015
	\warning Only for K60P144M150SF3RM SPI Driver (MDCR Project)

	SST26VF064B Flash Chip driver
*/

#include "sst26vf064b_driver.h"
#include "k60n_spi.h"
#include "header.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
//#include "misc.h"


/** @defgroup SST26VF064B_DEV_OP_INSTRUCTION_CODES Device operation Instruction Codes
  * @{
  */

/** @defgroup CONFIGURATUION
  * @{
  */
#define SST26_CMD_NOP           0x00        /*!< No Operation */
#define SST26_CMD_RSTEN         0x66        /*!< Reset Enable */
#define SST26_CMD_RST           0x99        /*!< Reset Memory */
#define SST26_CMD_EQIO          0x38        /*!< Enable Quad I/O */
#define SST26_CMD_RSTQIO        0xFF        /*!< Reset Qiad I/O */
#define SST26_CMD_RDSR          0x05        /*!< Read Status Register */
#define SST26_CMD_WRSR          0x01        /*!< Write Status Register (Actually, Conf reg) */
#define SST26_CMD_RDCR          0x35        /*!< Read Configuration Register */
/** @} */ /* End of group CONFIGURATUION */

/** @defgroup READ
  * @{
  */
#define SST26_CMD_READ          0x03        /*!< Read Memory */
#define SST26_CMD_HSREAD        0x0B        /*!< Hight Speed Read */
#define SST26_CMD_SQOR          0x6B        /*!< Read Memory at Higher Speed */
#define SST26_CMD_SQIOR         0xEB        /*!< SPI Quad Output Read */
#define SST26_CMD_SDOR          0x3B        /*!< SPI Dual Output Read */
#define SST26_CMD_SDIOR         0xBB        /*!< SPI Dual I/O Read */
#define SST26_CMD_SB            0xC0        /*!< Set Burst Length */
#define SST26_CMD_RBSQI         0x0C        /*!< SQI Read Burst with Wrap */
#define SST26_CMD_RBSPI         0xEC        /*!< SPI Read Burst with Wrap */
/** @} */ /* End of group READ */

/** @defgroup IDENTIFICATION
  * @{
  */
#define SST26_CMD_JEDEC_ID      0x9F        /*!< JEDEC-ID Read */
#define SST26_CMD_QUAD_J_ID     0xAF        /*!< Quad I/O J-ID Read */
#define SST26_CMD_SFDP          0x5A        /*!< Serial Flash Discoverable Parameters */
/** @} */ /* End of group IDENTIFICATION */

/** @defgroup WRITE
  * @{
  */
#define SST26_CMD_WREN          0x06        /*!< Write Enable */
#define SST26_CMD_WRDI          0x04        /*!< Write Disable */
#define SST26_CMD_SE            0x20        /*!< Erase 4 KBytes of Memory Array */
#define SST26_CMD_BE            0xD8        /*!< Erase 64, 32 or 8 KBytes of Memory Array */
#define SST26_CMD_CE            0xC7        /*!< Erase Full Array */
#define SST26_CMD_PP            0x02        /*!< Page Program */
#define SST26_CMD_QUAD_PP       0x32        /*!< SQI Quad Page Program */
#define SST26_CMD_WRSU          0xB0        /*!< Suspends Program/Erase */
#define SST26_CMD_WRRE          0x30        /*!< Resumes Program/Erase */
/** @} */ /* End of group WRITE */

/** @defgroup PROTECTION
  * @{
  */
#define SST26_CMD_RBPR          0x72        /*!< Read Block-Protection Register */
#define SST26_CMD_WBPR          0x42        /*!< Write Block-Protection Register */
#define SST26_CMD_LBPR          0x7D        /*!< Lock Down Block-Protection Register */
#define SST26_CMD_NVWLDR        0xE8        /*!< non-Volatile Write Lock-Down Register */
#define SST26_CMD_ULBPR         0x98        /*!< Global Block Protection Unlock */
#define SST26_CMD_RSID          0x88        /*!< Read Security ID */
#define SST26_CMD_PSID          0xA5        /*!< Program User Security ID area */
#define SST26_CMD_LSID          0x85        /*!< Lockout Security ID Programming */
/** @} */ /* End of group PROTECTION */

/** @} */ /* End of group SST26VF064B_DEV_OP_INSTRUCTION_CODES */

/** @defgroup SST26VF064B_OTHER Miscellaneous defines
  * @{
  */

#define SST26_BLOCK_PROTECT_SIZE    18

#define SST26_PAGE_SIZE             0x100
#define SST26_WAIT_READY(wait)      do {            \
                                        if (wait)   \
                                            while(sst26_read_status_reg().bitfield.busy)    \
                                                vTaskDelay(1 / portTICK_RATE_MS);   \
                                    }while(0)

/** @} */ /* End of group SST26VF064B_OTHER */

/** @defgroup SST26VF064B_REG_STRUCTS Device registers structures
  * @{
  */

typedef union {
    uint8_t byte;
    struct {
        uint8_t busy  : 1;      /*!< Write operation status (RO):
                                 * @value 1 = Internal Write operation is in progress
                                 * @value 0 = No internal Write operation is in progress */
        uint8_t wel   : 1;      /*!< Write-Enable Latch status (RO):
                                 * @value 1 = Device is write-enabled
                                 * @value 0 = Device is not write-enabled */
        uint8_t wse   : 1;      /*!< Write Suspend-Erase status (RO):
                                 * @value 1 = Erase suspended
                                 * @value 0 = Erase is not suspended */
        uint8_t wsp   : 1;      /*!< Write Suspend-Program status (RO):
                                 * @value 1 = Program suspended
                                 * @value 0 = Program is not suspended */
        uint8_t wpld  : 1;      /*!< Write Protection Lock-Down status (RO):
                                 * @value 1 = Write Protection Lock-Down enabled
                                 * @value 0 = Write Protection Lock-Down disabled */
        uint8_t sec   : 1;      /*!< Security ID status (RO):
                                 * @value 1 = Security ID space locked
                                 * @value 0 = Security ID space not locked
                                 * @Note
                                 * The Security ID status will always be ‘1’ at power-up 
                                 * after a successful execution of the Lockout Security ID 
                                 * instruction, otherwise default at power-up is ‘0’. */
        uint8_t res   : 1;      /*!< Reserved for future use */
        uint8_t busy1 : 1;      /*!< Write operation status (RO):
                                 * @value 1 = Internal Write operation is in progress
                                 * @value 0 = No internal Write operation is in progress */
    }bitfield;    
}sst26_status_register_t;

typedef union {
    uint8_t byte;
    struct {
        uint8_t res0 : 1;       /*!< Reserved */
        uint8_t ioc  : 1;       /*!< I/O Configuration for SPI Mode (RW):
                                 * @value 1 = WP# and HOLD# pins disabled
                                 * @value 0 = WP# and HOLD# pins enabled
                                 * @Note
                                 * SST26VF064B default at Power-up is ‘0’.
                                 * SST26VF064BA default at Power-up is ‘1’. */
        uint8_t res1 : 1;       /*!< Reserved */
        uint8_t bpnv : 1;       /*!< Block-Protection Volatility State (R):
                                 * @value 1 = No memory block has been permanently locked
                                 * @value 0 = Any block has been permanently locked */
        uint8_t res2 : 3;       /*!< Reserved */
        uint8_t wpen : 1;       /*!< Write-Protection Pin (WP#) Enable (RW):
                                 * @value 1 = WP# enabled
                                 * @value 0 = WP# disabled
                                 * @Note
                                 * Factory default setting. 
                                 * This is a non-volatile bit; default at power-up will be 
                                 * the setting prior to power-down. */
    }bitfield;    
}sst26_conf_register_t;

/** @} */ /* End of group SST26VF064B_REG_STRUCTS */


static void sst26_send_cmd_addr(uint8_t cmd, uint32_t address)
{
    uint8_t temp[4];
    temp[0] = cmd;
    temp[1] = ((address >> 16) & 0xFF);
    temp[2] = ((address >> 8)  & 0xFF);
    temp[3] = ((address >> 0)  & 0xFF);
    SPI_Send_Data_8bit(temp, 4);	
}

static sst26_status_register_t sst26_read_status_reg(void)
{
    sst26_status_register_t reg;
    uint8_t cmd[1] = {SST26_CMD_RDSR};
    uint8_t answer[1];
    SPI_Request_8bit(cmd, 1, answer, 1);
    reg = *((sst26_status_register_t*)&answer[0]);
    return reg;
}

static void sst26_write_enable(void)
{
    uint8_t temp[1];
    temp[0] = SST26_CMD_WREN;
    SPI_Send_Data_8bit(temp, 1);
    while (!sst26_read_status_reg().bitfield.wel)
        SPI_Send_Data_8bit(temp, 1);
}

static void sst26_global_block_protect_unlock(void)
{
    uint8_t temp[1];
    sst26_write_enable();
    temp[0] = SST26_CMD_ULBPR;
    SPI_Send_Data_8bit(temp, 1);
}

sst26_conf_register_t sst26_read_conf_reg(void)
{
    sst26_conf_register_t reg;
    uint8_t cmd[1] = {SST26_CMD_RDCR};
    uint8_t answer[1];
    SPI_Request_8bit(cmd, 1, answer, 1);
    reg = *((sst26_conf_register_t*)&answer[0]);
    return reg;
}


void sst26_read_block_protect_reg(uint8_t * buff)
{
    uint8_t cmd[1] = {SST26_CMD_RBPR};
    SPI_Request_8bit(cmd, 1, buff, SST26_BLOCK_PROTECT_SIZE);
}

void sst26_write_conf_reg(sst26_conf_register_t val)
{
    uint8_t cmd[3] = {SST26_CMD_WRSR, 0};
    cmd[2] = *((uint8_t*)&val);
    sst26_write_enable();
    SPI_Request_8bit(cmd, 3, NULL, 0);
}

void sst26_init(void)
{
    uint8_t buff_bpr[SST26_BLOCK_PROTECT_SIZE];
    sst26_conf_register_t cr = {0};
    cr.bitfield.ioc = 1;
    sst26_write_conf_reg(cr);
    sst26_global_block_protect_unlock();
    SST26_WAIT_READY(true);
    sst26_write_enable();
    sst26_read_block_protect_reg(buff_bpr);
    sst26_write_enable();    
}

void sst26_erase_chip(bool wait)
{
    uint8_t cmd[1] = {SST26_CMD_CE};
    sst26_write_enable();
    SPI_Request_8bit(cmd, 1, NULL, 0);
    SST26_WAIT_READY(wait);
}


void sst26_erase_sector(uint32_t address, bool wait)
{
    SST26_WAIT_READY(true);
    sst26_write_enable();
    sst26_send_cmd_addr(SST26_CMD_SE, address);
    SST26_WAIT_READY(wait);
}

int8_t sst26_erase_block(uint32_t address, bool wait)
{
    if (address > SST26_ADDR_END)
        return -1;
    sst26_send_cmd_addr(SST26_CMD_BE, address);
    SST26_WAIT_READY(wait);
    return 0;
}

int8_t sst26_erase_block_8kb(uint32_t address, bool wait)
{
    if ((address > SST26_ADDR_8K_LOW_END && address < SST26_ADDR_8K_HIGHT_START) || 
        (address > SST26_ADDR_8K_HIGHT_END))
        return -1;
    sst26_erase_block(address, wait);
    SST26_WAIT_READY(wait);
    return 0;
}

int8_t sst26_erase_block_32kb(uint32_t address, bool wait)
{
    if ((address > SST26_ADDR_32K_LOW_END && address < SST26_ADDR_32K_HIGHT_START) || 
        (address < SST26_ADDR_32K_LOW_START || address > SST26_ADDR_32K_HIGHT_END))
        return -1;
    sst26_erase_block(address, wait);
    SST26_WAIT_READY(wait);
    return 0;
}

int8_t sst26_erase_block_64kb(uint32_t address, bool wait)
{
    if (address < SST26_ADDR_64K_START || address > SST26_ADDR_64K_END)
        return -1;
    sst26_erase_block(address, wait);
    SST26_WAIT_READY(wait);
    return 0;
}


static uint32_t address_write_current;
static uint32_t address_read_current;

/**
  * @brief  Prepare sequential write. Should called once before writing a
  *         sequential data.
  * @param  start_address: address of the start to write.
  * @param  len: the length of the data to be written.
  * @retval None.
  */
void sst26_sequential_write_prepare(uint32_t start_address)
{
    address_write_current = start_address;
}

/**
  * @brief  Perform write piece of data.
  * @param  buff: data to be written.
  * @param  len: the length of the data to be written.
  * @retval None.
  */
int32_t sst26_sequential_write(uint8_t *buff, int32_t len, bool wait)
{
    uint8_t cmdbuff[4];
    volatile uint32_t len_cur;
    volatile uint32_t len_start = len;
    volatile uint32_t buff_start_cur;
    cmdbuff[0] = SST26_CMD_PP;
    while (len)
    {
        SST26_WAIT_READY(true);
        sst26_write_enable();
        cmdbuff[1] = ((address_write_current >> 16) & 0xFF);
        cmdbuff[2] = ((address_write_current >> 8)  & 0xFF);
        cmdbuff[3] = ((address_write_current >> 0)  & 0xFF);
        SPI_Send_Push_Data_8bit(cmdbuff, 4);
        len_cur = SST26_PAGE_SIZE - (address_write_current%SST26_PAGE_SIZE);
        if (len_cur > len)
            len_cur = len;
        buff_start_cur = len_start - len;
        SPI_Send_Finalize_Data_8bit((buff + buff_start_cur), len_cur);
        address_write_current+= len_cur;
        len-= len_cur;
    }
    SST26_WAIT_READY(wait);
    return len_start - len_cur;
}

int32_t sst26_sequential_write_16bit(uint16_t *buff, int32_t len, bool wait)
{
    uint8_t cmdbuff[4];
    uint32_t len_piece;
    uint32_t len_start;
    uint8_t *buff8bit;
    uint32_t i;
    buff8bit = (uint8_t *)buff;
    cmdbuff[0] = SST26_CMD_PP;
    
    len*= 2;
    len_start = len;
    while (len)
    {
        SST26_WAIT_READY(true);
        sst26_write_enable();
        cmdbuff[1] = ((address_write_current >> 16) & 0xFF);
        cmdbuff[2] = ((address_write_current >> 8)  & 0xFF);
        cmdbuff[3] = ((address_write_current >> 0)  & 0xFF);
        SPI_Send_Push_Data_8bit(cmdbuff, 4);
        len_piece = SST26_PAGE_SIZE - (address_write_current%SST26_PAGE_SIZE);
        if (len_piece > len)
            len_piece = len;
        for (i = 0; i < (len_piece - 2); i+=2)
        {
            SPI_Send_Push_Data_8bit((buff8bit + (len_start - len + i + 1)), 1);
            SPI_Send_Push_Data_8bit((buff8bit + (len_start - len + i + 0)), 1);
        }
        if (i == (len_piece - 2))
            SPI_Send_Push_Data_8bit((buff8bit + (len_start - len + i + 1)), 1);
        SPI_Send_Finalize_Data_8bit((buff8bit + (len_start - len + i)), 1);
        address_write_current+= len_piece;
        len-= len_piece;
    }
    SST26_WAIT_READY(wait);
    return len_start - len_piece;
}

void sst26_write_16bit(uint32_t start_address, uint16_t *buff, int32_t len, bool wait)
{
    sst26_sequential_write_prepare(start_address);
    sst26_sequential_write_16bit(buff, len, wait);
}

void sst26_write(uint32_t start_address, uint8_t *buff, int32_t len, bool wait)
{
    sst26_sequential_write_prepare(start_address);
    sst26_sequential_write(buff, len, wait);
}

/**
  * @brief  Prepare sequential read. Should called once before writing a
  *         sequential data.
  * @param  start_address: address of the start to read.
  * @retval None.
  */
void sst26_sequential_read_prepare(uint32_t start_address)
{
    address_read_current = start_address;
}

/**
  * @brief  Perform write piece of data.
  * @param  buff: buffer to put the data.
  * @param  len: the length of the data to be read.
  * @retval None.
  */
void sst26_sequential_read(uint8_t *buff, uint32_t len)
{
    uint8_t cmdbuff[5];
    cmdbuff[0] = SST26_CMD_HSREAD;
    cmdbuff[1] = ((address_read_current >> 16) & 0xFF);
    cmdbuff[2] = ((address_read_current >> 8)  & 0xFF);
    cmdbuff[3] = ((address_read_current >> 0)  & 0xFF);
    SPI_Request_8bit(cmdbuff, 5, buff, len);
    address_read_current+= len;
}

void sst26_sequential_read_16bit(uint16_t *buff, uint32_t len)
{
    uint8_t cmdbuff[5];
    cmdbuff[0] = SST26_CMD_HSREAD;
    cmdbuff[1] = ((address_read_current >> 16) & 0xFF);
    cmdbuff[2] = ((address_read_current >> 8)  & 0xFF);
    cmdbuff[3] = ((address_read_current >> 0)  & 0xFF);
    SPI_Request_16bit(cmdbuff, 5, buff, len);
    address_read_current+= len*2;
}

void sst26_read(uint32_t start_address, uint8_t *buff, int32_t len)
{
    sst26_sequential_read_prepare(start_address);
    sst26_sequential_read(buff, len);
}

void sst26_read_16bit(uint32_t start_address, uint16_t *buff, int32_t len)
{
    sst26_sequential_read_prepare(start_address);
    sst26_sequential_read_16bit(buff, len);
}
