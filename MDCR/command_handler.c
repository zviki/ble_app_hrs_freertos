/*******************************************************************************
 *	File:	command_handler.c
 *******************************************************************************/
#include "header.h"
#include "command_handler.h"
#include "k60n_uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

extern xSemaphoreHandle xSemaphoreADC0End;
extern xSemaphoreHandle xSemaphoreADC1End;
extern xSemaphoreHandle xSemaphoreADC2End;
extern xSemaphoreHandle xSemaphoreADC3End;

volatile unsigned int adc_result_low = 0, adc_result_high = 0;

typedef struct
{
	unsigned int callback_index;
	ErrorStatus (*Callback)(device_command command);
}Callback_desc;

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
static ErrorStatus GetIdnCall(device_command command)
{
	return (SendDevResponce(command.source, "EV-MK60FN1M0/512..."));
}


//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
static ErrorStatus GetSystErrCall(device_command command)
{
	return (SendDevResponce(command.source, "recv: syst:err..."));
}


//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
static ErrorStatus GetServSensNadc0Call(device_command command)
{
#ifdef mdcr_interface
	ADC0->SC1[0] = ADC_SC1_ADCH(0)|ADC_SC1_AIEN_MASK;
 #endif
	if (xSemaphoreTake(xSemaphoreADC0End, 5000/portTICK_RATE_MS) != pdPASS)
	{	//UART_DEBUG("Error: xSemaphoreTake(xSemaphoreADC0End)...\r\n");
		return ERROR;
	}
	return (SendDevResponce(command.source, "ADC0: high = %d, low = %d", adc_result_high, adc_result_low));
}

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
static ErrorStatus GetServSensNadc1Call(device_command command)
{
#ifdef mdcr_interface
	ADC1->SC1[0] = ADC_SC1_ADCH(0)|ADC_SC1_AIEN_MASK;
#endif
	if (xSemaphoreTake(xSemaphoreADC1End, 5000/portTICK_RATE_MS) != pdPASS)
	{	//UART_DEBUG("Error: xSemaphoreTake(xSemaphoreADC1End)...\r\n");
		return ERROR;
	}
	return(SendDevResponce(command.source, "ADC1: high = %d, low = %d", adc_result_high, adc_result_low));
}


//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
static ErrorStatus GetServSensNadc2Call(device_command command)
{
#ifdef mdcr_interface
	ADC2->SC1[0] = ADC_SC1_ADCH(0)|ADC_SC1_AIEN_MASK;
#endif
	if (xSemaphoreTake(xSemaphoreADC2End, 5000/portTICK_RATE_MS) != pdPASS)
	{	//UART_DEBUG("Error: xSemaphoreTake(xSemaphoreADC2End)...\r\n");
		return ERROR;
	}
	return(SendDevResponce(command.source, "ADC2: high = %d, low = %d", adc_result_high, adc_result_low));
}


//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
static ErrorStatus GetServSensNadc3Call(device_command command)
{
#ifdef mdcr_interface
	ADC3->SC1[0] = ADC_SC1_ADCH(0)|ADC_SC1_AIEN_MASK;
#endif
	if (xSemaphoreTake(xSemaphoreADC3End, 5000/portTICK_RATE_MS) != pdPASS)
	{	//UART_DEBUG("Error: xSemaphoreTake(xSemaphoreADC3End)...\r\n");
		return ERROR;
	}
	return(SendDevResponce(command.source, "ADC3: high = %d, low = %d", adc_result_high, adc_result_low));
}


/*************************** Callback handlers list ***********************/
/// Callback обработчиков команд
Callback_desc callback_table[] =
{
	/*************************** SCPI main commands********************************/
	{get_scpi_idn, 							GetIdnCall},					/// *IDN?
	/************************** Device main commands ******************************/	/// FORM?
	/// SYST subsystem:
	{get_syst_err, 							GetSystErrCall},				/// SYST:ERR?
	/// SERV subsystem:
	{get_serv_sens_nadc0,					GetServSensNadc0Call},			/// SYST:ERR?
	{get_serv_sens_nadc1,					GetServSensNadc1Call},			/// SYST:ERR?
	{get_serv_sens_nadc2,					GetServSensNadc2Call},			/// SYST:ERR?
	{get_serv_sens_nadc3,					GetServSensNadc3Call},			/// SYST:ERR?
};

unsigned int callback_table_size = sizeof(callback_table)/sizeof(Callback_desc);
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
ErrorStatus CommandHandler(device_command command)
{
	unsigned int i = 0;
	ErrorStatus status = ERROR;
	for (i = 0; i < callback_table_size; i++)
	{	if (command.type == callback_table[i].callback_index)
		{	//UART_DEBUG("Catch command: %d\r\n", callback_table[i].callback_index);
			status = callback_table[i].Callback(command);
			break;
		}
	}
	if (command.type == empty_command)
	{	//UART_DEBUG("\r\nClear bTag, type = empty\r\n");
		return ERROR;
	}
	if (!status)
	{	//UART_DEBUG("Error: DevCommandHandler()->status = ERROR\r\n");
		//UART_DEBUG("command.type = %d\r\n", command.type);
	}
	return status;
}
