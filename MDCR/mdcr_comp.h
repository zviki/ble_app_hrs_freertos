/* 
 * File:   mdcr.h
 * Author: Vladimir
 *
 * Created on 11 мая 2015 г., 20:51
 */

#ifndef MDCR_H
#define	MDCR_H

#ifdef	__cplusplus
extern "C" {
#endif

//mCompress возвращаемое значение количество сжатых данных
int mdcr_comp(unsigned char* data, int ilen, unsigned char* compressed, int channels);
//mCompress возвращаемое значение количество разсжатых данных
int mdcr_decomp(unsigned char* compressed, int ilen, unsigned char* decompressed, int channels);

#ifdef	__cplusplus
}
#endif
    
#endif	/* MDCR_H */

