/*******************************************************************************
 *	File:	command_decode.c
 *******************************************************************************/
#include "header.h"
#include "command_decode.h"
#include "command_handler.h"
#include "k60n_uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"

extern xQueueHandle xQueueDeviceCommand;
//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------
ErrorStatus CommandDecode(serial_port serial)
{
	device_command command;
	unsigned int offset = 0;

	command.type = empty_command;													// Command Type
	command.data = NULL;																	// These commands
	command.data_size = 0;																// Size of data
	command.source = serial.data_source;									// Source command

	// Decode input command:
	if (serial.data[offset] == '*'){
		offset++;
		if (compare_string((char*)&serial.data[offset], "IDN?", strlen("IDN?"))){
			command.type = get_scpi_idn;											// Command Type
			command.data = NULL;															// These commands
			command.data_size = 0;														// Size of data
			//command.source = serial.data_source;						// Source command
		}
		else{
			return ERROR;
		}
	}
	else{
		if (serial.data[offset] == ':'){
			offset++;
		}
		if (compare_string((char*)&serial.data[offset], "SYST", strlen("SYST"))){
			offset = offset + strlen("SYST");
			if (compare_string((char*)&serial.data[offset], "em", strlen("em"))){
				offset = offset + strlen("em");
			}
			if (compare_string((char*)&serial.data[offset], ":ERR", strlen(":ERR"))){
				offset = offset + strlen(":ERR");
				if (compare_string((char*)&serial.data[offset], "or", strlen("or"))){
					offset = offset + strlen("or");
				}
				if (compare_string((char*)&serial.data[offset], "?\r\n", strlen("?\r\n"))){
					command.type = get_syst_err;											// Command Type
					command.data = NULL;															// These commands
					command.data_size = 0;														// Size of data
					//command.source = serial.data_source;						// Source command
				}
				else{
					return ERROR;
				}
			}
		}
		else if (compare_string((char*)&serial.data[offset], "SERV", strlen("SERV"))){
			offset = offset + strlen("SERV");
			if (compare_string((char*)&serial.data[offset], "ice", strlen("ice"))){
				offset = offset + strlen("ice");
			}
			if (compare_string((char*)&serial.data[offset], ":SENS", strlen(":SENS"))){
				offset = offset + strlen(":SENS");
				if (compare_string((char*)&serial.data[offset], "or", strlen("or"))){
					offset = offset + strlen("or");
				}
				if (compare_string((char*)&serial.data[offset], ":NADC0?\r\n", strlen(":NADC0?\r\n"))){
					command.type = get_serv_sens_nadc0;
					command.data = NULL;
					command.data_size = 0;
				}
				else if (compare_string((char*)&serial.data[offset], ":NADC1?\r\n", strlen(":NADC1?\r\n"))){
					command.type = get_serv_sens_nadc1;
					command.data = NULL;
					command.data_size = 0;
				}
				else if (compare_string((char*)&serial.data[offset], ":NADC2?\r\n", strlen(":NADC2?\r\n"))){
					command.type = get_serv_sens_nadc2;
					command.data = NULL;
					command.data_size = 0;
				}
				else if (compare_string((char*)&serial.data[offset], ":NADC3?\r\n", strlen(":NADC3?\r\n"))){
					command.type = get_serv_sens_nadc3;
					command.data = NULL;
					command.data_size = 0;
				}
			}
		}
	}
	if (command.type != empty_command){
		if (xQueueDeviceCommand != NULL){
			if (xQueueSend(xQueueDeviceCommand, &command, 1000/portTICK_RATE_MS) != pdPASS){
				// UART_DEBUG("Error: xQueueSend(xQueueDeviceCommand)...\r\n");
				return ERROR;
			}
		}
		else{
			// UART_DEBUG("Error: SerialPortDecodeData()->xQueueDevCommands = NULL.\r\n");
			return ERROR;
		}
		return SUCCESS;
	}
	return ERROR;
}
