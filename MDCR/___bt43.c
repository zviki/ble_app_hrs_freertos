/***********************************************************
 *	File: 			BT43.c
 *	Description:	
 ***********************************************************/

#define __C_BT43_
 
#include "header.h"
#include "k60n_uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "k60n_gpio.h"
#include "BT43.h"
#include "mdcr_func.h"
#include "mdcr_protocol.h"

static int bt43InitState=0;
static int recvCmdCode=eUndefined;
static int stageInit=0;

#define AnswerBT_sizebuf 100
char AnswerBT[AnswerBT_sizebuf];												//buff for resived command
unsigned short AnswerBT_len;

uMetaBT43Element BT43DataFlag;

//---------------------------------------------------------------
//---------------------------------------------------------------
unsigned short printf_to_mas(char * buff)
{
	unsigned short i;

	memset(AnswerBT, 0, AnswerBT_sizebuf);								//clear
	
	AnswerBT_len = strlen(buff);													//calk size and save
	for(i=0; i<AnswerBT_len; i++)
	{
		AnswerBT[i] = buff[i];															//set mas 
	}	
	
	return 	AnswerBT_len;
}


//---------------------------------------------------------------
//---------------------------------------------------------------
static int BT43_Configuration( int cmdCommand )
{
	int status=0;
	if (cmdCommand==eOK)
	{
		switch(stageInit)
		{					

			case 0:
			UART0_printf("AT+AB config DeviceName = MDCRv3\r\n");
			printf_to_mas("AT-AB ConfigOk");						
			break;

							case 1:
							UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;		
//-------			
			case 2:
			UART0_printf("AT+AB config COD = 502918\r\n");				
			printf_to_mas("AT-AB ConfigOk");						
			break;
			
							case 3:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;		
														
//-------			
			case 4:
			UART0_printf("AT+AB config CreditMax = 10\r\n");				
			printf_to_mas("AT-AB ConfigOk");						
			break;
			
							case 5:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;		
//-------			
			case 6:
			UART0_printf("AT+AB config AccName = MDCRv3\r\n");				
			printf_to_mas("AT-AB ConfigOk");						
			break;

							case 7:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;		
//-------							
			case 8:
			UART0_printf("AT+AB config AccManufacturer = MDCRv3\r\n");				
			printf_to_mas("AT-AB ConfigOk");						
			break;
			
							case 9:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;		
//-------
			case 10:
			UART0_printf("AT+AB config AccModelNumber = 2.1\r\n");				
			printf_to_mas("AT-AB ConfigOk");						
			break;

							case 11:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;		
//-------
			case 12:
			UART0_printf("AT+AB config EnableIAP = true\r\n");				
			printf_to_mas("AT-AB ConfigOk");						
			break;
							case 13:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;									
//-------							
			case 14:
			UART0_printf("AT+AB config iAPAppIDStr = com.mdcr.Demo\r\n");				
			printf_to_mas("AT-AB ConfigOk");						
			break;

							case 15:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;									
//-------
			case 16:
			UART0_printf("AT+AB config iAPProtocolStrMain = com.mdcr.Demo\r\n");				
			printf_to_mas("AT-AB ConfigOk");						
			break;

							case 17:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;									
//-------							
			case 18:
			UART0_printf("AT+AB config iAPProtocolStrAlt = com.mdcr.ProtocolAl\r\n");				
			printf_to_mas("AT-AB ConfigOk");						
			break;

							case 19:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;		
//-------														
			case 20:
			UART0_printf("AT+AB config AccSerialNumber = 1.0\r\n");				
			printf_to_mas("AT-AB ConfigOk");						
			break;

							case 21:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;		
//-------
			case 22:
			UART0_printf("AT+AB config CpuMHz = 4\r\n");				//8MHz default
			printf_to_mas("AT-AB ConfigOk");				
			break;

							case 23:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;		
//-------
			case 24:
			UART0_printf("AT+AB config RmtEscapeSequence = true\r\n");				//Remote escape sequence enabled: @#@$@%
			printf_to_mas("AT-AB ConfigOk");				
			break;
			
							case 25:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;		
							
//-------
			case 26:
			UART0_printf("AT+AB config AllowSniff = true\r\n");				//Enables sniff mode. Must be False when no 32.768KHz
			printf_to_mas("AT-AB ConfigOk");				
			break;
			
							case 27:
								UART0_printf("AT+AB reset\r\n");
							printf_to_mas("AT-AB ResetPending");									
							break;		
//-------																					
			case 28:
			UART0_printf("AT+AB config StreamingSerial = false\r\n");				//RST/CTS flow control is enabled.
			printf_to_mas("AT-AB ConfigOk");				
			break;
									
			case 29:
#ifdef MDCR_interfac
				PTD->PSOR |= (1<<14);						// power off to BT
#endif
				vTaskDelay(100);	
#ifdef MDCR_interfac
                                        
				PTD->PCOR |= (1<<14);						// power on to BT
#endif
				printf_to_mas("AT-AB BDAddress");		
                                                                    

			break;
			
			case 30:
				UART0_printf("AT+AB LocalName MDCRv3\r\n");
				printf_to_mas("AT-AB LocalNameOk");						
			break;

			case 31:
				UART0_printf("AT+AB config BD_ADDR\r\n");	//Read local MAC BT Address
				printf_to_mas("var02 BD_ADDR");									
			break;

			case 32:
				UART0_printf("AT+AB config PIN\r\n");	//Read PIN Code BT 
				printf_to_mas("var05 PIN");									
			break;
			
			case 33:
//				UARTx_Init(UART0, SystemCoreClock/1000, 57600);				
				status=2;
				stageInit=-1;
				LED1_RED_CLR;
				LED2_RED_CLR; 	
				LED1_GR_SET; 	
				LED2_YW_SET;	
			break;

			
/*			
			case 1:
				UART0_printf("AT+AB LeAdv Disable\r\n");			//The LeAdv command is used to enable and disable LE advertising functionality
				printf_to_mas("AT-AB AdvOk");						
			break;
*/			
			
/*			case 2:
//				UART0_printf("AT+AB ChangeBaud 460800\r\n");
//				printf_to_mas("AT-AB Baudrate Changed");						
			break;
*/			
			/*			
//			case 0:
//				UART0_printf("^#^$^%\r\n");
//				printf_to_mas("AT-AB -Command Mode-");						
//			break;
			case 0:
//				vTaskDelay(2000);
				UART0_printf("AT+AB LocalName MDCRv3\r\n");
				printf_to_mas("AT-AB LocalNameOk");						
			break;
//			case 2:
//				UART0_printf("AT+AB reset\r\n");
//				printf_to_mas("AT-AB ResetPending");			
//			break;
			case 1:
				status=2;
				stageInit=-1;
				LED1_RED_CLR;
				LED2_RED_CLR; 	
				LED1_GR_SET; 	
				LED2_YW_SET;	
				break;
*/				
	}
		stageInit+=1;
	}
	return status;
}


//---------------------------------------------------------------
//	Description:	The function for decoding received data.
//---------------------------------------------------------------
int BT43_Decode(serial_port serial)
{
	unsigned int offset = 0;
	int status;

	if(serial.support_protocol==MDCRprotocol)
	{
		mdcr_decode_recv_command(serial);
	}
	else
		//	if(serial.support_protocol==BT43protocol)
	{
//		if(compare_string((char*)&serial.data[offset], AnswerBT, AnswerBT_len))
		if(compare_string((char*)&serial.data[offset], AnswerBT, AnswerBT_len))		
//		if(compare_string((char*)&serial.data[offset], "OK", strlen("OK")))			
		{	recvCmdCode=eOK;
//			 ("BT43: eOK\r\n");
		}
		if(bt43InitState==0)																				//not yet init
		{	status=BT43_Configuration(recvCmdCode);
			if (status==2)																						// init BT ?	
			{	
				bt43InitState=1;																				//YES - finish init BT module
				serial.support_protocol=MDCRprotocol;
				Uart0Protocol = MDCRprotocol;		
				BT43DataFlag.ALL=0;								
					vTaskDelay(500);				
	//				UARTx_Init(UART0, SystemCoreClock/1000, 115200);
	//			UART_DEBUG("BT43: Init SUCCESS - 115200\r\n");
			}
		}
	}
	return SUCCESS;
}

