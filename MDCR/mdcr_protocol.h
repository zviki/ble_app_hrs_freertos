/*******************************************************
 *	File:			mdcr_protocol.h
 *	Description:
 *******************************************************/
#ifndef __H_INCLUDE_MDCR_PROTOCOL_
#define __H_INCLUDE_MDCR_PROTOCOL_

#define TEST1_LENGTH 68
#define TEST2_LENGTH 64
#define TEST3_LENGTH 63
#define OK_MEAS_LENGTH 9

#define CHUNK_MODE 0x01
#define SINGLE_READ_MODE 0x02
#define COMPRESSED_CHUNK_MODE 0x03

enum
{
	DUMMY=0,					//	0
	TEST1,						//	1
	TEST2,						//	2
	TEST3,						//	3
	TEST4,						//  4
	MEAS_START=5,				//	5
	MEAS_FINISH, 				//	6
	MEAS_OK, 					//	7
	DATA_INT, 					//	8
	DATA_CON,					//	9
	MEAS_REPEAT,				//	10
	SIGNAL_NOVALID,				//	11
	GO_HIB=0x0C,				//	12 - Going into hibernation
	EXT_PLUG_MISCONNECT,		//	13
	STOP_TEST=0x0E, 			//	14
	DATA_SEND=0x10,				//	16
	BUTTON_PRESSED = 0x11,		//	17
	
	GET_LAST_MEASUREMENT=0x20,	//	32
	SET_FILTER,					//	33
	GET_TEST_INFORMATION,		//	34
	SET_BT_CONFIGURATION,		//	35
	GET_BEE_SN=0x30,				//48
	GET_BEE_APP_VERSION,			//49 
	GET_BEE_DEVICE_VER,			//50 
	GET_DATE_DEVICE_FIRMWARE,	//51
	GET_CHARGED_LEVEL,				//52
	BUTTON_PRESSED_DEVICE = 0x45,
	APP_LESS_TEST = 0x46,
	SEND_INFO_ABOUT_LAST_SAVED_TEST = 0x47,
	SET_MEASURING_FREQUENCY     = 0x48,
	GET_MEASURING_FREQUENCY     = 0x49,
	SET_INVALID_SIGNAL_DETECT   = 0x4A,
  GET_INVALID_SIGNAL_DETECT   = 0x4B,
	APP_LESS_TEST2							= 0x4C,
};

enum {
    FILTER_DISABLE,
    FILTER_50HZ,
    FILTER_60HZ,
};

enum {
	YELLOW_BUTTON_PRESSED = 0x1,
	GREEN_BUTTON_PRESSED = 0x2,
	YELLOW_AND_GREEN_BUTTONS_PRESSED = 0x3
};

//Acquisition mode One byte.
//0x00 - Non
//0x01 - Stream.
//0x02 - Stream and write to flash.
//0x03 - write to flash.
enum
{
	NO_STREAM	=0,
	STREAM,
	STREAM_AND_WRITE,
	WRITE_TO_FLASH,	
	WRITE_TO_FLASH_AND_COMMUNICATE,	
};


enum
{
	eMDCR_START=1,
	eMDCR_STOP,
};

#endif	//	__H_INCLUDE_MDCR_PROTOCOL_
