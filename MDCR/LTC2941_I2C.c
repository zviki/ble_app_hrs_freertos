/*
 * File:        LTC2941_I2C.c
 * Purpose:     read-write
 *
 * Notes:
 *
 */

#include "header.h"
#include "command_handler.h"
#include "command_decode.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
//#include "misc.h"
#include "k60n_uart.h"

#include "iic.h"
#include "LTC2941_i2c.h"

tsIIC_LTC2941Data LTC2941_Reg;
//extern tsIICData I2C0Data;

/***********************************************************************************************\
* Private prototypes
\***********************************************************************************************/


void LTC2941_ask(void)
{
	unsigned int try_ask, len;

	len=0;

//---------------------Reading the LTC2941 Accumulated Charge Registers (A)
	try_ask=0;
	while (I2C_Start() && try_ask<100) {I2C_Stop(); I2C_Start(); try_ask++;}
	if (I2C_CycleWrite(IIC_SLAVE_ADDRESS<<1));
	else
		I2C_CycleWrite(LTC2941_ADR_Status);
			IIC_ReadData(&I2C0Data.RxBuff[len],1);
	LTC2941_Reg.Status = (unsigned char)I2C0Data.RxBuff[len++];

//---------------------Reading the LTC2941 Accumulated Charge Registers (B)
	try_ask=0;
	while (I2C_Start() && try_ask<100) {I2C_Stop(); I2C_Start(); try_ask++;}
	if (I2C_CycleWrite(IIC_SLAVE_ADDRESS<<1));
	else
		I2C_CycleWrite(LTC2941_ADR_Control);
			IIC_ReadData(&I2C0Data.RxBuff[len],1);
	LTC2941_Reg.Control = (unsigned char)I2C0Data.RxBuff[len++];

//---------------------Reading the LTC2941 Accumulated Charge Registers (C,D)
	try_ask=0;
	while (I2C_Start() && try_ask<100) {I2C_Stop(); I2C_Start(); try_ask++;}
	if (I2C_CycleWrite(IIC_SLAVE_ADDRESS<<1));
	else
		I2C_CycleWrite(LTC2941_ADR_Charge);
			IIC_ReadData(&I2C0Data.RxBuff[len],2);
	LTC2941_Reg.Charge = (unsigned short)(I2C0Data.RxBuff[len++]<<8);
	LTC2941_Reg.Charge |= (unsigned short)I2C0Data.RxBuff[len++];


//---------------------Reading the LTC2941 Accumulated Charge Registers (C,D)
	try_ask=0;
	while (I2C_Start() && try_ask<100) {I2C_Stop(); I2C_Start(); try_ask++;}
	if (I2C_CycleWrite(IIC_SLAVE_ADDRESS<<1));
	else
		I2C_CycleWrite(LTC2941_ADR_ChThHI);
			IIC_ReadData(&I2C0Data.RxBuff[len],2);
	LTC2941_Reg.ChThHI = (unsigned short)(I2C0Data.RxBuff[len++]<<8);
	LTC2941_Reg.ChThHI |= (unsigned short)I2C0Data.RxBuff[len++];

//---------------------Reading the LTC2941 Accumulated Charge Registers (C,D)
	try_ask=0;
	while (I2C_Start() && try_ask<100) {I2C_Stop(); I2C_Start(); try_ask++;}
	if (I2C_CycleWrite(IIC_SLAVE_ADDRESS<<1));
	else
		I2C_CycleWrite(LTC2941_ADR_ChThLO);
			IIC_ReadData(&I2C0Data.RxBuff[len],2);
	LTC2941_Reg.ChThLO = (unsigned short)(I2C0Data.RxBuff[len++]<<8);
	LTC2941_Reg.ChThLO |= (unsigned short)I2C0Data.RxBuff[len++];

	UART1SendData((char*)I2C0Data.RxBuff,len);

}


//------------Write the LTC2941 Accumulated Charge Registers
void LTC2941_write(unsigned char *ucIIC_TxBuff, unsigned char len,unsigned char LTC2941_REG)
{
	unsigned char try_ask;

	I2C0Data.FrameLength = len;
	try_ask=0;
	while (I2C_Start() && try_ask<100) {I2C_Stop(); I2C_Start(); try_ask++;}
	if (I2C_CycleWrite(IIC_SLAVE_ADDRESS<<1));
	else
		I2C_CycleWrite(LTC2941_REG);
			IIC_SendData(I2C0Data.TxBuff,len);
}

