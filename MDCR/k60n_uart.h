/**********************************************************************************
 *	File:	k60n_uart.h
 **********************************************************************************/
#ifndef __K60N_UART_H_
#define __K60N_UART_H_
 #include "header.h"

	#define MAX_COMMAND_SIZE	512	//256
	#define RX_BUFFER_SIZE  2*MAX_COMMAND_SIZE//MAX_COMMAND_SIZE + 64

	typedef struct
	{
		unsigned char 	data[100];				//	Указатель на данные.
		int 			data_source;		//	Источник данных	(приёмник ответа).
		int				support_protocol;	//	Поддерживаемый протокол.
		unsigned int	data_size;			//	Размер данных.
		short			flag;								// RX-0, TX-1
	}serial_port;

	typedef struct
	{
		unsigned int in;					//
		unsigned int out;					//
		unsigned char buf[RX_BUFFER_SIZE];			//
	}buf_st;

	bool compare_string(char *strA, char *strB, unsigned int str_size);
#ifdef MDCR_INTERFACE
	void UARTx_Init(UART_Type *UART, int sysclk, int baud);
	void UARTx_DeInit(UART_Type *UART);
#endif	
	void UART0SendData(char* buff, int size);
	void UART1SendData(char* buff, int size);
        void UART0SendByte_u(uint8_t byte);

	void UART0_printf(char *arg_list, ...);
	void UART1_printf(char *arg_list, ...);
	void UART5_printf(char *arg_list, ...);

	void UART0_RX_TX_IRQHandler(void);
	void UART1_RX_TX_IRQHandler(void);
	void UART5_RX_TX_IRQHandler(void);

	void serial_send_data(serial_port com_port);
	ErrorStatus SendDevResponce(int data_source, char *arg_list, ...);

#endif
