
#include "header.h"
#include "k60n_gpio.h"


void LED_Clear(void)					//
{
#ifdef mdcr_interface
	PTA->PCOR = LED1_GR;
	PTA->PCOR = LED1_RED;
	PTA->PCOR = LED2_YW;
	PTA->PCOR = LED2_RED;
#endif
}


void Dinit_portX(void)
{
#ifdef mdcr_interface
	SIM->SCGC6 &=~ SIM_SCGC6_DSPI0_MASK;		// Disable DSPI0 clock gate control
	SIM->SCGC6 &=~ SIM_SCGC6_DSPI1_MASK;		// Disable DSPI0 clock gate control

	SIM->SCGC4 &=~ SIM_SCGC4_IIC0_MASK; 		//Turn off clock to I2C0 module

	SIM->SCGC4 &=~ SIM_SCGC4_UART0_MASK;
	SIM->SCGC4 &=~ SIM_SCGC4_UART1_MASK;
	SIM->SCGC4 &=~ SIM_SCGC4_UART2_MASK;
	SIM->SCGC4 &=~ SIM_SCGC4_UART3_MASK;
	SIM->SCGC1 &=~ SIM_SCGC1_UART4_MASK;


	SIM->SCGC6 &=~ SIM_SCGC6_ADC0_MASK;			// Disable ADC clock
	SIM->SCGC3 &=~ SIM_SCGC3_ADC1_MASK;
	SIM->SCGC6 &=~ SIM_SCGC6_ADC2_MASK;
	SIM->SCGC3 &=~ SIM_SCGC3_ADC3_MASK;

	SIM->SCGC5 &=~ SIM_SCGC5_PORTA_MASK;		// Disable Clock to Port
	SIM->SCGC5 &=~ SIM_SCGC5_PORTB_MASK;		// Disable Clock to Port
	SIM->SCGC5 &=~ SIM_SCGC5_PORTC_MASK;		// Disable Clock to Port
	SIM->SCGC5 &=~ SIM_SCGC5_PORTD_MASK;		// Disable Clock to Port
	SIM->SCGC5 &=~ SIM_SCGC5_PORTE_MASK;		// Disable Clock to Port
	SIM->SCGC5 &=~ SIM_SCGC5_PORTF_MASK;		// Disable Clock to Port
#endif
}

