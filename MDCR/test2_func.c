/*************************************************************
 *	  File:		mdcr_func.c
 *	Author:		Kirillov A.V.
 *************************************************************/
#define __C_MDCR_

#include "mdcr_func.h"
#include "AT25DFspi_memory.h"
#include "header.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "k60n_gpio.h"

#if ENABLE_TestSignalADC > 0 
extern unsigned short TestCounter;
#endif
extern unsigned short FIRdataADC[4];	//буфер обработанных фильтрованных данных АЦП
extern uMetaSwElement SwDataFlag;
extern uTimersDataElement TimersDataFlag;
extern tsMDCRCheckSignal DataValidSignal;

#define MINIMUM_DELAY_MEAS_IN_START_OF_ITER_Test2			300
#define MINIMUM_DELAY_VALID_MEAS_IN_START_OF_ITER_Test2		350
#define MAX_DELAY_MEAS_IN_START_OF_ITER_Test2				2500
 
static int CheckBtnTest2(tsTestParam* param)
{
	#if (DISABLE_SW_CONFIRMATION_Test2 == 0)
		startTimer2();
		while (!TimersDataFlag.BIT.TIMER_2)
		{
			vTaskDelay(2);
			KeyScan();
			if ((SwDataFlag.BIT.SW2_flag && SwDataFlag.BIT.SW1_flag)
				||
				(SwDataFlag.BIT.SW2_GUI_flag && SwDataFlag.BIT.SW1_GUI_flag))
			{
				SwDataFlag.BIT.SW1_GUI_flag = 0;
				SwDataFlag.BIT.SW2_GUI_flag = 0;
				SendButtonPressedDevice(param);
				return 	1;
			}
		}
		return 	0;
	#else
		return 1;
	#endif
}

void bugN6Fix(tsTestParam* param)
{
	int i = 0;
	int ValidCounter = 0;
	unsigned short channel[4];
	bool DisableFilteringInMeasLocal = false;
	if (param->acqMode != STREAM)
		DisableFilteringInMeasLocal = true;
	i = 0;
	do{
		getMeasure (channel,FIRdataADC,param->numChannels, param->numFilter, DisableFilteringInMeasLocal);
		if (DataValidSignal.flagMain == 0)
			ValidCounter++;
		else
			ValidCounter = 0;
		if (ValidCounter > MINIMUM_DELAY_VALID_MEAS_IN_START_OF_ITER_Test2 && i > MINIMUM_DELAY_MEAS_IN_START_OF_ITER_Test2)
			break;
		i++;
	}while (i < MAX_DELAY_MEAS_IN_START_OF_ITER_Test2);
	if (ValidCounter == 0)
	{
		startTimer2();
		setLEDState(Red1AndRed2Solid);
		param->status = testCheckValidSignal;
	}
}
void interruptActionsTest2(tsTestParam* param)
{
	setLEDState(Red1AndRed2Solid);
	SendMessage_MeasInterrupt(param);
	startTimer2();
	param->CurrentSampleInIteration = 0;
	param->status=testCheckValidSignal;
}
void setNexIteration(tsTestParam* param)
{
	param->CurrentIteration++;
	param->curMetaDE.str.Iterations = param->CurrentIteration;
}
int Test2Func(tsTestParam* param, int recvCmd)
{
	unsigned short channel[4];
	int result = 1;
	int i = 0;
	static bool TestWasStarted = false;
	static bool DisableFilteringInMeas = false;
	static bool DisableInvalidCheckIn1SecAtStart = false;
	static enum FlashAddressShift RecordingShift = ContinueRecording;
	switch (param->status)
	{
		case testStart:
			SwDataFlag.BIT.SW1_GUI_flag = 0;
			SwDataFlag.BIT.SW2_GUI_flag = 0;
			#if ENABLE_TestSignalADC > 0 
				TestCounter = 0;
			#endif
			DisableInvalidCheckIn1SecAtStart = false;
			DisableInvalidCheckIn1SecAtStart = DisableInvalidCheckIn1SecAtStart;
			DisableFilteringInMeas = false;
			TestWasStarted = false;
			setLEDState(AllLEDsOff);
			vTaskDelay(500);
			setLEDState(YellowAndGreenSolid);
			if (CheckBtnTest2(param) == 0)
			{
				SendMessage_NoValidSignal(param);
				param->status = testStop; 
				break;
			}
			if (CheckAnalogSNS(param) == 0)
			{
				SendMessage_ExtPlugMisconnected(param);
				param->status=testStop; 
				break;
			}
			param->CurrentSampleInIteration = 0;
			param->numChankSamples = param->numReqSamples/param->Iterations;
			param->CurrentIteration = 0;
			setNexIteration(param);
			param->curMetaDE.str.numTest = param->numTest&0xf;
			param->curMetaDE.str.mode = 0;
			perform_the_analog_switch(param);
			param->addrMeasurement = ADDRESS_MEASUREMENTS;
			param->TestMode = 0;
			param->numChannels = 4;
			param->numMode = 0;
			initChecksValidSignal();
			InitDigFilter();
			ADC_Start(1000);
			for (i = 0; i < mdcr_get_frequency(); i++)								// выкинуть 500 выборок на инициализ фильтра
			{
				getMeasure (channel,FIRdataADC,param->numChannels, param->numFilter, DisableFilteringInMeas);
				GercelaPutBuf(channel,param->numChannels);	// need 500 sampl for work
			}
			if (param->numFilter == 0x03)	
				param->numFilter = GercelaAlgorithm(channel,param->numChannels);	// calc FFT for 50/60Hz from buf GercelaPutBuf()
			else if (param->numFilter == 0x04)	
				param->numFilter = AVGAlgorithm(channel,param->numChannels);
			InitDigFilter();
			vTaskDelay(10);
			for (i=0; i < mdcr_get_frequency(); i++)	// 500 выборок на инициализ детектора 150Гц
				getMeasure (channel,FIRdataADC,param->numChannels, param->numFilter, DisableFilteringInMeas);
			InitDigFilter();
			if (param->acqMode != STREAM)
			{
				DisableFilteringInMeas = true;
				flashWriteTaskInit();
				RecordingShift = ContinueRecording;
				param->mode = CHUNK_MODE;
				recordFlashHeader(param);
				param->subStatus = testFlashRecording;
			}else
			{
				param->mode = SINGLE_READ_MODE;
				param->subStatus = testStreamRunning;
			}
			startTimer60sec();
			param->status=testCheckValidSignal;
			break;
		case testFlashRecording:
			#if ENABLE_TestSignalADC > 0 
				TestCounter = 0;
			#endif
			if (recordMeasurementsToFlashCard(RecordingShift, param->numChankSamples, param->numChannels, ADD1SEC, false) == COMPLETED)
			{
				DisableInvalidCheckIn1SecAtStart = false;
				RecordingShift = ContinueRecording;
				if (param->acqMode == STREAM_AND_WRITE)
					readLastIterationAndSendToGUI(param);
				if (param->CurrentIteration == param->Iterations)
				{
					if (param->acqMode == STREAM_AND_WRITE)
					{
						SendMessage_MeasFinish(param);
						startTimer2();
						param->status = testWaitAnswer;
					}else if (param->acqMode == WRITE_TO_FLASH || param->acqMode == WRITE_TO_FLASH_AND_COMMUNICATE)
					{
						param->CurrentIteration = 0;
						setNexIteration(param);
						param->subStatus = testSendingAllTest;
						param->status = testSendingAllTest;
					}
				}else
				{
					setNexIteration(param);
					perform_the_analog_switch(param);
					bugN6Fix(param);
					setLEDState(YellowAndGreenBlink);
					DisableInvalidCheckIn1SecAtStart = true;
				}
			}else
			{
				DisableInvalidCheckIn1SecAtStart = false;
				interruptActionsTest2(param);
				RecordingShift = ReWriteLastIteration;
				prepareToRepeatLastIteration();
				startTimer2();
			}
			break;
		case testStreamRunning:
			if(param->CurrentSampleInIteration == param->numChankSamples)	//прошел чанк
			{
				if (param->CurrentIteration == param->Iterations) 
				{
					SendMessage_MeasFinish(param);
					startTimer2();
					param->status = testWaitAnswer;
					break;
				}
				param->CurrentSampleInIteration = 0;
				setNexIteration(param);
				perform_the_analog_switch(param);
				bugN6Fix(param);
				setLEDState(YellowAndGreenBlink);
			#if ENABLE_TestSignalADC > 0 
				TestCounter = 0;
			#endif
			}
			getMeasure(channel,FIRdataADC,param->numChannels, param->numFilter, DisableFilteringInMeas);
			param->validity = DataValidSignal.flagMain;
			SendResultsStreamMode(param,FIRdataADC);
			param->CurrentSampleInIteration++;
			if (DataValidSignal.flagMain)
			{
				interruptActionsTest2(param);
				break;
			}
			break;
		case testDataInterrupt:
			break;
		case testCheckValidSignal:
			getMeasure (channel,FIRdataADC,param->numChannels, param->numFilter, DisableFilteringInMeas);
			if (DataValidSignal.flagMain == 0)
			{
				setLEDState(YellowAndGreenBlink);
				startTimer2();
				if (param->acqMode == STREAM)
					for (i = 0; i < mdcr_get_frequency(); i++)
						getMeasure (channel,FIRdataADC,param->numChannels, param->numFilter, DisableFilteringInMeas);
				param->status = param->subStatus;
				if (TestWasStarted)
					SendMessage_MeasContinue(param);
				else
					stopTimer60sec();
				TestWasStarted = true;
				SendMessage_MeasStart(param);
			}else if ((TimersDataFlag.BIT.TIMER_2 && TestWasStarted) || (TimersDataFlag.BIT.TIMER_60sec && TestWasStarted == false))
			{
				SendMessage_NoValidSignal(param);
				param->status = testStop;
			}
			break;
		case testDummyRead:
			break;
		case testStop:
			SwDataFlag.BIT.SW1_GUI_flag = 0;
			SwDataFlag.BIT.SW2_GUI_flag = 0;
			ADC_Stop();
			stopTimer2();
			stopTimer60sec();
			setLEDState(YellowAndGreenSolid);
			SendMessage_GoingIntoHib(param);
			result = 0;
			break;
		case testRepeat:
			param->status=testStart;
			break;
		case testSendingAllTest:
			if (param->CurrentIteration <= param->Iterations)
			{
				readLastIterationAndSendToGUI(param);
//					SendMessage_MeasFinish(param);
				setNexIteration(param);
				startTimer2();
			}else
			{
				SendMessage_MeasFinish(param);
				startTimer2();
				param->status = testWaitAnswer;
			}
			break;
		case testWaitAnswer:
			if (TimersDataFlag.BIT.TIMER_2 == 0)
			{
				if (recvCmd == MEAS_OK)
				{
					if(param->CurrentIteration < param->Iterations)
						param->status = param->subStatus;
					else
						param->status=testStop;
				}
			}else
				param->status=testStop;
		break;
	}
	return result;
}
