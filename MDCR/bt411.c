/***********************************************************
 *	File: 			bt411.c
 *	Description:	
 ***********************************************************/

#define __C_BT411_

#include "header.h"
#include "k60n_uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "k60n_gpio.h"
#include "bt411.h"
#include "mdcr_func.h"
#include "mdcr_protocol.h"

static int btm411InitState = 0;
static int recvCmdCode = eUndefined;
static int stageInit = 0;
uMetaBT411Element BT411DataFlag;

//---------------------------------------------------------------
//---------------------------------------------------------------

static int BTM411_Configuration(int cmdCommand)
{
    int status = 0;
    if (cmdCommand == eOK)
    {
        switch (stageInit)
        {
            case 0:
                UART0_printf("ATE1\r\n");
                break;
            case 1:
                UART0_printf("ATS515=$522204\r\n"); // Cellphone device class
                break;
            case 2:
                UART0_printf("ATS320=3\r\n"); //Security Level Hi
                break;
            case 3:
                UART0_printf("ATS321=3\r\n"); //No input no output butons
                break;
            case 4:
                UART0_printf("ATS322=0\r\n"); //Force Man-In-The-Middle Protection
                break;
            case 5:
                UART0_printf("ATS323=0\r\n"); //Legacy Pairing
                break;
            case 6:
                UART0_printf("ATS330=$1F\r\n");
                break;
            case 7:
                UART0_printf("AT+BTK=\"1234\"\r\n");
                break;
            case 8:
                UART0_printf("AT+BTN=\"MDCRv2\"\r\n");
                break;
            case 9:
                UART0_printf("ATS512=7\r\n");
                break;
            case 10:
                UART0_printf("ATS0=1\r\n");
                break;
            case 11:
                UART0_printf("AT&W\r\n");
                break;
            case 12:
                UART0_printf("ATZ\r\n");
                break;
            case 13:
                UART0_printf("ATI4\r\n");
                break;
            case 14:
                UART0_printf("ATS541=-10\r\n");
                break;
            case 15:
                UART0_printf("AT+BTP\r\n");
                status = 2;
                stageInit = -1;
                LED1_RED_CLR;
                LED2_RED_CLR;
                LED1_GR_SET;
                LED2_YW_SET;
                break;
        }
        stageInit += 1;
    }
    return status;
}


//---------------------------------------------------------------
//	Description:	The function for decoding received data.
//---------------------------------------------------------------

int BT411_Decode(serial_port serial)
{
    unsigned int offset = 0;
    int status;

    //	if (compare_string((char*)&serial.data[offset], "CONNECT", strlen("CONNECT"))) BT411DataFlag.BIT.Connected=1;
    //	else if (compare_string((char*)&serial.data[offset], "CARRIER", strlen("CARRIER"))) BT411DataFlag.BIT.Connected=0;		

    if (serial.support_protocol == MDCRprotocol)
    {
        mdcr_decode_recv_command(serial);
    }else if (serial.support_protocol == USERprotocol)
    {
        if (compare_string((char*) &serial.data[offset], "*IDN?", strlen("*IDN?")))
        {
            UART0_printf("MDCR version 2.0\r\n");
        }else if (compare_string((char*) &serial.data[offset], "*GETINFO?", strlen("*GETINFO?")))
        {
            UART0_printf("Firmware 1.0\r\nLast update 05.04.2014\r\n");
        }else
        {
            recvCmdCode = eUndefined;
            return SUCCESS;
        }
    }else
        //	if(serial.support_protocol==BT411protocol)
    {
        if (compare_string((char*) &serial.data[offset], "OK", strlen("OK")))
        {
            recvCmdCode = eOK;
            //			 ("BT411: eOK\r\n");
        }
        if (btm411InitState == 0)
        {
            status = BTM411_Configuration(recvCmdCode);
            if (status == 2)
            {
                btm411InitState = 1;
                serial.support_protocol = MDCRprotocol;
                Uart0Protocol = MDCRprotocol;
                BT411DataFlag.ALL = 0;
#ifdef mdcr_interface
                PTD->PTOR ^= (1 << 8);
                PTD->PTOR ^= (1 << 9);
#endif
                vTaskDelay(500);
                //				UARTx_Init(UART0, SystemCoreClock/1000, 115200);
                //			UART_DEBUG("BT411: Init SUCCESS - 115200\r\n");
            }
        }
    }
    return SUCCESS;
}

