/*
 * File:        iic.c
 * Purpose:     Provide common adc routines
 *
 * Notes:       
 *              
 */

//#include "common.h"

#include "header.h"
#include "command_handler.h"
#include "command_decode.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
//#include "misc.h"

#include "iic.h"
#include "LTC2941_i2c.h"


tsIICData I2C0Data;
extern tsIIC_LTC2941Data LTC2941_Reg;

/***********************************************************************************************\
* Private prototypes
\***********************************************************************************************/

unsigned char I2C_Start(void);
unsigned char I2C_Stop(void);
unsigned char I2C_RepeatStart(void);
void I2C_Delay(void);
unsigned char I2C_CycleWrite(unsigned char bout);
//unsigned char I2C_CycleWrite(unsigned char bout);
unsigned char  I2C_CycleRead(unsigned char ack);
void Init_I2C(void);

/***********************************************************************************************\
* Private memory declarations
\***********************************************************************************************/

//#pragma DATA_SEG __SHORT_SEG _DATA_ZEROPAGE

static unsigned char error;
static unsigned int timeout;

//#pragma DATA_SEG DEFAULT

#define BUFFER_OUT_SIZE       8


/*****************************************************************************//*!
   +FUNCTION----------------------------------------------------------------
   * @function name: init_I2C
   *
   * @brief description: I2C Initialization, Set Baud Rate and turn on I2C
   *        
   * @parameter:  none 
   *
   * @return: none
   *
   * @ Pass/ Fail criteria: none
   *****************************************************************************/
void Init_I2C(void)
{
#ifdef mdcr_interface
	SIM->SCGC4 |= SIM_SCGC4_IIC0_MASK; //Turn on clock to I2C0 module  	
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;	// Enable Clock to Port A	
	PORTB->PCR[0] = PORT_PCR_MUX(2) | PORT_PCR_ODE_MASK;//| PORT_PCR_DSE_MASK;		// SCL
	PORTB->PCR[1] = PORT_PCR_MUX(2) | PORT_PCR_ODE_MASK;//| PORT_PCR_DSE_MASK;		// SDA

	I2C0->F = 0x21; // clock divider 192, 
	
   I2C0->C1 = I2C_C1_IICEN_MASK |    //I2C enable interrupt 
              I2C_C1_IICIE_MASK |    //I2C interrupt enableI2C enable
              //I2C_C1_MST_MASK   |    //Master mode select
              I2C_C1_TX_MASK;           


   I2C0->C2 = 0x00;	//&=~	I2C_C2_ADEXT_MASK;		//0 - 7-bit address scheme
#endif
	 
//  I2C0->C1 |= I2C_C1_MST_MASK;	
}
/*
void I2C0_IRQHandler(void)
{
int i;
	i++;
}
*/
/***********************************************************************************************\
* Private functions
\***********************************************************************************************/

/*********************************************************\
* Initiate I2C Start Condition
\*********************************************************/
unsigned char I2C_Start(void)
{
  error = 0x00;
#ifdef mdcr_interface

  I2C0->C1 |= I2C_C1_TX_MASK;			//Transmit mode select
	I2C0 ->C1 &=~ I2C_C1_MST_MASK; //Master mode select 	SET 0
  timeout = 0;
  while (timeout<100) timeout++;	

  I2C0->C1 |= I2C_C1_MST_MASK;
  timeout = 0;
  while ((!(I2C0->S & I2C_S_BUSY_MASK)) && (timeout<10000))
    timeout++;
  if (timeout >= 50000)
		error |= 0x01;
/*	if( I2C0->S & I2C_S_ARBL_MASK )
    {
        I2C0->S |= I2C_S_ARBL_MASK;
    }		
*/
#endif
  return error;
} //*** Wait until BUSY=1


/*********************************************************\
* Initiate I2C Stop Condition
\*********************************************************/
unsigned char I2C_Stop(void)
{
  error = 0x00;
  
/*
  I2C0->C1 |= I2C_C1_MST_MASK;	
  timeout = 0;
  while (timeout<10000) timeout++;		
*/
#ifdef mdcr_interface
	I2C0->C1 &= ~I2C_C1_MST_MASK;
	
  timeout = 0;
  while ( (I2C0->S & I2C_S_BUSY_MASK) && (timeout<10000))
    timeout++;
  if (timeout >= 50000)
    error |= 0x02;
  return error;
#endif
} //*** Wait until BUSY=0


/*********************************************************\
* Initiate I2C Repeat Start Condition
\*********************************************************/
unsigned char I2C_RepeatStart(void)
{
#ifdef mdcr_interface
      error = 0x00;
  I2C0->C1 |= I2C_C1_RSTA_MASK;
  timeout = 0;
  while ((!(I2C0->S & I2C_S_BUSY_MASK)) && (timeout<10000))
    timeout++;
  if (timeout >= 50000)
    error |= 0x04;
#endif

  return error;
} //*** Wait until BUSY=1


/*********************************************************\
* I2C Delay
\*********************************************************/
void I2C_Delay(void)
{
  unsigned int I2Cd;
  for (I2Cd=0; I2Cd<1000; I2Cd++);
}


/*********************************************************\
* I2C Cycle Write
\*********************************************************/
unsigned char I2C_CycleWrite(unsigned char bout)
{
  timeout = 0; 
  error = 0x00;
#ifdef mdcr_interface
  while ((!(I2C0->S & I2C_S_TCF_MASK)) && (timeout<10000))
    timeout++;
		if (timeout >= 10000)
			error |= 0x08; 

  I2C0->C1 |= I2C_C1_TX_MASK;
  I2C0->D = bout; 
  timeout = 0;
  while ((!(I2C0->S & I2C_S_IICIF_MASK)) && (timeout<10000))
    timeout++;
  if (timeout >= 10000)
    error |= 0x10;
  I2C0->S |= I2C_S_IICIF_MASK;    // clear the int pending flag

  timeout = 0;	
	if ((I2C0->S & I2C_S_RXAK_MASK) && (timeout<10000))  // 1 - No acknowledge signal detected
    timeout++;
 
	 if (I2C0->S & I2C_S_RXAK_MASK)  // 1 - No acknowledge signal detected
    error |= 0x20;
#endif
  return error;
}


/*********************************************************\
* I2C Cycle Read
\*********************************************************/
unsigned char I2C_CycleRead(unsigned char  ack)
{
  unsigned char bread; 
  timeout = 0;
  error = 0x00;

#ifdef mdcr_interface
  
  while ((!(I2C0->S & I2C_S_TCF_MASK)) && (timeout<10000))
    timeout++;
		if (timeout >= 50000)
			error|=0x08;
  I2C0->C1 &= ~I2C_C1_TX_MASK;     // Receive mode   
	bread = I2C0->D; 		
  if( ack )
  {
      I2C0->C1 |= I2C_C1_TXAK_MASK;	//1 - No acknowledge signal is sent to the bus
  }
  else
  {
    I2C0->C1 &= ~I2C_C1_TXAK_MASK;	// 0 - An acknowledge signal is sent to the bus
  }
  
	timeout = 0; 
  while ((!(I2C0->S & I2C_S_IICIF_MASK)) && (timeout<10000))
    timeout++;
		if (timeout >= 50000)
			error |= 0x10;

	I2C0->S &= I2C_S_IICIF_MASK;    // clear the int pending flag
#endif
  return bread;
}


unsigned char IIC_SendData(unsigned char *pData, unsigned int uiLength)
{
	unsigned int i;
  unsigned int ucState;
		
	for(i=0;i<uiLength;i++)
	{
		ucState = I2C_CycleWrite(pData[i]);
	}
	ucState = I2C_Stop();
	return ucState;
}


unsigned char IIC_ReadData(unsigned char *pData,unsigned int uiLength)
{
	unsigned int i;
	unsigned int ucState;
//	unsigned char Dummy;
	uiLength = uiLength - 1;
#ifdef mdcr_interface
    if( I2C0->S & I2C_S_ARBL_MASK )
    {
        I2C0->S |= I2C_S_ARBL_MASK;
    }
#endif
	I2C_RepeatStart();
	I2C_CycleWrite((IIC_SLAVE_ADDRESS<<1) | 0x01);				

		
	// dummy read
	/*Dummy = */I2C_CycleRead(0);
	for(i=0;i<uiLength;i++)
	{
		pData[i] = I2C_CycleRead(0);
	}

	// read the last byte, don't send ACK
	pData[i] = I2C_CycleRead(1);
	ucState = I2C_Stop();
	return ucState;
}


/*
 * end here
 */


