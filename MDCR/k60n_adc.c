/*****************************************************************************
 *	File:	k60n_adc.c
 *****************************************************************************/
#include "header.h"
#include "k60n_adc.h"
#include "k60n_uart.h"
//#include "misc.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "mdcr_func.h"

#include "nrf.h"
#include "nrf_drv_saadc.h"
#include "nrf_drv_ppi.h"
#include "nrf_drv_timer.h"

#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"

#define EVEN    1
#define ODD     0

xSemaphoreHandle xSemaphoreADC0End;
xSemaphoreHandle xSemaphoreADC1End;
xSemaphoreHandle xSemaphoreADC2End;
xSemaphoreHandle xSemaphoreADC3End;

extern xQueueHandle Queue_ADC0Buff;
extern xQueueHandle Queue_ADC1Buff;
extern xQueueHandle Queue_ADC2Buff;
extern xQueueHandle Queue_ADC3Buff;

static uint32_t current_frequency = 500;

extern unsigned short ADCbuffer[4][2];
extern unsigned long ADC_Sum_buffer[4];
unsigned char ADCCounter[4];

#if (CONFIG_TESTING_SIGNALS != 0)
static unsigned short countADC0Meas = 0;
static unsigned short countADC1Meas = 0;
#endif

static int ADC_startFlag = 0;

void PDBConfigure(void) {
    //	NVIC_InitTypeDef NVIC_InitStructure;
#ifdef mdcr_interface
    ADC0->SC2 |= ADC_SC2_ADTRG_MASK; //hardware trigger
    ADC1->SC2 |= ADC_SC2_ADTRG_MASK; //hardware trigger
    ADC2->SC2 |= ADC_SC2_ADTRG_MASK; //hardware trigger
    ADC3->SC2 |= ADC_SC2_ADTRG_MASK; //hardware trigger	

    SIM->SOPT7 = 0;
    ADC0->SC1[0] = ADC_SC1_ADCH(0) | ADC_SC1_AIEN_MASK;
    ADC1->SC1[0] = ADC_SC1_ADCH(0) | ADC_SC1_AIEN_MASK;
    ADC2->SC1[0] = ADC_SC1_ADCH(0) | ADC_SC1_AIEN_MASK;
    ADC3->SC1[0] = ADC_SC1_ADCH(0) | ADC_SC1_AIEN_MASK;
    /*
            ADC0->SC1[0] |= ADC_SC1_DIFF_MASK;								//диффер вход. = Сигнал-земля
            ADC1->SC1[0] |= ADC_SC1_DIFF_MASK;
            ADC2->SC1[0] |= ADC_SC1_DIFF_MASK;
            ADC3->SC1[0] |= ADC_SC1_DIFF_MASK;
     */
    SIM->SCGC6 |= SIM_SCGC6_PDB_MASK;

    /*
            PDB0->SC = PDB_SC_PDBEN_MASK		//PDB enabled
                                                    |PDB_SC_CONT_MASK			//PDB operation in Continuous mode
                                                    |PDB_SC_PRESCALER(1)	// 1:1 PRESCALER
                                                    |PDB_SC_PDBIE_MASK		//PDB interrupt enabled
    //						|PDB_SC_PDBEIE_MASK		//PDB sequence error interrupt enabled						
                                                    |PDB_SC_TRGSEL(15)		//Software trigger is selected 15
                                                    |PDB_SC_LDOK_MASK;		//Load OK
                                                    // Можно править: max 0xFFFF
            PDB0->MOD=25000;									//period of the counter 50Mgz ->1khz/2 = 500sampl/sec
     */

    PDB0->CH[0].C1 |= PDB_C1_EN(1) | PDB_C1_TOS(3); //ADC0 trigger
    PDB0->CH[1].C1 |= PDB_C1_EN(1) | PDB_C1_TOS(3); //ADC1 trigger
    PDB0->CH[2].C1 |= PDB_C1_EN(1) | PDB_C1_TOS(3); //ADC2 trigger
    PDB0->CH[3].C1 |= PDB_C1_EN(1) | PDB_C1_TOS(3); //ADC3 trigger

    /*
            NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);		// For FreeRTOS using only this Priority Group
            NVIC_InitStructure.NVIC_IRQChannel = PDB0_IRQn;
            NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;	// A lower priority value indicates a higher priority
            NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = (uint8_t)(15-1);
            NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
            NVIC_Init(&NVIC_InitStructure);
	
            NVIC_EnableIRQ(PDB0_IRQn);
     */
    //	PDB0->SC	|= PDB_SC_PDBEN_MASK |PDB_SC_SWTRIG_MASK|PDB_SC_LDOK_MASK;  		//software trigger (start PDB)
    //	ADC_startFlag = 1;
#endif
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void mdcr_set_frequency(uint16_t freq)
{
    switch (freq)
    {
        case 100:
        case 120:
        case 125:
        case 250:
        case 500:
        case 1000:
            break;
        default:
            freq = 500;
            break;
    }
    current_frequency = freq;
}

uint16_t mdcr_get_frequency(void)
{
    return current_frequency;
}

void PDB0_IRQHandler(void) {
    //	char i;
}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

void ADC_Start(unsigned int DelayAfter) {
    if (ADC_startFlag == 0) {
#ifdef mdcr_interface
        PDB0->SC = PDB_SC_PDBEN_MASK    //PDB enabled
                | PDB_SC_CONT_MASK      //PDB operation in Continuous mode
                | PDB_SC_PRESCALER(1)   // 1:1 PRESCALER
                | PDB_SC_PDBIE_MASK     //PDB interrupt enabled
                //						|PDB_SC_PDBEIE_MASK		//PDB sequence error interrupt enabled						
                | PDB_SC_TRGSEL(15)     //Software trigger is selected 15
                | PDB_SC_LDOK_MASK;     //Load OK
        // Можно править: max 0xFFFF
        switch (current_frequency)
        {
            case 100:
                PDB0->MOD = 62500;    // 100Hz
                break;
            case 120:       // For test
            case 125:
                PDB0->MOD = 50000;    // 100Hz
                break;
            case 250:
                PDB0->MOD = 25000;     // 250Hz
                break;
            case 500:
                PDB0->MOD = 12500;    // 500Hz
                break;
            case 1000:
                PDB0->MOD = 6250;      // 1000Hz
                break;
            default:
                PDB0->MOD = 6250;      // 1000Hz
                break;
        }
        PDB0->SC |= PDB_SC_PDBEN_MASK | PDB_SC_SWTRIG_MASK | PDB_SC_LDOK_MASK; //software trigger (start PDB)	

#else
        switch (current_frequency)
        {
            case 100:
                // 100Hz
                break;
            case 120:       // For test
            case 125:
                // 100Hz
                break;
            case 250:
                // 250Hz
                break;
            case 500:
                // 500Hz
                break;
            case 1000:
                // 1000Hz
                break;
            default:
                // 1000Hz
                break;
        }        
#endif
        ADC_startFlag = 1;
    }
    if (DelayAfter != 0)
        vTaskDelay(DelayAfter);
    //	vTaskDelay(1000);			// ADC initialization	
}

void emptiedADCQueues(void) {
    ADCQueueStruct ADC_All_Buffer = {0};
    int i = 0;
    for (i = 0; i < SIZE_ADC_QUEUE; i++) {
        if ((xQueueReceive(Queue_ADC0Buff, &ADC_All_Buffer, 0) != pdPASS)
                && (xQueueReceive(Queue_ADC1Buff, &ADC_All_Buffer, 0) != pdPASS)
                && (xQueueReceive(Queue_ADC2Buff, &ADC_All_Buffer, 0) != pdPASS)
                && (xQueueReceive(Queue_ADC3Buff, &ADC_All_Buffer, 0) != pdPASS)
                )
            break;
    }
#if (CONFIG_TESTING_SIGNALS != 0)
    countADC0Meas = 0;
    //		countADC1Meas = 0;
#endif
}

void ADC_Stop(void) {
#ifdef mdcr_interface
    PDB0->SC &= ~PDB_SC_PDBEN_MASK; //software trigger (stop PDB)
#endif
    emptiedADCQueues();
    ADC_startFlag = 0;
}
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#ifdef mdcr_interface
void ADCConfigure(ADC_Type* ADC) {
    //	char i;

    NVIC_InitTypeDef NVIC_InitStructure;
    if (ADC == ADC0) {
        SIM->SCGC6 |= SIM_SCGC6_ADC0_MASK; // Enable ADC0 clock
    } else if (ADC == ADC1) {
        SIM->SCGC3 |= SIM_SCGC3_ADC1_MASK;
    } else if (ADC == ADC2) {
        SIM->SCGC6 |= SIM_SCGC6_ADC2_MASK;
    } else if (ADC == ADC3) {
        SIM->SCGC3 |= SIM_SCGC3_ADC3_MASK;
    }

    ADC->CFG1 |= ADC_CFG1_ADICLK(ADC_Bus_Clock) // "ADC_CFG1_ADICLK(ADC_Bus_Clock_Dev_2)" not working
            | ADC_CFG1_MODE(ADC_16bit_MODE)
            | ADC_CFG1_ADLSMP_MASK // using long sample time
            | ADC_CFG1_ADIV(0); // clock devider = 1

    //	ADC->CFG1 |= ADC_CFG1_ADICLK(ADC_Bus_Clock_Dev_2)
    //							 |ADC_CFG1_MODE(ADC_16bit_MODE)
    //							 |ADC_CFG1_ADLSMP_MASK				// using long sample time
    //							 |ADC_CFG1_ADIV(3);		 			// clock divider = 1
    ///|ADC_CFG1_ADLPC_MASK; 			// using normal power mode

    //  ADC->SC1[0] = ADC_SC1_ADCH(31); //disable module

#if ENABLE_ExternalADC_Vcc >0 // if Vcc = 3,3V
    {
        ADC->SC2 = 0x00; // using software trigger (conversion start after wtiting to SC1[0])
        ADC->CV1 = 0x00; //
        ADC->PGA = 0;
    }
#else       // if Vcc = 1,2V
    {
        //	ADC3->SC1[0] |= ADC_SC1_ADCH(0)|ADC_SC1_AIEN_MASK;
        VREF ->TRM |= VREF_TRM_TRIM(0x1F); //setting the built-in 1.2V reference
        VREF ->SC = 0x00;
        VREF ->SC |= VREF_SC_VREFEN_MASK    //Internal Voltage Reference enable
                | VREF_SC_REGEN_MASK        //Regulator enable
                | VREF_SC_VREFST_MASK       //Internal Voltage Reference has settled
                | VREF_SC_MODE_LV(2);       //buffer is enabled to generate a buffered 1.2 V
        ADC->SC2 = 0x01; // кидаем опрорное на VREF_Out

        while (VREF ->SC & VREF_SC_VREFST_MASK) // waiting for the end of the transients Inc. VREF_OUT
        {
        }
    }
#endif


#if ENABLE_FreeRunADC > 0
    //		ADC->SC3 = 0;	
    ADC->SC3 = ADC_SC3_AVGE_MASK    //Hardware average function enabled
            | ADC_SC3_AVGS(3);      //32 samples averaged
    //							|ADC_SC3_ADCO_MASK;		//Continuous conversions	

#endif
    /*	
            for (i=0; i<4; i++)
            {
                    ADC_Sum_buffer[i]	=0;
                    ADCCounter[i] = 0;
            }
     */

    // ADC0 Interrupt configure
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4); // For FreeRTOS using only this Priority Group
    if (ADC == ADC0) {
        NVIC_InitStructure.NVIC_IRQChannel = ADC0_IRQn;
    } else if (ADC == ADC1) {
        NVIC_InitStructure.NVIC_IRQChannel = ADC1_IRQn;
    } else if (ADC == ADC2) {
        NVIC_InitStructure.NVIC_IRQChannel = ADC2_IRQn;
    } else if (ADC == ADC3) {
        NVIC_InitStructure.NVIC_IRQChannel = ADC3_IRQn;
    } else {
        return;
    }

    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0; // A lower priority value indicates a higher priority
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = (uint8_t) (15 - 1);
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

#endif

void ADC0_IRQHandler(void) {
#ifdef mdcr_interface
    if (ADC0->SC1[0] & ADC_SC1_COCO_MASK) {
        portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
        static ADCQueueStruct ADC0Buffer;
        static uint32_t R = 0ULL;
        static uint8_t count = 1;
        
        if (count--)
        {
            R = ADC0->R[0];
            return;
        }
#if (CONFIG_TESTING_SIGNALS == 1)
        ADC0Buffer.ADCxBuff = countADC0Meas;
        countADC0Meas += 2;
        if (countADC0Meas >= 32768)
            countADC0Meas = 0;
#else
        ADC0Buffer.ADCxBuff = (ADC0->R[0] + R) >> 1;
#endif
        xQueueSendToBackFromISR(Queue_ADC0Buff, &ADC0Buffer, &xHigherPriorityTaskWoken);
        count = 1;
        return;
    }
#endif
}

void ADC1_IRQHandler(void) 
{
#ifdef mdcr_interface
    if (ADC1->SC1[0] & ADC_SC1_COCO_MASK) 
    {
        static portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
        static ADCQueueStruct ADC1Buffer;
        static uint32_t R = 0ULL;
        static uint8_t count = 1;
        
        if (count--)
        {
            R = ADC1->R[0];
            return;
        }

#if (CONFIG_TESTING_SIGNALS == 1)
        ADC1Buffer.ADCxBuff = countADC1Meas;
        countADC1Meas += 2;
        if (countADC1Meas >= 32768)
            countADC1Meas = 0;
#else
        ADC1Buffer.ADCxBuff = (ADC1->R[0] + R) >> 1;
#endif
        xQueueSendToBackFromISR(Queue_ADC1Buff, &ADC1Buffer, &xHigherPriorityTaskWoken);
        count = 1;
        return;
    }
#endif
}

void ADC2_IRQHandler(void) 
{
#ifdef mdcr_interface
    if (ADC2->SC1[0] & ADC_SC1_COCO_MASK) 
    {
        static portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
        static ADCQueueStruct ADC2Buffer;
        static uint32_t R = 0ULL;
        static uint8_t count = 1;
        
        if (count--)
        {
            R = ADC2->R[0];
            return;
        }
        ADC2Buffer.ADCxBuff = (ADC2->R[0] + R) >> 1;
        xQueueSendToBackFromISR(Queue_ADC2Buff, &ADC2Buffer, &xHigherPriorityTaskWoken);
        count = 1;
        return;
    }
#endif
}

void ADC3_IRQHandler(void) 
{
#ifdef mdcr_interface
    if (ADC3->SC1[0] & ADC_SC1_COCO_MASK) 
    {
        static portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
        static ADCQueueStruct ADC3Buffer;
        static uint32_t R = 0ULL;
        static uint8_t count = 1;
        
        if (count--)
        {
            R = ADC3->R[0];
            return;
        }
        ADC3Buffer.ADCxBuff = (ADC3->R[0] + R) >> 1;
        xQueueSendToBackFromISR(Queue_ADC3Buff, &ADC3Buffer, &xHigherPriorityTaskWoken);
        count = 1;
        return;
    }
#endif
}

/* NRF52 - SADC Configure */

#define SAMPLES_IN_BUFFER 1
#define NUM_OF_BUFFERS 4
volatile uint8_t state = 1;

static const nrf_drv_timer_t m_timer = NRF_DRV_TIMER_INSTANCE(0);
static nrf_saadc_value_t     m_buffer_pool[2][SAMPLES_IN_BUFFER*NUM_OF_BUFFERS];
static nrf_ppi_channel_t     m_ppi_channel;
static uint32_t              m_adc_evt_counter;


void timer_handler(nrf_timer_event_t event_type, void * p_context)
{

}

void timer_set_freq(nrfx_timer_t const * const inst_timer, uint8_t ms)
{
    /* setup m_timer for compare event every 5ms */
    nrf_drv_timer_disable(inst_timer);
    uint32_t ticks = nrf_drv_timer_ms_to_ticks(&m_timer, ms);
    nrf_drv_timer_extended_compare(inst_timer,NRF_TIMER_CC_CHANNEL0, ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, false);
    nrf_drv_timer_enable(inst_timer);    
}

void saadc_sampling_event_init(void)
{
    ret_code_t err_code;

    err_code = nrf_drv_ppi_init();
    APP_ERROR_CHECK(err_code);

    nrf_drv_timer_config_t timer_cfg = NRF_DRV_TIMER_DEFAULT_CONFIG;
    timer_cfg.bit_width = NRF_TIMER_BIT_WIDTH_32;
    err_code = nrf_drv_timer_init(&m_timer, &timer_cfg, timer_handler);
    APP_ERROR_CHECK(err_code);

    /* setup m_timer for compare event every 5ms */
//    uint32_t ticks = nrf_drv_timer_ms_to_ticks(&m_timer, 5);
//    nrf_drv_timer_extended_compare(&m_timer,NRF_TIMER_CC_CHANNEL0, ticks, NRF_TIMER_SHORT_COMPARE0_CLEAR_MASK, false);
//    nrf_drv_timer_enable(&m_timer);

    timer_set_freq(&m_timer, 2);

    uint32_t timer_compare_event_addr = nrf_drv_timer_compare_event_address_get(&m_timer, NRF_TIMER_CC_CHANNEL0);
    uint32_t saadc_sample_task_addr   = nrf_drv_saadc_sample_task_get();

    /* setup ppi channel so that timer compare event is triggering sample task in SAADC */
    err_code = nrf_drv_ppi_channel_alloc(&m_ppi_channel);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_ppi_channel_assign(m_ppi_channel, timer_compare_event_addr, saadc_sample_task_addr);
    APP_ERROR_CHECK(err_code);
}


void saadc_sampling_event_enable(void)
{
    ret_code_t err_code = nrf_drv_ppi_channel_enable(m_ppi_channel);

    APP_ERROR_CHECK(err_code);
}


void saadc_callback(nrf_drv_saadc_evt_t const * p_event)
{
    if (p_event->type == NRF_DRV_SAADC_EVT_DONE)
    {
        ret_code_t err_code;

        err_code = nrf_drv_saadc_buffer_convert(p_event->data.done.p_buffer, SAMPLES_IN_BUFFER*NUM_OF_BUFFERS);
        APP_ERROR_CHECK(err_code);

        int i;
        NRF_LOG_INFO("ADC event number: %d", (int)m_adc_evt_counter);

        for (i = 0; i < SAMPLES_IN_BUFFER*NUM_OF_BUFFERS; i++)
        {
            NRF_LOG_INFO("%d", p_event->data.done.p_buffer[i]);
        }
        m_adc_evt_counter++;
    }
}


void saadc_init(void)
{
    ret_code_t err_code;

    nrf_drv_saadc_config_t saadc_config = NRF_DRV_SAADC_DEFAULT_CONFIG;
    saadc_config.resolution = NRF_SAADC_RESOLUTION_14BIT;

    err_code = nrf_drv_saadc_init(&saadc_config, saadc_callback);
    APP_ERROR_CHECK(err_code);

    nrf_saadc_channel_config_t channel_config_1 = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN0);
    nrf_saadc_channel_config_t channel_config_2 = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN1);
    nrf_saadc_channel_config_t channel_config_3 = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN2);
    nrf_saadc_channel_config_t channel_config_4 = NRF_DRV_SAADC_DEFAULT_CHANNEL_CONFIG_SE(NRF_SAADC_INPUT_AIN3);


    err_code = nrf_drv_saadc_channel_init(0, &channel_config_1);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(1, &channel_config_2);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(2, &channel_config_3);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_channel_init(3, &channel_config_4);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[0], SAMPLES_IN_BUFFER*NUM_OF_BUFFERS);
    APP_ERROR_CHECK(err_code);

    err_code = nrf_drv_saadc_buffer_convert(m_buffer_pool[1], SAMPLES_IN_BUFFER*NUM_OF_BUFFERS);
    APP_ERROR_CHECK(err_code);
}