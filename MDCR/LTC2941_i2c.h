/*
 * File:		LTC2941_i2c.h
 * Purpose:     
 *
 * Notes:
 */

#ifndef __LTC2941_i2c_H__
#define __LTC2941_i2c_H__

//#include "common.h"
//#include "LTC2941_i2c.h"
#include "iic.h"

/********************************************************************/													 
#define IIC_SLAVE_ADDRESS			0x64 //I2C address of LTC2941 is 1100100.+0rw/wr
#define IIC_ALERT_ADDRESS			0x19 //I2C ALERT RESPONSE ADDRESS of LTC2941 is 0001100.+1rw/wr

//LTC2941
#define LTC2941_ADR_Status					0x00	// A -1bite
#define LTC2941_ADR_Control					0x01	// B -1bite
#define LTC2941_ADR_Charge					0x02	// C,D - 2 bite (defol 7F FF)
#define LTC2941_ADR_ChThHI					0x04	// E,F - 2 bite Charge Threshold High def(FF FF)
#define LTC2941_ADR_ChThLO					0x06	// G,H - 2 bite Charge Threshold Lo 	def(00 00)

#define LTC2941_ChipIDMASK					0x80	//Chip Identification
#define LTC2941_OVFMASK							0x20	//Accumulated Charge Overflow/Underflow
#define LTC2941_ChHiAlertMASK				0x08	//Charge Alert High
#define LTC2941_ChLoAlertMASK				0x04	//Charge Alert Low
#define LTC2941_VBATAlertMASK				0x02	//VBAT Alert


typedef struct _tsMetaChrgElement
{
	unsigned short CHRG: 1;											//1=chrging
	unsigned short VALLERT: 1;									//1=lo volt (<3V)
	unsigned short CYCLE: 1;										//1=Cycle	
	unsigned short USB_CON: 1;										//1=USB connected		
	unsigned short dummy: 	12;
}tsMetaChrgElement;

// Meta Data Element
typedef union _uMetaElement
{
	tsMetaChrgElement 	BIT;
	unsigned short ALL;	
}uMetaChrgElement;


typedef struct 
{
	unsigned char Status;	
	unsigned char Control;	
	unsigned short Charge;	
	unsigned short ChThHI;	
	unsigned short ChThLO;		
}tsIIC_LTC2941Data;

/*
typedef struct
{
unsigned char Status;	
unsigned char Control;	
unsigned short Charge;	
unsigned short ChThHI;	
unsigned short ChThLO;	
}tsIIC_LTC2941Data;
*/



/********************************************************************/
extern void LTC2941_ask(void);
extern void LTC2941_write(unsigned char *ucIIC_TxBuff, unsigned char len,unsigned char LTC2941_REG);
															 
/********************************************************************/

#endif /* __IIC_H__ */
