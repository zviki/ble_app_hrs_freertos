#ifndef __K60N_GPIO_H_
#define __K60N_GPIO_H_

 #define LED1_RED				(1<<26)
 #define LED1_GR				(1<<27)
 #define LED2_RED				(1<<28)
 #define LED2_YW				(1<<29)


 #define LED3_BLUE				(1<<22)
 #define LED3_RED				(1<<23) 

#ifdef MDCR_interface
 #define LED_PORT				 PTA
 #define LED3_PORT				PTB


 #define LED1_YW_TGL 			LED_PORT->PTOR = LED2_YW
 #define LED1_RED_TGL 			LED_PORT->PTOR = LED2_RED
 #define LED2_GR_TGL 			LED_PORT->PTOR = LED1_GR
 #define LED2_RED_TGL 			LED_PORT->PTOR = LED1_RED

 #define LED3_BL_TGL 			LED3_PORT->PTOR = LED3_BLUE
 #define LED3_RED_TGL 			LED3_PORT->PTOR = LED3_RED

 #define LED1_YW_SET 			LED_PORT->PSOR = LED2_YW
 #define LED1_RED_SET 			LED_PORT->PSOR = LED2_RED
 #define LED2_GR_SET 			LED_PORT->PSOR = LED1_GR
 #define LED2_RED_SET 			LED_PORT->PSOR = LED1_RED

 #define LED3_BL_SET 			LED3_PORT->PCOR = LED3_BLUE
 #define LED3_RED_SET 			LED3_PORT->PCOR = LED3_RED

 #define LED1_YW_CLR 			LED_PORT->PCOR = LED2_YW
 #define LED1_RED_CLR 			LED_PORT->PCOR = LED2_RED
 #define LED2_GR_CLR 			LED_PORT->PCOR = LED1_GR
 #define LED2_RED_CLR 			LED_PORT->PCOR = LED1_RED

 #define LED3_BL_CLR 			LED3_PORT->PSOR = LED3_BLUE
 #define LED3_RED_CLR 			LED3_PORT->PSOR = LED3_RED
#endif
 #define LED_PORT				 
 #define LED3_PORT				


 #define LED1_YW_TGL 			
 #define LED1_RED_TGL 			
 #define LED2_GR_TGL 			
 #define LED2_RED_TGL 			

 #define LED3_BL_TGL 			
 #define LED3_RED_TGL 			

 #define LED1_YW_SET 			
 #define LED1_RED_SET 		
 #define LED2_GR_SET 			
 #define LED2_RED_SET 		

 #define LED3_BL_SET 			
 #define LED3_RED_SET 		

 #define LED1_YW_CLR 			
 #define LED1_RED_CLR 		
 #define LED2_GR_CLR 			
 #define LED2_RED_CLR 		

 #define LED3_BL_CLR 			
 #define LED3_RED_CLR 	
         
 #define LED1_GR_SET 	
 #define LED2_YW_SET

 #define SW2						24
 #define SW1						25
 #define ANALOG_SNS					9

typedef struct _tsMetaSwElement
{
	unsigned short SW1_flag: 1;
	unsigned short SW2_flag: 1;
	unsigned short SW3_flag: 1;
	unsigned short SW1_GUI_flag: 1;
	unsigned short SW2_GUI_flag: 1;
	unsigned short SW3_GUI_flag: 1;	
	unsigned short dummy: 	10;
}tsMetaSwElement;

// Meta Data Element
typedef union _uMetaSwElement
{
	tsMetaSwElement 	BIT;
	unsigned short 		ALL;
}uMetaSwElement;

void LED_Clear(void);
void Dinit_portX(void);

#endif
