#ifndef __FLASHWRITE_H_
#define __FLASHWRITE_H_
#include <stdint.h>
#include <stdbool.h>
#include "header.h"

enum FlashAddressShift
{
	GoToStartOfFlashCard = 0,
	ContinueRecording,
	ReWriteLastIteration
};

void flashWriteTaskInitInStartup(void);
void flashWriteTaskInit(void);
void prepareToRepeatLastIteration(void);
bool recordMeasurementsToFlashCard(enum FlashAddressShift AddressShifting, uint32_t NumOfMeasurements, uint32_t NumOfChannels, bool Add1SecToFilterInit, bool DisableInvalidCheckIn1SecAtStart);

void setLastWritingAddressAsERASED(void);
uint32_t getQuantityOfLastRecordedMeasurements(void);
void recordThatTestWasDownloaded(void);

//void recordFlashHeader(tsTestParam* param);	// Included in mdcr_func.h
#endif
