/*************************************************************
 *	  File:		mdcr_func.c
 *	Author:		Kirillov A.V.
 *************************************************************/

#define __C_MDCR_

#include "mdcr_func.h"
#include "mdcr_fir_old.h"
#include "header.h"
#include "k60n_uart.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "queue.h"
#include "k60n_gpio.h"
#include <math.h>
#include "bt43.h"
#include "lzfx.h"
#include "mdcr_comp.h"
#include "LTC2941_i2c.h"
#include "app_flash.h"
#include "sst26vf064b_driver.h"

#define _USE_MATH_DEFINES // for C
#define  M_PI       3.161592653

#define SPIBUF_len     4096
unsigned short pointerSPIbuf;
volatile uint32_t AddressFlash_old;
volatile uint32_t AddressFlash_delta;
bool waitAppLessTest = true;
extern uint32_t timeOfAPP_LESS_TEST2;
extern uint32_t tim;

#if ENABLE_TestSignalADC > 0 
unsigned short TestCounter = 0;
#endif

//	long long addressNextPage;
//	unsigned char FlashReady=0;									//1 - стерта, готова к записи. Флаг
unsigned short flashData[512];
//unsigned char SPIbuf[SPIBUF_len]={0};
//---------------------------
#if (CompressedFirmWare != 0)
unsigned char tempOut[7000];
unsigned char buff4k[4100];
unsigned int sizeObuf = sizeof (buff4k);
#endif
//---------------------------
extern const char date_of_compile[30];
extern const char time_of_compile[];
extern const char SN_device[9];
extern const char Bee_APP_Software[2];
extern const char Bee_Device_version[2];
extern tsIIC_LTC2941Data LTC2941_Reg;
//---------------------------

extern xTaskHandle xHandleLed1Gr;
extern xTaskHandle xHandleLed2Yw;

extern xQueueHandle xQueueMdcrTest;
extern xSemaphoreHandle xSemaphoreADC0End;
extern xSemaphoreHandle xSemaphoreADC1End;
extern xSemaphoreHandle xSemaphoreADC2End;
extern xSemaphoreHandle xSemaphoreADC3End;
extern xSemaphoreHandle xSemaphoreKeyEnd;
extern uTimersDataElement TimersDataFlag;
extern xTimerHandle xOneShotTimers[NUMBER_OF_TIMERS]; // Descriptor array interval timers
extern uMetaBT43Element BT43DataFlag;
extern xQueueHandle xQueueBTSend;

extern xQueueHandle Queue_ADC0Buff;
extern xQueueHandle Queue_ADC1Buff;
extern xQueueHandle Queue_ADC2Buff;
extern xQueueHandle Queue_ADC3Buff;

extern xQueueHandle Queue_BuffToWriteIntoFlash;
enum StatesOfLEDsInTests currentLEDState = AllLEDsOff;
unsigned short ADCbuffer[4][2];
extern uMetaSwElement SwDataFlag;
uint32_t ADC_Sum_buffer[4];
uADCMask ADCMaskMeasuring;
tsMDCRCheckSignal DataValidSignal;
tsMDCRCheckSignal2 ValidateSignal2;
tsMDCRCheckSignal3 ValidateSignal3;

//----------------filter 50-60Hz
SampleFilter FIRFilter0L;
SampleFilter FIRFilter1L;
SampleFilter FIRFilter2L;
SampleFilter FIRFilter3L;
//-----------filter 100-120Hz
SampleFilter FIRFilter0H;
SampleFilter FIRFilter1H;
SampleFilter FIRFilter2H;
SampleFilter FIRFilter3H;
BandPasFilter FFTFilter50;
BandPasFilter FFTFilter60;
unsigned short FIRdataADC[4];

tsFFTDataSignal FFTData;
tsFFTDataBuf FFTData50Hz;
tsFFTDataBuf FFTData60Hz;

void setLEDState(enum StatesOfLEDsInTests LEDState)
{
    vTaskSuspend(xHandleLed1Gr); //stop blinks Green
    vTaskSuspend(xHandleLed2Yw); //stop blinks Yellow
    LED_Clear();
    switch (LEDState)
    {
        case YellowSolid:
            LED1_YW_SET;
            break;
        case YellowBlink:
            vTaskResume(xHandleLed2Yw);
            break;
        case GreenSolid:
            LED2_GR_SET;
            break;
        case GreenBlink:
            vTaskResume(xHandleLed1Gr);
            break;
        case YellowAndGreenSolid:
            LED2_GR_TGL;
            LED1_YW_TGL;
            break;
        case YellowAndGreenBlink:
            vTaskResume(xHandleLed1Gr);
            vTaskResume(xHandleLed2Yw);
            break;
        case YellowAndRed2Solid:
            LED1_YW_SET;
            LED2_RED_SET;
            break;
        case Red1Solid:
            LED1_RED_SET;
            break;
        case AllLEDsOff:
            break;
        case NotDefined:
            break;
        case Red1AndRed2Solid:
            LED1_RED_SET;
            LED2_RED_SET;
            break;
        default:
            LED1_YW_SET;
            LED2_GR_SET;
            LED1_RED_SET;
            LED2_RED_SET;
            break;
    }
    currentLEDState = LEDState;
}

void startTimerLED30SecAfterTests(void)
{
    int i = 0;
    if (xTimerIsTimerActive(xOneShotTimers[ID_TIMER_1]) == pdTRUE)
    {
        if (xTimerReset(xOneShotTimers[ID_TIMER_1], 0) == pdFAIL)
            xTimerReset(xOneShotTimers[ID_TIMER_1], 0);
    } else
        xTimerStart(xOneShotTimers[ID_TIMER_1], 0);
    for (i = 0; i < 0xFFFF; i++)
        i = i;
    TimersDataFlag.BIT.TIMER_1 = 0;
}

void stopTimerLED30SecAfterTests(void)
{
    xTimerStop(xOneShotTimers[ID_TIMER_1], 0);
    xTimerStop(xOneShotTimers[ID_TIMER_1], 0);
    TimersDataFlag.BIT.TIMER_1 = 0;
}

void startTimer2(void)
{
    unsigned int i = 0;
    unsigned int j = 0;
    if (xTimerIsTimerActive(xOneShotTimers[ID_TIMER_2]) == pdTRUE)
    {
        for (i = 0; i < 5; i++)
        {
            if (xTimerReset(xOneShotTimers[ID_TIMER_2], 8) == pdFAIL)
            {
                for (j = 0; j < 0xFFFF; j++)
                    j = j + 1 - 1;
            } else
                break;
        }
    } else
    {
        for (i = 0; i < 5; i++)
        {
            if (xTimerStart(xOneShotTimers[ID_TIMER_2], 5) == pdFAIL)
            {
                for (j = 0; j < 0xFFFF; j++)
                    j = j + 1 - 1;
            } else
                break;
        }
    }
    for (i = 0; i < 0xFFFF; i++)
        i = i + 1 - 1;
    TimersDataFlag.BIT.TIMER_2 = 0;
}

void startTimer60sec(void)
{
    unsigned int i = 0;
    unsigned int j = 0;
    if (xTimerIsTimerActive(xOneShotTimers[ID_TIMER_60sec]) == pdTRUE)
    {
        for (i = 0; i < 5; i++)
        {
            if (xTimerReset(xOneShotTimers[ID_TIMER_60sec], 8) == pdFAIL)
            {
                for (j = 0; j < 0xFFFF; j++)
                    j = j + 1 - 1;
            } else
                break;
        }
    } else
    {
        for (i = 0; i < 5; i++)
        {
            if (xTimerStart(xOneShotTimers[ID_TIMER_60sec], 5) == pdFAIL)
            {
                for (j = 0; j < 0xFFFF; j++)
                    j = j + 1 - 1;
            } else
                break;
        }
    }
    for (i = 0; i < 0xFFFF; i++)
        i = i + 1 - 1;
    TimersDataFlag.BIT.TIMER_60sec = 0;
}

void stopTimer2(void)
{
    xTimerStop(xOneShotTimers[ID_TIMER_2], 0);
    TimersDataFlag.BIT.TIMER_2 = 0;
}

void stopTimer60sec(void)
{
    xTimerStop(xOneShotTimers[ID_TIMER_60sec], 0);
    TimersDataFlag.BIT.TIMER_60sec = 0;
}

void SendMessage_ExtPlugMisconnected(tsTestParam* param)
{
    int len = 0;
    char buff[10] = {0};
    len = ExtPlugMisconnect(buff);
    if (param->appLessMode == 0)
    {
        if (param->acqMode == STREAM)
            sendDataUSART_Async(buff, len);
        else
            UART0SendData(buff, len);
    }
}

void SendMessage_MeasStart(tsTestParam* param)
{
    int len = 0;
    char buff[10] = {0};
    len = fSendCmdMeasStart(buff, param);
    if (param->appLessMode == 0)
    {
        if (param->acqMode == STREAM)
            sendDataUSART_Async(buff, len);
        else
            UART0SendData(buff, len);
    }
}

void SendResultsStreamMode(tsTestParam* param, unsigned short* FIRdataADC)
{
    int len = 0;
    char buff[20] = {0};
    len = fWriteResults(buff, param, FIRdataADC);
    sendDataUSART_Async(buff, len);
}

void SendMessage_ReadResults(tsTestParam* param)
{
    int len = 0;
    char buff[10] = {0};
    if (param->appLessMode == 0)
    {
        len = fReadResults((unsigned char*) buff, param);
        UART0SendData(buff, len);
    }
}

void SendMessage_MeasInterrupt(tsTestParam* param)
{
    int len = 0;
    char buff[4] = {0};
    len = MeasInterrupt(buff);
    if (param->appLessMode == 0)
    {
        if (param->acqMode == STREAM)
            sendDataUSART_Async(buff, len);
        else
            UART0SendData(buff, len);
    }
}

void SendMessage_MeasContinue(tsTestParam* param)
{
    int len = 0;
    char buff[4] = {0};
    len = MeasContinue(buff);
    if (param->appLessMode == 0)
    {
        if (param->acqMode == STREAM)
            sendDataUSART_Async(buff, len);
        else
            UART0SendData(buff, len);
    }
}

void SendMessage_MeasFinish(tsTestParam* param)
{
    int len = 0;
    char buff[10] = {0};
    len = fSendFinishCommand(buff, param);
    if (param->appLessMode == 0)
    {
        if (param->acqMode == STREAM)
            sendDataUSART_Async(buff, len);
        else
            UART0SendData(buff, len);
    }
}

void SendMessage_NoValidSignal(tsTestParam* param)
{
    int len = 0;
    char buff[4] = {0};
    len = NoValidSignal(buff);
    if (param->appLessMode == 0)
    {
        if (param->acqMode == STREAM)
            sendDataUSART_Async(buff, len);
        else
            UART0SendData(buff, len);
    }
}

void SendMessage_GoingIntoHib(tsTestParam* param)
{
    int len = 0;
    char buff[4] = {0};
    len = GoingIntoHibernation(buff);
    if (param->appLessMode == 0)
    {
        if (param->acqMode == STREAM)
            sendDataUSART_Async(buff, len);
        else
            UART0SendData(buff, len);
    }
}

void SampleFilter_init(SampleFilter* f)
{
    int i;
    for (i = 0; i < SAMPLEFILTER_TAP_NUM; ++i)
        f->history[i] = 0;
    f->last_index = 0;
}

void SampleFilter_put(SampleFilter* f, int input)
{
    f->history[f->last_index++] = input;
    if (f->last_index == SAMPLEFILTER_TAP_NUM)
        f->last_index = 0;
}
//-------------for 3 ch ADC___________---------------

int SampleFilter_get50_3(SampleFilter* f)
{
    long long acc = 0;
    int index = f->last_index, i;
    for (i = 0; i < SAMPLEFILTER_TAP_NUM; ++i)
    {
        index = index != 0 ? index - 1 : SAMPLEFILTER_TAP_NUM - 1;
        acc += (long long) f->history[index] * FIRCoef50_3[i];
    };
    return acc >> 16;
}

int SampleFilter_get60_3(SampleFilter* f)
{
    long long acc = 0;
    int index = f->last_index, i;
    for (i = 0; i < SAMPLEFILTER_TAP_NUM; ++i)
    {
        index = index != 0 ? index - 1 : SAMPLEFILTER_TAP_NUM - 1;
        acc += (long long) f->history[index] * FIRCoef60_3[i];
    };
    return acc >> 16;
}

int SampleFilter_get100_3(SampleFilter* f)
{
    long long acc = 0;
    int index = f->last_index, i;
    for (i = 0; i < SAMPLEFILTER_TAP_NUM; ++i)
    {
        index = index != 0 ? index - 1 : SAMPLEFILTER_TAP_NUM - 1;
        acc += (long long) f->history[index] * FIRCoef100_3[i];
    };
    return acc >> 16;
}

int SampleFilter_get120_3(SampleFilter* f)
{
    long long acc = 0;
    int index = f->last_index, i;
    for (i = 0; i < SAMPLEFILTER_TAP_NUM; ++i)
    {
        index = index != 0 ? index - 1 : SAMPLEFILTER_TAP_NUM - 1;
        acc += (long long) f->history[index] * FIRCoef120_3[i];
    };
    return acc >> 16;
}

void BandPasFilter_init(BandPasFilter* f)
{
    int i;
    for (i = 0; i < BandPasFILTER_TAP_NUM; ++i)
        f->history[i] = 0;
    f->last_index = 0;
}

void BandPas50Filter_put(BandPasFilter* f, int input)
{
    f->history[f->last_index++] = input;
    if (f->last_index == BandPasFILTER_TAP_NUM)
        f->last_index = 0;
}

void BandPas60Filter_put(BandPasFilter* f, int input)
{
    f->history[f->last_index++] = input;
    if (f->last_index == BandPasFILTER_TAP_NUM)
        f->last_index = 0;
}

int BandPas50Filter_get(BandPasFilter* f)
{
    long long acc = 0;
    int index = f->last_index, i;
    for (i = 0; i < BandPasFILTER_TAP_NUM; ++i)
    {
        index = index != 0 ? index - 1 : BandPasFILTER_TAP_NUM - 1;
        acc += (long long) f->history[index] * FIRCoef50BandPass[i];
    };
    return acc >> 16;
}

int BandPas60Filter_get(BandPasFilter* f)
{
    long long acc = 0;
    int index = f->last_index, i;
    for (i = 0; i < BandPasFILTER_TAP_NUM; ++i)
    {
        index = index != 0 ? index - 1 : BandPasFILTER_TAP_NUM - 1;
        acc += (long long) f->history[index] * FIRCoef60BandPass[i];
    };
    return acc >> 16;
}

int fWriteResults(char* cmdBuff, tsTestParam* pDataStruct, unsigned short* pData)
{
    memset(&cmdBuff[0], 0, 16);
    if (pDataStruct->acqMode == STREAM)
    {
        if (pDataStruct->CurrentSampleInIteration == 0) //DATA SEND																				// DATA SEND
        {
            uint32_t DataLength;
            DataLength = pDataStruct->numChankSamples;
            cmdBuff[0] = DATA_SEND;
            cmdBuff[1] = 0;
            cmdBuff[2] = (unsigned char) (pDataStruct->curMetaDE.byte & 0xFF); // Meta Data Element
            cmdBuff[3] = (unsigned char) (pDataStruct->mode); // Send mode					
            cmdBuff[4] = (unsigned char) (DataLength >> 0) & 0xFF;
            cmdBuff[5] = (unsigned char) (DataLength >> 8) & 0xFF;
            cmdBuff[6] = (unsigned char) (DataLength >> 16) & 0xFF;
            cmdBuff[7] = (unsigned char) (DataLength >> 24) & 0xFF;
            cmdBuff[8] = pDataStruct->validity;
            cmdBuff[9] = (char) (pData[0]&0xFF);
            cmdBuff[10] = (char) ((pData[0] >> 8)&0xFF);
            cmdBuff[11] = (char) (pData[1]&0xFF);
            cmdBuff[12] = (char) ((pData[1] >> 8)&0xFF);
            cmdBuff[13] = (char) (pData[2]&0xFF);
            cmdBuff[14] = (char) ((pData[2] >> 8)&0xFF);

            if (pDataStruct->numChannels == 4)
            {
                cmdBuff[15] = (char) (pData[3]&0xFF);
                cmdBuff[16] = (char) ((pData[3] >> 8)&0xFF);
                return 17;
            } else
                return 15;
        } else // Single read mode data 
        {
            cmdBuff[0] = pDataStruct->validity;
            cmdBuff[1] = (char) (pData[0]&0xFF);
            cmdBuff[2] = (char) ((pData[0] >> 8)&0xFF);
            cmdBuff[3] = (char) (pData[1]&0xFF);
            cmdBuff[4] = (char) ((pData[1] >> 8)&0xFF);
            cmdBuff[5] = (char) (pData[2]&0xFF);
            cmdBuff[6] = (char) ((pData[2] >> 8)&0xFF);
            if (pDataStruct->numChannels == 4)
            {
                cmdBuff[7] = (char) (pData[3]&0xFF);
                cmdBuff[8] = (char) ((pData[3] >> 8)&0xFF);
                return 9;
            } else
                return 7;
        }
    }
    return 0;
}

int fWriteResultsHiLo(char* cmdBuff, tsTestParam* pDataStruct, unsigned short* pData)
{
    uint32_t numData;
    numData = pDataStruct->numChannels * 2; //
    memset(&cmdBuff[0], 0, 16);
    cmdBuff[0] = DATA_SEND; //
    cmdBuff[1] = 0; //
    cmdBuff[2] = (unsigned char) (pDataStruct->curMetaDE.byte & 0xFF); // Meta Data Element
    cmdBuff[3] = (unsigned char) (pDataStruct->mode); // Send mode
    cmdBuff[4] = (unsigned char) numData; // Data Length.
    cmdBuff[5] = 0; //
    cmdBuff[6] = 0; //
    cmdBuff[7] = 0; //
    cmdBuff[8] = pDataStruct->validity;
    cmdBuff[10] = (char) (pData[0]&0xFF);
    cmdBuff[9] = (char) ((pData[0] >> 8)&0xFF);
    cmdBuff[12] = (char) (pData[1]&0xFF);
    cmdBuff[11] = (char) ((pData[1] >> 8)&0xFF);
    cmdBuff[14] = (char) (pData[2]&0xFF);
    cmdBuff[13] = (char) ((pData[2] >> 8)&0xFF);
    cmdBuff[16] = (char) (pData[3]&0xFF);
    cmdBuff[15] = (char) ((pData[3] >> 8)&0xFF);
    return 9 + numData;
}

int fReadResults(unsigned char* cmdBuff, tsTestParam* pDataStruct)
{
    uint32_t numData;
    if (pDataStruct->mode == CHUNK_MODE)
        numData = pDataStruct->numChankSamples;
    else if (pDataStruct->mode == COMPRESSED_CHUNK_MODE)
        numData = pDataStruct->numCompressedData;
    else
        numData = pDataStruct->CurrentSampleInIteration;
    memset(&cmdBuff[0], 0, 8);
    cmdBuff[0] = DATA_SEND; //
    cmdBuff[1] = 0; //
    cmdBuff[2] = (unsigned char) (pDataStruct->curMetaDE.byte & 0xFF); // Meta Data Element
    cmdBuff[3] = (unsigned char) (pDataStruct->mode); // Send mode
    cmdBuff[4] = numData & 0xFF;
    cmdBuff[5] = (numData >> 8)&0xFF;
    cmdBuff[6] = (numData >> 16)&0xFF;
    cmdBuff[7] = (numData >> 24)&0xFF;
    return 8;
}

int fSendCmdMeasStart(char* buff, tsTestParam* pTestData)
{
    buff[0] = MEAS_START;
    buff[1] = 0;
    buff[2] = pTestData->numTest;
    buff[3] = (unsigned char) pTestData->numMode;
    switch (pTestData->numTest)
    {
        case TEST1:
            buff[4] = pTestData->Iterations;
            break;
        case TEST2:
            buff[4] = pTestData->Iterations;
            break;
        case TEST3:
            buff[4] = 1;
            break;
        default:
            buff[4] = 0;
            break;
    }
    buff[5] = pTestData->sizeBreach;
    return 6;
}

void SendButtonPressedDevice(tsTestParam* param)
{
    char buffer[4] = {0};
    buffer[0] = BUTTON_PRESSED_DEVICE;
    buffer[1] = 0;
    if ((SwDataFlag.BIT.SW1_flag != 0) && (SwDataFlag.BIT.SW2_flag == 0))
        buffer[2] = YELLOW_BUTTON_PRESSED;
    else if ((SwDataFlag.BIT.SW1_flag == 0) && (SwDataFlag.BIT.SW2_flag != 0))
        buffer[2] = GREEN_BUTTON_PRESSED;
    else if ((SwDataFlag.BIT.SW1_flag != 0) && (SwDataFlag.BIT.SW2_flag != 0))
        buffer[2] = YELLOW_AND_GREEN_BUTTONS_PRESSED;
    if (param->appLessMode == 0)
    {
        sendDataUSART_Async(buffer, 3);
    }
}

int fSendFinishCommand(char* buff, tsTestParam* pTestData)
{
    uint32_t numData;
    numData = pTestData->numChankSamples;
    buff[0] = MEAS_FINISH;
    buff[1] = 0;
    buff[2] = pTestData->numTest;
    buff[3] = (unsigned char) pTestData->numMode;
    buff[4] = pTestData->CurrentIteration;
    buff[5] = numData & 0xFF;
    buff[6] = (numData >> 8)&0xFF;
    buff[7] = (numData >> 16)&0xFF;
    buff[8] = (numData >> 24)&0xFF;
    return 9;
}

int GoingIntoHibernation(char* buff)
{
    buff[0] = GO_HIB;
    buff[1] = 0;
    return 2;
}

int MeasInterrupt(char* buff)
{
    buff[0] = DATA_INT;
    buff[1] = 0;
    return 2;
}

int MeasContinue(char* buff)
{
    buff[0] = DATA_CON;
    buff[1] = 0;
    return 2;
}

int NoValidSignal(char* buff)
{
    buff[0] = SIGNAL_NOVALID;
    buff[1] = 0;
    return 2;
}

int ExtPlugMisconnect(char* buff)
{
    buff[0] = EXT_PLUG_MISCONNECT;
    buff[1] = 0;
    return 2;
}

int fStartFlashHeader(unsigned char* cmdBuff, tsTestParam* pTestData)
{
    uint32_t numData = 0;
    char i;
    cmdBuff[0] = 0;
    cmdBuff[1] = pTestData->numTest;
    cmdBuff[2] = 0;
    cmdBuff[3] = pTestData->Iterations;
    for (i = 0; i < 20; i++)
        cmdBuff[i + 4] = pTestData->Date[i];
    for (i = 0; i < 40; i++)
        cmdBuff[i + 24] = pTestData->UserName[i];
    cmdBuff[64] = (unsigned char) numData; // Data Length.
    cmdBuff[65] = 0;
    cmdBuff[66] = 0;
    cmdBuff[67] = 0;
    cmdBuff[68] = (pTestData->TF[0] >> 8)&0xFF;
    cmdBuff[69] = pTestData->TF[0]&0xFF;
    cmdBuff[70] = (pTestData->TF[1] >> 8)&0xFF;
    cmdBuff[71] = pTestData->TF[1]&0xFF;
    cmdBuff[72] = (pTestData->TF[2] >> 8)&0xFF;
    cmdBuff[73] = pTestData->TF[2]&0xFF;
    cmdBuff[74] = (pTestData->TF[3] >> 8)&0xFF;
    cmdBuff[75] = pTestData->TF[3]&0xFF;
    cmdBuff[76] = (pTestData->TF[4] >> 8)&0xFF;
    cmdBuff[77] = pTestData->TF[4]&0xFF;
    cmdBuff[78] = 0;
    cmdBuff[79] = pTestData->numFilter;
    cmdBuff[80] = (pTestData->numReqSamples >> 24)&0xFF;
    cmdBuff[81] = (pTestData->numReqSamples >> 16)&0xFF;
    cmdBuff[82] = (pTestData->numReqSamples >> 8)&0xFF;
    cmdBuff[83] = (pTestData->numReqSamples >> 0)&0xFF;
    cmdBuff[84] = (pTestData->RealQuantMeasInTF2 >> 24)&0xFF;
    cmdBuff[85] = (pTestData->RealQuantMeasInTF2 >> 16)&0xFF;
    cmdBuff[86] = (pTestData->RealQuantMeasInTF2 >> 8)&0xFF;
    cmdBuff[87] = (pTestData->RealQuantMeasInTF2 >> 0)&0xFF;
    cmdBuff[88] = 0;
    cmdBuff[89] = pTestData->appLessMode;
    cmdBuff[90] = 0xFF; // Test Was NOT downloaded
    cmdBuff[91] = 0xFF; // Test Was NOT downloaded
    return 92;
}

int fSendGetTestInformation(char* buff, tsTestParam* pTestData)
{
    char i;
    memset(&buff[0], 0, 80);
    buff[0] = GET_TEST_INFORMATION;
    ;
    buff[1] = 0;
		if (pTestData->appLessMode == 2)
			buff[2] = TEST4;
		else
			buff[2] = pTestData->numTest;
    buff[3] = 0;
    buff[4] = pTestData->Iterations;
    for (i = 0; i < 20; i++)
        buff[i + 5] = pTestData->Date[i];
    for (i = 0; i < 40; i++)
        buff[i + 25] = pTestData->UserName[i];
    buff[65] = pTestData->TF[0]&0xFF;
    buff[66] = (pTestData->TF[0] >> 8)&0xFF;
    buff[67] = pTestData->TF[1]&0xFF;
    buff[68] = (pTestData->TF[1] >> 8)&0xFF;
    buff[69] = pTestData->TF[2]&0xFF;
    buff[70] = (pTestData->TF[2] >> 8)&0xFF;
    buff[71] = pTestData->TF[3]&0xFF;
    buff[72] = (pTestData->TF[3] >> 8)&0xFF;
    buff[73] = pTestData->TF[4]&0xFF;
    buff[74] = (pTestData->TF[4] >> 8)&0xFF;
    return 75;
}

void InitCheckValidSignal(void)
{
    short i;

    DataValidSignal.flagMain = 0;
    for (i = 0; i < 4; i++)
    {
        memset(&DataValidSignal.buff[i][0], 0, CheckSignalBufSize);
        DataValidSignal.counter[i] = 0;
        DataValidSignal.pbuff[i] = 0;
        DataValidSignal.flag[i] = 0;
    }
}

void InitCheckValidSignal2(void)
{
    short i;

    ValidateSignal2.flagMain = 0;
    for (i = 0; i < 4; i++)
    {
        memset(&ValidateSignal2.buff[i][0], 0, CheckSignal2BufSize);
        ValidateSignal2.counter[i] = 0;
        ValidateSignal2.pbuff[i] = 0;
        ValidateSignal2.flag[i] = 0;
    }
}
//----------- for analis 100 sampl and if 50 error -> signal wrong--------
// 0 - signal right, 1 - wrong
//-------------------------------------------------------------------------

int CheckValidSignal2(unsigned short* pFIRBuffer, int numChannels)
{
    int result;
    int i, j;

    result = 0;

    for (i = 0; i < numChannels; i++) //4 Ch ADC
    {
        j = ValidateSignal2.pbuff[i];
        ValidateSignal2.buff[i][j] = pFIRBuffer[i]; // копируем для пост обработки

        if (j < CheckSignal2BufSize)
            ValidateSignal2.pbuff[i]++; //круговой буфер
        else
            ValidateSignal2.pbuff[i] = 0;

        if ((ValidateSignal2.buff[i][j] < CheckSignalMIN) || (ValidateSignal2.buff[i][j] > CheckSignalMAX))
        {
            if (ValidateSignal2.counter[i] < CheckSignal2BufSize)
                ValidateSignal2.counter[i]++;
            if (ValidateSignal2.counter[i] > CheckSignal2Count) // значений с отклонением сигнала много			
            {
                ValidateSignal2.flag[i] = 1;
                ValidateSignal2.flagMain = 1;
                result = 1;
            }
        } else
        {
            if (ValidateSignal2.counter[i])
                ValidateSignal2.counter[i]--;
            if (ValidateSignal2.counter[i] < CheckSignal2Count)
                ValidateSignal2.flag[i] = 0;
            else result = 1;
        }
    }
    //--------- сбрасываем глобальный флаг, если сигнал вернулся на всех электродах-----------
    if (numChannels == 4) //4 Chanels
    {
        if (!ValidateSignal2.flag[0] && !ValidateSignal2.flag[1] && !ValidateSignal2.flag[2] && !ValidateSignal2.flag[3])
        {
            ValidateSignal2.flagMain = 0;
            result = 0;
        }
    } else //3 chanels
    {
        if (!ValidateSignal2.flag[0] && !ValidateSignal2.flag[1] && !ValidateSignal2.flag[2])
        {
            ValidateSignal2.flagMain = 0;
            result = 0;
        }
    }

    return result;
}

void InitCheckValidSignal3(void)
{
    short i;
    ValidateSignal3.flagMain = 0;
    for (i = 0; i < 4; i++)
    {
        memset(&ValidateSignal3.buff[i][0], 0, CheckSignal3BufSize);
        ValidateSignal3.counter[i] = 0;
        ValidateSignal3.pbuff[i] = 0;
        ValidateSignal3.flag[i] = 0;
    }
}
//----------------for calc AVG(data) of 200 samples-----------------------
// 0 - signal right, 1 - wrong
//-------------------------------------------------------------------------

int CheckValidSignal3(unsigned short* pFIRBuffer, int numChannels)
{
    int result;
    int i, j, k;
    unsigned short AVG_bot;
    unsigned short AVG_top;
    //	unsigned short AVG_delta;	
    result = 0;
    for (i = 0; i < numChannels; i++) //4 Ch ADC
    {
        j = ValidateSignal3.pbuff[i];
        ValidateSignal3.buff[i][j] = pFIRBuffer[i]; // копируем для пост обработки

        if (j < CheckSignal3BufSize)
            ValidateSignal3.pbuff[i]++; //круговой буфер
        else
            ValidateSignal3.pbuff[i] = 0;

        ValidateSignal3.SUM[i] = 0; // сбросили
        for (k = 0; k < CheckSignal3BufSize; k++) // считаем сумму значений в буфере
            ValidateSignal3.SUM[i] += ValidateSignal3.buff[i][k];

        ValidateSignal3.AVG[i] = ValidateSignal3.SUM[i] / CheckSignal3BufSize; // вычисляем среднее в буфере

        //------------------
        if (ValidateSignal3.AVG[i] > CheckSignal3dif)
            AVG_bot = (unsigned short) ValidateSignal3.AVG[i] - CheckSignal3dif;
        else
            AVG_bot = 0;
        if (ValidateSignal3.AVG[i] < (0xFFFF - CheckSignal3dif))
            AVG_top = (unsigned short) ValidateSignal3.AVG[i] + CheckSignal3dif;
        else
            AVG_top = 0xFFFF;
        //------------------
        //		if ((unsigned short)ValidateSignal3.AVG[i] > ValidateSignal3.buff[i][j])	
        //			AVG_delta = (unsigned short)ValidateSignal3.AVG[i] - ValidateSignal3.buff[i][j];
        //		else
        //			AVG_delta = ValidateSignal3.buff[i][j] - (unsigned short)ValidateSignal3.AVG[i] ;
        //------------------
        if ((ValidateSignal3.buff[i][j] < AVG_bot) || (ValidateSignal3.buff[i][j] > AVG_top))
        {
            ValidateSignal3.flag[i] = 1; //текущее значение выпало из диапазона
            ValidateSignal3.flagMain = 1;
            ValidateSignal3.counter[i] = CheckSignal3BufSize;
            result = 1;
        } else
        {
            if (ValidateSignal3.counter[i])
            {
                ValidateSignal3.counter[i]--;
                result = 1;
            } else
                ValidateSignal3.flag[i] = 0; // если НЕ прошло 200 самплов после инвалида - считаем инвалид
        }
    }
    //--------- сбрасываем глобальный флаг, если сигнал вернулся на всех электродах-----------
    if (numChannels == 4) //4 Chanels
    {
        if (!ValidateSignal3.flag[0] && !ValidateSignal3.flag[1] && !ValidateSignal3.flag[2] && !ValidateSignal3.flag[3])
        {
            ValidateSignal3.flagMain = 0;
            result = 0;
        }
    } else //3 chanels
    {
        if (!ValidateSignal3.flag[0] && !ValidateSignal3.flag[1] && !ValidateSignal3.flag[2])
        {
            ValidateSignal3.flagMain = 0;
            result = 0;
        }
    }
    return result;
}

//static int globalCheckCount = 0;
//--------------Filter BandPass for 50 & 60 Hz----------------------------------------------------------

void InitDigFilter(void)
{
    //	globalCheckCount = 0;
    SampleFilter_init(&FIRFilter0L); // initialization of the filter 50-60 Hz
    SampleFilter_init(&FIRFilter1L); // initialization of the filter 50-60 Hz
    SampleFilter_init(&FIRFilter2L); // initialization of the filter 50-60 Hz
    SampleFilter_init(&FIRFilter3L); // initialization of the filter 50-60 Hz

    SampleFilter_init(&FIRFilter0H); // initialization of the filter 100-120 Hz
    SampleFilter_init(&FIRFilter1H); // initialization of the filter 100-120 Hz
    SampleFilter_init(&FIRFilter2H); // initialization of the filter 100-120 Hz
    SampleFilter_init(&FIRFilter3H); // initialization of the filter 100-120 Hz
}

int GercelDetect150Hz(float inSample)
{
    static float threshold = 900;
    const static float NUM_OF_SAMPLES_IN_MASS = 500;
    const static float constA = -0.618031085; //2 * cos(3.14159 * 2 * ((150*NUM_OF_SAMPLES_IN_MASS/SampPerSecDev_GercelAVG) / SampPerSecDev_GercelAVG));
    static float D0 = 0;
    static float D1 = 0;
    static float D2 = 0;
    static float result;
    static float maxSample = 0;
    static float minSample = 0xFFFF;
    static float sampleDown = 0;
    static int n = 0;
    if (inSample > maxSample)
        maxSample = inSample;
    if (inSample < minSample)
        minSample = inSample;
    inSample -= sampleDown;
    D0 = (inSample + constA * D1 - D2);
    D2 = D1;
    D1 = D0;
    n++;
    if (n > NUM_OF_SAMPLES_IN_MASS)
    {
        sampleDown = (maxSample + minSample) / 2;
        result = (D1 * D1 + D2 * D2 - constA * D1 * D2);
        result = sqrt(result);
        //		if (globalCheckCount < 1)
        //			result = 0;
        //		globalCheckCount++;
        maxSample = 0;
        minSample = 0xFFFF;
        D0 = 0;
        D1 = 0;
        D2 = 0;
        n = 0;
    }
    if (result < threshold)
        return 0;
    else
        return 1;
}

uint8_t app_invalid_signal_detect_enabled = 1;

int getMeasure(unsigned short* pBuffer, unsigned short* pFIRBuffer, int numChannels, unsigned int FIRHz, bool DisableFiltering)
{
    int counter = 0;
    unsigned short temp = 0;
    ADCQueueStruct ADC_All_Buffer[4] = {0};
#if ENABLE_FreeRunADC == 0            // 	
    /*		ADC0->SC1[0] |= ADC_SC1_DIFF_MASK;								//диффер вход. = Сигнал-земля
                    ADC1->SC1[0] |= ADC_SC1_DIFF_MASK;
                    ADC2->SC1[0] |= ADC_SC1_DIFF_MASK;
                    ADC3->SC1[0] |= ADC_SC1_DIFF_MASK;
     */
    ADC0->SC1[0] = ADC_SC1_ADCH(0) | ADC_SC1_AIEN_MASK;
    ADC1->SC1[0] = ADC_SC1_ADCH(0) | ADC_SC1_AIEN_MASK;
    ADC2->SC1[0] = ADC_SC1_ADCH(0) | ADC_SC1_AIEN_MASK;
    ADC3->SC1[0] = ADC_SC1_ADCH(0) | ADC_SC1_AIEN_MASK;
#endif
    xQueueReceive(Queue_ADC0Buff, &ADC_All_Buffer[0], 10); //portMAX_DELAY);
    xQueueReceive(Queue_ADC1Buff, &ADC_All_Buffer[1], 10); //portMAX_DELAY);
    xQueueReceive(Queue_ADC2Buff, &ADC_All_Buffer[2], 10); //portMAX_DELAY);
    xQueueReceive(Queue_ADC3Buff, &ADC_All_Buffer[3], 10); //portMAX_DELAY);
#if (DISABLE_FILTER == 0)
    if (DisableFiltering == false)
    {
        for (counter = 0; counter < 4; counter++)
        {
            pBuffer[counter] = ADC_All_Buffer[counter].ADCxBuff;
        }
        if (FIRHz == Filter50Hz) // если в ГУЕ выбрали фильтр 50Гц
        {
            SampleFilter_put(&FIRFilter0L, pBuffer[0]); // set 0 chanel
            temp = SampleFilter_get50_3(&FIRFilter0L) << 1; //set filter 50Hz (LoFilter)
            SampleFilter_put(&FIRFilter0H, temp);
            pFIRBuffer[0] = SampleFilter_get100_3(&FIRFilter0H) << 1; //set filter 100Hz (HiFilter)

            SampleFilter_put(&FIRFilter1L, pBuffer[1]); //set FIr 1 chanel
            temp = SampleFilter_get50_3(&FIRFilter1L) << 1;
            SampleFilter_put(&FIRFilter1H, temp);
            pFIRBuffer[1] = SampleFilter_get100_3(&FIRFilter1H) << 1;

            SampleFilter_put(&FIRFilter2L, pBuffer[2]); //set FIr 1 chanel
            temp = SampleFilter_get50_3(&FIRFilter2L) << 1;
            SampleFilter_put(&FIRFilter2H, temp);
            pFIRBuffer[2] = SampleFilter_get100_3(&FIRFilter2H) << 1;

            SampleFilter_put(&FIRFilter3L, pBuffer[3]); //set FIr 1 chanel
            temp = SampleFilter_get50_3(&FIRFilter3L) << 1;
            SampleFilter_put(&FIRFilter3H, temp);
            pFIRBuffer[3] = SampleFilter_get100_3(&FIRFilter3H) << 1;
        } else if (FIRHz == Filter60Hz) //------filtered DATA 60-120Hz--------
        {
            SampleFilter_put(&FIRFilter0L, pBuffer[0]); // set 0 chanel
            temp = SampleFilter_get60_3(&FIRFilter0L) << 1; //set filter 50Hz (LoFilter)
            SampleFilter_put(&FIRFilter0H, temp);
            pFIRBuffer[0] = SampleFilter_get120_3(&FIRFilter0H) << 1; //set filter 100Hz (HiFilter)

            SampleFilter_put(&FIRFilter1L, pBuffer[1]); //set FIr 1 chanel
            temp = SampleFilter_get60_3(&FIRFilter1L) << 1;
            SampleFilter_put(&FIRFilter1H, temp);
            pFIRBuffer[1] = SampleFilter_get120_3(&FIRFilter1H) << 1;

            SampleFilter_put(&FIRFilter2L, pBuffer[2]); //set FIr 1 chanel
            temp = SampleFilter_get60_3(&FIRFilter2L) << 1;
            SampleFilter_put(&FIRFilter2H, temp);
            pFIRBuffer[2] = SampleFilter_get120_3(&FIRFilter2H) << 1;

            SampleFilter_put(&FIRFilter3L, pBuffer[3]); //set FIr 1 chanel
            temp = SampleFilter_get60_3(&FIRFilter3L) << 1;
            SampleFilter_put(&FIRFilter3H, temp);
            pFIRBuffer[3] = SampleFilter_get120_3(&FIRFilter3H) << 1;
        } else
        {
            for (counter = 0; counter < 4; counter++)
            {
                pFIRBuffer[counter] = ADC_All_Buffer[counter].ADCxBuff;
                pBuffer[counter] = ADC_All_Buffer[counter].ADCxBuff;
            }
        }
    } else
    {
        for (counter = 0; counter < 4; counter++)
        {
            pFIRBuffer[counter] = ADC_All_Buffer[counter].ADCxBuff;
            pBuffer[counter] = ADC_All_Buffer[counter].ADCxBuff;
        }
    }
#else
    for (counter = 0; counter < 4; counter++)
    {
        pFIRBuffer[counter] = ADC_All_Buffer[counter].ADCxBuff;
        pBuffer[counter] = ADC_All_Buffer[counter].ADCxBuff;
    }
#endif

#if ENABLE_TestSignalADC > 0 
    {
        static unsigned short test[4] = {0};

        if (TestCounter % (2 * mdcr_get_frequency()) == 0)
        {
            if (test[0] == 0)
                test[0] = 0xFFFF;
            else
                test[0] = 0;
        }
        TestCounter += 2;
        test[0] = test[0];
        test[1] = TestCounter;
        test[2] = TestCounter * 2;
        test[3] = TestCounter * 4;

        pFIRBuffer[0] = test[0];
        pFIRBuffer[1] = test[1];
        pFIRBuffer[2] = test[2];
        pFIRBuffer[3] = test[3];
    }
#endif

#if ENABLE_CheckValidSignal2
    if (app_invalid_signal_detect_enabled)
    {
        if (CheckValidSignal2(pFIRBuffer, numChannels))
            DataValidSignal.flagMain = 1;
        else
            DataValidSignal.flagMain = 0;
    } else
        DataValidSignal.flagMain = 0;
#endif 
#if ENABLE_CheckValidSignal2_3
    if (app_invalid_signal_detect_enabled)
    {
        if (CheckValidSignal2(pFIRBuffer, numChannels) || CheckValidSignal3(pFIRBuffer, numChannels))
            DataValidSignal.flagMain = 1;
        else
            DataValidSignal.flagMain = 0;
    } else
        DataValidSignal.flagMain = 0;
#endif


#if ENABLE_CheckDataValidSignal == 0
    DataValidSignal.flagMain = 0; //отключить проверку сигнала
#endif

#if (ENABLE_SNS_PIN_EQUAL_INVALID_SIGNAL == 1)
    DataValidSignal.flagMain = ((PTE->PDIR >> ANALOG_SNS) & 0x01);
#endif

#if (ENABLE_SW3_PIN_EQUAL_INVALID_SIGNAL == 1)
    DataValidSignal.flagMain = (~(PTA->PDIR >> SW1)& 0x01UL);
#endif

#if (ENABLE_Check150HzNoise == 1)
    if (GercelDetect150Hz((float) (pBuffer[1] >> 8)))
        DataValidSignal.flagMain = 1;
#endif
    return 0;
}

//--------------fft for 50 & 60 Hz----------------------------------------------------------

void GercelaPutBuf(unsigned short* pBuffer, int numChannels)
{
    unsigned short i, j;
    for (i = 0; i < numChannels; i++) //4 Ch ADC
    {
        j = FFTData.pbuff[i]; // pointer

        FFTData50Hz.INbuff[i][j] = (pBuffer[i]); // Filter Data & copy data to buff 
        FFTData60Hz.INbuff[i][j] = (pBuffer[i]); // Filter Data & copy data to buff

        BandPas50Filter_put(&FFTFilter50, pBuffer[i]); // Filter Data & copy data to buff
        FFTData50Hz.INbuff[i][j] = BandPas50Filter_get(&FFTFilter50) >> 4;

        BandPas60Filter_put(&FFTFilter60, pBuffer[i]); // Filter Data & copy data to buff
        FFTData60Hz.INbuff[i][j] = BandPas60Filter_get(&FFTFilter60) >> 4;

        if (j < FFTBufSize)
            FFTData.pbuff[i]++; //set pointer
        else
            FFTData.pbuff[i] = 0;
    }
}

//--------------------find 50/60HZ from AVG data ------------------------------

int AVGAlgorithm(unsigned short* pBuffer, int numChannels)
{
    unsigned short i, k;
    unsigned short max50 = 0;
    unsigned short max60 = 0;
    int result = 0;
    for (i = 0; i < numChannels; i++)
    {

        FFTData50Hz.SUM[i] = 0; // сбросили
        for (k = 0; k < FFTBufSize; k++) // считаем сумму значений в буфере
            FFTData50Hz.SUM[i] += FFTData50Hz.INbuff[i][k];
        FFTData50Hz.AVG[i] = FFTData50Hz.SUM[i] / FFTBufSize; // вычисляем среднее в буфере
        FFTData60Hz.SUM[i] = 0; // сбросили
        for (k = 0; k < FFTBufSize; k++) // считаем сумму значений в буфере
            FFTData60Hz.SUM[i] += FFTData60Hz.INbuff[i][k];
        FFTData60Hz.AVG[i] = FFTData60Hz.SUM[i] / FFTBufSize; // вычисляем среднее в буфере

        if (max50 < FFTData50Hz.AVG[i])
            max50 = FFTData50Hz.AVG[i];
        if (max60 < FFTData60Hz.AVG[i])
            max60 = FFTData60Hz.AVG[i];
    }
    if (max50 > max60)
        result = FILTER_50HZ; //result = 50;
    else
        result = FILTER_60HZ; //result = 60;
    FFTData.frequency_avg = result;
    return result;
}


//--------------fft for 50 & 60 Hz----------------------------------------------------------
//http://www.dsplib.ru/content/goertzel/goertzel.html
//http://www.dsplib.ru/content/goertzelmod/goertzelmod.html

int GercelaAlgorithm(unsigned short* pBuffer, int numChannels)
{
    unsigned short i, j;
    int N = FFTBufSize; //точек ДПФ
    double k50Hz = (50 * FFTBufSize / SampPerSecDev_GercelAVG); //номер спектрального отсчта 50Hz    (50*FFTBufSize / 500saml)
    double k60Hz = (60 * FFTBufSize / SampPerSecDev_GercelAVG); // 60Hz
    int result;
    result = 0;
    FFTData.frequency = 0;
    //----------------- calc FFT for 50 Hz -----------------
    for (i = 0; i < FFTBufSize; i++)
        FFTData.temp[i] = 0;
    FFTData50Hz.SR_max = 0;
    FFTData.alp = 2 * cos(2 * M_PI * k50Hz / N); //параметр альфа
    //поворотный к-т W_N^-k
    FFTData.wr = cos(2 * M_PI * k50Hz / N); //реальная часть
    //     wi =    sin(2*M_PI*k50Hz/N); 	//мнимая часть

    // итерационный расчет массива v согласно (14)
    for (j = 0; j < numChannels; j++)
    {
        //учет v[-1] = v[-2] = 0
        FFTData.temp[0] = FFTData50Hz.INbuff[j][0];
        FFTData.temp[1] = FFTData50Hz.INbuff[j][1] + FFTData.alp * FFTData.temp[0];

        for (i = 2; i < N; i++)
            FFTData.temp[i] = FFTData50Hz.INbuff[j][i] + FFTData.alp * FFTData.temp[i - 1] - FFTData.temp[i - 2];

        //реальная и мнимая части спектрального отсчета S(1) согласно (13)
        FFTData50Hz.SR[j] = FFTData.temp[N - 1] * FFTData.wr - FFTData.temp[N - 2]; //use only Real data
        //     FFTData.SI_50[j] = temp[N-1]*wi;

        if (FFTData50Hz.SR_max < FFTData50Hz.SR[j])
            FFTData50Hz.SR_max = FFTData50Hz.SR[j];
    }
    //----------------- calc FFT for 60 Hz -----------------
    for (i = 0; i < FFTBufSize; i++)
        FFTData.temp[i] = 0;
    FFTData60Hz.SR_max = 0;
    FFTData.alp = 2 * cos(2 * M_PI * k60Hz / N); //параметр альфа
    //поворотный к-т W_N^-k
    FFTData.wr = cos(2 * M_PI * k60Hz / N); //реальная часть
    //     wi =    sin(2*M_PI*k60Hz/N); 	//мнимая часть
    // итерационный расчет массива v согласно (14)
    for (j = 0; j < numChannels; j++)
    {
        //учет v[-1] = v[-2] = 0
        FFTData.temp[0] = FFTData60Hz.INbuff[j][0];
        FFTData.temp[1] = FFTData60Hz.INbuff[j][1] + FFTData.alp * FFTData.temp[0];
        for (i = 2; i < N; i++)
            FFTData.temp[i] = FFTData60Hz.INbuff[j][i] + FFTData.alp * FFTData.temp[i - 1] - FFTData.temp[i - 2];
        //реальная и мнимая части спектрального отсчета S(1) согласно (13)
        FFTData60Hz.SR[j] = FFTData.temp[N - 1] * FFTData.wr - FFTData.temp[N - 2];
        //     FFTData.SI_60[j] = temp[N-1]*wi;
        if (FFTData60Hz.SR_max < FFTData60Hz.SR[j])
            FFTData60Hz.SR_max = FFTData60Hz.SR[j];
    }
    if (FFTData50Hz.SR_max > FFTData60Hz.SR_max)
        result = FILTER_50HZ; //result = 50;
    else
        result = FILTER_60HZ; //result = 60;
    FFTData.frequency = result;
    return result;
}

void mdcr_decode_recv_command(serial_port serial)
{
    tsMDCRCommandType command = {0};
    int flagError = 1;
    char i = 0;
    command.source = serial.data_source;
    command.type = eMDCR_START;
    flagError = 1;
    switch (serial.data[0])
    {
        case TEST1:
            if (!serial.data[1]) //0x00 впереди есть?, т.к. комманда 0х0001
            {
                command.cmdID = TEST1;
                command.acqMode = serial.data[2];
                command.TF1 = (serial.data[4] << 8) | serial.data[3];
                command.TF2 = (serial.data[6] << 8) | serial.data[5];
                command.TF3 = (serial.data[8] << 8) | serial.data[7];
                command.iteration = serial.data[9];
                for (i = 0; i < 20; i++) command.Date[i] = serial.data[i + 10];
                for (i = 0; i < 40; i++) command.UserName[i] = serial.data[i + 30];
                flagError = 0;
            }
            break;
        case TEST2:
            if (!serial.data[1]) //0x00 впереди есть?, т.к. комманда 0х0002
            {
                command.cmdID = TEST2;
                command.acqMode = serial.data[2];
                command.TF4 = (serial.data[4] << 8) | serial.data[3];
                command.iteration = serial.data[5];
                for (i = 0; i < 20; i++)
                    command.Date[i] = serial.data[i + 6];
                for (i = 0; i < 40; i++)
                    command.UserName[i] = serial.data[i + 26];
                flagError = 0;
            }
            break;
        case TEST3:
            if (!serial.data[1]) //0x00 впереди есть?, т.к. комманда 0х0003
            {
                command.cmdID = TEST3;
                command.acqMode = serial.data[2];
                command.TF5 = (serial.data[4] << 8) | serial.data[3];
                for (i = 0; i < 20; i++)
                    command.Date[i] = serial.data[i + 5];
                for (i = 0; i < 40; i++)
                    command.UserName[i] = serial.data[i + 25];
                i = 0;
                flagError = 0;
            }
            break;
        case MEAS_OK:
            if (!serial.data[1]) //0x00 впереди есть?
            {
                command.cmdID = MEAS_OK;
                flagError = 0;
            }
            break;
        case SET_FILTER:
            if (!serial.data[1]) //0x00 впереди есть?, т.к. комманда 0х0003
            {
                command.cmdID = SET_FILTER;
                command.numFilter = serial.data[2];
                flagError = 0;
            }
            break;
        case STOP_TEST:
            if (!serial.data[1]) //0x00 впереди есть?,
            {
                command.cmdID = STOP_TEST;
                flagError = 0;
            }
            break;
        case MEAS_REPEAT:
            if (!serial.data[1]) //0x00 впереди есть?,
            {
                command.cmdID = MEAS_REPEAT;
                flagError = 0;
            }
            break;
            //--------------------- get status -------------
        case GET_LAST_MEASUREMENT:
            if (!serial.data[1])
            {
                command.cmdID = GET_LAST_MEASUREMENT;
                flagError = 0;
            }
            break;
        case GET_BEE_SN:
            if (!serial.data[1])
            {
                command.cmdID = GET_BEE_SN;
                flagError = 0;
            }
            break;
        case GET_BEE_APP_VERSION:
            if (!serial.data[1])
            {
                command.cmdID = GET_BEE_APP_VERSION;
                flagError = 0;
            }
            break;
        case GET_BEE_DEVICE_VER:
            if (!serial.data[1])
            {
                command.cmdID = GET_BEE_DEVICE_VER;
                flagError = 0;
            }
            break;
        case GET_DATE_DEVICE_FIRMWARE:
            if (!serial.data[1])
            {
                command.cmdID = GET_DATE_DEVICE_FIRMWARE;
                flagError = 0;
            }
            break;
        case GET_CHARGED_LEVEL:
            if (!serial.data[1])
            {
                command.cmdID = GET_CHARGED_LEVEL;
                flagError = 0;
            }
            break;
        case SEND_INFO_ABOUT_LAST_SAVED_TEST:
            if (!serial.data[1])
            {
                command.cmdID = SEND_INFO_ABOUT_LAST_SAVED_TEST;
                flagError = 0;
            }
            break;
        case SET_MEASURING_FREQUENCY:
            if (!serial.data[1])
            {
                command.cmdID = SET_MEASURING_FREQUENCY;
                memcpy(command.data, &serial.data[2], 4);
                flagError = 0;
            }
            break;
        case GET_MEASURING_FREQUENCY:
            if (!serial.data[1])
            {
                command.cmdID = GET_MEASURING_FREQUENCY;
                flagError = 0;
            }
            break;
        case SET_INVALID_SIGNAL_DETECT:
            if (!serial.data[1])
            {
                command.cmdID = SET_INVALID_SIGNAL_DETECT;
                memcpy(command.data, &serial.data[2], 4);
                flagError = 0;
            }
            break;
        case GET_INVALID_SIGNAL_DETECT:
            if (!serial.data[1])
            {
                command.cmdID = GET_INVALID_SIGNAL_DETECT;
                flagError = 0;
            }
            break;
    }
    if (flagError == 0)
    {
        if (xQueueSend(xQueueMdcrTest, &command, 1000 / portTICK_RATE_MS) != pdPASS)
        {
        }
    }
}

void Decode_Flash_Header(tsTestParam* param, unsigned short *buff)
{
    char i = 0;
    char j = 0;
    param->numTest = buff[0]; // read 2 bytes
    param->Iterations = buff[1];
    for (i = 0, j = 0; i < 10; i++) //convert in Char
    {
        param->Date[j] = buff[i + 2] >> 8;
        j++;
        param->Date[j] = buff[i + 2];
        j++;
    }
    for (i = 0, j = 0; i < 20; i++)
    {
        param->UserName[j] = buff[i + 22] >> 8;
        j++;
        param->UserName[j] = buff[i + 22];
        j++;
    }
    param->TF[0] = buff[34];
    param->TF[1] = buff[35];
    param->TF[2] = buff[36];
    param->TF[3] = buff[37];
    param->TF[4] = buff[38];
    param->numFilter = buff[39];
    param->numChankSamples = (buff[40] << 16) | (buff[41] << 0);
    param->RealQuantMeasInTF2 = (buff[42] << 16) | (buff[43] << 0);
    if (param->RealQuantMeasInTF2 > (param->TF[1] * mdcr_get_frequency()))
        param->RealQuantMeasInTF2 = param->TF[1] * mdcr_get_frequency();

    if (buff[44] == 1) // Чтобы сообщения между итерациями доходили до GUI
        param->appLessMode = 0; // = 1;
    else
        param->appLessMode = 0;

    if (buff[45] == 0xFFFF)
        param->testWasDownloaded = 0;
    else
        param->testWasDownloaded = 1;

}

//----- проверяет подключена ли наша плата к их. должен быть 1, вместо 0 ------------------

int CheckAnalogSNS(tsTestParam* param)
{
#if (ENABLE_CheckAnalogSNS > 0)
    bool LEDsWasSet = false;
    int Result = 1;
    unsigned int Counter = 0;
    unsigned char switchCounter = 0;
    startTimer2();
    while (TimersDataFlag.BIT.TIMER_2 == 0) // waiting for the timer to 20 seconds
    {
#ifdef MDCR_INTERFACE
        if ((PTE->PDIR >> ANALOG_SNS)& 0x01) //если подключена 1
        {
            switchCounter++;
            if (switchCounter >= 30)
            {
                switchCounter = 0;
                switch (param->numTest)
                {
                    case TEST1:
                        break;
                    case TEST2:
                        setLEDState(YellowAndGreenSolid);
                        break;
                    case TEST3:
                        setLEDState(YellowSolid);
                        break;
                    default:
                        break;
                }
                return Result;
            }
        } else if (Counter > 40 && LEDsWasSet == false)
        {
            switch (param->numTest)
            {
                case TEST1:
                    break;
                case TEST2:
                    setLEDState(Red1AndRed2Solid);
                    break;
                case TEST3:
                    setLEDState(Red1Solid);
                    break;
                default:
                    break;
            }
            SendMessage_ExtPlugMisconnected(param);
            LEDsWasSet = true;
            Result = 0;
        }
#endif
        vTaskDelay(10);
        Counter++;
    }
    return 0;
#else
    return 1;
#endif
}
//static int ffffff  =0;

void readFromFlashFilter(unsigned short* pBuffer, unsigned short* pFIRBuffer, int NumOfChannels, unsigned int FIRHz)
{
    //	int counter=0;
    unsigned short temp = 0;

#if (DISABLE_FILTER == 0)
    if (FIRHz == Filter50Hz) // ???? ? ??? ??????? ?????? 50??
    {
        SampleFilter_put(&FIRFilter0L, pBuffer[0]); // set 0 chanel
        temp = SampleFilter_get50_3(&FIRFilter0L) << 1; //set filter 50Hz (LoFilter)
        SampleFilter_put(&FIRFilter0H, temp);
        pFIRBuffer[0] = SampleFilter_get100_3(&FIRFilter0H) << 1; //set filter 100Hz (HiFilter)

        SampleFilter_put(&FIRFilter1L, pBuffer[1]); //set FIr 1 chanel
        temp = SampleFilter_get50_3(&FIRFilter1L) << 1;
        SampleFilter_put(&FIRFilter1H, temp);
        pFIRBuffer[1] = SampleFilter_get100_3(&FIRFilter1H) << 1;

        SampleFilter_put(&FIRFilter2L, pBuffer[2]); //set FIr 1 chanel
        temp = SampleFilter_get50_3(&FIRFilter2L) << 1;
        SampleFilter_put(&FIRFilter2H, temp);
        pFIRBuffer[2] = SampleFilter_get100_3(&FIRFilter2H) << 1;
        if (NumOfChannels == 4)
        {
            SampleFilter_put(&FIRFilter3L, pBuffer[3]); //set FIr 1 chanel
            temp = SampleFilter_get50_3(&FIRFilter3L) << 1;
            SampleFilter_put(&FIRFilter3H, temp);
            pFIRBuffer[3] = SampleFilter_get100_3(&FIRFilter3H) << 1;
        }
    } else if (FIRHz == Filter60Hz) //------filtered DATA 60-120Hz--------
    {
        SampleFilter_put(&FIRFilter0L, pBuffer[0]); // set 0 chanel
        temp = SampleFilter_get60_3(&FIRFilter0L) << 1; //set filter 50Hz (LoFilter)
        SampleFilter_put(&FIRFilter0H, temp);
        pFIRBuffer[0] = SampleFilter_get120_3(&FIRFilter0H) << 1; //set filter 100Hz (HiFilter)

        SampleFilter_put(&FIRFilter1L, pBuffer[1]); //set FIr 1 chanel
        temp = SampleFilter_get60_3(&FIRFilter1L) << 1;
        SampleFilter_put(&FIRFilter1H, temp);
        pFIRBuffer[1] = SampleFilter_get120_3(&FIRFilter1H) << 1;

        SampleFilter_put(&FIRFilter2L, pBuffer[2]); //set FIr 1 chanel
        temp = SampleFilter_get60_3(&FIRFilter2L) << 1;
        SampleFilter_put(&FIRFilter2H, temp);
        pFIRBuffer[2] = SampleFilter_get120_3(&FIRFilter2H) << 1;

        if (NumOfChannels == 4)
        {
            SampleFilter_put(&FIRFilter3L, pBuffer[3]); //set FIr 1 chanel
            temp = SampleFilter_get60_3(&FIRFilter3L) << 1;
            SampleFilter_put(&FIRFilter3H, temp);
            pFIRBuffer[3] = SampleFilter_get120_3(&FIRFilter3H) << 1;
        }
    } else
    {
        int counter = 0;
        for (counter = 0; counter < NumOfChannels; counter++)
        {
            pFIRBuffer[counter] = pBuffer[counter];
        }
    }
#else
    int counter = 0;
    for (counter = 0; counter < NumOfChannels; counter++)
    {
        pFIRBuffer[counter] = pBuffer[counter];
    }
    vTaskDelay(1);      // Important delay!
#endif
}

#define QUANT_OF_READ_MEAS_IN_INIT_FILTER   25
#define NUM_OF_MEAS_IN_PACKAGE      16

void filterData(unsigned short* InputBuff, unsigned short* OutputBuff, tsTestParam* param, int NumOfMeas)
{
    int i = 0;
    //	for (i = 0; i < param->numChannels*NumOfMeas; i++)
    //		reversBytes(&InputBuff[i]);
    for (i = 0; i < NumOfMeas; i++)
    {
        readFromFlashFilter(&InputBuff[i * param->numChannels], &OutputBuff[i * param->numChannels], param->numChannels, param->numFilter);
    }
    if (&InputBuff != &OutputBuff)
        memcpy(OutputBuff, InputBuff, NumOfMeas * param->numChannels * 2);
}

uint32_t initFiltration(uint32_t LocalAddressFlash, tsTestParam* param)
{
    uint32_t CurSample = 0;
    unsigned short TempBuff[4];
    int i = 0;
    InitDigFilter();
    vTaskDelay(10);
    for (CurSample = 0; CurSample < mdcr_get_frequency(); CurSample += QUANT_OF_READ_MEAS_IN_INIT_FILTER)
    {
        app_flash_read_data_16bit(LocalAddressFlash, flashData, param->numChannels * QUANT_OF_READ_MEAS_IN_INIT_FILTER);
        LocalAddressFlash += (uint32_t) (param->numChannels * QUANT_OF_READ_MEAS_IN_INIT_FILTER * 2);
        for (i = 0; i < QUANT_OF_READ_MEAS_IN_INIT_FILTER; i++)
        {
            readFromFlashFilter(&flashData[i * param->numChannels], TempBuff, param->numChannels, param->numFilter);
            GercelaPutBuf(&flashData[i * param->numChannels], param->numChannels); // need 500 sampl for work
        }
    }
    vTaskDelay(10);
    return LocalAddressFlash;
}

uint32_t readAndFilterAndSendMeasurements(uint32_t LocalAddressFlash, uint32_t QuantityMeasurements, tsTestParam* param)
{
    uint32_t CurSample;
    for (CurSample = 0; CurSample < QuantityMeasurements - NUM_OF_MEAS_IN_PACKAGE; CurSample += NUM_OF_MEAS_IN_PACKAGE)
    {
        app_flash_read_data_16bit(LocalAddressFlash, flashData, param->numChannels * NUM_OF_MEAS_IN_PACKAGE);
        LocalAddressFlash += (uint32_t) (param->numChannels * 2 * NUM_OF_MEAS_IN_PACKAGE);
        filterData(flashData, flashData, param, NUM_OF_MEAS_IN_PACKAGE);
        UART0SendData((char*) flashData, param->numChannels * 2 * NUM_OF_MEAS_IN_PACKAGE);
    }
    for (; CurSample < QuantityMeasurements; CurSample++)
    {
        app_flash_read_data_16bit(LocalAddressFlash, flashData, param->numChannels);
        LocalAddressFlash += (uint32_t) (param->numChannels * 2);
        filterData(flashData, flashData, param, 1);
        UART0SendData((char*) flashData, param->numChannels * 2);
    }
    return LocalAddressFlash;
}

int readLastIterationAndSendToGUI(tsTestParam* param)
{
    static uint32_t AddressFlash = 0;
    bool DoFilterInit = true;
    switch (currentLEDState)
    {
        case GreenBlink:
            setLEDState(GreenSolid);
            break;
        case YellowBlink:
            setLEDState(YellowSolid);
            break;
        case YellowAndGreenBlink:
            setLEDState(YellowAndGreenSolid);
            break;
        default:
            ;
            break;
    }
    vTaskDelay(100);
    if (param->numTest == TEST1 && param->CurrentIteration == 2)
    {
        DoFilterInit = false;
        param->numChankSamples = param->RealQuantMeasInTF2;
    }
#if CompressedFirmWare == 0
    SendMessage_ReadResults(param);
    if (param->numChankSamples == 0)
        return 0;
    if (param->CurrentIteration == 1)
        AddressFlash = 0x100;
    if (DoFilterInit)
        AddressFlash = initFiltration(AddressFlash, param);
    AddressFlash = readAndFilterAndSendMeasurements(AddressFlash, param->numChankSamples, param);
#else
    {
        param->mode = COMPRESSED_CHUNK_MODE;
        if (param->numTest == TEST1)
        {
            switch (param->CurrentIteration)
            {
                case 1:
                    TimeTotal = param->TF[0];
                    break;
                case 2:
                    TimeTotal = param->TF[1];
                    break;
                default:
                    TimeTotal = param->TF[2];
                    break;
            }
        } else
        {
            TimeTotal = param->TF[0] + param->TF[1] + param->TF[2] + param->TF[3] + param->TF[4];
        }
        for (time_counter = 0; time_counter < TimeTotal; time_counter++)
        {
            vTaskDelay(1);
            for (i = 0; i < 10; i++)
            {
                AT25D_read_data2(AddressFlash, &flashData[(param->numChannels * 50) * i], param->numChannels * 50);
                AddressFlash = AddressFlash + (uint32_t) (param->numChannels * 2 * 50);
            }
            mdcr_comp_len = mdcr_comp((unsigned char*) &flashData[0], param->numChannels * mdcr_get_frequency()*2, buff4k, param->numChannels);
            sizeObuf = sizeof (buff4k); //очень надо поставить ее перед lzfx_compress
            lzfx_compress(&buff4k[0], mdcr_comp_len, tempOut, &sizeObuf);
            param->numCompressedData = sizeObuf;
            len = fReadResults(buff, param); //заголовок отправили, шлем данные за 1 сек
            UART0SendData(buff, len);
            UART0SendData((char*) &tempOut[0], sizeObuf);
        }
    }
#endif 
    return 0;
}

void clearWriteToFlashElement(WriteToFlashStruct *WriteStruct)
{
    int i = 0;
    WriteStruct->CheckPointFlag = false;
    WriteStruct->CheckPointLeftOffset = 0;
    WriteStruct->EndOfDataFlag = false;
    for (i = 0; i < MAX_LENGTH_DATATOWRITE_INTO_FLASH; i++)
        WriteStruct->DataToWrite[i] = 0;
    WriteStruct->Length = 0;
}

static int sendLastTest(tsTestParam* param, int recvCmd)
{
    char buff[100];
    int result = 1;
    int len = 0;
    bool DecodingError = false;
    switch (param->status)
    {
        case testStart:
            mdcr_set_frequency(DEFAULT_QUANTITY_SAMPLES_PER_SEC);
            vTaskDelay(100);
            app_flash_read_data_16bit(0, flashData, MDCR_FLASH_HEADER / 2);
            Decode_Flash_Header(param, flashData);
            param->CurrentSampleInIteration = 0;
            param->CurrentIteration = 0;
            param->addrMeasurement = ADDRESS_MEASUREMENTS;
#if CompressedFirmWare == 0
            param->mode = CHUNK_MODE;
#else
            param->mode = COMPRESSED_CHUNK_MODE;
#endif
            param->TestMode = 0;
            param->acqMode = WRITE_TO_FLASH;
            param->numMode = 0;
            pointerSPIbuf = 0;
            param->curMetaDE.str.numTest = param->numTest & 0x7;
            param->curMetaDE.str.mode = 0;
            param->curMetaDE.str.Iterations = param->Iterations;
            param->validity = 0;
            len = fSendGetTestInformation(buff, param); // send GET_TEST_INFORMATION
            UART0SendData(buff, len);
            switch (param->numTest)
            {
                case TEST1:
                    param->numReqSamples = param->TF[0] * mdcr_get_frequency() + param->TF[1] * mdcr_get_frequency() +  \
													param->TF[2] * mdcr_get_frequency()*(param->Iterations - 2); // t1+t2+t3*Niter
                    param->numChannels = 3;
                    setLEDStateInTest1Measurements(param);
                    break;
                case TEST2:
                    param->CurrentIteration = 0;
                    param->numChankSamples = param->TF[3] * mdcr_get_frequency();
                    param->numChannels = 4;
                    setLEDState(YellowAndGreenBlink);
                    break;
                case TEST3:
                    param->numReqSamples = param->TF[4] * mdcr_get_frequency(); //mdcr_get_frequency() выборок/сек
                    param->numChankSamples = param->numReqSamples;
                    param->numChannels = 3;
                    setLEDState(YellowBlink);
                    break;
                default:
                    DecodingError = true;
                    break;
            }
            if (DecodingError == false)
            {
                param->status = testWaitAnswer;
                startTimer2();
            } else
                param->status = testStop;
            break;
        case testStreamRunning:
            if (param->numTest == TEST1)
            {
                Test1_setNextParamIter(param);
                setLEDStateInTest1Measurements(param);
            } else
            {
                param->CurrentIteration++;
                param->curMetaDE.str.Iterations = param->CurrentIteration;
            }
            readLastIterationAndSendToGUI(param);
            SendMessage_MeasFinish(param);
            startTimer2();
            param->status = testWaitAnswer;
            break;
        case testWaitAnswer:
            if (recvCmd == MEAS_OK)
            {
                if (param->CurrentIteration == (param->Iterations))
                {
                    SendMessage_MeasFinish(param);
                    param->testWasDownloaded = 0;
                    recordThatTestWasDownloaded();
                    param->status = testStop;
                } else
                    param->status = testStreamRunning;
            } else if (TimersDataFlag.BIT.TIMER_2 == 1)
                param->status = testStop;
            break;
        case testStop:
            SendMessage_GoingIntoHib(param);
            stopTimer2();
            setLEDState(YellowAndGreenSolid);
            result = 0;
            break;
        default:
            param->status = testStop;
            break;
    }
    return result;
}

#define MDCR_BLE_TESTS      0

uint32_t addr;
uint32_t addr_next;
uint32_t addr_prev;
void vMdcrTestTask(void* pvParameters)
{
    int receiveCommand = 0;
    int fContinue = 0;
    int result = 0;
    int fGetlast = 0;
    short i = 0;
    char buff[10] = {0};
    int len = 0;
    long Charge_in_procent = 0;
    portTickType delay = portMAX_DELAY >> 4;
    //	tsTestParam param = {0};
    tsTestParam *param;
    tsMDCRCommandType command = {0};
    tsMDCRCommandType appLessCommand = {0};
		tsMDCRCommandType appLessCommand2 = {0};
    appLessCommand.cmdID = APP_LESS_TEST;
		appLessCommand2.cmdID = APP_LESS_TEST2;
    flashWriteTaskInitInStartup();
    param = (tsTestParam*) pvPortMalloc(sizeof (tsTestParam));
    param->status = testDummy;
    param->numTest = DUMMY;
    param->appLessMode = 0;
    waitAppLessTest = true;
#ifndef MDCR_INTERFACE // THIS FUNCTION CAUSED HARD FAULT (related to AT25DF Writes) 
    //app_flash_init();
#endif
//    {
//        uint8_t buff_out[0x100] = {0};
//        uint32_t inc;
//        uint32_t temp;
//        for (i = 0; i < 0x100; i++)
//            buff_out[i] = i;
//        sst26_erase_chip(true);
//        for (temp = 0x100/2; addr < SST26_ADDR_END; temp+= 33)
//        {
//            inc = temp%0x100;
//            addr_next = addr + inc;
//            app_flash_write_data_8bit_verify(addr, buff_out, inc);
//            addr_prev = addr;
//            addr+= inc;
//        }
//        while(1);
//    }
    while (waitAppLessTest == 1 && (xQueuePeek(xQueueMdcrTest, &command, 2) != pdTRUE))
    {
        KeyScan();
				if (SwDataFlag.BIT.SW2_flag && SwDataFlag.BIT.SW1_flag)
						xQueueSend(xQueueMdcrTest, &appLessCommand2, 1000 / portTICK_RATE_MS);
        else if (SwDataFlag.BIT.SW1_flag)
            xQueueSend(xQueueMdcrTest, &appLessCommand, 1000 / portTICK_RATE_MS);
    }
#if defined MDCR_BLE_TESTS && MDCR_BLE_TESTS != 0
    vTaskDelay(400);
    while(1)
    {
        static uint8_t count = 0;
        volatile int i, k;
        for (i = 0; i < 2000; i++)
            i = i;
				for (k = 0; k < 20; k++)
					UART0SendByte_u(count);
        count++;
    }
#endif
    for (;;)
    {
        while (delay == portMAX_DELAY && xQueuePeek(xQueueMdcrTest, &command, 2) != pdTRUE)
        {
            KeyScan();
            if (SwDataFlag.BIT.SW1_flag)
            {
                char buffer[4] = {0};
                int timeoutCount = 0;
                buffer[0] = BUTTON_PRESSED_DEVICE;
                buffer[1] = 0;
                buffer[2] = YELLOW_BUTTON_PRESSED;
                UART0SendData(buffer, 3);
                for (timeoutCount = 0; timeoutCount < 2000 && xQueuePeek(xQueueMdcrTest, &command, 2) != pdTRUE; timeoutCount++)
                    vTaskDelay(1);
            }
        }
        if (xQueueReceive(xQueueMdcrTest, &command, delay) == pdPASS)
        {
            waitAppLessTest = false;
            stopTimerLED30SecAfterTests();
            receiveCommand = command.cmdID;
            switch (command.cmdID)
            {
                case TEST1:
                    if (command.acqMode != STREAM)
                        mdcr_set_frequency(DEFAULT_QUANTITY_SAMPLES_PER_SEC);
                    param->testWasDownloaded = 0xFFFF;
                    param->appLessMode = 0;
                    param->numChannels = 3;
                    param->numTest = TEST1;
                    param->status = testStart;
                    param->TF[0] = command.TF1; //требуемое время из GUI t1
                    param->TF[1] = command.TF2; //требуемое время из GUI t2
                    param->TF[2] = command.TF3; //требуемое время из GUI t3
                    param->TF[3] = 0;
                    param->TF[4] = 0;
                    if (param->TF[2] == 0)
                        param->Iterations = 2;
                    else
                        param->Iterations = command.iteration + 2; //число повторов t3
                    if (((param->TF[4] * param->Iterations) <= 330)) // TF5 <= 5.5 minute
                        param->sizeBreach = 0;
                    else
                        param->sizeBreach = 1;
                    param->numReqSamples = command.TF1 * mdcr_get_frequency() + command.TF2 * mdcr_get_frequency() + \
															command.TF3 * mdcr_get_frequency()*(param->Iterations - 2); // t1+t2+t3*Niter
                    param->acqMode = command.acqMode; //streem/write Flash
                    for (i = 0; i < 20; i++)
                        param->Date[i] = command.Date[i];
                    for (i = 0; i < 40; i++)
                        param->UserName[i] = command.UserName[i];
                    delay = 1;
                    break;
                case TEST2:
                    if (command.acqMode != STREAM)
                        mdcr_set_frequency(DEFAULT_QUANTITY_SAMPLES_PER_SEC);
                    param->testWasDownloaded = 0xFFFF;
                    param->appLessMode = 0;
                    param->numChannels = 4;
                    param->numTest = TEST2;
                    param->status = testStart;
                    param->TF[0] = 0;
                    param->TF[1] = 0;
                    param->TF[2] = 0;
                    param->TF[3] = command.TF4; //требуемое время из GUI
                    param->TF[4] = 0;
                    param->Iterations = command.iteration; // число повторов из GUI
                    if (((param->TF[4] * param->Iterations) <= 330)) // TF5 <= 5.5 minute
                        param->sizeBreach = 0;
                    else
                        param->sizeBreach = 1;
                    param->numReqSamples = command.TF4 * mdcr_get_frequency() * param->Iterations; // число выборок общее
                    param->acqMode = command.acqMode; //streem/write Flash
                    for (i = 0; i < 20; i++)
                        param->Date[i] = command.Date[i];
                    for (i = 0; i < 40; i++)
                        param->UserName[i] = command.UserName[i];
                    delay = 1;
                    break;
                case TEST3:
                    if (command.acqMode != STREAM)
                        mdcr_set_frequency(DEFAULT_QUANTITY_SAMPLES_PER_SEC);
                    param->testWasDownloaded = 0xFFFF;
                    param->appLessMode = 0;
                    param->numChannels = 3;
                    param->numTest = TEST3;
                    param->status = testStart;
                    param->TF[0] = 0;
                    param->TF[1] = 0;
                    param->TF[2] = 0;
                    param->TF[3] = 0;
                    param->TF[4] = command.TF5;
                    if (((param->TF[4] * param->Iterations) <= 330)) // TF5 <= 5.5 minute
                        param->sizeBreach = 0;
                    else
                        param->sizeBreach = 1;
                    param->numReqSamples = command.TF5 * mdcr_get_frequency(); //500 выборок/сек
                    param->acqMode = command.acqMode; //stream/write Flash
                    for (i = 0; i < 20; i++)
                        param->Date[i] = command.Date[i];
                    for (i = 0; i < 40; i++)
                        param->UserName[i] = command.UserName[i];
                    delay = 1;
                    break;
                case MEAS_OK:
                    receiveCommand = MEAS_OK;
                    break;
                case STOP_TEST:
                    param->status = testStop;
                    vTaskSuspend(xHandleLed1Gr); //stop blinks Green
                    vTaskSuspend(xHandleLed2Yw); //stop blinks Yellow
                    LED_Clear();
                    for (i = 0; i < NUMBER_OF_TIMERS; i++)
                        if (xTimerStop(xOneShotTimers[i], 0) != pdPASS)// Stop the timer
                            ;
                    len = GoingIntoHibernation(buff);
                    UART0SendData(buff, len);
                    break;
                case SET_FILTER:
                    param->numFilter = command.numFilter;
                    delay = 1;
                    break;
                case GET_LAST_MEASUREMENT:
                    param->status = testStart;
                    delay = 1;
                    fGetlast = 1;
                    break;
                case MEAS_REPEAT:
                    command.cmdID = MEAS_REPEAT;
                    delay = 1;
                    break;
                    //-------------- status comand--------------
                case GET_BEE_SN:
                    buff[1] = 0;
                    buff[0] = GET_BEE_SN + 0x10;
                    UART0SendData((char*) &buff[0], 2);
                    UART0SendData((char*) &SN_device[0], 9);
                    delay = 1;
                    break;
                case GET_BEE_APP_VERSION:
                    buff[1] = 0;
                    buff[0] = GET_BEE_APP_VERSION + 0x10;
                    UART0SendData((char*) &buff[0], 2);
                    UART0SendData((char*) &Bee_APP_Software[0], 2);
                    delay = 1;
                    break;
                case GET_BEE_DEVICE_VER:
                    buff[1] = 0;
                    buff[0] = GET_BEE_DEVICE_VER + 0x10;
                    UART0SendData((char*) &buff[0], 2);
                    UART0SendData((char*) &Bee_Device_version[0], 2);
                    delay = 1;
                    break;
                case GET_DATE_DEVICE_FIRMWARE:
                    buff[1] = 0;
                    buff[0] = GET_DATE_DEVICE_FIRMWARE + 0x10;
                    UART0SendData((char*) &buff[0], 2);
                    UART0SendData((char*) &date_of_compile[0], 30);
                    delay = 1;
                    break;
                case GET_CHARGED_LEVEL:
                    buff[1] = 0;
                    buff[0] = GET_CHARGED_LEVEL + 0x10;
                    UART0SendData((char*) &buff[0], 2);
                    Charge_in_procent = ((uint32_t) LTC2941_Reg.Charge * 100) / (uint32_t) LTC2941_Reg.ChThHI;
                    UART0SendData((char*) &Charge_in_procent, 2);
                    delay = 1;
                    break;
                case APP_LESS_TEST:
                {
                    const char defaultDate[20] = "20000101010000+00:00";
                    const char defaultUserName[40] = "John           Doe            0       ";
                    mdcr_set_frequency(DEFAULT_QUANTITY_SAMPLES_PER_SEC);
                    param->testWasDownloaded = 0xFFFF;
                    param->appLessMode = 1;
                    param->numChannels = 3;
                    param->numTest = TEST1;
                    param->status = testStart;
                    param->TF[0] = 15;
                    param->TF[1] = 0;
                    param->TF[2] = 15;
                    param->TF[3] = 0;
                    param->TF[4] = 0;
                    param->numFilter = 0x03;
                    param->Iterations = NUM_OF_ITER_IN_APP_LESS_TEST; //число повторов t3 + t1 + t2
                    param->numReqSamples = command.TF1 * mdcr_get_frequency() + command.TF2 * mdcr_get_frequency() + \
															command.TF3 * mdcr_get_frequency()*(param->Iterations - 2); // t1+t2+t3*Niter
                    param->acqMode = WRITE_TO_FLASH; //stream/write Flash

                    for (i = 0; i < 20; i++)
                        param->Date[i] = defaultDate[i];
                    for (i = 0; i < 40; i++)
                        param->UserName[i] = defaultUserName[i];
                    delay = 1;
                    break;
                }
								case APP_LESS_TEST2:
                {
                    const char defaultDate[20] = "20000101010000+00:00";
                    const char defaultUserName[40] = "John           Doe            0       ";
                    mdcr_set_frequency(DEFAULT_QUANTITY_SAMPLES_PER_SEC);
                    param->testWasDownloaded = 0xFFFF;
                    param->appLessMode = 2;
                    param->numChannels = 3;
                    param->numTest = TEST1;
                    param->status = testStart;
                    param->TF[0] = 30;
                    param->TF[1] = 300;
                    param->TF[2] = 0;
                    param->TF[3] = 0;
                    param->TF[4] = 0;
                    param->numFilter = 0x03;
                    param->Iterations = NUM_OF_ITER_IN_APP_LESS_TEST2; //число повторов t3 + t1 + t2
                    param->numReqSamples = command.TF1 * mdcr_get_frequency() + command.TF2 * mdcr_get_frequency(); // t1+t2
                    param->acqMode = WRITE_TO_FLASH; //stream/write Flash

                    for (i = 0; i < 20; i++)
                        param->Date[i] = defaultDate[i];
                    for (i = 0; i < 40; i++)
                        param->UserName[i] = defaultUserName[i];
                    delay = 1;
                    break;
                }
                case SEND_INFO_ABOUT_LAST_SAVED_TEST:
                {
                    tsTestParam paramTemp;
                    unsigned char buffTemp[20] = {0};
                    unsigned short buffFlashTemp[230] = {0};
                    app_flash_read_data_16bit(0, buffFlashTemp, MDCR_FLASH_HEADER / 2);
                    if (buffFlashTemp[44] == 1)
                        paramTemp.appLessMode = 1;
										else if (buffFlashTemp[44] == 2)
                        paramTemp.appLessMode = 2;							
                    else
                        paramTemp.appLessMode = 0;
                    if (buffFlashTemp[45] == 0xFFFF)
                        paramTemp.testWasDownloaded = 0;
                    else
										{
                        paramTemp.testWasDownloaded = 1;
												timeOfAPP_LESS_TEST2 = 0;
										}
                    buffTemp[0] = SEND_INFO_ABOUT_LAST_SAVED_TEST;
                    buffTemp[1] = 0;
                    buffTemp[2] = paramTemp.appLessMode;
                    buffTemp[3] = 0;
                    buffTemp[4] = paramTemp.testWasDownloaded;
                    buffTemp[5] = 0;
										
										memcpy((uint8_t*)&buffTemp[6], (uint8_t*)&timeOfAPP_LESS_TEST2, 2);
									
                    UART0SendData((char*) buffTemp, 8);
                    delay = 1;
                    break;
                }

                case SET_MEASURING_FREQUENCY:
                {
                    uint16_t cmdId = SET_MEASURING_FREQUENCY;
                    uint16_t success_flag = *((uint16_t*) command.data);
                    uint16_t old_frequency = mdcr_get_frequency();
                    mdcr_set_frequency(*((uint16_t*) command.data));
                    success_flag = success_flag == mdcr_get_frequency();
                    if (!success_flag)
                        mdcr_set_frequency(old_frequency);
                    UART0SendData((char*) (&cmdId), sizeof (cmdId));
                    UART0SendData((char*) (&success_flag), sizeof (success_flag));
                }
                    break;
                case GET_MEASURING_FREQUENCY:
                {
                    uint16_t cmdId = GET_MEASURING_FREQUENCY;
                    uint16_t freq = mdcr_get_frequency();
                    //uint16_t freq = 0xADDE;
                    UART0SendData((char*) (&cmdId), sizeof (cmdId));
                    UART0SendData((char*) (&freq), sizeof (freq));
                }
                case SET_INVALID_SIGNAL_DETECT:
                {
                    uint16_t cmdId = SET_INVALID_SIGNAL_DETECT;
                    uint16_t success_flag = 0x0001;
                    app_invalid_signal_detect_enabled = (!!(*((uint16_t*) command.data)));
                    UART0SendData((char*) (&cmdId), sizeof (cmdId));
                    UART0SendData((char*) (&success_flag), sizeof (success_flag));
                }
                    break;
                case GET_INVALID_SIGNAL_DETECT:
                {
                    uint16_t cmdId = GET_INVALID_SIGNAL_DETECT;
                    uint16_t temp = app_invalid_signal_detect_enabled;
                    UART0SendData((char*) (&cmdId), sizeof (cmdId));
                    UART0SendData((char*) (&temp), sizeof (temp));
                }
                    break;
                default:
                    fContinue = 1;
                    break;
            }
        }
        if (fContinue != 1)
        {
            if (fGetlast != 1)
            {
                stopTimerLED30SecAfterTests();
                if (param->numTest == TEST1)
                {		
										result = Test1Func(param, receiveCommand);
                } else if (param->numTest == TEST2)
                {
                    result = Test2Func(param, receiveCommand);
                } else if (param->numTest == TEST3)
                {
                    result = Test3Func(param, receiveCommand);
                    result = result;
                }
            } else if (fGetlast == 1)
            {
                stopTimerLED30SecAfterTests();
                result = sendLastTest(param, receiveCommand);
                command.cmdID = DUMMY;
                receiveCommand = DUMMY;
            }
            if (result == 0) //finish
            {
                param->appLessMode = 0;
                startTimerLED30SecAfterTests();
                delay = portMAX_DELAY;
                param->numTest = DUMMY;
                fContinue = 1;
                fGetlast = 0;
            }
        } else
        {
            startTimerLED30SecAfterTests();
            fContinue = 0; //continue
        }
    }
}

int sendDataUSART_Async(char * data, int data_len)
{
    SendBuffer send_buff;
    send_buff.data_size = data_len;
    memcpy(send_buff.data, data, data_len);
    xQueueSendToBack(xQueueBTSend, &send_buff, portMAX_DELAY);
    return 0;
}


//--------------set PIN to connect dif analog dev-------------------------

void perform_the_analog_switch(tsTestParam* param)
{
    switch (param->CurrentIteration & 0xF)
    {
        case 1:
            CLR_ANALOG_CTRL1;
            CLR_ANALOG_CTRL2;
            CLR_ANALOG_CTRL3;
            break;
        case 2:
            SET_ANALOG_CTRL1;
            CLR_ANALOG_CTRL2;
            CLR_ANALOG_CTRL3;
            break;
        case 3:
            CLR_ANALOG_CTRL1;
            SET_ANALOG_CTRL2;
            CLR_ANALOG_CTRL3;
            break;
        case 4:
            SET_ANALOG_CTRL1;
            SET_ANALOG_CTRL2;
            CLR_ANALOG_CTRL3;
            break;
        case 5:
            CLR_ANALOG_CTRL1;
            CLR_ANALOG_CTRL2;
            SET_ANALOG_CTRL3;
            break;
        case 6:
            SET_ANALOG_CTRL1;
            CLR_ANALOG_CTRL2;
            SET_ANALOG_CTRL3;
            break;
        case 7:
            CLR_ANALOG_CTRL1;
            SET_ANALOG_CTRL2;
            SET_ANALOG_CTRL3;
            break;
        case 8:
            SET_ANALOG_CTRL1;
            SET_ANALOG_CTRL2;
            SET_ANALOG_CTRL3;
            break;
        case 0:
            while (true)
                ;
        default:
            CLR_ANALOG_CTRL1;
            CLR_ANALOG_CTRL2;
            CLR_ANALOG_CTRL3;
            break;
    }
}

void initChecksValidSignal(void)
{
    InitCheckValidSignal();
    InitCheckValidSignal2();
    InitCheckValidSignal3();
}
