/*******************************************************************************
 *	File:	command_handler.h
 *******************************************************************************/
#ifndef __COMMAND_HANDLER_H_
#define __COMMAND_HANDLER_H_

#define DEVICE0		"UART0"
#define DEVICE1		"BLUETOOTH"

typedef enum
{
	empty_command = 0,
	get_scpi_idn,
	get_syst_err,
	get_serv_sens_nadc0,
	get_serv_sens_nadc1,
	get_serv_sens_nadc2,
	get_serv_sens_nadc3,
}device_command_type;

typedef struct
{
	device_command_type	 	type;
	char					*data;
	unsigned int			data_size;
	int 					source;			
}device_command;

ErrorStatus CommandHandler(device_command command);

#endif
