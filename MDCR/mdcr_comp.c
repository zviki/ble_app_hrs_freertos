
#include <stdio.h>
#include <stdlib.h>
#include "mdcr_comp.h"

int mdcr_comp(unsigned char* data, int ilen, unsigned char* compressed, int channels)
{
    int cnt = 0, idx = 0, sign = 1, diff = 0, ch = 0;
    unsigned short pval, val = 0;
    unsigned char byte[2];
    unsigned short *var;

    //на выходе буфер где идут раздельные сжатые данные по каналам |ch1|ch2|ch3|ch4 если есть|
    for(ch=0; ch < channels; ch++)//количество повторов = кол-во каналов
    {   
    //первые 2 байта несжатые, без символа перемены знака
        for(cnt = ch*2; cnt < ilen; cnt += channels*2 )//цикл идет по данным одного канала, 0+channels*2  
        {
            if(cnt==0)
            {
                compressed[idx++] = byte[0] = data[cnt+1];//idx
                compressed[idx++] = byte[1] = data[cnt];   
                var = (unsigned short *)byte;
                pval = var[0];
            }
            else
            {
                byte[1] = data[cnt];
                byte[0] = data[cnt+1];
                var = (unsigned short *)byte;
                val = var[0];

                diff = pval - val;

                if(diff < 0 && sign == 1 && abs(diff) <= 255)
                {
                    compressed[idx++] = 0x0;
                    sign = -1;
                }

                if(diff > 0 && sign == -1 && abs(diff) <= 255)//если положительное
                {
                    compressed[idx++] = 0x0;
                    sign = 1;
                }

                if(diff == 0)//исключения случая с 0
                {
                    compressed[idx++] = 0x0;
                    compressed[idx++] = 0x0;
                }    
                else
                {                
                    if(abs(diff) > 255)
                    {
                        compressed[idx++] = 0xff;
                        compressed[idx++] = byte[0];
                        compressed[idx++] = byte[1];
                    }
                    else
                    {
                        compressed[idx++] = (unsigned char)abs(diff);
                    }
                }
                pval = val;//передали текущее значение в предыдущее
            }
        }
    }
    return idx;
}

int mdcr_decomp(unsigned char* compressed, int ilen, unsigned char* decompressed, int channels)
{
    int cnt = 0, len = 0, idx = 0, sign = 1, ch = 0, olen = 0;
    unsigned short pval = 0;
    unsigned short * var;//ушорт в байты
    unsigned char * dbyte; //для разбора ушорт в байты
    unsigned char byte[2];// 
    //запись в выходной буфер идет по типу |ch1|ch2|ch3|
    for(ch = 0; ch < channels; ch++)
    { 
        idx = 0;
        idx += ch*2;
        
        for (len = 0; len < 1000; len += 2)        
        {
            if (cnt == 0)
            {
                byte[1] = decompressed[idx++] = compressed[cnt];//idx
                byte[0] = decompressed[idx++] = compressed[cnt + 1];
                idx += channels*2-2;//запись данных по виду |ch1|ch2|ch3| 2+(channels*2-2) 
                var = (unsigned short *) byte;
                pval = var[0];
                cnt += 2;
            }
            else
            {    
                if (compressed[cnt] == 0x0 && compressed[cnt+1] == 0x0)//если байт 0, записывается преыдущее значение
                {
                    var[0] = pval;
                    dbyte = (unsigned char*) var;
                    decompressed[idx++] = dbyte[0];//idx
                    decompressed[idx++] = dbyte[1];
                    idx += channels*2-2;
                    cnt+=2;
                }
                else
                {
                    if (compressed[cnt] == 0x0 && sign == 1)//проверка смены 
                    {
                        cnt++;//если это знаковый байт, плюсуем счетчик, чтобы проверка след условия начиналась со след знака
                        sign = -1;
                    }
                    if (compressed[cnt] == 0x0 && sign == -1)//знака 
                    {
                        cnt++;
                        sign = 1;
                    }
                    if (compressed[cnt] == 0xff)//проверка на 2-х байтовые значения
                    {
                        byte[0] = decompressed[idx++] = compressed[cnt + 1];
                        byte[1] = decompressed[idx++] = compressed[cnt + 2];
                        idx += channels*2-2;
                        var = (unsigned short *) byte;
                        pval = var[0];
                        cnt += 3;
                    }
                    else
                    {
                        if (sign == -1)
                        {
                            var[0] = pval += compressed[cnt];
                            dbyte = (unsigned char*)var;
                            decompressed[idx++] = dbyte[0];
                            decompressed[idx++] = dbyte[1];
                            idx += channels*2-2;
                            cnt++;
                        }
                        else
                        {
                            var[0] = pval -= compressed[cnt];
                            dbyte = (unsigned char*)var;
                            decompressed[idx++] = dbyte[0];
                            decompressed[idx++] = dbyte[1];
                            idx += channels*2-2;
                            cnt++;
                        }
                    }
                }
            }
        }
        olen += len;
    }
    return olen;
}
