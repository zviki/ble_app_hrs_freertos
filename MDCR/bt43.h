#ifndef __H_BT43_APPFUNC_
#define __H_BT43_APPFUNC_

#ifdef __C_BT43_
	#define BT43_VAR volatile
#else
	#define BT43_VAR extern
#endif

enum
{
	eUndefined=0,
	eOK,
	eATcmd,
};

typedef struct _tsMetaBT43Element
{
	unsigned short Connected:	 1;							// PC - Dev connected = 1
	unsigned short dummy: 		15;
}tsMetaBT43Element;

// Meta Data Element
typedef union _uMetaBTElement
{
	tsMetaBT43Element 	BIT;
	unsigned short 		ALL;
}uMetaBT43Element;

#endif	//	__H_BT411_APPFUNC_
